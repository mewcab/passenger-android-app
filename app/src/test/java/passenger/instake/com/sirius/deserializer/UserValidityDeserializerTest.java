package passenger.instake.com.sirius.deserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instake.passenger.api.deserializer.UserValidityDeserializer;
import com.instake.passenger.model.UserValidity;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Tushar on 5/21/17.
 */

public class UserValidityDeserializerTest extends BaseDeserializerTest {
    private Gson gson;

    @Before
    public void setUp() throws Exception {
        gson = new GsonBuilder()
                .registerTypeAdapter(UserValidity.class, new UserValidityDeserializer())
                .create();
    }

    @Test
    public void testDeserialization() throws Exception {
        UserValidity userValidity = gson.fromJson(getResourceReader("response_user_validity.json"), UserValidity.class);
        System.out.println(userValidity.toString());
    }

}