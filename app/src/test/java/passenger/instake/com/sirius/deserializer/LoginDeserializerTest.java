package passenger.instake.com.sirius.deserializer;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.instake.passenger.api.deserializer.LoginDeserializer;
import com.instake.passenger.model.User;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by Tushar on 5/22/17.
 */
public class LoginDeserializerTest extends BaseDeserializerTest {
    private Gson gson;

    @Before
    public void setUp() throws Exception {
        gson = new GsonBuilder()
                .registerTypeAdapter(User.class, new LoginDeserializer())
                .create();
    }

    @Test
    public void testDeserialization() throws Exception {
        User user = gson.fromJson(getResourceReader("response_login.json"), User.class);
        System.out.println(user.toString());
    }
}