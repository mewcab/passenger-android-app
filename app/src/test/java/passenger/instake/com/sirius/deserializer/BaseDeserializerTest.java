package passenger.instake.com.sirius.deserializer;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;

public class BaseDeserializerTest {
    protected InputStream getResourceInputStream(String name) {
        return this.getClass().getClassLoader().getResourceAsStream(name);
    }

    protected Reader getResourceReader(String name) {
        return new InputStreamReader(getResourceInputStream(name));
    }
}