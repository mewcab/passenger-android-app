package com.instake.passenger.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.telephony.SmsMessage;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Tushar on 8/20/17.
 */

public abstract class VerificationCodeReceiver extends BroadcastReceiver {

    private static final String TAG = "VerificationCode";

    public static final String CODE_PATTERN_4_DIGIT = "\\b\\d{4}\\b";
    private List<String> phoneNumberFilter = new ArrayList<>();
    private String pattern;

    @Override
    public void onReceive(Context context, Intent intent) {
        final Bundle bundle = intent.getExtras();
        try {
            if (bundle != null) {
                final Object[] pdusObj = (Object[]) bundle.get("pdus");
                for (int i = 0; i < pdusObj.length; i++) {
                    SmsMessage currentMessage = getIncomingMessage(pdusObj[i], bundle);
                    String phoneNumber = currentMessage.getDisplayOriginatingAddress();
                    if (phoneNumberFilter.size() != 0 && isNumberFiltered(phoneNumber)) {
                        String message = currentMessage.getDisplayMessageBody();
                        if (pattern != null && parseCode(message).matches(pattern)) {
                            onVerificationCode(parseCode(message));
                        }
                    }
                }
            }

        } catch (Exception e) {
            Log.e(TAG, "SmsReceiver Exception smsReceiver");
        }
    }

    private boolean isNumberFiltered(String phoneNumber) {
        for (String filter : phoneNumberFilter) {
            if (filter.contentEquals(phoneNumber)) {
                return true;
            }
        }
        return false;
    }

    private SmsMessage getIncomingMessage(Object aObject, Bundle bundle) {
        SmsMessage currentSMS;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            String format = bundle.getString("format");
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject, format);
        } else {
            currentSMS = SmsMessage.createFromPdu((byte[]) aObject);
        }
        return currentSMS;
    }

    /**
     * Set phone number pattern
     *
     * @param phoneNumberFilter phone number
     */
    public void addPhoneNumberFilter(String phoneNumberFilter) {
        this.phoneNumberFilter.add(phoneNumberFilter);
    }

    /**
     * set message pattern with regexp
     *
     * @param regularExpression regexp
     */
    public void setPattern(String regularExpression) {
        this.pattern = regularExpression;
    }

    /**
     * Parse verification code
     *
     * @param message sms message
     * @return only four numbers from massage string
     */
    private String parseCode(String message) {
        Pattern p = Pattern.compile(CODE_PATTERN_4_DIGIT);
        Matcher m = p.matcher(message);
        String code = "";
        while (m.find()) {
            code = m.group(0);
        }
        return code;
    }

    public abstract void onVerificationCode(String code);
}
