package com.instake.passenger.common.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.utility.Utility;

/**
 * Created by Masum on 21/06/2015.
 */

public class InstakeAddressView extends LinearLayout {
    private TextView txtViewMain;
    private TextView txtViewSecond;

    public InstakeAddressView(Context context) {
        this(context, null);
    }

    public InstakeAddressView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.AddressViewAttrs, 0, 0);
        String titleText = a.getString(R.styleable.AddressViewAttrs_text1);
        String titleSecondText = a.getString(R.styleable.AddressViewAttrs_text2);
        String fontName = a.getString(R.styleable.AddressViewAttrs_fontName);
        String textShorting = a.getString(R.styleable.AddressViewAttrs_textShorting);
        float textShortedSize = a.getFloat(R.styleable.AddressViewAttrs_textShortedSize, 0);

        int gravity = a.getInt(R.styleable.AddressViewAttrs_gravity, Gravity.NO_GRAVITY);
        int textColor = a.getColor(R.styleable.AddressViewAttrs_textColor, ContextCompat.getColor(context, R.color.dark_gray));
        int text2Color = a.getColor(R.styleable.AddressViewAttrs_text2Color, ContextCompat.getColor(context, R.color.dark_gray));

        float textSize = a.getDimensionPixelSize(R.styleable.AddressViewAttrs_textSize, 0);
        float text2Size = a.getDimensionPixelSize(R.styleable.AddressViewAttrs_text2Size, 0);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.text_view_instake, this, true);

        txtViewMain = (TextView) getChildAt(0);
        txtViewSecond = (TextView) getChildAt(1);


        if (fontName != null) {
            Typeface myTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/" + fontName);
            txtViewMain.setTypeface(myTypeface);
        }
        a.recycle();

        setOrientation(LinearLayout.VERTICAL);
        setGravity(Gravity.CENTER_VERTICAL);


        if (textColor != 0) {
            txtViewMain.setTextColor(textColor);
        }

        if (text2Color != 0) {
            txtViewSecond.setTextColor(text2Color);
        }

        if (textSize != 0) {
            txtViewMain.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        }

        if (text2Size != 0) {
            txtViewSecond.setTextSize(TypedValue.COMPLEX_UNIT_PX, text2Size);
        }

        if (titleText != null) {
            txtViewMain.setText(titleText);
        }

        if (titleSecondText != null) {
            txtViewSecond.setText(titleSecondText);
        }

        if (gravity != 0) {
            txtViewMain.setGravity(gravity);
            txtViewSecond.setGravity(gravity);
        }

        if (textShorting != null) {
            if (txtViewMain.getText().toString().toLowerCase().contains("tk")) {
                txtViewMain.setText(Utility.getTextStyleSlightly(txtViewMain.getText().toString(), textShorting, textShortedSize));
            }

        }

        invalidate();

    }

    public String getText() {
        return txtViewMain.getText().toString();
    }

    public void setText(String value) {
        txtViewMain.setText(value);
    }

    public void setText2(String value) {
        txtViewSecond.setText(value);
    }

    public void setVisible(boolean visible) {
        txtViewSecond.setVisibility(visible ? View.VISIBLE : View.GONE);
    }

    public void setSmallDecoratedText(String smallText, float smallSize) {
        txtViewMain.setText(Utility.getTextStyleSlightly(txtViewMain.getText().toString(), smallText, smallSize));
    }
}
