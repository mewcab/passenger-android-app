package com.instake.passenger.common.session;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.instake.passenger.model.User;
import com.instake.passenger.utility.validation.Validator;

import java.lang.reflect.Type;

/**
 * Created by Tushar on 5/21/17.
 */

public class SharedPreferenceSessionImpl implements Session {
    private static final String KEY_USER = "user";
    private static final String KEY_LOCAL_APP_STATE = "local_app_state";

    private SharedPreferences sharedPreferences;
    private Gson gson;
    private Validator validator;
    private User user;

    public SharedPreferenceSessionImpl(SharedPreferences sharedPreferences, Gson gson, Validator validator) {
        this.sharedPreferences = sharedPreferences;
        this.gson = gson;
        this.validator = validator;
    }

    @Override
    public boolean saveUser(User user) {
        this.user = user;
        return putString(KEY_USER, gson.toJson(user));
    }

    @Override
    public User getUser() {
        if (user == null) {
            user = gson.fromJson(getString(KEY_USER), User.class);
        }
        return user;
    }

    @Override
    public boolean isUserLoggedIn() {
        return getUser() != null && getUser().isActive
                && validator.isNotEmpty(getUser().id)
                && validator.isNotEmpty(getUser().token);
    }

    @Override
    public void logOutUser() {
        user = null;
        removeKey(KEY_USER);
    }

    @Override
    public String getToken() {
        return getUser().token;
    }

    @Override
    public boolean saveObject(String key, Object o) {
        Gson gson = new Gson();
        return putString(key, gson.toJson(o));
    }

    @Override
    public boolean saveLocalAppState(int state) {
        return putInt(KEY_LOCAL_APP_STATE, state);
    }

    @Override
    public int getAppLocalState() {
        return sharedPreferences.getInt(KEY_LOCAL_APP_STATE, 1);
    }

    @Override
    public <T> T getObject(String key, Type type) {
        String json = getString(key);
        return gson.fromJson(json, type);
    }

    private boolean putString(String key, String value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        return editor.commit();
    }

    private boolean putInt(String key, int value) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(key, value);
        return editor.commit();
    }

    private String getString(String key) {
        return sharedPreferences.getString(key, null);
    }

    private int getInt(String key) {
        return sharedPreferences.getInt(key, -1);
    }

    private void removeKey(String key) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }
}
