package com.instake.passenger.common.base;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.instake.passenger.common.App;

/**
 * Created by Tushar on 4/9/17.
 */

public abstract class SiriusFragment<V, T extends BasePresenter<V>> extends Fragment {

    protected T presenter;
    protected V view;

    protected abstract T bindPresenter();

    protected App app;
    private ViewDataBinding dataBinding;

    protected abstract int getLayoutResourceId();

    protected abstract void initFragmentComponents();

    public ViewDataBinding getDataBinding() {
        return dataBinding;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = App.getApp(getActivity());

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(
                inflater, getLayoutResourceId(), container, false);
        View view = dataBinding.getRoot();
        presenter = bindPresenter();
        initFragmentComponents();

        return view;
    }

    public View getView() {
        return dataBinding.getRoot();
    }

    @Override
    public void onDestroy() {
        presenter.onDestroyPresenter();
        super.onDestroy();
    }
}
