package com.instake.passenger.common.base;

import android.os.Bundle;
import android.support.annotation.Nullable;

/**
 * Created by Tushar on 5/12/17.
 */

public abstract class SiriusPresenterActivity<V, T extends BasePresenter> extends SiriusActivity {

    protected T presenter;
    protected V view;

    protected abstract T bindPresenter();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = bindPresenter();
        presenter.onCreatePresenter();
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResumePresenter();
    }

    @Override
    protected void onPause() {
        super.onPause();
        presenter.onPausePresenter();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroyPresenter();
    }
}
