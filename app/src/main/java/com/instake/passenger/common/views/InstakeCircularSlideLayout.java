package com.instake.passenger.common.views;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.widget.LinearLayout;

import com.instake.passenger.feature.vehicledetails.InstakeCircularPagerAdapter;

/**
 * Created by masum on 3/14/17.
 */

public class InstakeCircularSlideLayout extends LinearLayout {

    private float scale = InstakeCircularPagerAdapter.BIG_SLIDE;
    private float x = 0f;
    private float y = 100f;

    public InstakeCircularSlideLayout(Context context) {
        super(context);
    }

    public InstakeCircularSlideLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public InstakeCircularSlideLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setScale(float scale) {
        this.scale = scale;
        this.invalidate();
    }

    public void setTranslate(float x, float y) {
        this.x = x;
        this.y = y;
        this.invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int w = this.getWidth();
        int h = this.getHeight();
        canvas.scale(scale, scale, w/2, h/2);
        //canvas.translate(x, y);

    }
}
