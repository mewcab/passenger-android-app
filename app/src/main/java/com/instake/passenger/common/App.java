package com.instake.passenger.common;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatDelegate;
import android.util.Base64;
import android.util.Log;

import com.facebook.appevents.AppEventsLogger;
import com.franmontiel.persistentcookiejar.PersistentCookieJar;
import com.franmontiel.persistentcookiejar.cache.SetCookieCache;
import com.franmontiel.persistentcookiejar.persistence.SharedPrefsCookiePersistor;
import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.instake.passenger.BuildConfig;
import com.instake.passenger.R;
import com.instake.passenger.Rx.RxBus;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.deserializer.EditProfileDeserializer;
import com.instake.passenger.api.deserializer.EducationDetailsDeserializer;
import com.instake.passenger.api.deserializer.LoginDeserializer;
import com.instake.passenger.api.deserializer.RideHistoryDeserializer;
import com.instake.passenger.api.deserializer.UserProfileDeserializer;
import com.instake.passenger.api.deserializer.UserValidityDeserializer;
import com.instake.passenger.dipendency.AppComponent;
import com.instake.passenger.dipendency.AppModule;
import com.instake.passenger.dipendency.DaggerAppComponent;
import com.instake.passenger.model.EditProfile;
import com.instake.passenger.model.EducationDetails;
import com.instake.passenger.model.RiderHistoryModel;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;
import com.instake.passenger.model.UserValidity;
import com.instake.passenger.model.historydata.HistoryResp;
import com.instake.passenger.model.payment.Tips;
import com.instake.passenger.monowar.SignupResponseData;
import com.instake.passenger.monowar.SignupValidationDeserializer;
import com.instake.passenger.monowar.VerificationStatus;
import com.instake.passenger.monowar.VerificationStatusDeserializer;
import com.instake.passenger.socket.SocketCallbackInterface;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.URISyntaxException;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uk.co.chrisjenx.calligraphy.CalligraphyConfig;


/**
 * Created by masum on 3/29/17.
 */

public class App extends Application {
    public static int APP_IN_HOME_UI = 1;
    public static int APP_IN_TIPS_UI = 2;
    public static int APP_IN_RATING_UI = 3;

    public static final int DISK_CACHE_SIZE = 50 * 1024 * 1024; // 50MB
    public static String BASE_URL = "http://167.114.95.0";

    public static App app;
    private AppComponent appComponent;
    private Gson gson;
    private ApiService apiService;
    private ApiService apiServiceCaching;
    private RxBus bus;

    public String tripId;
    public String driverId;


    public static App getInstance() {
        return app;
    }

    public static App getApp(Activity activity) {
        return (App) activity.getApplication();
    }

    @Override
    public void onCreate() {
        super.onCreate();

        app = this;

        bus = new RxBus();

        AppEventsLogger.activateApp(this);

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/CircularStd-Book.ttf")
                .setFontAttrId(R.attr.fontPath)
                .build()
        );

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
        }

        if (BuildConfig.DEBUG) {
            try {
                PackageInfo info = getPackageManager().getPackageInfo("com.instake.passenger", PackageManager.GET_SIGNATURES);
                for (Signature signature : info.signatures) {
                    MessageDigest md = MessageDigest.getInstance("SHA");
                    md.update(signature.toByteArray());
                    Log.e("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            }
        }
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public Cache getCache() {
        File cacheDir = new File(getCacheDir(), "cache");
        Cache cache = new Cache(cacheDir, DISK_CACHE_SIZE);
        return cache;
    }

    private OkHttpClient provideOkHttpClient() {
        OkHttpClient.Builder okhttpClientBuilder = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();
                        if (!isNetworkAvailable()) {
                            request = request.newBuilder().header("Cache-Control",
                                    "public, only-if-cached, max-stale=" + 60 * 60 * 24).build();
                            Response response = chain.proceed(request);
                            Log.d("Cache Response", "" + response.body());
                            if (response.cacheResponse() == null) {
                                // TODO: decide later what to do here
                            }
                            return response;
                        } else {
                            okhttp3.Response response = chain.proceed(request);
                            Log.d("App", "*Response Code: " + response.code());
                            Log.d("Response", "" + response.body());
                            response.cacheResponse();
                            return response;
                        }

                    }
                });
        okhttpClientBuilder.connectTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.readTimeout(30, TimeUnit.SECONDS);
        okhttpClientBuilder.writeTimeout(30, TimeUnit.SECONDS);

        okhttpClientBuilder.cache(getCache());

        if (!BuildConfig.BUILD_TYPE.contentEquals("release")) {
            HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
            okhttpClientBuilder.addInterceptor(loggingInterceptor);
        }

        CookieManager cookieManager = new CookieManager();
        cookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        okhttpClientBuilder.cookieJar(new PersistentCookieJar(new SetCookieCache(),
                new SharedPrefsCookiePersistor(this)));
        return okhttpClientBuilder.build();
    }

    private OkHttpClient provideOkHttpClientWithCache(File cacheFile) {
        Cache cache = null;
        try {
            cache = new Cache(cacheFile, 10 * 1024 * 1024);
        } catch (Exception e) {
            e.printStackTrace();
        }

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        if (BuildConfig.DEBUG)
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.HEADERS);

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public okhttp3.Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();

                        // Customize the request
                        Request request = original.newBuilder()
                                .header("Content-Type", "application/json")
                                .removeHeader("Pragma")
                                .header("Cache-Control", String.format("max-age=%d", 432000))
                                .build();

                        okhttp3.Response response = chain.proceed(request);
                        response.cacheResponse();
                        // Customize or return the response
                        return response;
                    }
                })
                .cache(cache)
                .addInterceptor(loggingInterceptor)
                .build();

        return okHttpClient;
    }

    private Gson getGson() {
        // @formatter:off
        if (gson == null) {
            gson = new GsonBuilder()
                    .registerTypeAdapter(UserValidity.class, new UserValidityDeserializer())
                    .registerTypeAdapter(VerificationStatus.class, new VerificationStatusDeserializer())
                    .registerTypeAdapter(SignupResponseData.class, new SignupValidationDeserializer())
                    .registerTypeAdapter(User.class, new LoginDeserializer())
                    .registerTypeAdapter(EditProfile.class, new EditProfileDeserializer())
                    .registerTypeAdapter(UserProfile.class, new UserProfileDeserializer())
                    .registerTypeAdapter(EducationDetails.class, new EducationDetailsDeserializer())
                    .registerTypeAdapter(HistoryResp.class, new RideHistoryDeserializer())
                    .create();
        }
        //@formatter:on
        return gson;
    }

    private Retrofit provideRetrofit(String url) {
        Gson gson = new GsonBuilder().registerTypeAdapter(HistoryResp.class, new RideHistoryDeserializer()).create();
        return new Retrofit.Builder()
                .baseUrl(url)
                .client(provideOkHttpClient())
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    private Retrofit provideRetrofitRx(String url, File cacheFile) {
        return new Retrofit.Builder()
                .baseUrl(url)
                .client(provideOkHttpClientWithCache(cacheFile))
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(getGson()))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
    }

    public ApiService getApiService() {
        if (apiService == null) {
            apiService = provideRetrofit(ApiService.URL).create(ApiService.class);
        }
        return apiService;
    }

    public ApiService getApiServiceWithCaching(File cacheFile) {
        if (apiServiceCaching == null) {
            File cacheFile1 = new File(getCacheDir(), "responses");
            apiServiceCaching = provideRetrofitRx(ApiService.URL_GOOGLE_DISTANCE_MATRIX, cacheFile1).create(ApiService.class);
        }
        return apiServiceCaching;
    }

    public SharedPreferences getSharedPreferenceManager() {
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    public RxBus bus() {
        return bus;
    }


    /////////////Start Socket Service Code/////////////////

    private static final String MESSAGE_EVENT_TEXT = "message";
    private static final String LOCATION_EVENT_TEXT = "location";
    private static final String ACCEPT_DRIVER_REQUEST = "messages";
    private static final String ON_ACCEPTED = "on-accept";
    private static final String ON_RIDE_STARTED = "on-ride-started";
    private static final String ON_RIDE_END = "on-end-ride";
    private static final String ON_STATUS = "status";
    private static final String ON_PASSENGER_SYSTEM_STATUS = "on_passenger_system_status";
    private static final String ON_SINGLE_DRIVER_LOCATION_UPDATE = "on_single_driver_location_update";
    private static final String ON_CANCEL_RIDE_REQUEST_PASSENGER = "on_cancel_ride_request_passenger";
    private static final String ON_SEND_RIDE_REQUEST_SUCCESS = "on-send-ride-request";
    public static final String TAG = "App";

    public Socket socket;
    public String socketUrl = "http://167.114.95.0";
    public SocketCallbackInterface socketCallbackInterface;

    public void bindCallBackInterface(SocketCallbackInterface socketCallbackInterface) {
        this.socketCallbackInterface = socketCallbackInterface;
    }

    public void connectSocket(String phoneNumber, String token) {
        try {
            IO.Options opts = new IO.Options();
            opts.forceNew = true;
            try {
                opts.query = "userId=" + URLEncoder.encode(phoneNumber, "UTF-8") + "&token=" + token;
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            socket = IO.socket(socketUrl, opts);

            //socket = IO.socket(socketUrl);

            socket.on(Socket.EVENT_CONNECT, onConnect);
            socket.on(ON_STATUS, onStatus);
            socket.on(Socket.EVENT_DISCONNECT, onDisconnect);

            socket.on(LOCATION_EVENT_TEXT, onNewLocation);

            socket.on(ON_ACCEPTED, onAccepted);
            socket.on(ON_RIDE_STARTED, onRideStarted);
            socket.on(ON_RIDE_END, onRideEnd);

            socket.on(ACCEPT_DRIVER_REQUEST, onAcceptDriverRequest);
            socket.on(MESSAGE_EVENT_TEXT, onNewMessage);
            socket.on(ON_PASSENGER_SYSTEM_STATUS, onPassengerSystemStatus);
            socket.on(ON_SINGLE_DRIVER_LOCATION_UPDATE, onSingleDriverLocationUpdate);
            socket.on(ON_CANCEL_RIDE_REQUEST_PASSENGER, onCancelRideRequestPassenger);
            socket.on(ON_SEND_RIDE_REQUEST_SUCCESS, onSendRideRequestSuccess);

            socket.connect();

        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public void disconnectSocket() {
        if (socket != null) {
            socket.disconnect();
        }
    }

    public boolean isConnected() {
        if (socket != null) {
            return socket.connected();
        }

        return false;
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            socketCallbackInterface.onConnect(args);
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            socketCallbackInterface.onDisconnect(args);
        }
    };

    private Emitter.Listener onStatus = args -> {
        try {
            JSONObject data = (JSONObject) args[0];
            Log.d(TAG, "Connected" + data.getString("status").toString());
            //Log.e(TAG, "Connected" + data.toString());
            try {
                getAppRunningState();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    };

    private Emitter.Listener onAccepted = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            Log.i(TAG, "call: on-accept");
            socketCallbackInterface.onRideAccepted(args);

        }
    };

    private Emitter.Listener onRideStarted = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            socketCallbackInterface.onRideStarted(args);

        }
    };

    private Emitter.Listener onRideEnd = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            socketCallbackInterface.onRideEnd(args);
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                socketCallbackInterface.onRideAccepted(data.toString());
            } catch (Exception e) {
                Log.e(TAG, "onNewMessage:" + e.getMessage());
            }

        }
    };

    private Emitter.Listener onSendRideRequestSuccess = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            try {
                JSONObject data = (JSONObject) args[0];
                JSONObject jsonObject = new JSONObject(data.toString());
                tripId = jsonObject.getJSONObject("trip").getString("_id");
                driverId = jsonObject.getJSONObject("driver").getString("_id");
                Log.d(TAG, "request send and get id: " + data.toString());
                //socketCallbackInterface.onSendRideRequestSuccess(args);

            } catch (Exception e) {

            }
        }
    };

    private Emitter.Listener onNewLocation = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            socketCallbackInterface.onUpdatedDriverList(args);

        }
    };

    private Emitter.Listener onAcceptDriverRequest = args -> {
        socketCallbackInterface.onSendRideRequestSuccess(args);
    };

    private Emitter.Listener onPassengerSystemStatus = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            JSONObject data = (JSONObject) args[0];
            Log.e(TAG, "call: " + data);
            socketCallbackInterface.setAppState(data.toString());

        }

    };

    private Emitter.Listener onSingleDriverLocationUpdate = args -> {
        JSONObject data = (JSONObject) args[0];
        Log.e(TAG, "single_driver_location_update: " + data);
    };

    private Emitter.Listener onCancelRideRequestPassenger = args -> {
        JSONObject data = (JSONObject) args[0];
        Log.e(TAG, "cancel_ride_request_driver: " + data);

    };

    public void getLocation(LatLng latLng) {
        try {
            socket.emit("location-search", new JSONObject()
                    .put("limit", 5)
                    .put("distance", 500)
                    .put("longitude", latLng.longitude)
                    .put("latitude", latLng.latitude)
                    .put("car_id", ""));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getSingleDriverLocation(String driverPhoneNo) {
        try {
            socket.emit("get_single_driver_location", new JSONObject()
                    .put("driver_phone_no", driverPhoneNo));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelRide(String tripId) {
        try {
            Log.d(TAG, "cancelRide: " + " Emitted");

            JSONObject trip = new JSONObject()
                    .put("_id", tripId);

            socket.emit("cancel_ride_request_passenger", new JSONObject()
                    .put("trip", trip)
                    .put("cancel_reason", "Passenger has some problem to go"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void cancelRideWhenNotAccepted(String tripId, String driverPhoneNo) {
        try {
            Log.d(TAG, "cancelRide: " + " Emitted");

            JSONObject trip = new JSONObject()
                    .put("_id", tripId);

            JSONObject tripObject = new JSONObject()
                    .put("trip", trip)
                    .put("driver_phone_no", driverPhoneNo)
                    .put("cancel_reason", "Passenger changed his mind");

            Log.d(TAG, "cancel ride info: " + tripObject);

            socket.emit("cancel_ride_request_passenger", tripObject);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendDriverRequest(LatLng latLng, String driverMobileNo, JSONObject jsonObject) {
        try {
            Log.e(TAG, "startRideRequest: " + jsonObject.toString());
            socket.emit("send-ride-request", jsonObject);

        } catch (Exception e) {
            Log.e("send-request-error", "" + e.getMessage());
        }
    }

    public void getAppRunningState() throws Exception {
        socket.emit("get_passenger_system_status", new JSONObject());
    }

    public void sendMessage() throws Exception {
        socket.emit("add-message", new JSONObject().put("type", "value").put("text",
                "another value").put("username", "Masum"));
    }

    public void requestDriver() throws Exception {
        socket.emit("add-message", new JSONObject().put("phone_no", "").put("trip_ip",
                "").put("username", "Masum"));
    }

    public void sendTips(Tips tips, String driverPhoneNo) throws Exception {

        JSONObject tipsJsonObject = new JSONObject()
                .put("trip_id", tips.getTrip_id())
                .put("tips_amount", tips.getTips_amount())
                .put("total", tips.getTips_total());

        JSONObject JsonObjectDriverInfo = new JSONObject()
                .put("phone_no", driverPhoneNo);

        JSONObject json = new JSONObject()
                .put("tips", tipsJsonObject)
                .put("driver", JsonObjectDriverInfo);

        Log.d(TAG, "Socket trip send " + json.toString());

        socket.emit("on-tips-added", json);

    }

}
