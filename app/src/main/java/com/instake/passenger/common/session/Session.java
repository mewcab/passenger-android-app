package com.instake.passenger.common.session;

import com.instake.passenger.model.User;

import java.lang.reflect.Type;

/**
 * Created by Tushar on 5/21/17.
 */

public interface Session {
    User getUser();

    boolean saveUser(User user);

    boolean isUserLoggedIn();

    void logOutUser();

    String getToken();

    boolean saveObject(String key, Object o);

    boolean saveLocalAppState(int state);

    int getAppLocalState();

    <T> T getObject(String key, Type type);
}
