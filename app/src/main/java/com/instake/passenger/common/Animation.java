package com.instake.passenger.common;

import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.view.View;

import com.instake.passenger.R;


/**
 * Created by masum on 2/22/17.
 */

public class Animation {
    private Context mContext;
    private AnimatorSet mAnimationSet;

    public Animation(Context context) {
        this.mContext = context;
    }

    public AnimatorSet flipAnimation(View view) {
        mAnimationSet = (AnimatorSet) AnimatorInflater.loadAnimator(mContext, R.animator.grow_from_middle);
        mAnimationSet.setTarget(view);
        return mAnimationSet;
    }

    public void stopAnimation() {
        mAnimationSet.cancel();
    }

}
