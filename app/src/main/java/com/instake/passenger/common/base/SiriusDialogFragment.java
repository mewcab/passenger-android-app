package com.instake.passenger.common.base;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by Tushar on 5/10/17.
 */

public abstract class SiriusDialogFragment extends android.support.v4.app.DialogFragment {

    private ViewDataBinding dataBinding;

    protected abstract int getLayoutResourceId();

    protected abstract AlertDialog initDialogComponents();

    public ViewDataBinding getDataBinding() {
        return dataBinding;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);
        return view;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        dataBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()),
                getLayoutResourceId(), null, false);
        return initDialogComponents();
    }

    public View getView() {
        return dataBinding.getRoot();
    }
}
