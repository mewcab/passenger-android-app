package com.instake.passenger.common.base;


import android.app.Activity;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.f2prateek.dart.Dart;
import com.instake.passenger.common.App;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public abstract class SiriusActivity extends AppCompatActivity {

    protected App app;
    private ViewDataBinding dataBinding;

    protected ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = App.getApp(this);

        dataBinding = DataBindingUtil.setContentView(this, getLayoutResourceId());
        Dart.inject(this);

        initComponents();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }

    @LayoutRes
    protected abstract int getLayoutResourceId();

    protected abstract void initComponents();

    public ViewDataBinding getDataBinding() {
        return dataBinding;
    }

    public Context getContext() {
        return this;
    }

    public Activity getActivity() {
        return this;
    }

    public void showLoader() {
        runOnUiThread(() ->
                progressDialog = ProgressDialog.show(SiriusActivity.this, null, "Please wait", true));
    }

    public void hideLoader() {
        if (progressDialog != null && progressDialog.isShowing()) {
            runOnUiThread(() ->
                    progressDialog.dismiss());
        }
    }

    public void hideKeyboard(View view) {
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public void showSoftKeyboard(EditText editText) {
        if (editText == null)
            return;
        InputMethodManager imm = (InputMethodManager) getSystemService(Service.INPUT_METHOD_SERVICE);
        imm.showSoftInput(editText, 0);
    }

    public void startActivity(Class<?> cls, boolean finishSelf) {
        Intent intent = new Intent(this, cls);
        startActivity(intent);
        if (finishSelf) {
            finish();
        }
    }

    public void openUrl(String url) {
        try {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
        } catch (ActivityNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
