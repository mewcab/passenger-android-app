package com.instake.passenger.socket;

/**
 * Created by masum on 6/4/17.
 */

public interface SocketCallbackInterface {
    void onConnect(final Object... args);
    void onUpdatedDriverList(final Object... args);
    void onRideAccepted(final Object... args);
    void onRideEnd(final Object... args);
    void onRideStarted(final Object... args);
    void onDisconnect(final Object... args);
    void setAppState(String appStateAsString);
    void onSendRideRequestSuccess(final Object... args);

}
