package com.instake.passenger.utility.validation;

import java.util.regex.Pattern;

/**
 * Created by Tushar on 4/26/17.
 */

public class ValidatorImpl implements Validator {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    private static final String FULL_NAME_PATTERN = "^[a-zA-Z\\s]+";

    // TODO: need to decide countries to allow for sing up
    private static final String MOBILE_NUMBER_PATTERN = "^\\+88(011|017|018|019|015|016)\\d{8}$";

    private static final String OTP_PATTERN = "(\\d{4})";

    // TODO: decide valid length
    private static final int VALID_REFERRAL_CODE_LENGTH = 8;

    // TODO: decide valid length
    private static final int VALID_COUPON_CODE_LENGTH = 8;

    private static final int VALID_PHONE_NUMBER_LENGTH = 14;
    private static final int VALID_PASSWORD_LENGTH = 6;

    @Override
    public boolean isNotEmpty(CharSequence value) {
        return StringUtil.isNotEmpty(value);
    }

    @Override
    public boolean isNotEmpty(String value) {
        return StringUtil.isNotEmpty(value);
    }

    @Override
    public boolean isEmailValid(String email) {
        return Pattern
                .compile(EMAIL_PATTERN)
                .matcher(email)
                .matches();
    }

    @Override
    public boolean isFullNameValid(String name) {
        return Pattern
                .compile(FULL_NAME_PATTERN)
                .matcher(name)
                .matches();
    }

    @Override
    public boolean isPhoneNumberValid(String phoneNumber) {
        return Pattern
                .compile(MOBILE_NUMBER_PATTERN)
                .matcher(phoneNumber)
                .matches();
    }

    @Override
    public boolean isPhoneNumberLengthValid(String phoneNumber) {
        return phoneNumber.length() == VALID_PHONE_NUMBER_LENGTH;
    }

    @Override
    public boolean isOtpValid(String otp) {
        return Pattern
                .compile(OTP_PATTERN)
                .matcher(otp)
                .matches();
    }

    @Override
    public boolean isAddressValid(String address) {
        // TODO: decide validation strategy
        return false;
    }

    @Override
    public boolean isReferralCodeValid(String code) {
        return code.length() == VALID_REFERRAL_CODE_LENGTH;
    }

    @Override
    public boolean isCouponCodeValid(String code) {
        return code.length() == VALID_COUPON_CODE_LENGTH;
    }

    @Override
    public boolean isPasswordValid(String password) {
        return isNotEmpty(password) && password.length() >= VALID_PASSWORD_LENGTH;
    }
}
