package com.instake.passenger.utility.photocompressor;

import java.io.File;

public interface CompressionListener {
    void onCompressComplete(File compressedFile);
}