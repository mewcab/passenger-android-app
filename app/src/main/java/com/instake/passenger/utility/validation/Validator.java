package com.instake.passenger.utility.validation;

/**
 * Created by Tushar on 5/13/17.
 */

public interface Validator {

    boolean isNotEmpty(CharSequence value);

    boolean isNotEmpty(String value);

    boolean isEmailValid(String email);

    boolean isFullNameValid(String name);

    boolean isPhoneNumberValid(String phoneNumber);

    boolean isPhoneNumberLengthValid(String phoneNumber);

    boolean isOtpValid(String otp);

    boolean isReferralCodeValid(String code);

    boolean isCouponCodeValid(String code);

    boolean isAddressValid(String address);

    boolean isPasswordValid(String password);
}
