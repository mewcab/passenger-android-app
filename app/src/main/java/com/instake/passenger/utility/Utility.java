package com.instake.passenger.utility;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.instake.passenger.R;

import java.text.NumberFormat;

/**
 * Created by masum on 2/28/17.
 */

public class Utility {
    private Context mContext;

    public Utility(Context context) {
        this.mContext = context;
    }

    public boolean isGooglePlayAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(mContext);
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, (Activity) mContext, 0).show();
            return false;
        }
    }

    public static String specialCharacterRemover(String text, String specialChar) {
        if (text != null && text.length() > 2) {
            if ((",").equalsIgnoreCase(text.substring(text.length() - 2, text.length() - 1))) {
                text = text.substring(0, text.length() - 2);

            }
        }
        return text;
    }

    private static boolean textHasContent(String aText) {
        String EMPTY_STRING = "";
        return (aText != null) && (!aText.trim().equals(EMPTY_STRING));
    }

    public static SpannableString getSpannableText(String text, String spanString, int color) {
        if (!textHasContent(spanString)) {
            throw new IllegalArgumentException("Spannable string has no content.");
        }
        int startIndex = text.indexOf(spanString);
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new StyleSpan(Typeface.BOLD), startIndex, text.length(), 0);
        ss.setSpan(new RelativeSizeSpan(1.5f), startIndex, text.length(), 0);
        ss.setSpan(new ForegroundColorSpan(color), startIndex, text.length(), 0);
        return ss;
    }

    public static SpannableString getTextStyleSlightly(String text, String spanString) {
        if (!textHasContent(spanString)) {
            throw new IllegalArgumentException("Spannable string has no content.");
        }
        int startIndex = text.indexOf(spanString);
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new RelativeSizeSpan(0.60f), startIndex, text.length(), 0);
        return ss;
    }

    public static SpannableString getTextStyleSlightly(String text, String spanString, float smallSize) {
        if (!textHasContent(spanString)) {
            throw new IllegalArgumentException("Spannable string has no content.");
        }
        int startIndex = text.indexOf(spanString);
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new RelativeSizeSpan(smallSize), startIndex, text.length(), 0);
        return ss;
    }

    public static SpannableString getSpannableTextSignupBottom(String text, String spanString, int color) {
        if (!textHasContent(spanString)) {
            throw new IllegalArgumentException("Spannable string has no content.");
        }
        int startIndex = text.indexOf(spanString);
        SpannableString ss = new SpannableString(text);
        ss.setSpan(new ForegroundColorSpan(color), startIndex, text.length(), 0);
        return ss;
    }

    public static void setBackground(View view, Drawable drawable) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackground(drawable);
        } else {
            view.setBackgroundDrawable(drawable);
        }
    }

    public Bitmap getMarkerBitmapFromView(@DrawableRes int resId, String title, String address) {

        View customMarkerView = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.layout_custom_map_marker, null);

        TextView markerImageView = (TextView) customMarkerView.findViewById(R.id.imageViewLavel);
        TextView textViewPickupAddress = (TextView) customMarkerView.findViewById(R.id.textViewPickupAddress);

        markerImageView.setText(title);
        textViewPickupAddress.setText(address);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();

        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);

        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();

        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);

        return returnedBitmap;
    }

    public static LatLngBounds getLatLngBounds(LatLng pickUpLatLan, LatLng dropOffLatLng) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(pickUpLatLan);
        builder.include(dropOffLatLng);
        return builder.build();
    }

    public static String twoDecimalFormat(float value) {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);
        return nf.format(value);

    }

    public static String ratingText(float rating) {
        if (rating >= 0.5 && rating <= 1) {
            return "Poor";
        } else if (rating >= 1.5f && rating <= 2.5) {
            return "Average";
        } else if (rating >= 2.5f && rating <= 3) {
            return "Good";
        } else if (rating >= 3.5f && rating <= 4) {
            return "Excellent!";
        } else if (rating >= 4.5f) {
            return "Awesome!";
        } else {
            return "";
        }
    }

}
