package com.instake.passenger.utility.photocompressor;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;


import com.instake.passenger.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class ImageCompressionUtil {
    private static final String TAG = ImageCompressionUtil.class.getName();

    private static final int JPEG_COMPRESSION_QUALITY = 95;

    private static int HARDCODED_MAX_IMAGE_WIDTH = 2560;
    private static int HARDCODED_MAX_IMAGE_HEIGHT = 1380;

    private static Integer configuredWidth;
    private static Integer configuredHeight;

    /**
     * This must be done asynchronously
     *
     * @param context
     * @param imageUri
     */
    public static void getResizedImageUri(CompressionListener listener, Context context, Uri imageUri, Integer configuredWidth, Integer configuredHeight) {
        Uri selectedImageUri = imageUri;

        if (selectedImageUri == null) {
            return;
        }

        if (TextUtils.isEmpty(imageUri.toString())) {
            return;
        }

        ImageCompressionUtil.configuredWidth = configuredWidth; // khal 1280 value expected
        ImageCompressionUtil.configuredHeight = configuredHeight; // khal 960 value expected

        try {
            InputStream bitmapStream = context.getContentResolver().openInputStream(selectedImageUri);
            if (bitmapStream == null) {
                return;
            }

            // We save the position at the beginning of the stream, since we are going to need it later
            int rotateAngle = getImageOrientation(selectedImageUri);

            // Decode bounds first
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeStream(bitmapStream, null, options);
            bitmapStream.close();

            // We check the limits for the image size
            Triplet<Integer, Integer, Boolean> maxSizeAndCrop = getMaxImageSize(options);
            int maxWidth = maxSizeAndCrop.first;
            int maxHeight = maxSizeAndCrop.second;
            boolean shouldCrop = maxSizeAndCrop.third;

            int originalWidth = options.outWidth;
            int originalHeight = options.outHeight;

            if (originalWidth > 0 && originalHeight > 0) {
                // Calculate inSampleSize so we can load the image in memory
                options.inSampleSize = calculateInSampleSize(options, maxWidth, maxHeight, shouldCrop);
            }

            // Decode bitmap
            options.inJustDecodeBounds = false;
            options.inDither = false;
            options.inPurgeable = true;
            options.inInputShareable = true;
            options.inTempStorage = new byte[64 * 1024];

            bitmapStream = context.getContentResolver().openInputStream(selectedImageUri);
            Bitmap bitmap = BitmapFactory.decodeStream(bitmapStream, null, options);
            bitmapStream.close();

            int cropWidth = 0;
            int cropHeight = 0;

            Matrix matrix = null;

            // If the bitmap is still larger than the limits, we use a matrix to reduce it
            if (bitmap.getWidth() > maxWidth ||
                    bitmap.getHeight() > maxHeight) {
                matrix = new Matrix();

                if (shouldCrop) {
                    cropHeight = Math.max(0, bitmap.getHeight() - maxHeight);
                    cropWidth = Math.max(0, bitmap.getWidth() - maxWidth);
                } else {
                    final int width = bitmap.getWidth();
                    final int height = bitmap.getHeight();
                    final float sx = maxWidth / (float) width;
                    final float sy = maxHeight / (float) height;
                    matrix.setScale(sx, sy);
                }
            }

            // If the image is rotated, we rotate it again
            if (rotateAngle != 0) {
                if (matrix == null) {
                    matrix = new Matrix();
                }
                matrix.postRotate(rotateAngle);
            }

            Bitmap newBitmap = null;
            if (matrix != null) {
                newBitmap = Bitmap.createBitmap(bitmap,
                        cropWidth / 2, cropHeight / 2,
                        bitmap.getWidth() - cropWidth, bitmap.getHeight() - cropHeight,
                        matrix, true);
                bitmap.recycle();
            }


            // we create a new file in the sdcard folder.
            File f = new File(getCompressImagesDirectory(context), String.valueOf(System.currentTimeMillis()) + ".jpg");
            OutputStream fOut = new FileOutputStream(f);
            if (newBitmap != null) {
                newBitmap.compress(Bitmap.CompressFormat.JPEG, JPEG_COMPRESSION_QUALITY, fOut);
                newBitmap.recycle();
            } else {
                bitmap.compress(Bitmap.CompressFormat.JPEG, JPEG_COMPRESSION_QUALITY, fOut);
                bitmap.recycle();
            }
            fOut.flush();
            fOut.close();

            Uri newUri = Uri.fromFile(f);

            if (newUri != null) {
                selectedImageUri = newUri;
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return;
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        if (listener != null) {
            listener.onCompressComplete(new File(selectedImageUri.getPath()));
        }
    }

    private static int getImageOrientation(Uri imageUri) throws IOException {
        int rotateAngle = 0;
        ExifInterface exif = new ExifInterface(imageUri.getPath());
        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
        switch (orientation) {
            case 1:
                break;  // top left
            case 2:
                break;  // top right
            case 3:
                rotateAngle = 180;
                break;  // bottom right
            case 4:
                rotateAngle = 180;
                break;  // bottom left
            case 5:
                rotateAngle = 90;
                break;  // left top
            case 6:
                rotateAngle = 90;
                break;  // right top
            case 7:
                rotateAngle = 270;
                break;  // right bottom
            case 8:
                rotateAngle = 270;
                break;  // left bottom
            default:
                break;  // Unknown
        }
        return rotateAngle;
    }

    /**
     * Gets the best image sample size to decode and send based on Khal API parameters
     *
     * @param options The current image dimensions, which is used to calculate the aspect ratio
     * @return A Pair containing two pairs, the first one contains the width and height as the first and second value respectively
     * the second one returns the crop area if there's any needed to fit the dimensions
     */
    private static Triplet<Integer, Integer, Boolean> getMaxImageSize(BitmapFactory.Options options) {
        Integer destWidth = null;
        Integer destHeight = null;

        Integer minWidth = 80;
        Integer minHeight = 80;

        if (configuredWidth != null && configuredHeight != null) {
            destWidth = configuredWidth;
            destHeight = configuredHeight;
        }

        // If we still don't have the image width and height, we simply add them ourselves
        if (destWidth == null) {
            destWidth = HARDCODED_MAX_IMAGE_WIDTH;
        }
        if (destHeight == null) {
            destHeight = HARDCODED_MAX_IMAGE_HEIGHT;
        }

        // We set the dimensions keeping the image's aspect ratio
        float rw = (float) destWidth / (float) options.outWidth;
        float rh = (float) destHeight / (float) options.outHeight;

        float resizedRatio = Math.min(rw, rh);

        int ratioWidth = (int) (resizedRatio * options.outWidth);
        int ratioHeight = (int) (resizedRatio * options.outHeight);

        boolean shouldCrop = false;

        if ((minWidth != null && minHeight != null) &&
                (minWidth > ratioWidth || minHeight > ratioHeight)) {
            // The reduced width or height don't fit inside the minimum bounds
            // This can happen with weird aspect ratios
            // So we will have to crop
            shouldCrop = true;
        } else {
            destWidth = ratioWidth;
            destHeight = ratioHeight;
        }

        Triplet<Integer, Integer, Boolean> destSize = new Triplet<Integer, Integer, Boolean>(destWidth, destHeight, shouldCrop);

        return destSize;
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param options   An options object with out* params already populated (run through a decode*
     *                  method with inJustDecodeBounds==true
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight, boolean forCropping) {
        // Raw height and width of image
        final int curHeight = options.outHeight;
        final int curWidth = options.outWidth;
        return calculateInSampleSize(curWidth, curHeight, reqWidth, reqHeight, forCropping);
    }

    /**
     * Calculate an inSampleSize for use in a {@link BitmapFactory.Options} object when decoding
     * bitmaps using the decode* methods from {@link BitmapFactory}. This implementation calculates
     * the closest inSampleSize that will result in the final decoded bitmap having a width and
     * height equal to or larger than the requested width and height. This implementation does not
     * ensure a power of 2 is returned for inSampleSize which can be faster when decoding but
     * results in a larger bitmap which isn't as useful for caching purposes.
     *
     * @param curWidth  The current width
     * @param curHeight The current height
     * @param reqWidth  The requested width of the resulting bitmap
     * @param reqHeight The requested height of the resulting bitmap
     * @return The value to be used for inSampleSize
     */
    public static int calculateInSampleSize(int curWidth, int curHeight, int reqWidth, int reqHeight, boolean forCropping) {
        int inSampleSize = 1;

        if (curHeight > reqHeight || curWidth > reqWidth) {
            final float totalReqPixelsCap;

            final int halfWidth = curWidth / 2;
            final int halfHeight = curHeight / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }

            if (forCropping) {
                totalReqPixelsCap = reqWidth * reqHeight * 2;
            } else {
                totalReqPixelsCap = reqWidth * reqHeight;
            }

            final float totalPixels = curWidth * curHeight;
            while (totalPixels * (1 / Math.pow(inSampleSize, 2)) >
                    totalReqPixelsCap) {
                inSampleSize++;
            }
        }

        //Log.v(TAG, "calculateInSampleSize: " + inSampleSize + " from dimensions: image: " + height + ":w" + width + "req h" + reqHeight + ":w" + reqWidth);
        return inSampleSize;
    }

    public static File getCompressImagesDirectory(Context context) {
        // We create the Images folder
        // create a File object for the parent directory
        // We use the flavor as the directory name
        File imagesDirectory = new File(Environment.getExternalStorageDirectory()
                + File.separator + "." + context.getString(R.string.app_name) + File.separator);
        // have the object build the directory structure, if needed.
        if (!imagesDirectory.exists()) {
            imagesDirectory.mkdirs();
        }

        return imagesDirectory;
    }
}
