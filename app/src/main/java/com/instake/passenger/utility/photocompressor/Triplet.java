package com.instake.passenger.utility.photocompressor;

/**
 * Created by Víctor González on 23/04/14.
 */
public class Triplet<F, S, T> {
    public F first;
    public S second;
    public T third;

    public Triplet(F first, S second, T third) {
        this.first = first;
        this.second = second;
        this.third = third;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Triplet)) return false;

        Triplet triplet = (Triplet) o;

        if (first != null ? !first.equals(triplet.first) : triplet.first != null) return false;
        if (second != null ? !second.equals(triplet.second) : triplet.second != null) return false;
        if (third != null ? !third.equals(triplet.third) : triplet.third != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = first != null ? first.hashCode() : 0;
        result = 31 * result + (second != null ? second.hashCode() : 0);
        result = 31 * result + (third != null ? third.hashCode() : 0);
        return result;
    }

    public static <A, B, C> Triplet <A, B, C> create(A a, B b, C c) {
        return new Triplet<A, B, C>(a, b, c);
    }

    @Override
    public String toString() {
        return new StringBuilder().append(first.toString()).append(second.toString()).append(third.toString()).toString();
    }
}
