package com.instake.passenger.Rx;

import android.util.Log;

import com.android.volley.NetworkError;
import com.google.android.gms.maps.model.LatLng;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.Rating;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.geocoder.GeoCoderPlaceResponse;
import com.instake.passenger.model.historydata.HistoryResp;
import com.instake.passenger.model.mapdistance.DistanceResponse;
import com.instake.passenger.model.offers.OfferResponse;
import com.instake.passenger.model.payment.Tips;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

import static com.instake.passenger.api.JsonBuilder.createPostRatingBody;
import static com.instake.passenger.api.JsonBuilder.createTipsBody;

public class RxService {
    private static String TAG = "RxService";
    private ApiService networkService = null;
    private Session session;

    public RxService(ApiService networkService, Session session) {
        this.networkService = networkService;
        this.session = session;
    }

    public RxService(Session session) {
        this.session = session;
    }

    public Subscription getDistanceInfo(LatLng pickUpLatLng, LatLng dropOffLatLng,
                                        final getDistanceInfoCallback callback) {

        return App.getInstance().getApiServiceWithCaching(null)
                .getDistanceInfo(pickUpLatLng.latitude + "," + pickUpLatLng.longitude, dropOffLatLng.latitude + "," + dropOffLatLng.longitude, "false", "matric", "driving")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends DistanceResponse>>() {
                    @Override
                    public Observable<? extends DistanceResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<DistanceResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(DistanceResponse respose) {
                        try {
                            callback.onSuccess(respose);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                });
    }

    public Subscription getGeoCoderPlace(LatLng pickUpLatLng,
                                         final getGeoCoderPlaceNameCallback callback) {

        return App.getInstance().getApiServiceWithCaching(null)
                .getGeoCoderPlaceName(pickUpLatLng.latitude + "," + pickUpLatLng.longitude, true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends GeoCoderPlaceResponse>>() {
                    @Override
                    public Observable<? extends GeoCoderPlaceResponse> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<GeoCoderPlaceResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(GeoCoderPlaceResponse respose) {
                        callback.onSuccess(respose);

                    }
                });
    }

    public Subscription postRating(Trip trip, Rating rating, final postApiCallBack callback) {

        return App.getInstance().getApiService()
                .postRating(session.getToken(), trip.getTripId(), RequestBody.create(MediaType.parse("text/plain"), createPostRatingBody(rating)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ResponseBody>>() {
                    @Override
                    public Observable<? extends ResponseBody> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(ResponseBody respose) {
                        callback.onSuccess(respose);

                    }
                });
    }

    public Subscription postTips(Tips tips, final postApiCallBack callback) {

        return App.getInstance().getApiService()
                .postTips(session.getToken(), tips.getTrip_id(), RequestBody.create(MediaType.parse("text/plain"), createTipsBody(tips)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ResponseBody>>() {
                    @Override
                    public Observable<? extends ResponseBody> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Log.d("Tips success", "onCompleted:*");
                    }

                    @Override
                    public void onError(Throwable e) {
                        callback.onError(new NetworkError(e));

                    }

                    @Override
                    public void onNext(ResponseBody respose) {
                        Log.d("trip_response", "onNext: " + respose);
                        callback.onSuccess(respose);

                    }
                });
    }

    public Subscription getHistory(String pagination, final getHistoryCallBack callback) {
        return App.getInstance().getApiService()
                .getRideHistory(session.getToken(), pagination)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<HistoryResp>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: get ride history: " + e.getMessage());
                        callback.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(HistoryResp historyResp) {
                        callback.onSuccess(historyResp);
                        Log.e(TAG, "getHistory: " + historyResp.getCount());
                    }
                });
    }

    public Subscription getOffer(final getOfferCallBack getOfferCallBack) {
        return App.getInstance().getApiService()
                .getOffer(session.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<OfferResponse>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, "onError: get offer: " + e.getMessage());
                        getOfferCallBack.onError(new NetworkError(e));
                    }

                    @Override
                    public void onNext(OfferResponse response) {
                        getOfferCallBack.onSuccess(response);
                    }
                });
    }

    interface NetworkErrorInterface {
        void onError(NetworkError networkError);
    }

    public interface getDistanceInfoCallback extends NetworkErrorInterface {
        void onSuccess(DistanceResponse cityListResponse) throws Exception;
    }

    public interface getGeoCoderPlaceNameCallback extends NetworkErrorInterface {
        void onSuccess(GeoCoderPlaceResponse response);
    }

    public interface postApiCallBack extends NetworkErrorInterface {
        void onSuccess(ResponseBody response);
    }

    public interface getHistoryCallBack extends NetworkErrorInterface {
        void onSuccess(HistoryResp response);
    }

    public interface getOfferCallBack extends NetworkErrorInterface {
        void onSuccess(OfferResponse response);
    }
}