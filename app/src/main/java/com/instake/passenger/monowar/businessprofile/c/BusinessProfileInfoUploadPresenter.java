package com.instake.passenger.monowar.businessprofile.c;

import android.util.Log;

import com.instake.passenger.R;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;
import com.instake.passenger.monowar.ImageUploadListener;
import com.instake.passenger.monowar.ProgressRequestBody;
import com.instake.passenger.utility.validation.Validator;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class BusinessProfileInfoUploadPresenter implements BasePresenter<com.instake.passenger.monowar.invitefriends.InviteFriendsView> {

    private final static String TAG = "InfoUpload";

    private WeakReference<BusinessProfileInfoUploadView> view;
    private ApiService apiService;
    private Session session;
    private Validator validator;

    private UserProfile userProfile;

    public BusinessProfileInfoUploadPresenter(BusinessProfileInfoUploadView view, ApiService apiService, Session session, Validator validator) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onImageClick() {
        if (view.get().getIdImageUri() == null) {
            CropImage.activity()
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .setActivityTitle(view.get().getContext().getString(R.string.title_activity_crop_image))
                    .start(view.get().getActivity());
        } else {
            view.get().showFullScreenImage();
        }
    }

    public void onImageEditClick() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle(view.get().getContext().getString(R.string.title_activity_crop_image))
                .start(view.get().getActivity());
    }

    public void onSubmitClick() {
        if (!validator.isNotEmpty(view.get().getIdImageUri())) {
            view.get().showImageError();
            return;
        }
        view.get().showLoader();
        uploadInfo("profile-business");
    }

    private void uploadInfo(String apiAction) {
        apiService.postUserProfiles(session.getToken(), apiAction,
                RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createBusinessProfileBody(view.get().getEmail(), view.get().getCompanyName(), view.get().getCompanyAddress())))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends UserProfile>>() {
                    @Override
                    public Observable<? extends UserProfile> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<UserProfile>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onUploadFailed(e.getMessage());
                    }

                    @Override
                    public void onNext(UserProfile userProfile) {
                        onUploadSuccess(userProfile);
                    }
                });
    }

    private void onUploadSuccess(UserProfile userProfile) {
        User user = session.getUser();
        user.userProfile = userProfile;
        session.saveUser(user);
        if (view.get().getIdImageUri() != null) {
            this.userProfile = userProfile;
            uploadImage(new File(view.get().getIdImageUri()), new ImageUploadListener() {
                @Override
                public void onProgress(int progress) {

                }

                @Override
                public void onSuccess() {

                }

                @Override
                public void onFailure() {

                }
            }, "images_front");
        } else {
            view.get().hideLoader();
            App.getInstance().bus().send(userProfile);
            view.get().backToProfileActivity();
        }
    }

    private void onUploadFailed(String error) {
        Log.e(TAG, "Error: " + error);
        view.get().hideLoader();
        view.get().showUploadInfoFailed();
    }

    private void uploadImage(File file, ImageUploadListener listener, String category) {
        apiService.uploadImage(session.getToken(), getMultipartImage(file, listener), getCategory(category))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ResponseBody>>() {
                    @Override
                    public Observable<? extends ResponseBody> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        onUploadFailed(e.getMessage());
                        App.getInstance().bus().send(userProfile);
                        view.get().backToProfileActivity();
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        try {
                            String response = responseBody.string();
                            Log.e(TAG, "responseBody Image " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            userProfile.idCardImageURL = jsonObject.getJSONObject("images").getString("150X150");
                            User user = session.getUser();
                            user.setUserProfile(userProfile);
                            session.saveUser(user);
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                        view.get().hideLoader();
                        App.getInstance().bus().send(userProfile);
                        view.get().backToProfileActivity();
                    }
                });
    }

    private MultipartBody.Part getMultipartImage(File idCardImage, ImageUploadListener listener) {
        return MultipartBody.Part.createFormData("upload", idCardImage.getName(),
                new ProgressRequestBody(idCardImage, listener));
    }

    private RequestBody getCategory(String category) {
        return RequestBody.create(MediaType.parse("text/plain"), category);
    }
}
