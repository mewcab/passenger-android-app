package com.instake.passenger.monowar.teacherprofile.b;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityTeacherProfileInfoBinding;
import com.instake.passenger.monowar.studentprofile.b.StudentProfileInfoUploadActivity;
import com.instake.passenger.monowar.teacherprofile.c.TeacherProfileInfoUploadActivity;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class TeacherProfileInfoActivity extends SiriusPresenterActivity<TeacherProfileInfoView, TeacherProfileInfoPresenter> implements TeacherProfileInfoView {

    private final static String TAG = "TeacherProfileInfo";

    public static Activity activity;

    ActivityTeacherProfileInfoBinding binding;

    private Bundle bundle;

    @Inject
    TeacherProfileInfoPresenter presenter;

    @Override
    protected TeacherProfileInfoPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new TeacherProfileInfoModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_teacher_profile_info;
    }

    @Override
    protected void initComponents() {
        activity = this;

        binding = (ActivityTeacherProfileInfoBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        bundle = getIntent().getExtras();

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNextClick();
            }
        });
    }

    @Override
    public String getInstitutionName() {
        return binding.etInstitutionName.getText().toString();
    }

    @Override
    public String getDepartmentName() {
        return binding.etDepartmentName.getText().toString();
    }

    @Override
    public void showInstitutionNameError() {
        binding.tilInstitutionName.setError(getString(R.string.error_institution_name));
    }

    @Override
    public void showDepartmentNameError() {
        binding.tilDepartmentName.setError(getString(R.string.error_department_name));
    }

    @Override
    public void hideAllErrors() {
        binding.tilInstitutionName.setErrorEnabled(false);
        binding.tilDepartmentName.setErrorEnabled(false);
    }

    @Override
    public void proceedToUploadIdCardActivity() {
        bundle.putString("BUNDLE_INSTITUTION_NAME", getInstitutionName());
        bundle.putString("BUNDLE_DEPARTMENT_NAME", getDepartmentName());
        Log.e(TAG, bundle.toString());
        Intent intent = new Intent(getActivity(), TeacherProfileInfoUploadActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
