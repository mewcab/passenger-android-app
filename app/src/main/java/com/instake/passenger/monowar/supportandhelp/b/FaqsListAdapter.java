package com.instake.passenger.monowar.supportandhelp.b;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.model.suppotnhelp.Help;

import java.util.List;

/**
 * Created by Mostafa Monowar on 20-Sep-17.
 */

public class FaqsListAdapter extends RecyclerView.Adapter<FaqsListAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Help> helpList;

    FaqsListAdapter(Context context, List<Help> helpList) {
        layoutInflater = LayoutInflater.from(context);
        this.helpList = helpList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.single_item_faqs, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvTitle.setText(helpList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return helpList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;

        private MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}
