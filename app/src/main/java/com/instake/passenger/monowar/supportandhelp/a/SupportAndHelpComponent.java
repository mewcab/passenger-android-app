package com.instake.passenger.monowar.supportandhelp.a;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

@Singleton
@Subcomponent(modules = SupportAndHelpModule.class)
public interface SupportAndHelpComponent {
    void inject (SupportAndHelpActivity activity);
}
