package com.instake.passenger.monowar.supportandhelp.b;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Module
public class FaqsDetailsModule {
    private FaqsDetailsView view;

    public FaqsDetailsModule(FaqsDetailsView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public FaqsDetailsPresenter provideFaqsDetailsPresenter() {
        return new FaqsDetailsPresenter(view);
    }
}
