package com.instake.passenger.monowar.mobilenumbervalidation.a;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

@Singleton
@Subcomponent(modules = SignupGetStartedModule.class)
public interface SignupGetStartedComponent {
    void inject(SignupGetStartedActivity activity);
}
