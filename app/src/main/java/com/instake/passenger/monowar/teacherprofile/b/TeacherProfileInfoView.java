package com.instake.passenger.monowar.teacherprofile.b;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.EducationDetails;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface TeacherProfileInfoView extends BaseView {

    String getInstitutionName();

    String getDepartmentName();

    void showInstitutionNameError();

    void showDepartmentNameError();

    void hideAllErrors();

    void proceedToUploadIdCardActivity();
}
