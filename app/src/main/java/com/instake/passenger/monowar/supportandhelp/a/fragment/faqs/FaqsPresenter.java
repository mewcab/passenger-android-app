package com.instake.passenger.monowar.supportandhelp.a.fragment.faqs;

import com.instake.passenger.common.base.BasePresenter;

import java.lang.ref.WeakReference;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public class FaqsPresenter implements BasePresenter<FaqsView> {

    private WeakReference<FaqsView> view;

    public FaqsPresenter(FaqsView view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}
