package com.instake.passenger.monowar.mobilenumbervalidation.b;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.monowar.VerificationStatus;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

public interface SignupOtpView extends BaseView {

    String getVerificationCode();

    void showOtpValidationError();

    void hideOtpValidationError();

    String getRequestingPhoneNumber();

    void showVerificationSuccessMessage();

    void proceedToSignupActivity(VerificationStatus verificationStatus);

    void proceedToSplashActivity();

    void proceedToLoginActivity();

    void showVerificationFailureMessage();

    void showVerificationProcessFailureMessage();

    void showVerificationCodeSendingSuccessMessage();

    void showVerificationCodeSendingFailureMessage();

    void hideKeyboard();

    void updateWaitTime(String time);

    void updateOTPResendMessage(boolean showResend);

    void enableResend(boolean isEnabled);

    void registerVerificationCodeReceiver();

    void unregisterVerificationCodeReceiver();
}
