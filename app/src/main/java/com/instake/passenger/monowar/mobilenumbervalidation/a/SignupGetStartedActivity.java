package com.instake.passenger.monowar.mobilenumbervalidation.a;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySignupGetStartedBinding;
import com.instake.passenger.feature.login.LoginActivity;
import com.instake.passenger.feature.splash.SplashActivity;
import com.instake.passenger.monowar.mobilenumbervalidation.b.SignupOtpActivity;
import com.transitionseverywhere.TransitionManager;

import java.util.Arrays;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class SignupGetStartedActivity extends SiriusPresenterActivity<SignupGetStartedView, SignupGetStartedPresenter> implements SignupGetStartedView {

    private static final String TAG = SignupGetStartedActivity.class.getSimpleName();

    private ActivitySignupGetStartedBinding binding;

    private CallbackManager mCallbackManager;

    @Inject
    SignupGetStartedPresenter presenter;

    public static void open(Activity activity) {
        Intent intent = new Intent(activity, SignupGetStartedActivity.class);
        activity.startActivity(intent);
    }

    public static void clearAllScreensAndOpen(Activity activity) {
        Intent intent = new Intent(activity, SignupGetStartedActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    @Override
    protected SignupGetStartedPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SignupGetStartedModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_signup_get_started;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySignupGetStartedBinding) getDataBinding();

        initFacebookComponent();

        SpannableString spannableString = new SpannableString(getString(R.string.login_options));
        spannableString.setSpan(passwordLogin, 11, 19, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(facebookLogin, 27, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.text_color_pink)), 11, 19, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.text_color_pink)), 27, 35, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvLogin.setText(spannableString);
        binding.tvLogin.setMovementMethod(LinkMovementMethod.getInstance());

        binding.layoutPhoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TransitionManager.beginDelayedTransition(binding.layoutRoot);
                binding.layoutBackground.setVisibility(View.GONE);
                binding.etPhoneNumber.setVisibility(View.VISIBLE);
                binding.tvMessage.setVisibility(View.VISIBLE);
                binding.layoutLogin.setVisibility(View.VISIBLE);
                binding.viewBesideCode.setVisibility(View.INVISIBLE);
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        showSoftKeyboard(binding.etPhoneNumber);
                    }
                }, 1000);
            }
        });

        binding.etPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    binding.viewBackgroundTint.setBackgroundColor(ContextCompat.getColor(SignupGetStartedActivity.this, R.color.hot_pink));
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        binding.viewBackgroundTint.setBackground(ContextCompat.getDrawable(SignupGetStartedActivity.this, R.drawable.button_background_grey));
                    } else {
                        binding.viewBackgroundTint.setBackgroundDrawable(ContextCompat.getDrawable(SignupGetStartedActivity.this, R.drawable.button_background_grey));
                    }
                }
            }
        });

        binding.etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.checkPhoneNumberValidation(getPhoneNumber());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onClickNext();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public String getPhoneNumber() {
        return binding.tvCountryCode.getText().toString() + binding.etPhoneNumber.getText().toString();
    }

    @Override
    public void showPhoneNumberError() {
        binding.tvMessage.setText(R.string.please_enter_a_valid_phone_number);
        binding.tvMessage.setTextColor(ContextCompat.getColor(SignupGetStartedActivity.this, R.color.red));
    }

    @Override
    public void hidePhoneNumberError() {
        binding.tvMessage.setText(R.string.we_ll_send_a_code_to_verify_your_phone);
        binding.tvMessage.setTextColor(ContextCompat.getColor(SignupGetStartedActivity.this, R.color.warm_gray));
    }

    @Override
    public void showVerificationCodeSendingSuccessMessage() {

    }

    @Override
    public void showVerificationCodeSendingFailureMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SignupGetStartedActivity.this, getString(R.string.err_code_is_not_sent), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onLoginFailed(String error) {
        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void proceedSignupOTPActivity() {
        SignupOtpActivity.clearAllAndOpen(this, getPhoneNumber());
    }

    @Override
    public void proceedToLoginActivity() {
        Bundle bundle = new Bundle();
        bundle.putString("BUNDLE_PHONE_NUMBER", getPhoneNumber());
        bundle.putBoolean("BUNDLE_IS_SIGN_UP", true);
        LoginActivity.open(this, bundle);
        Log.e("TAG", bundle.toString());
    }

    @Override
    public void clearAllActivitiesAndNavigateToSplash() {
        SplashActivity.ClearAllScreenAndOpen(this);
    }

    @Override
    public void hideKeyboard() {
        hideKeyboard(binding.etPhoneNumber);
    }

    public void initFacebookComponent() {
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("****", "***Twice****");

                        presenter.onFbLoginSuccess(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e(TAG, exception.toString());
                        LoginManager.getInstance().logOut();
                    }
                });
    }

    private void buildAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(message)
                .setCancelable(false)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.dismiss();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    ClickableSpan passwordLogin = new ClickableSpan() {
        @Override
        public void onClick(View textView) {
            proceedToLoginActivity();
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };

    ClickableSpan facebookLogin = new ClickableSpan() {
        @Override
        public void onClick(View textView) {
            LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email"));
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };
}
