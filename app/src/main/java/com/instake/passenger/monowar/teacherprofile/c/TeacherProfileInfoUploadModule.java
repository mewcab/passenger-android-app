package com.instake.passenger.monowar.teacherprofile.c;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class TeacherProfileInfoUploadModule {

    private TeacherProfileInfoUploadView view;

    public TeacherProfileInfoUploadModule(TeacherProfileInfoUploadView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public TeacherProfileInfoUploadPresenter provideTeacherProfileInfoUploadPresenter(ApiService apiService, Session session, Validator validator) {
        return new TeacherProfileInfoUploadPresenter(view, apiService, session, validator);
    }
}
