package com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.model.historydata.Trip;

import java.util.List;

/**
 * Created by annanovasit on 4/20/17.
 */

public class TripsHelpAdapter extends RecyclerView.Adapter<TripsHelpAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Trip> arrayList;
    private Context context;


    public TripsHelpAdapter(Context context, List<Trip> arrayList) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.single_item_trips_help, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Trip trip = arrayList.get(position);

        holder.tvTimeStamp.setText(trip.getStartTime());
        holder.tvPickUpLocation.setText(trip.getPickupName());
        holder.tvDestination.setText(trip.getDropoffName());
        holder.tvFare.setText(String.valueOf(trip.getSubTotal()));
        holder.tvPaymentType.setText(trip.getPaymentMethod());
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvTimeStamp, tvPickUpLocation, tvDestination, tvFare, tvPaymentType;

        private MyViewHolder(View itemView) {
            super(itemView);

            tvTimeStamp = (TextView) itemView.findViewById(R.id.tv_time_stamp);
            tvPickUpLocation = (TextView) itemView.findViewById(R.id.tv_pick_up);
            tvDestination = (TextView) itemView.findViewById(R.id.tv_destination);
            tvFare = (TextView) itemView.findViewById(R.id.tv_fare);
            tvPaymentType = (TextView) itemView.findViewById(R.id.tv_payment_type);
        }
    }
}
