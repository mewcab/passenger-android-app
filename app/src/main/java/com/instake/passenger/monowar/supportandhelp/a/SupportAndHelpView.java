package com.instake.passenger.monowar.supportandhelp.a;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.suppotnhelp.Video;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

public interface SupportAndHelpView extends BaseView {
    void loadDataIntoAdapter(List<Video> video_list);

    void showLoading();

    void hideLoading();
}
