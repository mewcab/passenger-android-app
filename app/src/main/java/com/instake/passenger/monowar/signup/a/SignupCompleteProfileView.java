package com.instake.passenger.monowar.signup.a;

import android.os.Bundle;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

public interface SignupCompleteProfileView extends BaseView {

    void initFacebookComponent();

    String getPhoneNumber();

    String getVerificationCode();

    void signupFailed();

    void proceedToProvideInformationActivity(String token);

    void proceedPasswordConfirmActivity(Bundle bundle);
}
