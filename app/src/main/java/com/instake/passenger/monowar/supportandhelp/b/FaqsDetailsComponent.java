package com.instake.passenger.monowar.supportandhelp.b;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Singleton
@Subcomponent(modules = FaqsDetailsModule.class)
public interface FaqsDetailsComponent {
    void inject (FaqsDetailsActivity activity);
}
