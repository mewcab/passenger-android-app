package com.instake.passenger.monowar.supportandhelp.c.help;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.instake.passenger.R;

import java.util.ArrayList;

/**
 * Created by annanovasit on 4/20/17.
 */

public class HelpAdapter extends RecyclerView.Adapter<HelpAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private ArrayList<String> arrayList;
    private Context context;


    public HelpAdapter(Context context, ArrayList<String> arrayList) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.arrayList = arrayList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.single_item_faqs, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvTitle.setText(arrayList.get(position));
    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;

        private MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}
