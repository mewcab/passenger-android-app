package com.instake.passenger.monowar.profileonboard;


import com.instake.passenger.R;

/**
 * Created by Adib on 31-Mar-17.
 */

public enum StudentProfileOnBoardingObject {

    ONBOARDING_OBJECT_1(0, R.layout.content_onboarding_education_slide_1),
    ONBOARDING_OBJECT_2(1, R.layout.content_onboarding_education_slide_1),
    ONBOARDING_OBJECT_3(2, R.layout.content_onboarding_education_slide_1);


    private int mTitleResId;
    private int mLayoutResId;

    StudentProfileOnBoardingObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
