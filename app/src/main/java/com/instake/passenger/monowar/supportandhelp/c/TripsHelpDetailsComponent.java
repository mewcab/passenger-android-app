package com.instake.passenger.monowar.supportandhelp.c;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

@Singleton
@Subcomponent(modules = TripsHelpDetailsModule.class)
public interface TripsHelpDetailsComponent {
    void inject (TripsHelpDetailsActivity activity);
}
