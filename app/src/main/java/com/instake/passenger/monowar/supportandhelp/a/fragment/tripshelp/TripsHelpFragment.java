package com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp;

import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusFragment;
import com.instake.passenger.databinding.FragmentTripsHelpBinding;
import com.instake.passenger.model.historydata.Trip;
import com.instake.passenger.monowar.EqualSpacingItemDecoration;
import com.instake.passenger.monowar.RecyclerTouchListener;
import com.instake.passenger.monowar.supportandhelp.a.SupportAndHelpActivity;
import com.instake.passenger.monowar.supportandhelp.c.TripsHelpDetailsActivity;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 21-Aug-17.
 */

public class TripsHelpFragment extends SiriusFragment<TripsHelpView, TripsHelpPresenter> implements TripsHelpView {

    private static final int SCROLL_DIRECTION_UP = -1;

    private FragmentTripsHelpBinding binding;

    private List<Trip> tripList;
    private TripsHelpAdapter tripsHelpAdapter;

    @Inject
    TripsHelpPresenter presenter;

    @Override
    protected TripsHelpPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new TripsHelpModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_trips_help;
    }

    @Override
    protected void initFragmentComponents() {
        binding = (FragmentTripsHelpBinding) getDataBinding();

        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        int defaultGap = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_4dp), getResources().getDisplayMetrics());
        binding.recyclerView.addItemDecoration(new EqualSpacingItemDecoration(defaultGap, EqualSpacingItemDecoration.VERTICAL));
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());

        presenter.getApiData();

        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), binding.recyclerView, new RecyclerTouchListener.OnRecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Trip trip = tripList.get(position);
                        Intent intent = new Intent(getActivity(), TripsHelpDetailsActivity.class);
                        intent.putExtra("EXTRA_RIDE_HISTORY", Parcels.wrap(trip));
                        startActivity(intent);
                    }
                }, 200);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_2dp), getResources().getDisplayMetrics());
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (binding.recyclerView.canScrollVertically(SCROLL_DIRECTION_UP)) {
                        ((SupportAndHelpActivity) getActivity()).binding.appbar.setElevation(elevation);
                    } else {
                        ((SupportAndHelpActivity) getActivity()).binding.appbar.setElevation(0);
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        ((SupportAndHelpActivity) getActivity()).showLoader();
    }

    @Override
    public void hideLoader() {
        ((SupportAndHelpActivity) getActivity()).hideLoader();
    }

    @Override
    public void loadDataIntoAdapter(List<Trip> arrayList) {
        if (tripsHelpAdapter == null) {
            tripList = arrayList;
            tripsHelpAdapter = new TripsHelpAdapter(getActivity(), arrayList);
            binding.recyclerView.setAdapter(tripsHelpAdapter);
        } else {
            tripList.addAll(arrayList);
            tripsHelpAdapter.notifyDataSetChanged();
        }
    }
}
