package com.instake.passenger.monowar.supportandhelp.c.receipt;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Singleton
@Subcomponent(modules = ReceiptModule.class)
public interface ReceiptComponent {
    void inject(ReceiptFragment fragment);
}
