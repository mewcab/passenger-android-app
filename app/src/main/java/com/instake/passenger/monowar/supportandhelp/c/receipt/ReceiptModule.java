package com.instake.passenger.monowar.supportandhelp.c.receipt;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Module
public class ReceiptModule {
    private ReceiptView view;

    public ReceiptModule(ReceiptView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public ReceiptPresenter provideReceiptPresenter(ApiService apiService, Session session) {
        return new ReceiptPresenter(view, apiService, session);
    }
}
