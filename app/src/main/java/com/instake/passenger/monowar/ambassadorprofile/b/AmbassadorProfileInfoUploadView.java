package com.instake.passenger.monowar.ambassadorprofile.b;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface AmbassadorProfileInfoUploadView extends BaseView {
    String getEmail();
    String getIdImageUri();
    void showFullScreenImage();
    void hideFullScreenImage();
    void showImageError();
    void showUploadInfoFailed();
    void showUploadInfoSuccess();
    void backToProfileActivity();
}
