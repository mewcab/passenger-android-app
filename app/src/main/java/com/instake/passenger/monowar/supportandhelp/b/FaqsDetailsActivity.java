package com.instake.passenger.monowar.supportandhelp.b;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.text.Html;
import android.util.TypedValue;
import android.view.View;

import com.f2prateek.dart.InjectExtra;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityFaqsDetailsBinding;
import com.instake.passenger.model.suppotnhelp.Help;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public class FaqsDetailsActivity extends SiriusPresenterActivity<FaqsDetailsView, FaqsDetailsPresenter> implements FaqsDetailsView {

    private static final int SCROLL_DIRECTION_UP = -1;

    ActivityFaqsDetailsBinding binding;

    @Inject
    FaqsDetailsPresenter presenter;

    @InjectExtra("EXTRA_HELP")
    Help help;

    @Override
    protected FaqsDetailsPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new FaqsDetailsModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_faqs_details;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityFaqsDetailsBinding) getDataBinding();

        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setHomeButtonEnabled(true);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        binding.collapsingToolbarLayout.setTitle(getString(R.string.title_activity_support_and_help));
        Typeface expandedFont = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Black.ttf");
        binding.collapsingToolbarLayout.setExpandedTitleTypeface(expandedFont);

        int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_2dp), getResources().getDisplayMetrics());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (binding.scrollView.canScrollVertically(SCROLL_DIRECTION_UP)) {
                            binding.appbar.setElevation(elevation);
                        } else {
                            binding.appbar.setElevation(0);
                        }

                    }
                }
            });
        }

        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.N) {
            binding.tvQuestion.setText(Html.fromHtml(help.getTitle()));
            binding.tvAnswer.setText(Html.fromHtml(help.getContent()));
        } else {
            binding.tvQuestion.setText(Html.fromHtml(help.getTitle(), Html.FROM_HTML_MODE_COMPACT));
            binding.tvAnswer.setText(Html.fromHtml(help.getContent(), Html.FROM_HTML_MODE_COMPACT));
        }

        if (help.getIsComplain()) {
            binding.layoutComplain.setVisibility(View.VISIBLE);
        } else {
            binding.layoutComplain.setVisibility(View.GONE);
        }

        binding.tvComplain.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), RequestHelpActivity.class);
            intent.putExtra("EXTRA_HELP", help);
            startActivity(intent);
        });
    }
}
