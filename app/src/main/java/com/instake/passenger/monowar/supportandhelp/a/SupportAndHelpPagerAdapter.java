package com.instake.passenger.monowar.supportandhelp.a;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.instake.passenger.R;
import com.instake.passenger.monowar.supportandhelp.a.fragment.faqs.FaqsFragment;
import com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp.TripsHelpFragment;

/**
 * Created by kai_h on 21-Jul-17.
 */

public class SupportAndHelpPagerAdapter extends FragmentPagerAdapter {

    Context context;

    public SupportAndHelpPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TripsHelpFragment tripsHelpFragment = new TripsHelpFragment();
                return tripsHelpFragment;
            case 1:
                FaqsFragment faqsFragment = new FaqsFragment();
                return faqsFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return context.getString(R.string.title_fragment_trips_help);
            case 1:
                return context.getString(R.string.title_fragment_faqs);
            default:
                return null;
        }
    }
}
