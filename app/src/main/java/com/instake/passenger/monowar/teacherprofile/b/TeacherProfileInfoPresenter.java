package com.instake.passenger.monowar.teacherprofile.b;

import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.model.EducationDetails;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class TeacherProfileInfoPresenter implements BasePresenter<TeacherProfileInfoView> {

    private final static String TAG = "InviteFriends";

    private WeakReference<TeacherProfileInfoView> view;

    public TeacherProfileInfoPresenter(TeacherProfileInfoView view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onNextClick() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().proceedToUploadIdCardActivity();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (view.get().getInstitutionName() == null) {
            view.get().showInstitutionNameError();
            isOkay = false;
        }
        if (view.get().getDepartmentName() == null) {
            view.get().showDepartmentNameError();
            isOkay = false;
        }
        return isOkay;
    }
}
