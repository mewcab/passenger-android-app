package com.instake.passenger.monowar.supportandhelp.b;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 23-Aug-17.
 */

@Singleton
@Subcomponent(modules = RequestHelpModule.class)
public interface RequestHelpComponent {
    void inject (RequestHelpActivity activity);
}
