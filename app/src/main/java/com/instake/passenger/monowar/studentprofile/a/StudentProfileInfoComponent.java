package com.instake.passenger.monowar.studentprofile.a;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = StudentProfileInfoModule.class)
public interface StudentProfileInfoComponent {
    void inject(StudentProfileInfoActivity activity);
}
