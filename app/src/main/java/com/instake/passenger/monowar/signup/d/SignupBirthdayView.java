package com.instake.passenger.monowar.signup.d;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public interface SignupBirthdayView extends BaseView {
    String getGender();
    String getBirthDate();
    void showGenderError();
    void showBirthDateError();
    void hideAllErrors();
    void proceedToPasswordActivity();
    void skipToPasswordActivity();
}
