package com.instake.passenger.monowar.studentprofile.b;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = StudentProfileInfoUploadModule.class)
public interface StudentProfileInfoUploadComponent {
    void inject(StudentProfileInfoUploadActivity activity);
}
