package com.instake.passenger.monowar.teacherprofile.b;

import com.instake.passenger.api.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class TeacherProfileInfoModule {

    private TeacherProfileInfoView view;

    public TeacherProfileInfoModule(TeacherProfileInfoView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public TeacherProfileInfoPresenter provideTeacherProfileInfoPresenter() {
        return new TeacherProfileInfoPresenter(view);
    }
}
