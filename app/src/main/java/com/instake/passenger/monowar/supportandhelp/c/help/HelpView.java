package com.instake.passenger.monowar.supportandhelp.c.help;

import com.instake.passenger.common.base.BaseView;

import java.util.ArrayList;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public interface HelpView extends BaseView {
    void initRecyclerView(ArrayList<String> arrayList);
}
