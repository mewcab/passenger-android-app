package com.instake.passenger.monowar.signup.c;

import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class SignupEmailModule {

    private SignupEmailView view;

    public SignupEmailModule(SignupEmailView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SignupEmailPresenter provideSignupEmailPresenter(Validator validator) {
        return new SignupEmailPresenter(view, validator);
    }
}
