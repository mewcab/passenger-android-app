package com.instake.passenger.monowar.invitefriends;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = InviteFriendsModule.class)
public interface InviteFriendsComponent {
    void inject(InviteFriendsActivity activity);
}
