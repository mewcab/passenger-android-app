package com.instake.passenger.monowar.profile.switchprofile;


import com.instake.passenger.R;

/**
 * Created by Adib on 31-Mar-17.
 */

public enum SwitchProfileObject {

    PROFILE_OBJECT_1(0, R.layout.content_change_profile_business),
    PROFILE_OBJECT_2(1, R.layout.content_change_profile_student),
    PROFILE_OBJECT_3(1, R.layout.content_change_profile_teacher),
    PROFILE_OBJECT_4(2, R.layout.content_change_profile_ambassador);


    private int mTitleResId;
    private int mLayoutResId;

    SwitchProfileObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
