package com.instake.passenger.monowar.supportandhelp.b;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.f2prateek.dart.InjectExtra;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityFaqsListBinding;
import com.instake.passenger.model.suppotnhelp.Help;
import com.instake.passenger.monowar.EqualSpacingItemDecoration;
import com.instake.passenger.monowar.RecyclerTouchListener;

import java.util.List;

/**
 * Created by Mostafa Monowar on 20-Sep-17.
 */

public class FaqsListActivity extends SiriusActivity {

    private static final int SCROLL_DIRECTION_UP = -1;

    ActivityFaqsListBinding binding;

    private FaqsListAdapter adapter;

    @InjectExtra("EXTRA_LIST")
    List<Help> helpList;
    @InjectExtra("EXTRA_TITLE")
    String title;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_faqs_list;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityFaqsListBinding) getDataBinding();

        initRecycleView();
    }

    private void initRecycleView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        binding.collapsingToolbarLayout.setTitle(title);
        Typeface expandedFont = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Black.ttf");
        binding.collapsingToolbarLayout.setExpandedTitleTypeface(expandedFont);

        int defaultGap = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_1dp), getResources().getDisplayMetrics());
        binding.recyclerView.addItemDecoration(new EqualSpacingItemDecoration(defaultGap, EqualSpacingItemDecoration.VERTICAL));
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());

        adapter = new FaqsListAdapter(getContext(), helpList);
        binding.recyclerView.setAdapter(adapter);

        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), binding.recyclerView, new RecyclerTouchListener.OnRecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                view.postDelayed(() -> {
                    Intent intent = new Intent(getActivity(), FaqsDetailsActivity.class);
                    intent.putExtra("EXTRA_HELP", helpList.get(position));
                    startActivity(intent);
                }, 200);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_2dp), getResources().getDisplayMetrics());
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (binding.recyclerView.canScrollVertically(SCROLL_DIRECTION_UP)) {
                        binding.appbar.setElevation(elevation);
                    } else {
                        binding.appbar.setElevation(0);
                    }
                }
            }
        });
    }
}
