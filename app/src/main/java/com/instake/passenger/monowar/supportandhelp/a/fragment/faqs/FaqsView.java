package com.instake.passenger.monowar.supportandhelp.a.fragment.faqs;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.suppotnhelp.HelpNSupport;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public interface FaqsView extends BaseView {
    void loadDataIntoAdapter(List<HelpNSupport> help_n_support_list);
}
