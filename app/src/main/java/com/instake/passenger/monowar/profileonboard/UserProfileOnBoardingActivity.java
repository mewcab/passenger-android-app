package com.instake.passenger.monowar.profileonboard;

import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioButton;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityOnboardingProfileBinding;
import com.instake.passenger.monowar.ambassadorprofile.a.ProvideAmbassadorEmailActivity;
import com.instake.passenger.monowar.businessprofile.a.ProvideBusinessEmailActivity;
import com.instake.passenger.monowar.studentprofile.a.StudentProfileInfoActivity;
import com.instake.passenger.monowar.teacherprofile.a.ProvideTeacherEmailActivity;

import javax.inject.Inject;

public class UserProfileOnBoardingActivity extends SiriusPresenterActivity<UserProfileOnBoardingView, UserProfileOnBoardingPresenter> implements UserProfileOnBoardingView {

    public static int BUSINESS_PROFILE = 1;
    public static int STUDENT_PROFILE = 2;
    public static int TEACHER_PROFILE = 3;
    public static int AMBASSADOR_PROFILE = 4;

    @Inject
    UserProfileOnBoardingPresenter presenter;

    private ActivityOnboardingProfileBinding binding;
    private UserProfileViewPagerAdapter userProfileViewPagerAdapter;
    private Class<?> mClass;

    private int current_position = 0;
    boolean isFirst = true;
    private int profileType;

    @NonNull
    private final ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {

        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            switch (position) {
                case 0:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(false);
                    break;
                case 1:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(false);
                    break;
                case 2:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(true);
                    break;
            }

            current_position = position;
        }
    };

    private Handler handler = new Handler();
    Runnable runnable = this::afficher;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_onboarding_profile;
    }

    @Override
    protected UserProfileOnBoardingPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new UserProfileOnBoardingModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityOnboardingProfileBinding) getDataBinding();

        profileType = getIntent().getIntExtra("EXTRA_PROFILE_TYPE", 0);

        setupViewPager();

        binding.viewPagerOnBoarding.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction()) {

                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        // calls when touch release on ViewPager
                        startPagerRotation();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        // calls when ViewPager touch
                        handler.removeCallbacks(runnable);
                        break;
                }
                return false;
            }
        });

        binding.buttonGetStarted.setOnClickListener(v -> {
            if (profileType == BUSINESS_PROFILE) {
                mClass = ProvideBusinessEmailActivity.class;
            } else if (profileType == STUDENT_PROFILE) {
                mClass = StudentProfileInfoActivity.class;
            } else if (profileType == TEACHER_PROFILE) {
                mClass = ProvideTeacherEmailActivity.class;
            } else if (profileType == AMBASSADOR_PROFILE){
                mClass = ProvideAmbassadorEmailActivity.class;
            }
            startActivity(mClass, true);
        });
    }

    private void setupViewPager() {
        userProfileViewPagerAdapter = new UserProfileViewPagerAdapter(this, profileType);
        binding.viewPagerOnBoarding.setAdapter(userProfileViewPagerAdapter);
        binding.viewPagerOnBoarding.addOnPageChangeListener(mOnPageChangeListener);
    }

    @Override
    public void startPagerRotation() {
        runnable.run();
    }

    public void afficher() {
        if (isFirst) {
            handler.postDelayed(runnable, 5000);
            isFirst = false;
            return;
        }
        if (current_position == 5) {
            current_position = 0;
        } else {
            current_position++;
        }

        if (current_position == 0) {
            binding.viewPagerOnBoarding.setCurrentItem(current_position, false);
        } else {
            binding.viewPagerOnBoarding.setCurrentItem(current_position, true);
        }
        handler.postDelayed(runnable, 5000);
    }

}
