package com.instake.passenger.monowar.mobilenumbervalidation.b;

import android.app.Activity;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.f2prateek.dart.InjectExtra;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySignupOtpBinding;
import com.instake.passenger.feature.login.LoginActivity;
import com.instake.passenger.feature.splash.SplashActivity;
import com.instake.passenger.monowar.VerificationStatus;
import com.instake.passenger.monowar.signup.a.SignupCompleteProfileActivity;
import com.instake.passenger.receiver.VerificationCodeReceiver;

import java.util.Arrays;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class SignupOtpActivity extends SiriusPresenterActivity<SignupOtpView, SignupOtpPresenter> implements SignupOtpView {

    private static final String TAG = "SignupOtpActivity";

    private CallbackManager mCallbackManager;

    private ActivitySignupOtpBinding binding;

    @Inject
    SignupOtpPresenter presenter;

    private VerificationCodeReceiver verificationCodeReceiver;

    @InjectExtra("EXTRA_PHONE_NUMBER")
    String requestingPhoneNumber;

    public static void clearAllAndOpen(Activity activity, String phoneNumber) {
        Intent intent = new Intent(activity, SignupOtpActivity.class);
        intent.putExtra("EXTRA_PHONE_NUMBER", phoneNumber);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        activity.startActivity(intent);
    }

    @Override
    protected SignupOtpPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SignupOtpModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_signup_otp;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySignupOtpBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.tvPhoneNumber.setText(requestingPhoneNumber.substring(3));

        SpannableString spannableString = new SpannableString(getString(R.string.login_options));
        spannableString.setSpan(passwordLogin, 11, 19, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(facebookLogin, 27, 35, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.text_color_pink)), 11, 19, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
        spannableString.setSpan(new ForegroundColorSpan(ContextCompat.getColor(this, R.color.text_color_pink)), 27, 35, SpannableString.SPAN_EXCLUSIVE_EXCLUSIVE);
        binding.tvLogin.setText(spannableString);
        binding.tvLogin.setMovementMethod(LinkMovementMethod.getInstance());

        initFacebookComponent();

        verificationCodeReceiver = new VerificationCodeReceiver() {
            @Override
            public void onVerificationCode(String code) {
                binding.txtPinEntry.setText(code);
            }
        };
        verificationCodeReceiver.addPhoneNumberFilter("+8801819249868");
        verificationCodeReceiver.setPattern(VerificationCodeReceiver.CODE_PATTERN_4_DIGIT);

        binding.buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onClickConfirm();
            }
        });

        binding.tvOtpResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onClickResend();
            }
        });

        binding.txtPinEntry.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String verificationCode = s.toString();
                if (verificationCode.length() == 4) {
                    binding.buttonConfirm.performClick();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public String getVerificationCode() {
        return binding.txtPinEntry.getText().toString();
    }

    @Override
    public void showOtpValidationError() {
        binding.tvMessage.setVisibility(View.VISIBLE);
        binding.tvMessage.setText(R.string.err_invalid_code_entered);
        binding.tvMessage.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
    }

    @Override
    public void hideOtpValidationError() {
        binding.tvMessage.setVisibility(View.INVISIBLE);
        binding.tvMessage.setText(R.string.otp_message);
        binding.tvMessage.setTextColor(ContextCompat.getColor(getContext(), R.color.warm_gray));
    }

    @Override
    public String getRequestingPhoneNumber() {
        return requestingPhoneNumber;
    }

    @Override
    public void showVerificationSuccessMessage() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SignupOtpActivity.this, getString(R.string.phone_number_verification_success), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void showVerificationFailureMessage() {
        binding.tvMessage.setVisibility(View.VISIBLE);
        binding.tvMessage.setText(R.string.phone_number_verification_failed);
        binding.tvMessage.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
    }

    @Override
    public void showVerificationProcessFailureMessage() {
        binding.tvMessage.setVisibility(View.VISIBLE);
        binding.tvMessage.setText(R.string.phone_number_verification_process_failed);
        binding.tvMessage.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
    }

    @Override
    public void showVerificationCodeSendingSuccessMessage() {

    }

    @Override
    public void showVerificationCodeSendingFailureMessage() {
        binding.tvMessage.setVisibility(View.VISIBLE);
        binding.tvMessage.setText(R.string.err_code_is_not_sent);
        binding.tvMessage.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
    }

    @Override
    public void proceedToSignupActivity(VerificationStatus verificationStatus) {
        Bundle bundle = new Bundle();
        bundle.putString("BUNDLE_PHONE_NUMBER", requestingPhoneNumber);
        bundle.putString("BUNDLE_VERIFICATION_CODE", getVerificationCode());
        SignupCompleteProfileActivity.clearAllScreenAndOpen(this, bundle);
    }

    @Override
    public void proceedToSplashActivity() {
        SplashActivity.ClearAllScreenAndOpen(this);
    }

    @Override
    public void proceedToLoginActivity() {
        Bundle bundle = new Bundle();
        bundle.putString("BUNDLE_PHONE_NUMBER", requestingPhoneNumber);
        LoginActivity.open(this, bundle);
    }

    @Override
    public void hideKeyboard() {
        hideKeyboard(binding.txtPinEntry);
    }

    @Override
    public void updateWaitTime(String time) {
        binding.tvOtpResend.setText(String.format(getString(R.string.otp_waiting), time));
    }

    @Override
    public void updateOTPResendMessage(boolean showResend) {
        if (showResend) {
            binding.tvOtpResendMessage.setText(getString(R.string.otp_resend_message));
            binding.tvOtpResend.setText(getString(R.string.otp_resend));
        } else {
            binding.tvOtpResendMessage.setText(getString(R.string.otp_waiting_message));
            binding.tvOtpResend.setText(getString(R.string.otp_waiting_placeholder));
        }
    }

    @Override
    public void enableResend(boolean isEnabled) {
        binding.tvOtpResend.setClickable(isEnabled);
    }

    @Override
    public void registerVerificationCodeReceiver() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(verificationCodeReceiver, intentFilter);
    }

    @Override
    public void unregisterVerificationCodeReceiver() {
        if (verificationCodeReceiver != null) {
            unregisterReceiver(verificationCodeReceiver);
            verificationCodeReceiver = null;
        }
    }

    public void initFacebookComponent() {
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("****", "***Twice****");

                        presenter.onFbLoginSuccess(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e(TAG, exception.toString());
                        LoginManager.getInstance().logOut();
                    }
                });
    }

    ClickableSpan passwordLogin = new ClickableSpan() {
        @Override
        public void onClick(View textView) {
            proceedToLoginActivity();
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };

    ClickableSpan facebookLogin = new ClickableSpan() {
        @Override
        public void onClick(View textView) {
            LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email"));
        }

        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setUnderlineText(false);
        }
    };
}
