package com.instake.passenger.monowar.supportandhelp.b;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 23-Aug-17.
 */

@Module
public class RequestHelpModule {

    private RequestHelpView view;

    public RequestHelpModule(RequestHelpView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public RequestHelpPresenter providesRequestHelpPresenter(Session session, ApiService apiService, Validator validator) {
        return new RequestHelpPresenter(view, session, apiService, validator);
    }
}
