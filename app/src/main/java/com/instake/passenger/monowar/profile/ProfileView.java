package com.instake.passenger.monowar.profile;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;

/**
 * Created by Mostafa Monowar on 28-Aug-17.
 */

public interface ProfileView extends BaseView {
    void setUserInfo(User user);

    void initFacebookComponent();

    void setProfileVerificationInfo(UserProfile userProfile);

    void setFacebookConnectivity(String facebookId);

    void proceedToGetStartedActivityAndCloseAll();
}
