package com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Module
public class TripsHelpModule {
    private TripsHelpView view;

    public TripsHelpModule (TripsHelpView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public TripsHelpPresenter provideTripsHelpPresenter(ApiService apiService, Session session) {
        return new TripsHelpPresenter(view, apiService, session);
    }
}
