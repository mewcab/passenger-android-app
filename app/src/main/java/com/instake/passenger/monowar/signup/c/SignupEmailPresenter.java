package com.instake.passenger.monowar.signup.c;

import com.instake.passenger.R;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.utility.validation.Validator;

import java.lang.ref.WeakReference;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class SignupEmailPresenter implements BasePresenter<SignupEmailView> {

    private WeakReference<SignupEmailView> view;
    private Validator validator;

    public SignupEmailPresenter(SignupEmailView view, Validator validator) {
        this.view = new WeakReference<>(view);
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onClickNext() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().proceedToBirthDateActivity();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (!validator.isNotEmpty(view.get().getEmail())) {
            view.get().showEmailError(view.get().getContext().getString(R.string.error_email_empty));
            isOkay = false;
        }
        if (validator.isNotEmpty(view.get().getEmail()) && !validator.isEmailValid(view.get().getEmail())) {
            view.get().showEmailError(view.get().getContext().getString(R.string.error_invalid_email));
            isOkay = false;
        }
        if (validator.isNotEmpty(view.get().getReferralCode()) && !validator.isReferralCodeValid(view.get().getReferralCode())) {
            view.get().showReferralCodeError(view.get().getContext().getString(R.string.error_referral_code_invalid));
            isOkay = false;
        }
        return isOkay;
    }
}
