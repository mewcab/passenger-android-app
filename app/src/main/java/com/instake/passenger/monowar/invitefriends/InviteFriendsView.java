package com.instake.passenger.monowar.invitefriends;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface InviteFriendsView extends BaseView {
    void setReferralCode(String referralCode);

    String getReferralCode();
}
