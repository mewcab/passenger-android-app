package com.instake.passenger.monowar.supportandhelp.b;

import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.monowar.supportandhelp.a.SupportAndHelpView;

import java.lang.ref.WeakReference;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public class FaqsDetailsPresenter implements BasePresenter<FaqsDetailsView> {

    private WeakReference<FaqsDetailsView> view;

    public FaqsDetailsPresenter(FaqsDetailsView view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}
