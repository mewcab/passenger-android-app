package com.instake.passenger.monowar.signup.e;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = SignupPasswordModule.class)
public interface SignupPasswordComponent {
    void inject(SignupPasswordActivity activity);
}
