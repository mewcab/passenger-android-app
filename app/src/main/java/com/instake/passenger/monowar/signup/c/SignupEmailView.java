package com.instake.passenger.monowar.signup.c;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public interface SignupEmailView extends BaseView {
    String getEmail();
    String getReferralCode();
    void showEmailError(String error);
    void showReferralCodeError(String error);
    void hideAllErrors();
    void proceedToBirthDateActivity();
}
