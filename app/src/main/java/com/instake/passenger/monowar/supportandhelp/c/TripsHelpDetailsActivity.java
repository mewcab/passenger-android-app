package com.instake.passenger.monowar.supportandhelp.c;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;

import com.bumptech.glide.Glide;
import com.f2prateek.dart.InjectExtra;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityTripsHelpDetailsBinding;
import com.instake.passenger.model.historydata.Trip;
import com.instake.passenger.monowar.supportandhelp.c.help.HelpFragment;
import com.instake.passenger.monowar.supportandhelp.c.receipt.ReceiptFragment;

import org.parceler.Parcels;

import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

public class TripsHelpDetailsActivity extends SiriusPresenterActivity<TripsHelpDetailsView, TripsHelpDetailsPresenter> implements TripsHelpDetailsView {

    public ActivityTripsHelpDetailsBinding binding;

    @Inject
    TripsHelpDetailsPresenter presenter;

    @InjectExtra("EXTRA_RIDE_HISTORY")
    Trip trip;

    @Override
    protected TripsHelpDetailsPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new TripsHelpDetailsModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_trips_help_details;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityTripsHelpDetailsBinding) getDataBinding();

        binding.collapsingToolbarLayout.setTitle(getString(R.string.title_fragment_trips_help));
        binding.collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.tvTimeStamp.setText(trip.getStartTime());
        binding.tvPickUpLocation.setText(trip.getPickupName());
        binding.tvDestination.setText(trip.getDropoffName());
        binding.tvFare.setText(String.valueOf(trip.getTotalFear()));
        binding.tvPaymentType.setText(trip.getPaymentMethod());
        binding.tvDriverName.setText(trip.getDriverId().getFirstName() + " " + trip.getDriverId().getFullName());
        binding.tvRatings.setText(String.format(Locale.getDefault(), "%.01f", trip.getDriverId().getTotalRating() / trip.getDriverId().getRatingCount()));
        Glide.with(getContext().getApplicationContext())
                .load(App.BASE_URL + trip.getDriverId().getProfileImages().get150X150())
                .dontAnimate()
                .error(R.drawable.backgroud_select_image)
                .placeholder(R.drawable.backgroud_select_image)
                .into(binding.ivDriverImage);
    }

    @Override
    public void initFragmentViewPager() {
        TripsHelpDetailsPagerAdapter tripsHelpDetailsPagerAdapter = new TripsHelpDetailsPagerAdapter(getContext(), getSupportFragmentManager());

        Bundle bundle = new Bundle();
        ReceiptFragment receiptFragment = new ReceiptFragment();
        bundle.putParcelable("ARGUMENT_TRIP", Parcels.wrap(trip));
        receiptFragment.setArguments(bundle);

        tripsHelpDetailsPagerAdapter.addFrag(new HelpFragment(), getString(R.string.title_fragment_help));
        tripsHelpDetailsPagerAdapter.addFrag(receiptFragment, getString(R.string.title_fragment_receipt));

        binding.viewPager.setAdapter(tripsHelpDetailsPagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);

        binding.viewPager.setCurrentItem(1);
    }
}
