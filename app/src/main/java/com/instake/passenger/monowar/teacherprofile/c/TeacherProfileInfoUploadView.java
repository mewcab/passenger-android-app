package com.instake.passenger.monowar.teacherprofile.c;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface TeacherProfileInfoUploadView extends BaseView {
    String getEmail();
    String getInstitutionName();
    String getDepartmentName();
    String getIdImageUri();
    void showFullScreenImage();
    void hideFullScreenImage();
    void showImageError();
    void showUploadInfoFailed();
    void showUploadInfoSuccess();
    void backToProfileActivity();
}
