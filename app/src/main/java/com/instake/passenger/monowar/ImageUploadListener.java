package com.instake.passenger.monowar;

/**
 * Created by Tushar on 8/2/17.
 */

public interface ImageUploadListener {
    void onProgress(int progress);

    void onSuccess();

    void onFailure();
}
