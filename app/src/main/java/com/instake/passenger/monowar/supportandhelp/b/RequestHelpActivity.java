package com.instake.passenger.monowar.supportandhelp.b;

import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;

import com.f2prateek.dart.InjectExtra;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityRequestHelpBinding;
import com.instake.passenger.model.suppotnhelp.Help;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 23-Aug-17.
 */

public class RequestHelpActivity extends SiriusPresenterActivity<RequestHelpView, RequestHelpPresenter> implements RequestHelpView {

    ActivityRequestHelpBinding binding;

    @Inject
    RequestHelpPresenter presenter;

    @InjectExtra("EXTRA_HELP")
    Help help;

    @Override
    protected RequestHelpPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new RequestHelpModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_request_help;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityRequestHelpBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.tvTitle.setText(help.getTitle());

        binding.etPhoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    binding.tvPhoneNumberPlaceHolder.setTextColor(ContextCompat.getColor(RequestHelpActivity.this, R.color.hot_pink));
                    binding.viewBackgroundTint.setBackgroundColor(ContextCompat.getColor(RequestHelpActivity.this, R.color.hot_pink));
                } else {
                    binding.tvPhoneNumberPlaceHolder.setTextColor(ContextCompat.getColor(RequestHelpActivity.this, R.color.warm_gray));
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                        binding.viewBackgroundTint.setBackground(ContextCompat.getDrawable(RequestHelpActivity.this, R.drawable.button_background_grey));
                    } else {
                        binding.viewBackgroundTint.setBackgroundDrawable(ContextCompat.getDrawable(RequestHelpActivity.this, R.drawable.button_background_grey));
                    }
                }
            }
        });

        binding.etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                presenter.checkPhoneNumberValidation(getPhoneNumber());
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        binding.buttonSubmit.setOnClickListener(v -> presenter.onClickSubmit());
    }

    @Override
    public void setPhoneNumber(String phoneNumber) {
        binding.etPhoneNumber.setText(phoneNumber.substring(3));
    }

    @Override
    public String getPhoneNumber() {
        return binding.tvCountryCode.getText().toString() + binding.etPhoneNumber.getText().toString();
    }

    @Override
    public String getComplain() {
        return binding.etComplain.getText().toString();
    }

    @Override
    public String getHelpId() {
        return help.getId();
    }

    @Override
    public void showPhoneNumberError() {
        binding.tvMessage.setVisibility(View.VISIBLE);
    }

    @Override
    public void showComplainError() {
        binding.tilComplain.setError(getString(R.string.error_complain));
    }

    @Override
    public void hideAllError() {
        binding.tvMessage.setVisibility(View.GONE);
        binding.tilComplain.setErrorEnabled(false);
    }
}
