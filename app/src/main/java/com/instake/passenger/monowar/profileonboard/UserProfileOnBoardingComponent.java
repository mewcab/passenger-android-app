package com.instake.passenger.monowar.profileonboard;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = UserProfileOnBoardingModule.class)
public interface UserProfileOnBoardingComponent {
    void inject(UserProfileOnBoardingActivity activity);
}
