package com.instake.passenger.monowar.supportandhelp.b;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 23-Aug-17.
 */

public class RequestHelpPresenter implements BasePresenter<RequestHelpView> {

    private static final String TAG = RequestHelpPresenter.class.getSimpleName();

    private WeakReference<RequestHelpView> view;
    private Session session;
    private ApiService apiService;
    private Validator validator;

    public RequestHelpPresenter(RequestHelpView view, Session session, ApiService apiService, Validator validator) {
        this.view = new WeakReference<RequestHelpView>(view);
        this.session = session;
        this.apiService = apiService;
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void checkPhoneNumberValidation(String phoneNumber) {
        if (!validator.isPhoneNumberValid(phoneNumber)) {
            view.get().showPhoneNumberError();
        } else {
            view.get().hideAllError();
        }
    }

    public void onClickSubmit() {
        view.get().hideAllError();
        if (isInputOkay()) {
            postComplain();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (!validator.isPhoneNumberValid(view.get().getPhoneNumber())) {
            view.get().showPhoneNumberError();
            isOkay = false;
        }
        if (!validator.isNotEmpty(view.get().getComplain())) {
            view.get().showComplainError();
            isOkay = false;
        }
        return isOkay;
    }

    private void postComplain() {
        apiService.postComplain(session.getToken(), view.get().getHelpId(), getComplainBody())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(Observable::error)
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
//                        onUploadFailed(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody userProfile) {
//                        onUploadSuccess(userProfile);
                    }
                });
    }

    private RequestBody getComplainBody() {
        return RequestBody.create(MediaType.parse("application/json"),
                JsonBuilder.createComplainBody(view.get().getPhoneNumber(), view.get().getComplain()));
    }
}
