package com.instake.passenger.monowar.signup.a;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySignupCompleteProfileBinding;
import com.instake.passenger.feature.home.HomeActivity;
import com.instake.passenger.monowar.signup.b.SignupProvideInformationActivity;
import com.instake.passenger.monowar.signup.e.SignupPasswordActivity;

import java.util.Arrays;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class SignupCompleteProfileActivity extends SiriusPresenterActivity<SignupCompleteProfileView,
        SignupCompleteProfilePresenter> implements SignupCompleteProfileView {

    private String TAG = "SignupActivity";

    ActivitySignupCompleteProfileBinding binding;

    @Inject
    SignupCompleteProfilePresenter presenter;

    private Bundle bundle;

    private CallbackManager mCallbackManager;

    public static void clearAllScreenAndOpen(Activity activity, Bundle bundle) {
        Intent intent = new Intent(activity, SignupCompleteProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected SignupCompleteProfilePresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SignupCompleteProfileModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_signup_complete_profile;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySignupCompleteProfileBinding) getDataBinding();

        bundle = getIntent().getExtras();

        binding.facebookLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(SignupCompleteProfileActivity.this,
                        Arrays.asList("public_profile", "email"));
            }
        });

        binding.buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onClickConfirm();
            }
        });
    }

    @Override
    public void initFacebookComponent() {
        AppEventsLogger.activateApp(app);
        mCallbackManager = CallbackManager.Factory.create();

//        binding.facebookLogin.setHeight((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_50dp), getResources().getDisplayMetrics()));
//        binding.facebookLogin.setBackgroundResource(R.drawable.button_background_facebook);
//        binding.facebookLogin.setCompoundDrawablesWithIntrinsicBounds(null, null, null, null);

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("****" , "***Twice****");

                        presenter.onFbLoginSuccess(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e(TAG, exception.toString());
                        presenter.deleteAccessToken();
                    }
                });
    }

    @Override
    public String getPhoneNumber() {
        return bundle.getString("BUNDLE_PHONE_NUMBER");
    }

    @Override
    public String getVerificationCode() {
        return bundle.getString("BUNDLE_VERIFICATION_CODE");
    }

    @Override
    public void signupFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SignupCompleteProfileActivity.this, getString(R.string.phone_number_verification_process_failed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void proceedToProvideInformationActivity(String token) {
        bundle.putString("BUNDLE_TOKEN", token);
        SignupProvideInformationActivity.open(this, bundle);
    }

    @Override
    public void proceedPasswordConfirmActivity(Bundle bundle) {
        SignupPasswordActivity.openFromFacebookSignUP(this, bundle);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
}
