package com.instake.passenger.monowar.signup.d;

import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class SignupBirthdayModule {

    private SignupBirthdayView view;

    public SignupBirthdayModule(SignupBirthdayView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SignupBirthdayPresenter provideSignupBirthdayPresenter(Validator validator) {
        return new SignupBirthdayPresenter(view, validator);
    }
}
