package com.instake.passenger.monowar.signup.a;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

@Module
public class SignupCompleteProfileModule {

    private SignupCompleteProfileView view;

    public SignupCompleteProfileModule(SignupCompleteProfileView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SignupCompleteProfilePresenter provideSignupCompleteProfilePresenter(ApiService apiService, Session session, Validator validator) {
        return new SignupCompleteProfilePresenter(view, apiService, session, validator);
    }
}
