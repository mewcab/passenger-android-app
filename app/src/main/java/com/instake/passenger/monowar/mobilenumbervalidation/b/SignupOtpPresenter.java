package com.instake.passenger.monowar.mobilenumbervalidation.b;

import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.monowar.VerificationStatus;
import com.instake.passenger.utility.validation.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

public class SignupOtpPresenter implements BasePresenter<SignupOtpView> {

    private static final String TAG = "SignupOtpPresenter";

    private WeakReference<SignupOtpView> view;
    private ApiService apiService;
    private Validator validator;
    private Session session;

    private CountDownTimer countDownTimer;

    public SignupOtpPresenter(SignupOtpView view, ApiService apiService, Validator validator, Session session) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.validator = validator;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {
        startTimer();
        view.get().enableResend(false);
        view.get().updateOTPResendMessage(false);
    }

    @Override
    public void onResumePresenter() {
        view.get().registerVerificationCodeReceiver();
    }

    @Override
    public void onPausePresenter() {
        view.get().unregisterVerificationCodeReceiver();
    }

    @Override
    public void onDestroyPresenter() {
        stopTimer();
    }

    private void startTimer() {
        countDownTimer = new CountDownTimer(31 * 1000, 1000) {

            @Override
            public void onTick(long millisUntilFinished) {
                view.get().updateWaitTime(new SimpleDateFormat("ss", Locale.ENGLISH).format(new Date(millisUntilFinished)));
            }

            @Override
            public void onFinish() {
                view.get().enableResend(true);
                view.get().updateOTPResendMessage(true);
            }
        }.start();
    }

    private void stopTimer() {
        if (countDownTimer != null) {
            countDownTimer.cancel();
            countDownTimer = null;
        }
    }

    public void onClickConfirm() {
        if (!validator.isNotEmpty(view.get().getVerificationCode())
                && !validator.isOtpValid(view.get().getVerificationCode())) {
            view.get().showOtpValidationError();
            return;
        }

        view.get().unregisterVerificationCodeReceiver();
        view.get().hideOtpValidationError();
        view.get().hideKeyboard();
        view.get().showLoader();
        verifyVerificationCode(view.get().getRequestingPhoneNumber(), view.get().getVerificationCode());
    }

    private void verifyVerificationCode(String requestingPhoneNumber, String userEnteredPhoneCode) {
        Log.e("LOG", "Phone no " + requestingPhoneNumber + " PIN " + userEnteredPhoneCode);
        apiService.verifyVerificationCode(getVerificationCodeRequestBody(requestingPhoneNumber, userEnteredPhoneCode))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends VerificationStatus>>() {
                    @Override
                    public Observable<? extends VerificationStatus> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<VerificationStatus>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onVerificationResultFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(VerificationStatus verificationStatus) {
                        onVerificationResultSuccess(verificationStatus);
                    }
                });
    }

    private RequestBody getVerificationCodeRequestBody(String requestingPhoneNumber, String verificationCode) {
        return RequestBody.create(MediaType.parse("application/json"),
                JsonBuilder.getVerificationCodeRequestBody(requestingPhoneNumber, verificationCode));
    }

    private void onVerificationResultSuccess(VerificationStatus verificationStatus) {
        if (view != null) {
            if (verificationStatus.isVerified()) {
                view.get().showVerificationSuccessMessage();
                if (verificationStatus.isSignedUpCompleted()) {
                    loginUser(view.get().getRequestingPhoneNumber(), view.get().getVerificationCode());
                } else {
                    view.get().hideLoader();
                    view.get().proceedToSignupActivity(verificationStatus);
                }
            } else {
                view.get().hideLoader();
                view.get().showVerificationFailureMessage();
            }
        }
    }

    private void onVerificationResultFailure(String message) {
        if (view != null) {
            view.get().hideLoader();
            view.get().showVerificationProcessFailureMessage();
        }
    }

    private void loginUser(String phoneNumber, String verificationCode) {
        apiService.loginNew(getDefaultLoginRequestBody(phoneNumber, verificationCode))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends User>>() {
                    @Override
                    public Observable<? extends User> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onVerificationResultFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(User user) {
                        session.saveUser(user);
                        if (view != null) {
                            view.get().hideLoader();
                            view.get().proceedToSplashActivity();
                        }
                    }
                });
    }

    private RequestBody getDefaultLoginRequestBody(String mobileNumber, String verificationCode) {
        return RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createLoginBodyWithVerificationCode(mobileNumber, verificationCode));
    }

    public void onClickResend() {
        view.get().hideKeyboard();
        view.get().showLoader();
        sendVerificationCode(view.get().getRequestingPhoneNumber());
    }

    private void sendVerificationCode(String phoneNumber) {
        apiService.sendVerificationCode(getSendSmsRequestBody(phoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ResponseBody>>() {
                    @Override
                    public Observable<? extends ResponseBody> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Log.e("APIService", "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        onVerificationCodeSendingError(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        onVerificationCodeSendingSuccess();
                    }
                });
    }

    private RequestBody getSendSmsRequestBody(String phoneNumber) {
        return RequestBody.create(MediaType.parse("application/json"), JsonBuilder.getSendSmsRequestBody(phoneNumber));
    }

    private void onVerificationCodeSendingSuccess() {
        if (view != null) {
            view.get().hideOtpValidationError();
            view.get().hideLoader();
            view.get().showVerificationCodeSendingSuccessMessage();
            startTimer();
            view.get().enableResend(false);
            view.get().updateOTPResendMessage(false);
        }
    }

    private void onVerificationCodeSendingError(String message) {
        if (view != null) {
            view.get().hideLoader();
            view.get().showVerificationCodeSendingFailureMessage();
            view.get().enableResend(true);
            view.get().updateOTPResendMessage(true);
        }
    }

    public void onFbLoginSuccess(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            JSONObject json = response.getJSONObject();
            try {
                if (json != null) {
                    if (json.has("id")) {
                        view.get().showLoader();
                        loginUserWithFbId(json.getString("id"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginUserWithFbId(String fbId) {
        apiService.loginFacebook(RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createLoginBodyWithFbId(view.get().getRequestingPhoneNumber(), fbId)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends User>>() {
                    @Override
                    public Observable<? extends User> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onVerificationResultFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(User user) {
                        session.saveUser(user);
                        if (view != null) {
                            view.get().hideLoader();
                            view.get().proceedToSplashActivity();
                        }
                    }
                });
    }

}
