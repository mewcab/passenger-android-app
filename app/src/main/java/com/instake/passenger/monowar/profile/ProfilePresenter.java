package com.instake.passenger.monowar.profile;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class ProfilePresenter implements BasePresenter<ProfileView> {

    private final static String TAG = "Profile";

    private WeakReference<ProfileView> view;
    private ApiService apiService;
    private Session session;

    public ProfilePresenter(ProfileView view, ApiService apiService, Session session) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {
        getProfileVerification();
        User user = session.getUser();
        view.get().setUserInfo(user);
        view.get().initFacebookComponent();
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onLogoutClicked() {
        User user = session.getUser();
        user.setToken(null);
        session.saveUser(user);
        view.get().proceedToGetStartedActivityAndCloseAll();
    }

    private void getProfileVerification() {
        apiService.getUserProfileVerification(session.getToken())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(Observable::error)
                .subscribe(new Subscriber<UserProfile>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(UserProfile userProfile) {
                        User user = session.getUser();
                        user.userProfile = userProfile;
                        session.saveUser(user);
                        view.get().setProfileVerificationInfo(userProfile);
                    }
                });
    }

    public void onFbLoginSuccess(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            JSONObject json = response.getJSONObject();
            try {
                if (json != null) {
                    if (json.has("id")) {
                        Log.e(TAG, "Facebook ID: " + json.getString("id"));
                        view.get().showLoader();
                        loginUserWithFbId(json.getString("id"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginUserWithFbId(String fbId) {
        apiService.loginFacebook(RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createLoginBodyWithFbId(session.getUser().getPhoneNumber(), fbId)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends User>>() {
                    @Override
                    public Observable<? extends User> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        view.get().hideLoader();
                    }

                    @Override
                    public void onNext(User user) {
                        session.saveUser(user);
                        if (view != null) {
                            view.get().hideLoader();
                            view.get().setFacebookConnectivity(user.getFacebookId());
                        }
                    }
                });
    }
}
