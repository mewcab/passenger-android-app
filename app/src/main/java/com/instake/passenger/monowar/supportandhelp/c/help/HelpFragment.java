package com.instake.passenger.monowar.supportandhelp.c.help;

import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusFragment;
import com.instake.passenger.databinding.FragmentFaqsBinding;
import com.instake.passenger.monowar.EqualSpacingItemDecoration;
import com.instake.passenger.monowar.RecyclerTouchListener;
import com.instake.passenger.monowar.supportandhelp.c.TripsHelpDetailsActivity;

import java.util.ArrayList;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 21-Aug-17.
 */

public class HelpFragment extends SiriusFragment<HelpView, HelpPresenter> implements HelpView {

    private static final int SCROLL_DIRECTION_UP = -1;
    private static final String TAG = "ReceiptFragment";

    private FragmentFaqsBinding binding;

    private HelpAdapter helpAdapter;

    @Inject
    HelpPresenter presenter;

    @Override
    protected HelpPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new HelpModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_faqs;
    }

    @Override
    protected void initFragmentComponents() {
        binding = (FragmentFaqsBinding) getDataBinding();

        presenter.initRecyclerView();

        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), binding.recyclerView, new RecyclerTouchListener.OnRecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(getContext(), "Position " + position, Toast.LENGTH_SHORT).show();
                    }
                }, 200);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_2dp), getResources().getDisplayMetrics());
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (binding.recyclerView.canScrollVertically(SCROLL_DIRECTION_UP)) {
                        ((TripsHelpDetailsActivity) getActivity()).binding.appbar.setElevation(elevation);
                    } else {
                        ((TripsHelpDetailsActivity) getActivity()).binding.appbar.setElevation(0);
                    }
                }
            }
        });

    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void initRecyclerView(ArrayList<String> arrayList) {
        if (helpAdapter == null) {
            helpAdapter = new HelpAdapter(getActivity(), arrayList);
            binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            int defaultGap = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_1dp), getResources().getDisplayMetrics());
            binding.recyclerView.addItemDecoration(new EqualSpacingItemDecoration(defaultGap, EqualSpacingItemDecoration.VERTICAL));
            binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
            binding.recyclerView.setAdapter(helpAdapter);
        }
    }
}
