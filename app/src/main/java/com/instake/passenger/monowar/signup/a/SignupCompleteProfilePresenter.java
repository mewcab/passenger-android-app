package com.instake.passenger.monowar.signup.a;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.monowar.SignupResponseData;
import com.instake.passenger.utility.validation.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

public class SignupCompleteProfilePresenter implements BasePresenter<SignupCompleteProfileView> {

    private WeakReference<SignupCompleteProfileView> view;
    private ApiService apiService;
    private Session session;
    private Validator validator;
    private boolean isFacebookLogin = false;

    private String fbId, fbFirstName, fbLastName, fbEmail, fbProfilePhotoUrl, fbGender, token;

    public SignupCompleteProfilePresenter(SignupCompleteProfileView view, ApiService apiService, Session session, Validator validator) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {
        if (view != null) {
            Log.e("####", "####");
            view.get().initFacebookComponent();
        }
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onFbLoginSuccess(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            JSONObject json = response.getJSONObject();
            try {
                if (json != null) {
                    if (json.has("id")) {
                        fbId = json.getString("id");
                    }
                    if (object.has("first_name")) {
                        fbFirstName = object.getString("first_name");
                    }
                    if (object.has("last_name")) {
                        fbLastName = object.getString("last_name");
                    }
                    if (json.has("email")) {
                        fbEmail = json.getString("email");
                    }
                    if (json.has("picture") && json.getJSONObject("picture").has("data") &&
                            json.getJSONObject("picture").getJSONObject("data").has("url")) {
                        fbProfilePhotoUrl = json.getJSONObject("picture")
                                .getJSONObject("data")
                                .getString("url");
                    }
                    if (json.has("gender")) {
                        fbGender = json.getString("gender");
                    }

                    isFacebookLogin = true;
                    fbSignup();
                    view.get().showLoader();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email, link, birthday, picture.width(200).height(200), gender");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void fbSignup() {
        apiService.signupNew(getSignupRequestBody())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends SignupResponseData>>() {
                    @Override
                    public Observable<? extends SignupResponseData> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<SignupResponseData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onSignupFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(SignupResponseData signupValidation) {
                        onSignupSuccess(signupValidation);
                    }
                });
    }

    private void onSignupSuccess(SignupResponseData signupValidation) {
        if (signupValidation != null) {
            loginUser(signupValidation.getPhoneNumber(), view.get().getVerificationCode());
        }
    }

    private void onSignupFailure(String message) {
        view.get().signupFailed();
        view.get().hideLoader();
    }

    private void loginUser(String phoneNumber, String verificationCode) {
        apiService.loginNew(createRequestBodyForGettingToken(phoneNumber, verificationCode))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends User>>() {
                    @Override
                    public Observable<? extends User> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onSignupFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(User user) {
                        if (view != null) {
                            view.get().hideLoader();
                            token = user.getToken();
                            Log.e(SignupCompleteProfilePresenter.class.getSimpleName(), "Token: " + token);
                        }

                        view.get().hideLoader();

                        if (isFacebookLogin) {
                            Bundle bundle = new Bundle();
                            bundle.putString("BUNDLE_TOKEN", token);
                            bundle.putString("BUNDLE_FB_ID", fbId);
                            bundle.putString("BUNDLE_FULL_NAME", fbFirstName + " " + fbLastName);
                            bundle.putString("BUNDLE_EMAIL", fbEmail);
                            bundle.putString("BUNDLE_GENDER", fbGender);
                            bundle.putString("FB_PROFILE_IMAGE_URL", fbProfilePhotoUrl);
                            bundle.putString("BUNDLE_PHONE_NUMBER", view.get().getPhoneNumber());
                            bundle.putBoolean("BUNDLE_IS_FB_SIGN_UP", true);
                            view.get().proceedPasswordConfirmActivity(bundle);

                        } else {
                            view.get().proceedToProvideInformationActivity(token);
                        }

                    }
                });
    }

    private RequestBody createRequestBodyForGettingToken(String mobileNumber, String verificationCode) {
        return RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createSignUpFakeBody(mobileNumber, verificationCode));
    }

    // TODO: when user will log out from our app user should be logged out from facebook sdk also
    public void logOutFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return;
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, graphResponse ->
                LoginManager.getInstance().logOut()).executeAsync();
    }

    public void deleteAccessToken() {
        new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {

                if (currentAccessToken == null) {
                    //User logged out
                    LoginManager.getInstance().logOut();
                }
            }
        };
    }

    public void onClickConfirm() {
        view.get().showLoader();
        signup();
    }

    private void signup() {
        apiService.signupNew(getSignupRequestBody())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends SignupResponseData>>() {
                    @Override
                    public Observable<? extends SignupResponseData> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<SignupResponseData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onSignupFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(SignupResponseData signupValidation) {
                        onSignupSuccess(signupValidation);
                    }
                });
    }

    private RequestBody getSignupRequestBody() {
        return RequestBody.create(MediaType.parse("application/json"),
                JsonBuilder.createSignUpFakeBody(view.get().getPhoneNumber(), view.get().getVerificationCode()));
    }
}
