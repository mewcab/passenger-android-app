package com.instake.passenger.monowar.supportandhelp.c;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;

import java.lang.ref.WeakReference;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public class TripsHelpDetailsPresenter implements BasePresenter<TripsHelpDetailsView> {

    private WeakReference<TripsHelpDetailsView> view;
    private ApiService apiService;
    private Session session;

    public TripsHelpDetailsPresenter(TripsHelpDetailsView view, ApiService apiService, Session session) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {
        view.get().initFragmentViewPager();
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}

