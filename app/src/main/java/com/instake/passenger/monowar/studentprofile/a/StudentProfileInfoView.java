package com.instake.passenger.monowar.studentprofile.a;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.EducationDetails;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface StudentProfileInfoView extends BaseView {
    void generateEducationDetailsSpinner(EducationDetails educationDetails);

    String getInstitutionName();

    String getClassName();

    void showInstitutionNameError();

    void showClassNameError();

    void hideAllErrors();

    void proceedToUploadIdCardActivity();
}
