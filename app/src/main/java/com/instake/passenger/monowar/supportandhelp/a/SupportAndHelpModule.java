package com.instake.passenger.monowar.supportandhelp.a;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

@Module
public class SupportAndHelpModule {
    private SupportAndHelpView view;

    public SupportAndHelpModule(SupportAndHelpView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SupportAndHelpPresenter provideSupportAndHelpPresenter(ApiService apiService) {
        return new SupportAndHelpPresenter(view, apiService);
    }
}
