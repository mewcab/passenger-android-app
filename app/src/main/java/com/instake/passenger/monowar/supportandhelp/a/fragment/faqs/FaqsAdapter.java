package com.instake.passenger.monowar.supportandhelp.a.fragment.faqs;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.model.suppotnhelp.HelpNSupport;

import java.util.List;

/**
 * Created by annanovasit on 4/20/17.
 */

public class FaqsAdapter extends RecyclerView.Adapter<FaqsAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private List<HelpNSupport> helpNSupportList;

    FaqsAdapter(Context context, List<HelpNSupport> helpNSupportList) {
        layoutInflater = LayoutInflater.from(context);
        this.helpNSupportList = helpNSupportList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.single_item_faqs, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.tvTitle.setText(helpNSupportList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return helpNSupportList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView tvTitle;

        private MyViewHolder(View itemView) {
            super(itemView);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
        }
    }
}
