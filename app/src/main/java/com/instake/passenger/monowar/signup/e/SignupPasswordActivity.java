package com.instake.passenger.monowar.signup.e;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.InputType;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySignupPasswordBinding;
import com.instake.passenger.feature.splash.SplashActivity;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class SignupPasswordActivity extends SiriusPresenterActivity<SignupPasswordView, SignupPasswordPresenter> implements SignupPasswordView {

    private final static String TAG = "SignupActivity";

    ActivitySignupPasswordBinding binding;

    @Inject
    SignupPasswordPresenter presenter;

    private Bundle bundle;

    private boolean isPassword = true;

    public static void open(Activity activity, Bundle bundle) {
        //SignupPasswordActivity.bundle = bundle;
        Intent signupIntent = new Intent(activity, SignupPasswordActivity.class);
        Log.i(TAG, "Bundle: " + bundle.toString());
        signupIntent.putExtras(bundle);
        signupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(signupIntent);
    }

    public static void openFromFacebookSignUP(Activity activity, Bundle bundle) {
        Intent signupIntent = new Intent(activity, SignupPasswordActivity.class);
        Log.i(TAG, "Bundle fb: " + bundle.toString());
        signupIntent.putExtras(bundle);
        signupIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(signupIntent);
    }

    @Override
    protected SignupPasswordPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SignupPasswordModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_signup_password;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySignupPasswordBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        bundle = getIntent().getExtras();

        binding.buttonConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onClickConfirm();
            }
        });

        binding.tvShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tempEditText = binding.tilPassword.getEditText();
                if (isPassword) {
                    isPassword = false;
                    tempEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                    binding.tvShow.setText(R.string.hide);
                } else {
                    isPassword = true;
                    tempEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    binding.tvShow.setText(R.string.show);
                }
                tempEditText.setSelection(tempEditText.getText().length());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getToken() {
        return bundle.getString("BUNDLE_TOKEN");
    }

    @Override
    public String getPhoneNumber() {
        return bundle.getString("BUNDLE_PHONE_NUMBER");
    }

    @Override
    public String getVerificationCode() {
        return bundle.getString("BUNDLE_VERIFICATION_CODE");
    }

    @Override
    public String getFullName() {
        return bundle.getString("BUNDLE_FULL_NAME");
    }

    @Override
    public String getEmail() {
        return bundle.getString("BUNDLE_EMAIL");
    }

    @Override
    public String getReferralCode() {
        return bundle.getString("BUNDLE_REFERRAL_CODE");
    }

    @Override
    public String getGender() {
        if (bundle.containsKey("BUNDLE_GENDER"))
            return bundle.getString("BUNDLE_GENDER");
        else return null;
    }

    @Override
    public String getBirthDate() {
        if (bundle.containsKey("BUNDLE_BIRTH_DATE"))
            return bundle.getString("BUNDLE_BIRTH_DATE");
        else return null;
    }

    @Override
    public String getPassword() {
        return binding.tilPassword.getEditText().getText().toString();
    }

    @Override
    public String getImageUri() {
        if (bundle.containsKey("BUNDLE_PROFILE_IMAGE_URL")) {
            return bundle.getString("BUNDLE_PROFILE_IMAGE_URL");
        } else {
            return null;
        }
    }

    @Override
    public String getFBImageUrl() {
        return bundle.getString("FB_PROFILE_IMAGE_URL", "");
    }

    @Override
    public String getFbId() {
        if (bundle.containsKey("BUNDLE_FB_ID"))
            return bundle.getString("BUNDLE_FB_ID");
        else return null;
    }

    @Override
    public boolean getIsFBSignUp() {
        return bundle.getBoolean("BUNDLE_IS_FB_SIGN_UP", false);
    }

    @Override
    public void showPasswordError() {
        binding.tilPassword.setError(getString(R.string.error_invalid_password));
    }

    @Override
    public void hideAllErrors() {
        binding.tilPassword.setErrorEnabled(false);
    }

    @Override
    public void signupFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SignupPasswordActivity.this, getString(R.string.signup_failed), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void signupFailed(String message) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SignupPasswordActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void signupSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(SignupPasswordActivity.this, getString(R.string.signup_success), Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void proceedToSplashActivity() {
        SplashActivity.ClearAllScreenAndOpen(this);
    }

    @Override
    public void hideKeyboard() {
        hideKeyboard(binding.tilPassword.getEditText());
    }
}
