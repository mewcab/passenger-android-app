package com.instake.passenger.monowar.signup.d;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = SignupBirthdayModule.class)
public interface SignupBirthdayComponent {
    void inject(SignupBirthdayActivity activity);
}
