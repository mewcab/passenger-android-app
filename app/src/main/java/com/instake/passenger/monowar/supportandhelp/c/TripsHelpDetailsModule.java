package com.instake.passenger.monowar.supportandhelp.c;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

@Module
public class TripsHelpDetailsModule {
    private TripsHelpDetailsView view;

    public TripsHelpDetailsModule(TripsHelpDetailsView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public TripsHelpDetailsPresenter provideSupportAndHelpPresenter(ApiService apiService, Session session) {
        return new TripsHelpDetailsPresenter(view, apiService, session);
    }
}
