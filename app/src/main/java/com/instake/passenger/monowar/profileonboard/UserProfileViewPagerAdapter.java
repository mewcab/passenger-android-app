package com.instake.passenger.monowar.profileonboard;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Adib on 31-Mar-17.
 */

public class UserProfileViewPagerAdapter extends PagerAdapter {

    private Context mContext;

    private int profileType;

    public UserProfileViewPagerAdapter(Context context, int profileType) {
        mContext = context;
        this.profileType = profileType;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        StudentProfileOnBoardingObject studentProfileOnBoardingObject = StudentProfileOnBoardingObject.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(studentProfileOnBoardingObject.getLayoutResId(), collection, false);
        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return StudentProfileOnBoardingObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        StudentProfileOnBoardingObject customPagerEnum = StudentProfileOnBoardingObject.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

}
