package com.instake.passenger.monowar;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.instake.passenger.monowar.VerificationStatus;

import java.lang.reflect.Type;

/**
 * Created by Tushar on 7/21/17.
 */

public class VerificationStatusDeserializer implements JsonDeserializer<VerificationStatus> {
    @Override
    public VerificationStatus deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonData = json.getAsJsonObject();

        if (jsonData != null) {
            VerificationStatus verificationStatus = new VerificationStatus();
            if (jsonData.has("status")) {
                verificationStatus.setVerified(jsonData.get("status").getAsBoolean());
            }

            if (jsonData.has("is_exist_user")) {
                verificationStatus.setExistingUser(jsonData.get("is_exist_user").getAsBoolean());
            }

            if (jsonData.has("is_signup_completed")) {
                verificationStatus.setSignedUpCompleted(jsonData.get("is_signup_completed").getAsBoolean());
            }

            return verificationStatus;
        }
        return null;
    }
}
