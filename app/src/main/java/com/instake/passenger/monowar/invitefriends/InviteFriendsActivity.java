package com.instake.passenger.monowar.invitefriends;

import android.app.Activity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityInviteFriendsBinding;
import com.instake.passenger.databinding.ActivitySignupBirthdayBinding;

import java.util.Calendar;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class InviteFriendsActivity extends SiriusPresenterActivity<InviteFriendsView, InviteFriendsViewPresenter> implements InviteFriendsView {

    private final static String TAG = "InviteFriends";

    ActivityInviteFriendsBinding binding;

    @Inject
    InviteFriendsViewPresenter presenter;

    @Override
    protected InviteFriendsViewPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new InviteFriendsModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_invite_friends;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityInviteFriendsBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.layoutCopyReferralCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ClipboardManager clipboard = (ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("label", getReferralCode());
                clipboard.setPrimaryClip(clip);
                Toast.makeText(getContext(), R.string.text_copied, Toast.LENGTH_SHORT).show();
            }
        });

        binding.buttonInviteFriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareBody = String.format(getString(R.string.invite_body), getReferralCode());
                Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, getString(R.string.invite_subject));
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, getResources().getString(R.string.invite_friends)));
            }
        });

        binding.buttonInsertCouponCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ApplyCouponDialogFragment dialogFragment = new ApplyCouponDialogFragment();
                dialogFragment.show(getSupportFragmentManager(), ApplyCouponDialogFragment.TAG);
            }
        });
    }

    @Override
    public void setReferralCode(String referralCode) {
        binding.tvReferralCode.setText(referralCode);
    }

    @Override
    public String getReferralCode() {
        return binding.tvReferralCode.getText().toString();
    }
}
