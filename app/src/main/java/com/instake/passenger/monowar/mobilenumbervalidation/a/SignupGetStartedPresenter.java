package com.instake.passenger.monowar.mobilenumbervalidation.a;

import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.monowar.signup.a.SignupCompleteProfilePresenter;
import com.instake.passenger.utility.validation.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

public class SignupGetStartedPresenter implements BasePresenter<SignupGetStartedView> {

    private static final String TAG = SignupGetStartedPresenter.class.getSimpleName();

    private WeakReference<SignupGetStartedView> view;
    private ApiService apiService;
    private Session session;
    private Validator validator;

    public SignupGetStartedPresenter(SignupGetStartedView confirmAccountView, ApiService apiService, Session session, Validator validator) {
        this.view = new WeakReference<>(confirmAccountView);
        this.validator = validator;
        this.session = session;
        this.apiService = apiService;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void checkPhoneNumberValidation(String phoneNumber) {
        if (phoneNumber.length() > 14) {
            view.get().showPhoneNumberError();
        } else {
            view.get().hidePhoneNumberError();
        }
    }

    public void onClickNext() {
        if (!validator.isPhoneNumberValid(view.get().getPhoneNumber())
                && !validator.isPhoneNumberLengthValid(view.get().getPhoneNumber())) {
            view.get().showPhoneNumberError();
            return;
        }

        view.get().hidePhoneNumberError();
        view.get().hideKeyboard();
        view.get().showLoader();
        sendVerificationCode(view.get().getPhoneNumber());
    }

    private void sendVerificationCode(String phoneNumber) {
        apiService.sendVerificationCode(getSendSmsRequestBody(phoneNumber))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ResponseBody>>() {
                    @Override
                    public Observable<? extends ResponseBody> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Log.e("APIService", "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        onVerificationCodeSendingError(e.getMessage());
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        onVerificationCodeSendingSuccess();
                    }
                });
    }

    private RequestBody getSendSmsRequestBody(String phoneNumber) {
        return RequestBody.create(MediaType.parse("application/json"), JsonBuilder.getSendSmsRequestBody(phoneNumber));
    }

    private void onVerificationCodeSendingSuccess() {
        if (view != null) {
            view.get().hideLoader();
            view.get().showVerificationCodeSendingSuccessMessage();
            view.get().proceedSignupOTPActivity();
        }
    }

    private void onVerificationCodeSendingError(String message) {
        if (view != null) {
            view.get().hideLoader();
            view.get().showVerificationCodeSendingFailureMessage();
        }
    }

    public void onFbLoginSuccess(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            JSONObject json = response.getJSONObject();
            try {
                if (json != null) {
                    if (json.has("id")) {
                        view.get().showLoader();
                        loginUserWithFbId(json.getString("id"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id");
        request.setParameters(parameters);
        request.executeAsync();
    }

    private void loginUserWithFbId(String fbId) {
        apiService.loginFacebook(RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createLoginBodyWithFbId(view.get().getPhoneNumber(), fbId)))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends User>>() {
                    @Override
                    public Observable<? extends User> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {
                        view.get().hideLoader();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        onLoginFailed(e.getMessage());
                    }

                    @Override
                    public void onNext(User user) {
                        onLoginSuccess(user);
                    }
                });
    }

    private void onLoginFailed(String error) {
        if (view != null) {
            view.get().onLoginFailed(error);
        }
    }

    private void onLoginSuccess(User user) {
        if (user.getMessage() == null || user.getMessage().length() == 0) {
            session.saveUser(user);
            Log.d(TAG, "onSignupSuccess: " + user.getToken());

            if (view != null) {
                view.get().clearAllActivitiesAndNavigateToSplash();
            }
        } else {
            onLoginFailed(user.getMessage());
        }
    }
}
