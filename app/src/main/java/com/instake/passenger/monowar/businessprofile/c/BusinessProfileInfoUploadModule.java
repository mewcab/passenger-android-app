package com.instake.passenger.monowar.businessprofile.c;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class BusinessProfileInfoUploadModule {

    private BusinessProfileInfoUploadView view;

    public BusinessProfileInfoUploadModule(BusinessProfileInfoUploadView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public BusinessProfileInfoUploadPresenter provideBusinessProfileInfoUploadPresenter(ApiService apiService, Session session, Validator validator) {
        return new BusinessProfileInfoUploadPresenter(view, apiService, session, validator);
    }
}
