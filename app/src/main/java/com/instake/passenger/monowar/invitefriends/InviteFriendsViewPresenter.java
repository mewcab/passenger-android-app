package com.instake.passenger.monowar.invitefriends;

import android.nfc.Tag;
import android.util.Log;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.utility.validation.Validator;

import java.lang.ref.WeakReference;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class InviteFriendsViewPresenter implements BasePresenter<InviteFriendsView> {

    private final static String TAG = "InviteFriends";

    private WeakReference<InviteFriendsView> view;
    private ApiService apiService;
    private Session session;
    private Validator validator;

    public InviteFriendsViewPresenter(InviteFriendsView view, ApiService apiService, Session session, Validator validator) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {
        User user = session.getUser();
        Log.i(TAG, "Referral Code: " + user.getReferralCode());
        view.get().setReferralCode(user.getReferralCode());
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}
