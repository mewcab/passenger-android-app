package com.instake.passenger.monowar.businessprofile.b;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityProvideBusinessEmailBinding;
import com.instake.passenger.databinding.ActivityProvideCompanyInfoBinding;
import com.instake.passenger.monowar.businessprofile.c.BusinessProfileInfoUploadActivity;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 12-Sep-17.
 */

public class ProvideCompanyInfoActivity extends SiriusActivity {

    public static Activity activity;

    ActivityProvideCompanyInfoBinding binding;

    @Inject
    Validator validator;

    private Bundle bundle;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_provide_company_info;
    }

    @Override
    protected void initComponents() {
        app.getAppComponent().inject(this);
        binding = (ActivityProvideCompanyInfoBinding) getDataBinding();

        activity = this;

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        bundle = getIntent().getExtras();

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.tilCompanyName.setErrorEnabled(false);
                binding.tilCompanyAddress.setErrorEnabled(false);
                if (isInputOkay()) {
                    Bundle bundle = new Bundle();
                    bundle.putString("BUNDLE_COMPANY_NAME", binding.etCompanyName.getText().toString());
                    bundle.putString("BUNDLE_COMPANY_ADDRESS", binding.etCompanyAddress.getText().toString());
                    Intent intent = new Intent(getActivity(), BusinessProfileInfoUploadActivity.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            }
        });
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        String companyName = binding.etCompanyName.getText().toString();
        String companyAddress = binding.etCompanyAddress.getText().toString();
        if (!validator.isNotEmpty(companyName)) {
            binding.tilCompanyName.setError(getString(R.string.error_company_name));
            isOkay = false;
        }
        if (!validator.isNotEmpty(companyAddress)) {
            binding.tilCompanyAddress.setError(getString(R.string.error_company_address));
            isOkay = false;
        }
        return isOkay;
    }
}
