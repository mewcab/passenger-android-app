package com.instake.passenger.monowar.supportandhelp.b;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 23-Aug-17.
 */

public interface RequestHelpView extends BaseView {

    void setPhoneNumber(String phoneNumber);

    String getPhoneNumber();

    String getComplain();

    String getHelpId();

    void showPhoneNumberError();

    void showComplainError();

    void hideAllError();
}
