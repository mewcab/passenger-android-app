package com.instake.passenger.monowar.mobilenumbervalidation.b;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

@Singleton
@Subcomponent(modules = SignupOtpModule.class)
public interface SignupOtpComponent {
    void inject(SignupOtpActivity activity);
}
