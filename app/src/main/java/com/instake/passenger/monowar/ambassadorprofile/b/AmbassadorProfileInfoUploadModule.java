package com.instake.passenger.monowar.ambassadorprofile.b;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class AmbassadorProfileInfoUploadModule {

    private AmbassadorProfileInfoUploadView view;

    public AmbassadorProfileInfoUploadModule(AmbassadorProfileInfoUploadView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public AmbassadorProfileInfoUploadPresenter provideAmbassadorProfileInfoUploadPresenter(ApiService apiService, Session session, Validator validator) {
        return new AmbassadorProfileInfoUploadPresenter(view, apiService, session, validator);
    }
}
