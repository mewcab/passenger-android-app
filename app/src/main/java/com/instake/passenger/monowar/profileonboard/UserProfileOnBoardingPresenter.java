package com.instake.passenger.monowar.profileonboard;

import com.instake.passenger.common.base.BasePresenter;

import java.lang.ref.WeakReference;

/**
 * Created by Mostafa Monowar on 5/12/17.
 */
public class UserProfileOnBoardingPresenter implements BasePresenter<UserProfileOnBoardingView> {

    private WeakReference<UserProfileOnBoardingView> view;

    public UserProfileOnBoardingPresenter(UserProfileOnBoardingView view) {
        this.view = new WeakReference<>(view);
    }

    @Override
    public void onCreatePresenter() {
        if (view != null) {
            view.get().startPagerRotation();
        }
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}
