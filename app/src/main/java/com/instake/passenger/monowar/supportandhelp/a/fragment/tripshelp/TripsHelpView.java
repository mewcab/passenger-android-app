package com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.historydata.Trip;

import java.util.List;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public interface TripsHelpView extends BaseView {
    void loadDataIntoAdapter(List<Trip> arrayList);
}
