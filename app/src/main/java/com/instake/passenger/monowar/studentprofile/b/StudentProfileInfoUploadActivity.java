package com.instake.passenger.monowar.studentprofile.b;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityUploadProfileInfoBinding;
import com.instake.passenger.monowar.studentprofile.a.StudentProfileInfoActivity;
import com.theartofdev.edmodo.cropper.CropImage;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class StudentProfileInfoUploadActivity extends SiriusPresenterActivity<StudentProfileInfoUploadView, StudentProfileInfoUploadPresenter> implements StudentProfileInfoUploadView {

    private final static String TAG = "UploadInfo";

    ActivityUploadProfileInfoBinding binding;

    @Inject
    StudentProfileInfoUploadPresenter presenter;

    private Bundle bundle;
    private String imageUri;

    @Override
    protected StudentProfileInfoUploadPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new StudentProfileInfoUploadModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_upload_profile_info;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityUploadProfileInfoBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        bundle = getIntent().getExtras();

        binding.buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onSubmitClick();
            }
        });

        binding.buttonDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onSubmitClick();
            }
        });

        binding.ivIdImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onImageClick();
            }
        });

        binding.buttonEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onImageEditClick();
            }
        });

        binding.buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideFullScreenImage();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (binding.layoutImage.getVisibility() == View.VISIBLE) {
            hideFullScreenImage();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageUri = resultUri.getPath();
                Log.i(TAG, "Image info: " + resultUri.toString());
                Glide.with(getContext().getApplicationContext())
                        .load(resultUri)
                        .dontAnimate()
                        .error(R.drawable.backgroud_select_image)
                        .placeholder(R.drawable.backgroud_select_image)
                        .into(binding.ivIdImage);
                Glide.with(getContext().getApplicationContext())
                        .load(resultUri)
                        .dontAnimate()
                        .into(binding.ivIdImageFull);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        }
    }

    @Override
    public String getInstitutionName() {
        return bundle.getString("BUNDLE_INSTITUTION_NAME");
    }

    @Override
    public String getClassName() {
        return bundle.getString("BUNDLE_CLASS_NAME");
    }

    @Override
    public String getIdImageUri() {
        return imageUri;
    }

    @Override
    public void showFullScreenImage() {
        isClickable(false);
        binding.layoutImage.animate()
                .alpha(1f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationStart(Animator animation) {
                        super.onAnimationStart(animation);
                        binding.layoutImage.setVisibility(View.VISIBLE);
                    }
                });
    }

    @Override
    public void hideFullScreenImage() {
        isClickable(true);
        binding.layoutImage.animate()
                .alpha(0.0f)
                .setDuration(300)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        super.onAnimationEnd(animation);
                        binding.layoutImage.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void showImageError() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), R.string.error_id_card_image, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void showUploadInfoFailed() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), R.string.error_student_profile_create, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void showUploadInfoSuccess() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getContext(), R.string.message_student_profile_create, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void backToProfileActivity() {
        onBackPressed();
        StudentProfileInfoActivity.activity.finish();
    }

    private void isClickable(boolean result) {
        binding.buttonSubmit.setClickable(result);
        binding.ivIdImage.setClickable(result);
    }
}
