package com.instake.passenger.monowar.signup.e;

import android.net.Uri;
import android.util.Log;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.monowar.ImageUploadListener;
import com.instake.passenger.monowar.ProgressRequestBody;
import com.instake.passenger.monowar.SignupResponseData;
import com.instake.passenger.monowar.signup.a.SignupCompleteProfilePresenter;
import com.instake.passenger.utility.photocompressor.CompressionListener;
import com.instake.passenger.utility.photocompressor.ImageCompressionUtil;
import com.instake.passenger.utility.validation.Validator;

import java.io.File;
import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class SignupPasswordPresenter implements BasePresenter<SignupPasswordView> {

    private WeakReference<SignupPasswordView> view;
    private Validator validator;
    private ApiService apiService;
    private Session session;

    private String token;
    private Integer requiredWidth = 500;
    private Integer requiredHeight = 500;

    public SignupPasswordPresenter(SignupPasswordView view, Validator validator, ApiService apiService, Session session) {
        this.view = new WeakReference<>(view);
        this.validator = validator;
        this.apiService = apiService;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onClickConfirm() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().hideKeyboard();
            view.get().showLoader();
            signupUpdate();
        }
    }

    public void onClickConfirmLogin() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().hideKeyboard();
            view.get().showLoader();
            logIn();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (!validator.isPasswordValid(view.get().getPassword())) {
            view.get().showPasswordError();
            isOkay = false;
        }
        return isOkay;
    }

    private void signupUpdate() {
        apiService.signupUpdate(view.get().getToken(), getSignupRequestBody())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends SignupResponseData>>() {
                    @Override
                    public Observable<? extends SignupResponseData> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<SignupResponseData>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onSignupFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(SignupResponseData signupValidation) {
                        onSignupSuccess(signupValidation);
                    }
                });
    }

    private void logIn() {
        apiService.loginNew(getDefaultLoginRequestBody(view.get().getPhoneNumber(), view.get().getPassword()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends User>>() {
                    @Override
                    public Observable<? extends User> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onVerificationResultFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(User user) {
                        session.saveUser(user);
                        Log.d(SignupCompleteProfilePresenter.class.getSimpleName(), "onSignupSuccess: " + user.getFullName());
                        Log.d(SignupCompleteProfilePresenter.class.getSimpleName(), "onSignupSuccess: " + user.getProfileImageUrl());

                        if (view != null) {
                            token = user.getToken();
                            view.get().hideLoader();
                            view.get().proceedToSplashActivity();
                        }
                    }
                });
    }

    private void onVerificationResultFailure(String message) {
        Log.e("SignUpPasswordPresenter", "Log In Error" + message);
    }

    private RequestBody getSignupRequestBody() {
        return RequestBody.create(MediaType.parse("application/json"),
                JsonBuilder.createSignUpUpdateBody(view.get().getPhoneNumber(), view.get().getFBImageUrl(),
                        view.get().getFbId(), view.get().getFullName(), view.get().getEmail(),
                        view.get().getReferralCode(), view.get().getGender(), view.get().getBirthDate(),
                        view.get().getPassword()));
    }

    private void onSignupSuccess(SignupResponseData signupValidation) {
        if (signupValidation != null) {
            if (view.get().getImageUri() != null) {
                File file = new File(view.get().getImageUri());
                ImageCompressionUtil.getResizedImageUri(
                        new CompressionListener() {
                            @Override
                            public void onCompressComplete(File compressedFile) {
                                uploadImage(compressedFile, new ImageUploadListener() {
                                    @Override
                                    public void onProgress(int progress) {

                                    }

                                    @Override
                                    public void onSuccess() {
                                        logIn();
                                    }

                                    @Override
                                    public void onFailure() {

                                    }
                                }, "profile_images");
                            }
                        },
                        view.get().getContext(),
                        Uri.fromFile(file),
                        requiredWidth, requiredHeight);
            } else {

                logIn();
            }

        }
    }

    private void onSignupFailure(String message) {
        view.get().hideLoader();
        view.get().signupFailed(message);
    }

    private RequestBody getDefaultLoginRequestBody(String mobileNumber, String password) {
        return RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createLoginBodyWithPassword(mobileNumber, password));
    }

    private void uploadImage(File file, ImageUploadListener listener, String category) {
        apiService.uploadImage(token, getMultipartImage(file, listener), getCategory(category))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends ResponseBody>>() {
                    @Override
                    public Observable<? extends ResponseBody> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        listener.onFailure();
                        logIn();
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        listener.onSuccess();
                        logIn();
                    }
                });
    }

    private MultipartBody.Part getMultipartImage(File profilePhoto, ImageUploadListener listener) {
        return MultipartBody.Part.createFormData("upload", profilePhoto.getName(),
                new ProgressRequestBody(profilePhoto, listener));
    }

    private RequestBody getCategory(String category) {
        return RequestBody.create(MediaType.parse("text/plain"), category);
    }
}
