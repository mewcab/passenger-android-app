package com.instake.passenger.monowar.supportandhelp.c.help;

import android.util.Log;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public class HelpPresenter implements BasePresenter<HelpView> {
    private static final String TAG = "ReceiptPresenter";

    private WeakReference<HelpView> view;
    private ApiService apiService;
    private Session session;

    public HelpPresenter(HelpView view, ApiService apiService, Session session) {

        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void initRecyclerView() {
        Log.e(TAG, "loadDataIntoAdapter");
        ArrayList<String> arrayList = new ArrayList<String>();
        arrayList.add("How to identity a driver and vehicle?");
        arrayList.add("How to remove account?");
        arrayList.add("Do riders pay tolls or charge?");
        arrayList.add("How to remove account?");
        arrayList.add("TesDo riders pay tolls or charge?t");
        arrayList.add("EducationDetails");
        arrayList.add("EducationDetails");
        arrayList.add("How to identity a driver and vehicle?");
        arrayList.add("How to remove account?");
        arrayList.add("Do riders pay tolls or charge?");
        arrayList.add("How to remove account?");
        arrayList.add("TesDo riders pay tolls or charge?t");
        arrayList.add("How to identity a driver and vehicle?");
        arrayList.add("How to remove account?");
        arrayList.add("Do riders pay tolls or charge?");
        arrayList.add("How to remove account?");
        arrayList.add("TesDo riders pay tolls or charge?t");

        view.get().initRecyclerView(arrayList);
    }
}
