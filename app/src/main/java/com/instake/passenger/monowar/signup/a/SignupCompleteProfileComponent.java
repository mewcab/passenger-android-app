package com.instake.passenger.monowar.signup.a;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

@Singleton
@Subcomponent(modules = SignupCompleteProfileModule.class)
public interface SignupCompleteProfileComponent {
    void inject (SignupCompleteProfileActivity activity);
}
