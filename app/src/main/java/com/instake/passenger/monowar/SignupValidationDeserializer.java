package com.instake.passenger.monowar;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class SignupValidationDeserializer implements JsonDeserializer<SignupResponseData> {
    @Override
    public SignupResponseData deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonData = json.getAsJsonObject();

        if (jsonData != null) {
            SignupResponseData signupValidation = new SignupResponseData();
            if (jsonData.has("phone_no")) {
                signupValidation.setPhoneNumber(jsonData.get("phone_no").getAsString());
            }

            if (jsonData.has("referral")) {
                signupValidation.setReferral(jsonData.get("referral").getAsString());
            }

            if (jsonData.has("message")) {
                signupValidation.setMessage(jsonData.get("message").getAsString());
            }

            return signupValidation;
        }
        return null;
    }
}