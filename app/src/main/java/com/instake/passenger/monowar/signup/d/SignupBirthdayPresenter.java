package com.instake.passenger.monowar.signup.d;

import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.utility.validation.Validator;

import java.lang.ref.WeakReference;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class SignupBirthdayPresenter implements BasePresenter<SignupBirthdayView> {

    private WeakReference<SignupBirthdayView> view;
    private Validator validator;

    public SignupBirthdayPresenter(SignupBirthdayView view, Validator validator) {
        this.view = new WeakReference<>(view);
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onClickNext() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().proceedToPasswordActivity();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (view.get().getGender() == null) {
            view.get().showGenderError();
            isOkay = false;
        }
        if (view.get().getBirthDate() == null) {
            view.get().showBirthDateError();
            isOkay = false;
        }
        return isOkay;
    }
}
