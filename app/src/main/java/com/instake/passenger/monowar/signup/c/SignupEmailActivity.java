package com.instake.passenger.monowar.signup.c;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySignupEmailBinding;
import com.instake.passenger.monowar.signup.b.SignupProvideInformationActivity;
import com.instake.passenger.monowar.signup.b.SignupProvideInformationModule;
import com.instake.passenger.monowar.signup.d.SignupBirthdayActivity;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class SignupEmailActivity extends SiriusPresenterActivity<SignupEmailView, SignupEmailPresenter> implements SignupEmailView {

    private final static String TAG = "SignupActivity";

    ActivitySignupEmailBinding binding;

    @Inject
    SignupEmailPresenter presenter;

    private Bundle bundle;

    public static void open(Activity activity, Bundle bundle) {
        Intent signupIntent = new Intent(activity, SignupEmailActivity.class);
        Log.i(TAG, "Bundle: " + bundle.toString());
        signupIntent.putExtras(bundle);
        activity.startActivity(signupIntent);
    }

    @Override
    protected SignupEmailPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SignupEmailModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_signup_email;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySignupEmailBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onClickNext();
            }
        });

        bundle = getIntent().getExtras();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public String getEmail() {
        return binding.tilEmail.getEditText().getText().toString();
    }

    @Override
    public String getReferralCode() {
        return binding.tilReferralCode.getEditText().getText().toString();
    }

    @Override
    public void showEmailError(String error) {
        binding.tilEmail.setError(error);
    }

    @Override
    public void showReferralCodeError(String error) {
        binding.tilReferralCode.setError(error);
    }

    @Override
    public void hideAllErrors() {
        binding.tilEmail.setErrorEnabled(false);
        binding.tilReferralCode.setErrorEnabled(false);
    }

    @Override
    public void proceedToBirthDateActivity() {
        bundle.putString("BUNDLE_EMAIL", getEmail());
        bundle.putString("BUNDLE_REFERRAL_CODE", getReferralCode());
        SignupBirthdayActivity.open(this, bundle);
    }
}
