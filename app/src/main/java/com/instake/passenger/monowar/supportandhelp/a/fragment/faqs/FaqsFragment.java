package com.instake.passenger.monowar.supportandhelp.a.fragment.faqs;

import android.content.Intent;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;

import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.SiriusFragment;
import com.instake.passenger.databinding.FragmentFaqsBinding;
import com.instake.passenger.model.suppotnhelp.HelpNSupport;
import com.instake.passenger.model.suppotnhelp.SupportAndHelpResponse;
import com.instake.passenger.monowar.EqualSpacingItemDecoration;
import com.instake.passenger.monowar.RecyclerTouchListener;
import com.instake.passenger.monowar.supportandhelp.a.SupportAndHelpActivity;
import com.instake.passenger.monowar.supportandhelp.b.FaqsListActivity;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by Mostafa Monowar on 21-Aug-17.
 */

public class FaqsFragment extends SiriusFragment<FaqsView, FaqsPresenter> implements FaqsView {

    private static final int SCROLL_DIRECTION_UP = -1;
    private static final String TAG = "FaqsFragment";

    private FragmentFaqsBinding binding;

    private SupportAndHelpResponse supportAndHelpResponse;
    private List<HelpNSupport> helpNSupportList;
    private FaqsAdapter faqsAdapter;
    private Subscription busSubscription;

    @Inject
    FaqsPresenter presenter;

    @Override
    protected FaqsPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new FaqsModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_faqs;
    }

    @Override
    protected void initFragmentComponents() {
        binding = (FragmentFaqsBinding) getDataBinding();

        initRecycleView();

        autoUnsubBus();
        rxCallBackGettingHelpNSupport();
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    @Override
    public void loadDataIntoAdapter(List<HelpNSupport> help_n_support_list) {
        helpNSupportList = help_n_support_list;
        faqsAdapter = new FaqsAdapter(getActivity(), helpNSupportList);
        binding.recyclerView.setAdapter(faqsAdapter);
    }

    private void initRecycleView() {
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        int defaultGap = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_1dp), getResources().getDisplayMetrics());
        binding.recyclerView.addItemDecoration(new EqualSpacingItemDecoration(defaultGap, EqualSpacingItemDecoration.VERTICAL));
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());

        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), binding.recyclerView, new RecyclerTouchListener.OnRecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                view.postDelayed(() -> {
                    Intent intent = new Intent(getActivity(), FaqsListActivity.class);
                    intent.putExtra("EXTRA_LIST", Parcels.wrap(helpNSupportList.get(position).getHelp()));
                    intent.putExtra("EXTRA_TITLE", helpNSupportList.get(position).getTitle());
                    startActivity(intent);
                }, 200);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_2dp), getResources().getDisplayMetrics());
        binding.recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (binding.recyclerView.canScrollVertically(SCROLL_DIRECTION_UP)) {
                        ((SupportAndHelpActivity) getActivity()).binding.appbar.setElevation(elevation);
                    } else {
                        ((SupportAndHelpActivity) getActivity()).binding.appbar.setElevation(0);
                    }
                }
            }
        });
    }

    private void autoUnsubBus() {
        if (busSubscription != null && !busSubscription.isUnsubscribed()) {
            busSubscription.unsubscribe();
        }
    }

    private void rxCallBackGettingHelpNSupport() {
        busSubscription = App.getInstance().bus().toObserverable1().observeOn(AndroidSchedulers.mainThread())
                .subscribe(event -> {
                    if (event instanceof SupportAndHelpResponse) {
                        supportAndHelpResponse = (SupportAndHelpResponse) event;
                        if (getActivity() != null) {
                            loadDataIntoAdapter(supportAndHelpResponse.getHelpNSupport());
                        }
                    }
                });
    }
}
