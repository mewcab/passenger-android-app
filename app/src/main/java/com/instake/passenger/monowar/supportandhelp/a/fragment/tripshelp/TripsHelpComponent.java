package com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Singleton
@Subcomponent(modules = TripsHelpModule.class)
public interface TripsHelpComponent {
    void inject(TripsHelpFragment fragment);
}
