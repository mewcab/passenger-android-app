package com.instake.passenger.monowar.profile.switchprofile;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.model.UserProfile;
import com.instake.passenger.monowar.ambassadorprofile.a.ProvideAmbassadorEmailActivity;
import com.instake.passenger.monowar.businessprofile.a.ProvideBusinessEmailActivity;
import com.instake.passenger.monowar.profileonboard.UserProfileOnBoardingActivity;
import com.instake.passenger.monowar.studentprofile.a.StudentProfileInfoActivity;
import com.instake.passenger.monowar.teacherprofile.a.ProvideTeacherEmailActivity;


/**
 * Created by Adib on 31-Mar-17.
 */

public class SwitchProfilePagerAdapter extends PagerAdapter {

    private Context mContext;
    private UserProfile userProfile;
    private OnFragmentCloseListener onFragmentCloseListener;

    public SwitchProfilePagerAdapter(Context context, UserProfile userProfile) {
        mContext = context;
        this.userProfile = userProfile;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        SwitchProfileObject switchProfileObject = SwitchProfileObject.values()[position];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup view = (ViewGroup) inflater.inflate(switchProfileObject.getLayoutResId(), collection, false);

        Button buttonSelect = (Button) view.findViewById(R.id.button_select);
        TextView learnMore = (TextView) view.findViewById(R.id.learn_more);

        if (userProfile != null && userProfile.getProfileType() != null) {
            if (position == 0) {
                if (userProfile.getProfileType().equalsIgnoreCase("business")) {
                    buttonSelect.setEnabled(false);
                    buttonSelect.setBackgroundColor(ContextCompat.getColor(mContext, R.color.warm_gray));
                    buttonSelect.setText(R.string.current_profile);
                } else {
                    buttonSelect.setEnabled(true);
                    buttonSelect.setText(R.string.select);
                }
            } else if (position == 1) {
                if (userProfile.getProfileType().equalsIgnoreCase("student")) {
                    buttonSelect.setEnabled(false);
                    buttonSelect.setBackgroundColor(ContextCompat.getColor(mContext, R.color.warm_gray));
                    buttonSelect.setText(R.string.current_profile);
                } else {
                    buttonSelect.setEnabled(true);
                    buttonSelect.setText(R.string.select);
                }
            } else if (position == 2) {
                if (userProfile.getProfileType().equalsIgnoreCase("teacher")) {
                    buttonSelect.setEnabled(false);
                    buttonSelect.setBackgroundColor(ContextCompat.getColor(mContext, R.color.warm_gray));
                    buttonSelect.setText(R.string.current_profile);
                } else {
                    buttonSelect.setEnabled(true);
                    buttonSelect.setText(R.string.select);
                }
            } else {
                if (userProfile.getProfileType().equalsIgnoreCase("ambassador")) {
                    buttonSelect.setEnabled(false);
                    buttonSelect.setBackgroundColor(ContextCompat.getColor(mContext, R.color.warm_gray));
                    buttonSelect.setText(R.string.current_profile);
                } else {
                    buttonSelect.setEnabled(true);
                    buttonSelect.setText(R.string.select);
                }
            }
        }

        learnMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Intent intent = new Intent(mContext, UserProfileOnBoardingActivity.class);
                    intent.putExtra("EXTRA_PROFILE_TYPE", UserProfileOnBoardingActivity.BUSINESS_PROFILE);
                    mContext.startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(mContext, UserProfileOnBoardingActivity.class);
                    intent.putExtra("EXTRA_PROFILE_TYPE", UserProfileOnBoardingActivity.STUDENT_PROFILE);
                    mContext.startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(mContext, UserProfileOnBoardingActivity.class);
                    intent.putExtra("EXTRA_PROFILE_TYPE", UserProfileOnBoardingActivity.TEACHER_PROFILE);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, UserProfileOnBoardingActivity.class);
                    intent.putExtra("EXTRA_PROFILE_TYPE", UserProfileOnBoardingActivity.AMBASSADOR_PROFILE);
                    mContext.startActivity(intent);
                }

                if (onFragmentCloseListener != null) {
                    view.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onFragmentCloseListener.fragmentClose(true);
                        }
                    }, 200);
                }
            }
        });

        buttonSelect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Intent intent = new Intent(mContext, ProvideBusinessEmailActivity.class);
                    mContext.startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(mContext, StudentProfileInfoActivity.class);
                    mContext.startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(mContext, ProvideTeacherEmailActivity.class);
                    mContext.startActivity(intent);
                } else {
                    Intent intent = new Intent(mContext, ProvideAmbassadorEmailActivity.class);
                    mContext.startActivity(intent);
                }

                if (onFragmentCloseListener != null) {
                    view.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            onFragmentCloseListener.fragmentClose(true);
                        }
                    }, 200);
                }
            }
        });

        collection.addView(view);
        view.setTag("view" + position);
        return view;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return SwitchProfileObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        SwitchProfileObject customPagerEnum = SwitchProfileObject.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

    public void setOnFragmentCloseListener(OnFragmentCloseListener onFragmentCloseListener) {
        this.onFragmentCloseListener = onFragmentCloseListener;
    }

    public interface OnFragmentCloseListener {
        void fragmentClose(boolean close);
    }

}
