package com.instake.passenger.monowar.profile;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = ProfileModule.class)
public interface ProfileComponent {
    void inject(ProfileActivity activity);
}
