package com.instake.passenger.monowar.profileonboard;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class UserProfileOnBoardingModule {

    private UserProfileOnBoardingView view;

    public UserProfileOnBoardingModule(UserProfileOnBoardingView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public UserProfileOnBoardingPresenter provideUserProfilePresenter() {
        return new UserProfileOnBoardingPresenter(view);
    }
}
