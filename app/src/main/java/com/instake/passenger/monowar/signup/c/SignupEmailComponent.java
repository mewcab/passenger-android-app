package com.instake.passenger.monowar.signup.c;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = SignupEmailModule.class)
public interface SignupEmailComponent {
    void inject(SignupEmailActivity activity);
}
