package com.instake.passenger.monowar.supportandhelp.c.help;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Singleton
@Subcomponent(modules = HelpModule.class)
public interface HelpComponent {
    void inject(HelpFragment fragment);
}
