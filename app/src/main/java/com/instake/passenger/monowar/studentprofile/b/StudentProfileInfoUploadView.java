package com.instake.passenger.monowar.studentprofile.b;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface StudentProfileInfoUploadView extends BaseView {
    String getInstitutionName();
    String getClassName();
    String getIdImageUri();
    void showFullScreenImage();
    void hideFullScreenImage();
    void showImageError();
    void showUploadInfoFailed();
    void showUploadInfoSuccess();
    void backToProfileActivity();
}
