package com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp;

import android.util.Log;

import com.android.volley.NetworkError;
import com.instake.passenger.Rx.RxService;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.historydata.HistoryResp;

import java.lang.ref.WeakReference;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public class TripsHelpPresenter implements BasePresenter<TripsHelpView> {
    private static final String TAG = "TripsHelpPresenter";

    private WeakReference<TripsHelpView> view;
    private ApiService apiService;
    private Session session;

    public TripsHelpPresenter(TripsHelpView view, ApiService apiService, Session session) {

        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void getApiData() {
        view.get().showLoader();

        CompositeSubscription subscriptions = new CompositeSubscription();
        Subscription subscription = new RxService(session).getHistory("", new RxService.getHistoryCallBack() {
            @Override
            public void onSuccess(HistoryResp response) {
                view.get().hideLoader();
                Log.e("History", "onSuccess: " + response.toString());
                view.get().loadDataIntoAdapter(response.getTrips());
            }

            @Override
            public void onError(NetworkError networkError) {
                networkError.printStackTrace();
                view.get().hideLoader();
            }
        });

        subscriptions.add(subscription);

    }
}
