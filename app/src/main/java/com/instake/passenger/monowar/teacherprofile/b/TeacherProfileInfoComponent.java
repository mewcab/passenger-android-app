package com.instake.passenger.monowar.teacherprofile.b;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = TeacherProfileInfoModule.class)
public interface TeacherProfileInfoComponent {
    void inject(TeacherProfileInfoActivity activity);
}
