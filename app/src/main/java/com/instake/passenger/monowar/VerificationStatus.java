package com.instake.passenger.monowar;

import org.parceler.Parcel;

/**
 * Created by Tushar on 7/21/17.
 */

@Parcel
public class VerificationStatus {

    boolean isVerified;

    boolean isExistingUser;

    boolean isSignedUpCompleted;

    public boolean isVerified() {
        return isVerified;
    }

    public boolean isExistingUser() {
        return isExistingUser;
    }

    public void setVerified(boolean verified) {
        isVerified = verified;
    }

    public void setExistingUser(boolean existingUser) {
        isExistingUser = existingUser;
    }

    public boolean isSignedUpCompleted() {
        return isSignedUpCompleted;
    }

    public void setSignedUpCompleted(boolean signedUpCompleted) {
        isSignedUpCompleted = signedUpCompleted;
    }
}
