package com.instake.passenger.monowar.profileonboard;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Tushar on 5/12/17.
 */

public interface UserProfileOnBoardingView extends BaseView {

    void startPagerRotation();
}
