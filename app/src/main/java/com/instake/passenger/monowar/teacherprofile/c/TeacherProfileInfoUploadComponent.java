package com.instake.passenger.monowar.teacherprofile.c;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = TeacherProfileInfoUploadModule.class)
public interface TeacherProfileInfoUploadComponent {
    void inject(TeacherProfileInfoUploadActivity activity);
}
