package com.instake.passenger.monowar.ambassadorprofile.b;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = AmbassadorProfileInfoUploadModule.class)
public interface AmbassadorProfileInfoUploadComponent {
    void inject(AmbassadorProfileInfoUploadActivity activity);
}
