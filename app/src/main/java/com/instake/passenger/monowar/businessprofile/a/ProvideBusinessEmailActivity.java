package com.instake.passenger.monowar.businessprofile.a;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityProvideBusinessEmailBinding;
import com.instake.passenger.monowar.businessprofile.b.ProvideCompanyInfoActivity;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 12-Sep-17.
 */

public class ProvideBusinessEmailActivity extends SiriusActivity {

    public static Activity activity;

    ActivityProvideBusinessEmailBinding binding;

    @Inject
    Validator validator;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_provide_business_email;
    }

    @Override
    protected void initComponents() {
        app.getAppComponent().inject(this);
        binding = (ActivityProvideBusinessEmailBinding) getDataBinding();

        activity = this;

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.tilEmail.setErrorEnabled(false);
                String email = binding.etEmail.getText().toString();
                if (!validator.isNotEmpty(email)) {
                    binding.tilEmail.setError(getString(R.string.error_email_empty));
                    return;
                } else if (!validator.isEmailValid(email)) {
                    binding.tilEmail.setError(getString(R.string.error_invalid_email));
                    return;
                }

                Bundle bundle = new Bundle();
                bundle.putString("BUNDLE_EMAIL", email);
                Intent intent = new Intent(getActivity(), ProvideCompanyInfoActivity.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });
    }
}
