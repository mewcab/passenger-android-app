package com.instake.passenger.monowar.profile;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.monowar.invitefriends.InviteFriendsView;
import com.instake.passenger.monowar.invitefriends.InviteFriendsViewPresenter;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class ProfileModule {

    private ProfileView view;

    public ProfileModule(ProfileView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public ProfilePresenter provideProfilePresenter(ApiService apiService, Session session) {
        return new ProfilePresenter(view, apiService, session);
    }
}
