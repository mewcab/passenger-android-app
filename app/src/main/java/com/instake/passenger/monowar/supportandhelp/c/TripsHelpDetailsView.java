package com.instake.passenger.monowar.supportandhelp.c;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface TripsHelpDetailsView {
    void initFragmentViewPager();
}
