package com.instake.passenger.monowar.supportandhelp.a.fragment.faqs;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Singleton
@Subcomponent(modules = FaqsModule.class)
public interface FaqsComponent {
    void inject(FaqsFragment fragment);
}
