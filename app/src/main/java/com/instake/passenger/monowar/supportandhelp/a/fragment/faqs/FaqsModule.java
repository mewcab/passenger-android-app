package com.instake.passenger.monowar.supportandhelp.a.fragment.faqs;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

@Module
public class FaqsModule {
    private FaqsView view;

    public FaqsModule(FaqsView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public FaqsPresenter provideFaqsPresenter(ApiService apiService, Session session) {
        return new FaqsPresenter(view);
    }
}
