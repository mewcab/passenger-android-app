package com.instake.passenger.monowar.signup.b;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySignupProvideInformationBinding;
import com.instake.passenger.monowar.signup.c.SignupEmailActivity;
import com.theartofdev.edmodo.cropper.CropImage;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class SignupProvideInformationActivity extends SiriusPresenterActivity<SignupProvideInformationView, SignupProvideInformationPresenter> implements SignupProvideInformationView {

    private static final String TAG = "SignupActivity";
    private static final int REQUEST_CODE_ASK_PERMISSIONS = 1;

    ActivitySignupProvideInformationBinding binding;

    @Inject
    SignupProvideInformationPresenter presenter;

    private Bundle bundle;

    private String imageUri;

    public static void open(Activity activity, Bundle bundle) {
        Intent signupIntent = new Intent(activity, SignupProvideInformationActivity.class);
        Log.i(TAG, "Bundle: " + bundle.toString());
        signupIntent.putExtras(bundle);
        activity.startActivity(signupIntent);
    }

    @Override
    protected SignupProvideInformationPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SignupProvideInformationModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_signup_provide_information;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySignupProvideInformationBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        bundle = getIntent().getExtras();

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNextClick();
            }
        });

        binding.ivProfileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onImageClick();
            }
        });
    }

    @Override
    public String getFullName() {
        return binding.tilFirstName.getEditText().getText().toString();
    }

    @Override
    public String getImageUri() {
        return imageUri;
    }

    @Override
    public int getPermissionRequestCode() {
        return REQUEST_CODE_ASK_PERMISSIONS;
    }

    @Override
    public void showFullNameError() {
        binding.tilFirstName.setError(getString(R.string.error_full_name));
    }

    @Override
    public void hideAllErrors() {
        binding.tilFirstName.setErrorEnabled(false);
    }

    @Override
    public void openEmailActivity() {
        bundle.putString("BUNDLE_FULL_NAME", getFullName());
        if (imageUri != null) {
            bundle.putString("BUNDLE_PROFILE_IMAGE_URL", getImageUri());
        }
        SignupEmailActivity.open(this, bundle);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageUri = resultUri.getPath();
                Log.i(TAG, "Image info: " + resultUri.toString());
                Glide.with(getContext().getApplicationContext())
                        .load(resultUri)
                        .dontAnimate()
                        .error(R.drawable.backgroud_select_image)
                        .placeholder(R.drawable.backgroud_select_image)
                        .into(binding.ivProfileImage);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        }
    }
}
