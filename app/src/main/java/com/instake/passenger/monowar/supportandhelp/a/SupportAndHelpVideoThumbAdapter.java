package com.instake.passenger.monowar.supportandhelp.a;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.model.suppotnhelp.Video;

import java.util.List;

/**
 * Created by mm on 10/1/2016.
 */

public class SupportAndHelpVideoThumbAdapter extends RecyclerView.Adapter<SupportAndHelpVideoThumbAdapter.MyViewHolder> {

    private LayoutInflater layoutInflater;
    private List<Video> videoList;
    private Context context;

    public SupportAndHelpVideoThumbAdapter(Context context, List<Video> videoList) {
        layoutInflater = LayoutInflater.from(context);
        this.context = context;
        this.videoList = videoList;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.single_item_video_thumb, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Video video = videoList.get(position);

        holder.tvTitle.setText(video.getTitle());
        holder.tvDuration.setText(String.valueOf(video.getDuration()));

        Glide.with(context)
                .load(video.getImages())
                .placeholder(R.drawable.demo_support)
                .error(R.drawable.demo_support)
                .into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return videoList.size();
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;
        private TextView tvTitle, tvDuration;

        private MyViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.iv_image);
            tvTitle = (TextView) itemView.findViewById(R.id.tv_title);
            tvDuration = (TextView) itemView.findViewById(R.id.tv_duration);
        }
    }
}