package com.instake.passenger.monowar.supportandhelp.c.receipt;

import android.os.Build;
import android.util.TypedValue;
import android.view.View;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusFragment;
import com.instake.passenger.databinding.FragmentReceiptBinding;
import com.instake.passenger.model.historydata.Trip;
import com.instake.passenger.monowar.supportandhelp.c.TripsHelpDetailsActivity;

import org.parceler.Parcels;

import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 21-Aug-17.
 */

public class ReceiptFragment extends SiriusFragment<ReceiptView, ReceiptPresenter> implements ReceiptView {

    private static final int SCROLL_DIRECTION_UP = -1;
    private static final String TAG = "ReceiptFragment";

    private FragmentReceiptBinding binding;

    @Inject
    ReceiptPresenter presenter;

    @Override
    protected ReceiptPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new ReceiptModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_receipt;
    }

    @Override
    protected void initFragmentComponents() {
        binding = (FragmentReceiptBinding) getDataBinding();

        int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_2dp), getResources().getDisplayMetrics());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            binding.scrollView.setOnScrollChangeListener(new View.OnScrollChangeListener() {
                @Override
                public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        if (binding.scrollView.canScrollVertically(SCROLL_DIRECTION_UP)) {
                            ((TripsHelpDetailsActivity) getActivity()).binding.appbar.setElevation(elevation);
                        } else {
                            ((TripsHelpDetailsActivity) getActivity()).binding.appbar.setElevation(0);
                        }

                    }
                }
            });

            Trip trip = (Trip) Parcels.unwrap(getArguments().getParcelable("ARGUMENT_TRIP"));

            binding.tvBaseFare.setText(String.format(Locale.ENGLISH, "+%d Tk", trip.getBaseFareRate()));

            binding.tvDistance.setText(String.format(Locale.ENGLISH, "Distance(%.01f km)", trip.getDistance()));
            binding.tvDistanceFare.setText(String.format(Locale.ENGLISH, "+%d Tk", trip.getDistance().intValue()));

            binding.tvTime.setText(String.format(Locale.ENGLISH, "Time(%.02f m)", trip.getTotalTime()));
            binding.tvTimeFare.setText(String.format(Locale.ENGLISH, "+%d Tk", trip.getTotalTime().intValue()));

            binding.tvFare.setText(String.format(Locale.ENGLISH, "+%d Tk", trip.getSubTotal().intValue()));

            binding.tvDiscountAmount.setText(String.format(Locale.ENGLISH, "-%d Tk", trip.getDiscount().intValue()));

            binding.tvSubtotalAmount.setText(String.format(Locale.ENGLISH, "+%d Tk", trip.getTotalFear()));

            binding.tvTipsAmount.setText(String.format(Locale.ENGLISH, "+%d Tk", trip.getTipsAmount()));

            binding.tvTotalAmount.setText(String.format(Locale.ENGLISH, "+%d Tk", trip.getTotalFear()));

        }
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }
}
