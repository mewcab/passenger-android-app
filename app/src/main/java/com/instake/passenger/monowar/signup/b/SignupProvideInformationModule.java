package com.instake.passenger.monowar.signup.b;

import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class SignupProvideInformationModule {

    private SignupProvideInformationView view;

    public SignupProvideInformationModule(SignupProvideInformationView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SignupProvideInformationPresenter provideSignupProvideInformationPresenter(Validator validator) {
        return new SignupProvideInformationPresenter(view, validator);
    }
}
