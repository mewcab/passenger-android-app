package com.instake.passenger.monowar;

import org.parceler.Parcel;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Parcel
public class SignupResponseData {
    public String phoneNumber, referral, message;

    public SignupResponseData() {
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
