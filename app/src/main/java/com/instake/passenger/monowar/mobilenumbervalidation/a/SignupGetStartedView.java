package com.instake.passenger.monowar.mobilenumbervalidation.a;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

public interface SignupGetStartedView extends BaseView {

    String getPhoneNumber();

    void showPhoneNumberError();

    void hidePhoneNumberError();

    void showVerificationCodeSendingSuccessMessage();

    void showVerificationCodeSendingFailureMessage();

    void onLoginFailed(String error);

    void proceedSignupOTPActivity();

    void proceedToLoginActivity();

    void clearAllActivitiesAndNavigateToSplash();

    void hideKeyboard();
}
