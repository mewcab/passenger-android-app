package com.instake.passenger.monowar.supportandhelp.c.receipt;

import android.util.Log;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

/**
 * Created by Mostafa Monowar on 22-Aug-17.
 */

public class ReceiptPresenter implements BasePresenter<ReceiptView> {
    private static final String TAG = "ReceiptPresenter";

    private WeakReference<ReceiptView> view;
    private ApiService apiService;
    private Session session;

    public ReceiptPresenter(ReceiptView view, ApiService apiService, Session session) {

        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}
