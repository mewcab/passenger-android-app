package com.instake.passenger.monowar.signup.b;

import com.instake.passenger.monowar.signup.a.SignupCompleteProfileActivity;
import com.instake.passenger.monowar.signup.a.SignupCompleteProfileModule;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = SignupProvideInformationModule.class)
public interface SignupProvideInformationComponent {
    void inject(SignupProvideInformationActivity activity);
}