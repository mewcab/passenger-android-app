package com.instake.passenger.monowar.signup.e;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class SignupPasswordModule {

    private SignupPasswordView view;

    public SignupPasswordModule(SignupPasswordView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SignupPasswordPresenter provideSignupPasswordPresenter(Validator validator, ApiService apiService, Session session) {
        return new SignupPasswordPresenter(view, validator, apiService, session);
    }
}
