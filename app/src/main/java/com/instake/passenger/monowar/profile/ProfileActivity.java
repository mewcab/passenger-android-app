package com.instake.passenger.monowar.profile;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityProfileBinding;
import com.instake.passenger.feature.editprofile.EditProfileActivity;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;
import com.instake.passenger.monowar.mobilenumbervalidation.a.SignupGetStartedActivity;
import com.instake.passenger.monowar.profile.switchprofile.SwitchProfileFragment;

import java.util.Arrays;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class ProfileActivity extends SiriusPresenterActivity<ProfileView, ProfilePresenter> implements ProfileView {

    private final static String TAG = "Profile";

    ActivityProfileBinding binding;

    @Inject
    ProfilePresenter presenter;

    private CallbackManager mCallbackManager;
    private Subscription busSubscription;

    private SwitchProfileFragment switchProfileFragment;
    private FragmentManager fragmentManager;
    private FragmentTransaction transaction;

    @Override
    protected ProfilePresenter bindPresenter() {
        app.getAppComponent()
                .plus(new ProfileModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_profile;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityProfileBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        autoUnsubBus();
        rxCallBackGettingUserInfo();

        binding.editProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(EditProfileActivity.class, false);
            }
        });

        binding.connectFacebook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(getActivity(), Arrays.asList("public_profile", "email"));
            }
        });

        binding.personalProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(EditProfileActivity.class, false);
            }
        });

        binding.businessProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.studentProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.teacherProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.ambassadorProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.addProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                binding.switchProfile.performClick();
            }
        });

        binding.switchProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                v.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        switchProfileFragment = new SwitchProfileFragment();
                        fragmentManager = getSupportFragmentManager();
                        transaction = fragmentManager.beginTransaction();
                        transaction.add(R.id.frame_container, switchProfileFragment);
                        transaction.commit();
                    }
                }, 200);
            }
        });

        binding.logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onLogoutClicked();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (switchProfileFragment != null) {
            transaction = fragmentManager.beginTransaction();
            transaction.remove(switchProfileFragment);
            transaction.commit();
            switchProfileFragment = null;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void setUserInfo(User user) {
        binding.tvUserName.setText(user.getFullName());
        binding.tvEmail.setText(user.getEmail());

        if (user.getFacebookId() != null && user.getFacebookId().length() != 0) {
            binding.tvFacebookConnectivityStatus.setText(R.string.facebook_connected);
        } else {
            binding.tvFacebookConnectivityStatus.setText(R.string.facebook_disconnected);
        }

        Log.e(TAG, "ImageURL " + user.getProfileImageUrl());
        Glide.with(getContext().getApplicationContext())
                .load(user.getProfileImageUrl())
                .dontAnimate()
                .error(R.drawable.backgroud_select_image)
                .placeholder(R.drawable.backgroud_select_image)
                .into(binding.ivProfileImage);

        setProfileVerificationInfo(user.getUserProfile());
    }

    @Override
    public void initFacebookComponent() {
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("****", "***Twice****");

                        presenter.onFbLoginSuccess(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e(TAG, exception.toString());
                        LoginManager.getInstance().logOut();
                    }
                });
    }

    @Override
    public void setProfileVerificationInfo(UserProfile userProfile) {
        if (userProfile != null && userProfile.getProfileType() != null) {
            if (userProfile.getProfileType().equalsIgnoreCase("business")) {
                binding.studentProfile.setVisibility(View.GONE);
                binding.teacherProfile.setVisibility(View.GONE);
                binding.businessProfile.setVisibility(View.VISIBLE);
                binding.ambassadorProfile.setVisibility(View.GONE);
                binding.switchProfile.setVisibility(View.VISIBLE);
                binding.addProfile.setVisibility(View.GONE);
                if (userProfile.isVerify()) {
                    binding.tvBusinessProfileVerificationStatus.setText(R.string.verified);
                    binding.tvBusinessProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.deep_green));
                } else {
                    binding.tvBusinessProfileVerificationStatus.setText(R.string.unverified);
                    binding.tvBusinessProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
                }
            } else if (userProfile.getProfileType().equalsIgnoreCase("student")) {
                binding.studentProfile.setVisibility(View.VISIBLE);
                binding.teacherProfile.setVisibility(View.GONE);
                binding.businessProfile.setVisibility(View.GONE);
                binding.ambassadorProfile.setVisibility(View.GONE);
                binding.switchProfile.setVisibility(View.VISIBLE);
                binding.addProfile.setVisibility(View.GONE);
                if (userProfile.isVerify()) {
                    binding.tvStudentProfileVerificationStatus.setText(R.string.verified);
                    binding.tvStudentProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.deep_green));
                } else {
                    binding.tvStudentProfileVerificationStatus.setText(R.string.unverified);
                    binding.tvStudentProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
                }
            } else if (userProfile.getProfileType().equalsIgnoreCase("teacher")) {
                binding.studentProfile.setVisibility(View.GONE);
                binding.teacherProfile.setVisibility(View.VISIBLE);
                binding.businessProfile.setVisibility(View.GONE);
                binding.ambassadorProfile.setVisibility(View.GONE);
                binding.switchProfile.setVisibility(View.VISIBLE);
                binding.addProfile.setVisibility(View.GONE);
                if (userProfile.isVerify()) {
                    binding.tvTeacherProfileVerificationStatus.setText(R.string.verified);
                    binding.tvTeacherProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.deep_green));
                } else {
                    binding.tvTeacherProfileVerificationStatus.setText(R.string.unverified);
                    binding.tvTeacherProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
                }
            } else if (userProfile.getProfileType().equalsIgnoreCase("ambassador")) {
                binding.studentProfile.setVisibility(View.GONE);
                binding.teacherProfile.setVisibility(View.GONE);
                binding.businessProfile.setVisibility(View.GONE);
                binding.ambassadorProfile.setVisibility(View.VISIBLE);
                binding.switchProfile.setVisibility(View.VISIBLE);
                binding.addProfile.setVisibility(View.GONE);
                if (userProfile.isVerify()) {
                    binding.tvAmbassadorProfileVerificationStatus.setText(R.string.verified);
                    binding.tvAmbassadorProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.deep_green));
                } else {
                    binding.tvAmbassadorProfileVerificationStatus.setText(R.string.unverified);
                    binding.tvAmbassadorProfileVerificationStatus.setTextColor(ContextCompat.getColor(getContext(), R.color.red));
                }
            }
        } else {
            binding.studentProfile.setVisibility(View.GONE);
            binding.teacherProfile.setVisibility(View.GONE);
            binding.businessProfile.setVisibility(View.GONE);
            binding.ambassadorProfile.setVisibility(View.GONE);
            binding.switchProfile.setVisibility(View.GONE);
            binding.addProfile.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void setFacebookConnectivity(String facebookId) {
        if (facebookId != null && facebookId.length() != 0) {
            binding.tvFacebookConnectivityStatus.setText(R.string.facebook_connected);
        } else {
            binding.tvFacebookConnectivityStatus.setText(R.string.facebook_disconnected);
        }
    }

    @Override
    public void proceedToGetStartedActivityAndCloseAll() {
        SignupGetStartedActivity.clearAllScreensAndOpen(this);
    }

    private void autoUnsubBus() {
        if (busSubscription != null && !busSubscription.isUnsubscribed()) {
            busSubscription.unsubscribe();
        }
    }

    private void rxCallBackGettingUserInfo() {
        busSubscription = App.getInstance().bus().toObserverable1().observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object event) {
                        if (event instanceof User) {
                            setUserInfo((User) event);
                        } else if (event instanceof UserProfile) {
                            setProfileVerificationInfo((UserProfile) event);
                        }
                    }
                });
    }
}