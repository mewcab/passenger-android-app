package com.instake.passenger.monowar.signup.e;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public interface SignupPasswordView extends BaseView {
    String getToken();
    String getPhoneNumber();
    String getVerificationCode();
    String getFullName();
    String getEmail();
    String getReferralCode();
    String getGender();
    String getBirthDate();
    String getPassword();
    String getImageUri();
    String getFBImageUrl();
    String getFbId();
    boolean getIsFBSignUp();
    void showPasswordError();
    void hideAllErrors();
    void signupFailed(String message);
    void signupFailed();
    void signupSuccess();
    void proceedToSplashActivity();
    void hideKeyboard();
}
