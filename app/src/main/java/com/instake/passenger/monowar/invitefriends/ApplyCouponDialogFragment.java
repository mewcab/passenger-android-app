package com.instake.passenger.monowar.invitefriends;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import com.instake.passenger.R;
import com.instake.passenger.databinding.DialogApplyCouponBinding;

/**
 * Created by Mostafa Monowar on 25-Aug-17.
 */

public class ApplyCouponDialogFragment extends DialogFragment {
    public static final String TAG = ApplyCouponDialogFragment.class.getSimpleName();

    private DialogApplyCouponBinding dataBinding;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DialogFloating);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        dataBinding = DataBindingUtil.inflate(LayoutInflater.from(getActivity()), R.layout.dialog_apply_coupon, container, false);

        getDialog().requestWindowFeature(Window.FEATURE_NO_TITLE);
        getDialog().setCanceledOnTouchOutside(false);

        dataBinding.cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        return dataBinding.getRoot();
    }
}
