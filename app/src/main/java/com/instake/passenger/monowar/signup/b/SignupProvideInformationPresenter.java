package com.instake.passenger.monowar.signup.b;

import android.Manifest;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.instake.passenger.R;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.utility.validation.Validator;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class SignupProvideInformationPresenter implements BasePresenter<SignupProvideInformationView> {

    private WeakReference<SignupProvideInformationView> view;
    private Validator validator;

    public SignupProvideInformationPresenter(SignupProvideInformationView view, Validator validator) {
        this.view = new WeakReference<>(view);
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {
        if (view != null) {
            checkPermission();
        }
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onNextClick() {
        view.get().hideAllErrors();

        boolean hasError = false;
        if (!validator.isNotEmpty(view.get().getFullName())) {
            view.get().showFullNameError();
            hasError = true;
        }
        if (hasError) {
            return;
        }

        view.get().openEmailActivity();
    }

    public void onImageClick() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle(view.get().getContext().getString(R.string.title_activity_crop_image))
                .start(view.get().getActivity());
    }

    public void checkPermission() {
        final int hasCameraPermission = ContextCompat.checkSelfPermission(view.get().getContext(), android.Manifest.permission.CAMERA);
        final int hasStoragePermission = ContextCompat.checkSelfPermission(view.get().getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE);
        final List<String> listPermissionNeeded = new ArrayList<>();
        if (hasCameraPermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionNeeded.add(android.Manifest.permission.CAMERA);
        }
        if (hasStoragePermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionNeeded.add(android.Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }
        if (!listPermissionNeeded.isEmpty()) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(view.get().getActivity(), Manifest.permission.CAMERA) ||
                    ActivityCompat.shouldShowRequestPermissionRationale(view.get().getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                showAlertPermission(view.get().getContext().getString(R.string.alert_message_profile_image), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case DialogInterface.BUTTON_POSITIVE:
                                ActivityCompat.requestPermissions(view.get().getActivity(), listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]), view.get().getPermissionRequestCode());
                                break;
                            case DialogInterface.BUTTON_NEGATIVE:
                                Toast.makeText(view.get().getContext(), view.get().getContext().getString(R.string.toast_suggestion), Toast.LENGTH_SHORT).show();
                                break;
                        }
                    }
                });
            } else {
                ActivityCompat.requestPermissions(view.get().getActivity(), listPermissionNeeded.toArray(new String[listPermissionNeeded.size()]), view.get().getPermissionRequestCode());
            }
        }
    }

    public void showAlertPermission(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(view.get().getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }
}
