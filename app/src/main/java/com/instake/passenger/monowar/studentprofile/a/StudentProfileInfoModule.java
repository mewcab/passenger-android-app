package com.instake.passenger.monowar.studentprofile.a;

import com.instake.passenger.api.ApiService;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class StudentProfileInfoModule {

    private StudentProfileInfoView view;

    public StudentProfileInfoModule(StudentProfileInfoView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public StudentProfileInfoPresenter provideStudentProfileInfoPresenter(ApiService apiService) {
        return new StudentProfileInfoPresenter(view, apiService);
    }
}
