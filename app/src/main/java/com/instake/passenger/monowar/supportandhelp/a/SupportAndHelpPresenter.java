package com.instake.passenger.monowar.supportandhelp.a;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;
import com.instake.passenger.model.suppotnhelp.SupportAndHelpResponse;

import java.lang.ref.WeakReference;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

public class SupportAndHelpPresenter implements BasePresenter<SupportAndHelpView> {

    private WeakReference<SupportAndHelpView> view;
    private ApiService apiService;

    public SupportAndHelpPresenter(SupportAndHelpView view, ApiService apiService) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
    }

    @Override
    public void onCreatePresenter() {
        getApiData();
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    private void getApiData() {
        view.get().showLoading();
        apiService.getSupportAndHelp()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(Observable::error)
                .subscribe(new Subscriber<SupportAndHelpResponse>() {
                    @Override
                    public void onCompleted() {
                        view.get().hideLoading();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(SupportAndHelpResponse supportAndHelpResponse) {
                        view.get().loadDataIntoAdapter(supportAndHelpResponse.getVideos());
                        App.getInstance().bus().send(supportAndHelpResponse);
                    }
                });
    }

}
