package com.instake.passenger.monowar.studentprofile.a;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityStudentProfileInfoBinding;
import com.instake.passenger.model.EducationDetails;
import com.instake.passenger.monowar.studentprofile.b.StudentProfileInfoUploadActivity;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class StudentProfileInfoActivity extends SiriusPresenterActivity<StudentProfileInfoView, StudentProfileInfoPresenter> implements StudentProfileInfoView {

    private final static String TAG = "EducationProfileInfo";

    public static Activity activity;

    ActivityStudentProfileInfoBinding binding;

    @Inject
    StudentProfileInfoPresenter presenter;

    private String className;

    @Override
    protected StudentProfileInfoPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new StudentProfileInfoModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_student_profile_info;
    }

    @Override
    protected void initComponents() {
        activity = this;

        binding = (ActivityStudentProfileInfoBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.spinnerClass.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.e(TAG, parent.getItemAtPosition(position).toString());
                className = parent.getItemAtPosition(position).toString();
            }
        });

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onNextClick();
            }
        });
    }

    @Override
    public void generateEducationDetailsSpinner(EducationDetails educationDetails) {
        ArrayAdapter<String> adapterClass = new ArrayAdapter<String>(this, android.R.layout.simple_dropdown_item_1line, educationDetails.getClassList());
        binding.spinnerClass.setAdapter(adapterClass);
    }

    @Override
    public String getInstitutionName() {
        return binding.etInstitutionName.getText().toString();
    }

    @Override
    public String getClassName() {
        return className;
    }

    @Override
    public void showInstitutionNameError() {
        binding.tilInstitutionName.setError(getString(R.string.error_institution_name));
    }

    @Override
    public void showClassNameError() {
        binding.spinnerClass.setError(getString(R.string.error_class_name));
    }

    @Override
    public void hideAllErrors() {
        binding.tilInstitutionName.setErrorEnabled(false);
        binding.spinnerClass.setError(null);
    }

    @Override
    public void proceedToUploadIdCardActivity() {
        Bundle bundle = new Bundle();
        bundle.putString("BUNDLE_INSTITUTION_NAME", getInstitutionName());
        bundle.putString("BUNDLE_CLASS_NAME", getClassName());
        Log.e(TAG, bundle.toString());
        Intent intent = new Intent(getActivity(), StudentProfileInfoUploadActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
    }
}
