package com.instake.passenger.monowar.mobilenumbervalidation.b;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 14-Aug-17.
 */

@Module
public class SignupOtpModule {
    SignupOtpView view;

    public SignupOtpModule(SignupOtpView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SignupOtpPresenter provideSignupOtpPresenter(ApiService apiService, Validator validator, Session session) {
        return new SignupOtpPresenter(view, apiService, validator, session);
    }
}
