package com.instake.passenger.monowar.profile.switchprofile;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;

import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.databinding.FragmentSwitchProfileBinding;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 11-Sep-17.
 */

public class SwitchProfileFragment extends Fragment {

    FragmentSwitchProfileBinding binding;

    @Inject
    Session session;

    private SwitchProfilePagerAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_switch_profile, container, false);

        App.getInstance().getAppComponent().inject(this);

        adapter = new SwitchProfilePagerAdapter(getActivity(), session.getUser().getUserProfile());
        binding.viewPager.setAdapter(adapter);
        binding.viewPager.setOffscreenPageLimit(3);
        binding.viewPager.setClipToPadding(false);
        binding.viewPager.setPadding((int) getResources().getDimension(R.dimen.size_30dp), 0, (int) getResources().getDimension(R.dimen.size_30dp), 0);
        binding.viewPager.setPageMargin((int) getResources().getDimension(R.dimen.size_15dp));
        binding.viewPager.addOnPageChangeListener(mOnPageChangeListener);

        adapter.setOnFragmentCloseListener(new SwitchProfilePagerAdapter.OnFragmentCloseListener() {
            @Override
            public void fragmentClose(boolean close) {
                getActivity().onBackPressed();
            }
        });

        return binding.getRoot();
    }

    private final ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {
        @Override
        public void onPageSelected(int position) {
            switch (position) {
                case 0:
                    ((RadioButton) getActivity().findViewById(R.id.rb_one)).setChecked(true);
                    ((RadioButton) getActivity().findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_four)).setChecked(false);
                    break;
                case 1:
                    ((RadioButton) getActivity().findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_two)).setChecked(true);
                    ((RadioButton) getActivity().findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_four)).setChecked(false);
                    break;
                case 2:
                    ((RadioButton) getActivity().findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_three)).setChecked(true);
                    ((RadioButton) getActivity().findViewById(R.id.rb_four)).setChecked(false);
                    break;
                case 3:
                    ((RadioButton) getActivity().findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) getActivity().findViewById(R.id.rb_four)).setChecked(true);
                    break;
            }
        }
    };
}
