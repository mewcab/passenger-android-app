package com.instake.passenger.monowar.signup.b;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public interface SignupProvideInformationView extends BaseView {

    String getFullName();
    String getImageUri();
    int getPermissionRequestCode();
    void showFullNameError();
    void hideAllErrors();
    void openEmailActivity();
}
