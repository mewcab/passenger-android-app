package com.instake.passenger.monowar.supportandhelp.a;

import android.graphics.Color;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySupportAndHelpBinding;
import com.instake.passenger.model.suppotnhelp.Video;
import com.instake.passenger.monowar.EqualSpacingItemDecoration;
import com.instake.passenger.monowar.RecyclerTouchListener;

import java.util.List;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 20-Aug-17.
 */

public class SupportAndHelpActivity extends SiriusPresenterActivity<SupportAndHelpView, SupportAndHelpPresenter> implements SupportAndHelpView {

    public ActivitySupportAndHelpBinding binding;

    private List<Video> videoList;
    private SupportAndHelpVideoThumbAdapter supportAndHelpVideoThumbAdapter;

    @Inject
    SupportAndHelpPresenter presenter;

    @Override
    protected SupportAndHelpPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SupportAndHelpModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_support_and_help;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySupportAndHelpBinding) getDataBinding();

        binding.collapsingToolbarLayout.setTitle(getString(R.string.title_activity_support_and_help));
        binding.collapsingToolbarLayout.setExpandedTitleColor(Color.TRANSPARENT);
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        initFragmentViewPager();

        initRecyclerViewHorizontal();
    }

    @Override
    public void loadDataIntoAdapter(List<Video> video_list) {
        videoList = video_list;
        supportAndHelpVideoThumbAdapter = new SupportAndHelpVideoThumbAdapter(getActivity(), videoList);
        binding.recyclerViewHorizontal.setAdapter(supportAndHelpVideoThumbAdapter);
    }

    @Override
    public void showLoading() {
        binding.recyclerViewHorizontal.setVisibility(View.GONE);
        binding.layoutProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.recyclerViewHorizontal.setVisibility(View.VISIBLE);
        binding.layoutProgress.setVisibility(View.GONE);
    }

    private void initFragmentViewPager() {
        SupportAndHelpPagerAdapter supportAndHelpPagerAdapter = new SupportAndHelpPagerAdapter(getContext(), getSupportFragmentManager());
        binding.viewPager.setAdapter(supportAndHelpPagerAdapter);
        binding.tabLayout.setupWithViewPager(binding.viewPager);
    }

    private void initRecyclerViewHorizontal() {
        binding.recyclerViewHorizontal.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false));
        binding.recyclerViewHorizontal.addItemDecoration(new EqualSpacingItemDecoration(30, EqualSpacingItemDecoration.HORIZONTAL));
        binding.recyclerViewHorizontal.setItemAnimator(new DefaultItemAnimator());

        binding.recyclerViewHorizontal.addOnItemTouchListener(new RecyclerTouchListener(getContext(), binding.recyclerViewHorizontal, new RecyclerTouchListener.OnRecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                view.postDelayed(() -> openUrl(videoList.get(position).getUrl()), 200);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
}
