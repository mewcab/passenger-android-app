package com.instake.passenger.monowar.businessprofile.c;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = BusinessProfileInfoUploadModule.class)
public interface BusinessProfileInfoUploadComponent {
    void inject(BusinessProfileInfoUploadActivity activity);
}
