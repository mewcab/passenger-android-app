package com.instake.passenger.monowar.businessprofile.c;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface BusinessProfileInfoUploadView extends BaseView {
    String getEmail();
    String getCompanyName();
    String getCompanyAddress();
    String getIdImageUri();
    void showFullScreenImage();
    void hideFullScreenImage();
    void showImageError();
    void showUploadInfoFailed();
    void showUploadInfoSuccess();
    void backToProfileActivity();
}
