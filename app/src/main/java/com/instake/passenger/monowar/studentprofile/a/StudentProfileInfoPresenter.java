package com.instake.passenger.monowar.studentprofile.a;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.model.EducationDetails;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class StudentProfileInfoPresenter implements BasePresenter<StudentProfileInfoView> {

    private final static String TAG = "InviteFriends";

    private WeakReference<StudentProfileInfoView> view;
    private ApiService apiService;

    public StudentProfileInfoPresenter(StudentProfileInfoView view, ApiService apiService) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
    }

    @Override
    public void onCreatePresenter() {
        getEducationDetails();
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onNextClick() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().proceedToUploadIdCardActivity();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (view.get().getInstitutionName() == null) {
            view.get().showInstitutionNameError();
            isOkay = false;
        }
        if (view.get().getClassName() == null) {
            view.get().showClassNameError();
            isOkay = false;
        }
        return isOkay;
    }

    private void getEducationDetails() {
        apiService.getEducationDetails()
                .enqueue(new Callback<EducationDetails>() {
                    @Override
                    public void onResponse(Call<EducationDetails> call, Response<EducationDetails> response) {
                        if (response.isSuccessful() && response.body() != null) {
                            EducationDetails educationDetails = response.body();
                            view.get().generateEducationDetailsSpinner(educationDetails);
                        }
                    }

                    @Override
                    public void onFailure(Call<EducationDetails> call, Throwable t) {

                    }
                });
    }
}
