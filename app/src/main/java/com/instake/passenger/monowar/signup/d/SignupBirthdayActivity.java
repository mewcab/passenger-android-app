package com.instake.passenger.monowar.signup.d;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySignupBirthdayBinding;
import com.instake.passenger.monowar.HoloDatePickerDialog;
import com.instake.passenger.monowar.signup.e.SignupPasswordActivity;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 10-Aug-17.
 */

public class SignupBirthdayActivity extends SiriusPresenterActivity<SignupBirthdayView, SignupBirthdayPresenter> implements SignupBirthdayView {

    private final static String TAG = "SignupActivity";

    ActivitySignupBirthdayBinding binding;

    @Inject
    SignupBirthdayPresenter presenter;

    private Bundle bundle;
    private Calendar calendar;

    private String gender, birthDate;

    public static void open(Activity activity, Bundle bundle) {
        Intent signupIntent = new Intent(activity, SignupBirthdayActivity.class);
        Log.i(TAG, "Bundle: " + bundle.toString());
        signupIntent.putExtras(bundle);
        activity.startActivity(signupIntent);
    }

    @Override
    protected SignupBirthdayPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SignupBirthdayModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_signup_birthday;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySignupBirthdayBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        Context themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog);

        bundle = getIntent().getExtras();
        calendar = Calendar.getInstance();
        calendar.add(Calendar.YEAR, -13);

        binding.buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onClickNext();
            }
        });

        binding.layoutBirthDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new HoloDatePickerDialog(themedContext, datePickerListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        binding.radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb_temp = (RadioButton) group.findViewById(checkedId);
                if (rb_temp != null && checkedId > -1) {
                    gender = rb_temp.getText().toString();
                }

            }
        });
    }

    @Override
    public String getGender() {
        return gender;
    }

    @Override
    public String getBirthDate() {
        return birthDate;
    }

    @Override
    public void showGenderError() {
        binding.tvGenderError.setVisibility(View.VISIBLE);
    }

    @Override
    public void showBirthDateError() {
        binding.tvBirthDateError.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideAllErrors() {
        binding.tvGenderError.setVisibility(View.GONE);
        binding.tvBirthDateError.setVisibility(View.GONE);
    }

    @Override
    public void proceedToPasswordActivity() {
        bundle.putString("BUNDLE_GENDER", getGender());
        bundle.putString("BUNDLE_BIRTH_DATE", getBirthDate());
        SignupPasswordActivity.open(this, bundle);
    }

    @Override
    public void skipToPasswordActivity() {
        startActivity(SignupPasswordActivity.class, false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.skip, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_save:
                skipToPasswordActivity();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            // TODO Auto-generated method stub
            calendar.set(Calendar.YEAR, year);
            calendar.set(Calendar.MONTH, monthOfYear);
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            SimpleDateFormat viewFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.getDefault());
            SimpleDateFormat serverFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

            binding.tvBirthDate.setText(viewFormat.format(calendar.getTime()));
            birthDate = serverFormat.format(calendar.getTime());
        }
    };
}
