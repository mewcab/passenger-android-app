package com.instake.passenger.feature.nointernetdialog;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AlertDialog;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusDialogFragment;
import com.instake.passenger.databinding.DialogNoInternetBinding;

/**
 * Created by Mostafa Monowar on 5/10/17.
 */

public class NoInternetDialogFragment extends SiriusDialogFragment {

    public static final String TAG = NoInternetDialogFragment.class.getName();

    private DialogNoInternetBinding binding;

    private String number = "+8801757262754";

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_no_internet;
    }

    @Override
    protected AlertDialog initDialogComponents() {
        binding = (DialogNoInternetBinding) getDataBinding();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.DialogFullScreen);
        final AlertDialog dialog = builder.create();
        dialog.setContentView(getView());

        binding.btnClose.setOnClickListener(view -> dialog.dismiss());

        binding.btnCall.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + number));
            startActivity(intent);
        });

        dialog.show();
        return dialog;
    }
}

