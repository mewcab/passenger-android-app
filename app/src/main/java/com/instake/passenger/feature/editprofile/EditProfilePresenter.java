package com.instake.passenger.feature.editprofile;

import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.instake.passenger.R;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.EditProfile;
import com.instake.passenger.model.User;
import com.instake.passenger.monowar.ImageUploadListener;
import com.instake.passenger.monowar.ProgressRequestBody;
import com.instake.passenger.utility.photocompressor.ImageCompressionUtil;
import com.instake.passenger.utility.validation.Validator;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

public class EditProfilePresenter implements BasePresenter<EditProfileView> {

    private final static String TAG = "EditProfilePresenter";

    private WeakReference<EditProfileView> view;
    private ApiService apiService;
    private Session session;
    private Validator validator;

    private String fbProfileImageUrl;
    private Integer requiredWidth = 500;
    private Integer requiredHeight = 500;

    public EditProfilePresenter(EditProfileView view, ApiService apiService, Session session, Validator validator) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {
        User user = session.getUser();
        view.get().setUserInfo(user);
        view.get().initFacebookComponent();
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onSaveClick() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().hideKeyboard();
            view.get().showLoader();
            editProfile();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (!validator.isPhoneNumberValid(view.get().getPhoneNumber()) && !validator.isPhoneNumberLengthValid(view.get().getPhoneNumber())) {
            view.get().showPhoneNumberError();
            isOkay = false;
        }
        if (!validator.isNotEmpty(view.get().getFullName())) {
            view.get().showFullNameError();
            isOkay = false;
        }
        if (!validator.isNotEmpty(view.get().getEmail())) {
            view.get().showEmailError(view.get().getContext().getString(R.string.error_email_empty));
            isOkay = false;
        }
        if (validator.isNotEmpty(view.get().getEmail()) && !validator.isEmailValid(view.get().getEmail())) {
            view.get().showEmailError(view.get().getContext().getString(R.string.error_invalid_email));
            isOkay = false;
        }
        return isOkay;
    }

    public void onImageClick() {
        CropImage.activity()
                .setGuidelines(CropImageView.Guidelines.ON)
                .setActivityTitle(view.get().getContext().getString(R.string.title_activity_crop_image))
                .start(view.get().getActivity());
    }

    private void editProfile() {
        apiService.editProfile(session.getToken(), getEditProfileRequestBody())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(Observable::error)
                .subscribe(new Subscriber<EditProfile>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, e.toString());
                        onEditProfileFailure(e.getMessage());
                    }

                    @Override
                    public void onNext(EditProfile editProfile) {
                        User user = session.getUser();

                        user.id = editProfile.getId();
                        user.referralCode = editProfile.getReferralCode();
                        user.fullName = editProfile.getFullName();
                        user.email = editProfile.getEmail();
                        user.phoneNumber = editProfile.getPhoneNumber();
                        user.profileImageUrl = editProfile.getProfileImageUrl();
                        user.isActive = editProfile.isActive();
                        user.userProfile = editProfile.getUserProfile();

                        session.saveUser(user);
                        App.getInstance().bus().send(user);

                        onEditProfileSuccess(editProfile);
                    }
                });
    }

    private RequestBody getEditProfileRequestBody() {
        return RequestBody.create(MediaType.parse("application/json"),
                JsonBuilder.createEditProfileBody(view.get().getPhoneNumber(), view.get().getFbProfileImageUrl(),
                        view.get().getFullName(), view.get().getEmail()));
    }

    private void onEditProfileSuccess(EditProfile user) {
        if (user != null) {
            if (view.get().getImageUri() != null && view.get().getImageUri().length() != 0) {
                File file = new File(view.get().getImageUri());
                ImageCompressionUtil.getResizedImageUri(
                        compressedFile -> uploadImage(compressedFile, new ImageUploadListener() {
                            @Override
                            public void onProgress(int progress) {

                            }

                            @Override
                            public void onSuccess() {
                                view.get().hideLoader();
                                view.get().goBackToProfileActivity();
                            }

                            @Override
                            public void onFailure() {

                            }
                        }, "profile_images"),
                        view.get().getContext(),
                        Uri.fromFile(file),
                        requiredWidth, requiredHeight);
            } else {
                view.get().hideLoader();
                view.get().goBackToProfileActivity();
            }

        }
    }

    private void onEditProfileFailure(String error) {
        view.get().hideLoader();
        view.get().editProfileFailed(error);
    }

    private void uploadImage(File file, ImageUploadListener listener, String category) {
        apiService.uploadImage(session.getToken(), getMultipartImage(file, listener), getCategory(category))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(Observable::error)
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        view.get().hideLoader();
                        view.get().editProfileFailed(e.getMessage());
                        view.get().goBackToProfileActivity();
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        User user = session.getUser();
                        try {
                            String response = responseBody.string();
                            Log.e(TAG, "responseBody Image " + response);
                            JSONObject jsonObject = new JSONObject(response);
                            user.profileImageUrl = jsonObject.getJSONObject("images").getString("150X150");
                        } catch (IOException | JSONException e) {
                            e.printStackTrace();
                        }
                        session.saveUser(user);
                        App.getInstance().bus().send(user);
                        listener.onSuccess();
                    }
                });
    }

    private MultipartBody.Part getMultipartImage(File profilePhoto, ImageUploadListener listener) {
        return MultipartBody.Part.createFormData("upload", profilePhoto.getName(),
                new ProgressRequestBody(profilePhoto, listener));
    }

    private RequestBody getCategory(String category) {
        return RequestBody.create(MediaType.parse("text/plain"), category);
    }

    public void onFbLoginSuccess(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            JSONObject json = response.getJSONObject();
            try {
                if (json != null) {
                    if (json.has("picture") && json.getJSONObject("picture").has("data") && json.getJSONObject("picture").getJSONObject("data").has("url")) {
                        fbProfileImageUrl = json.getJSONObject("picture").getJSONObject("data").getString("url");
                        Log.e(TAG, "Facebook image: " + fbProfileImageUrl);
                    }
                    view.get().setFbProfileImageUrl(fbProfileImageUrl);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id, first_name, last_name, email, link, birthday, picture.width(200).height(200), gender");
        request.setParameters(parameters);
        request.executeAsync();
    }
}
