package com.instake.passenger.feature.vehicledetails;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.SparseArray;
import android.view.ViewGroup;

import com.instake.passenger.R;
import com.instake.passenger.common.views.InstakeCircularSlideLayout;
import com.instake.passenger.model.driver.CarType;

import org.parceler.Parcels;

import java.util.List;

public class InstakeCircularPagerAdapter extends FragmentPagerAdapter implements ViewPager.OnPageChangeListener {

    public final static float BIG_SLIDE = 1f;
    public final static float SMALL_SLIDE = 0.90f;
    public final static float DIFF_SLIDE = BIG_SLIDE - SMALL_SLIDE;
    private Context context;
    private FragmentManager fragmentManager;
    private SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();
    private List<CarType> vehicleList;

    private float scale;

    public InstakeCircularPagerAdapter(Context context, FragmentManager fm, List<CarType> vehicleList) {
        super(fm);
        this.fragmentManager = fm;
        this.context = context;
        this.vehicleList = vehicleList;
    }

    @Override
    public Fragment getItem(int position) {
        scale = SMALL_SLIDE;
        return VehicleInfoFragment.newInstance(context, vehicleList.get(position), position, scale);
    }

    @Override
    public int getCount() {
        return vehicleList.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        registeredFragments.put(position, fragment);
        return fragment;
    }

    public Fragment getRegisteredFragment(int position) {
        return registeredFragments.get(position);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        Log.e("position: ", "" + positionOffset);
        Log.e("position no: ", "" + position);
        //CarouselLinearLayout next1 = getRootView(position + 1);
       // next1.setScale(2f);

        try {
            if (positionOffset >= 0f && positionOffset <= 1f) {
                InstakeCircularSlideLayout cur =(InstakeCircularSlideLayout)  getRegisteredFragment(position).getView().findViewById(R.id.single_item_vehicle_details);
                InstakeCircularSlideLayout next = (InstakeCircularSlideLayout)  getRegisteredFragment(position + 1).getView().findViewById(R.id.single_item_vehicle_details);

                cur.setScale(BIG_SLIDE - DIFF_SLIDE * positionOffset);
                next.setScale(SMALL_SLIDE + DIFF_SLIDE * positionOffset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onPageSelected(int position) {

    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

}