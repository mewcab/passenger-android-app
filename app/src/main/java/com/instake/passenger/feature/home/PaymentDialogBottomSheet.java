package com.instake.passenger.feature.home;

import android.app.Activity;
import android.support.design.widget.BottomSheetDialog;
import android.view.View;
import android.widget.TextView;

import com.instake.passenger.R;

/**
 * Created by masum on 7/8/17.
 */

public class PaymentDialogBottomSheet {
    BottomSheetDialog dialog;
    View sheetViewPayment;
    TextView buttonCash;
    TextView buttonInstakePay;
    TextView buttonCardPay;
    TextView buttonAddNewPayment;

    public PaymentDialogBottomSheet(Activity context) {
        dialog = new BottomSheetDialog(context);
        sheetViewPayment = context.getLayoutInflater().inflate(R.layout.layout_payment_selection, null);

        dialog.setContentView(sheetViewPayment);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);

        buttonCash = (TextView) sheetViewPayment.findViewById(R.id.buttonCash);
        buttonInstakePay = (TextView) sheetViewPayment.findViewById(R.id.buttonInstakePay);
        buttonCardPay = (TextView) sheetViewPayment.findViewById(R.id.buttonCardPay);
        buttonAddNewPayment = (TextView) sheetViewPayment.findViewById(R.id.buttonAddPaymentMethod);
        buttonAddNewPayment.setVisibility(View.GONE);

    }


}
