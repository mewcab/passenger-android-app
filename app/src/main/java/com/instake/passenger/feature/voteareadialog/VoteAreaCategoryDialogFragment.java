package com.instake.passenger.feature.voteareadialog;

import android.app.Dialog;
import android.support.v7.app.AlertDialog;
import android.view.ViewGroup;
import android.view.Window;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusDialogFragment;
import com.instake.passenger.databinding.DialogVoteCategoryBinding;

/**
 * Created by Tushar on 5/10/17.
 */

public class VoteAreaCategoryDialogFragment extends SiriusDialogFragment {

    public static final String TAG = VoteAreaCategoryDialogFragment.class.getName();

    private DialogVoteCategoryBinding binding;

    @Override
    public void onStart() {
        super.onStart();
        Dialog d = getDialog();
        if (d != null) {
            int width = ViewGroup.LayoutParams.MATCH_PARENT;
            int height = ViewGroup.LayoutParams.MATCH_PARENT;
            d.getWindow().setLayout(width, height);
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_vote_category;
    }

    @Override
    protected AlertDialog initDialogComponents() {
        binding = (DialogVoteCategoryBinding) getDataBinding();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
        AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(getView());
        dialog.show();
        return dialog;
    }
}
