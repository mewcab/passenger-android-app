package com.instake.passenger.feature.signup.confirmaccount;

import android.os.Bundle;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.model.User;
import com.instake.passenger.utility.validation.StringUtil;
import com.instake.passenger.utility.validation.Validator;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.instake.passenger.api.JsonBuilder.createSignUpBody;

/**
 * Created by Tushar on 5/12/17.
 */

public class ConfirmAccountPresenter implements BasePresenter<ConfirmAccountView> {
    private WeakReference<ConfirmAccountView> view;
    private ApiService apiService;
    private String fbId;
    private String fbName;
    private String fbEmail;
    private String fbProfilePhotoUrl;
    private Validator validator;

    public ConfirmAccountPresenter(ConfirmAccountView confirmAccountView, Validator validator,
                                   ApiService apiService) {
        this.view = new WeakReference<>(confirmAccountView);
        this.apiService = apiService;
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {
        if (view != null) {
            view.get().initFbLoginComponent();
        }
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onFbLoginSuccess(AccessToken accessToken) {
        GraphRequest request = GraphRequest.newMeRequest(accessToken, (object, response) -> {
            JSONObject json = response.getJSONObject();
            try {
                if (json != null) {
                    if (json.has("id")) {
                        fbId = json.getString("id");
                    }
                    if (json.has("name")) {
                        fbName = json.getString("name");
                        if (StringUtil.isNotEmpty(fbName)) {
                            view.get().setNameOnView(fbName);
                        }
                    }
                    if (json.has("email")) {
                        fbEmail = json.getString("email");
                        if (StringUtil.isNotEmpty(fbEmail)) {
                            view.get().setEmailOnView(fbEmail);
                        }
                    }
                    if (json.has("picture") && json.getJSONObject("picture").has("data") &&
                            json.getJSONObject("picture").getJSONObject("data").has("url")) {
                        fbProfilePhotoUrl = json.getJSONObject("picture")
                                .getJSONObject("data")
                                .getString("url");
                    }
                }
            } catch (JSONException e) {

            }
        });

        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,email,link,birthday,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }

    // TODO: when user will log out from our app user should be logged out from facebook sdk also
    public void logOutFromFacebook() {
        if (AccessToken.getCurrentAccessToken() == null) {
            return;
        }

        new GraphRequest(AccessToken.getCurrentAccessToken(), "/me/permissions/", null, HttpMethod.DELETE, graphResponse ->
                LoginManager.getInstance().logOut()).executeAsync();
    }

    public void onConfirmClick() {
        hideErrorsIfShown();
        if (isInputValidationOkay()) {
            signUpUser();
        }
    }

    private void hideErrorsIfShown() {
        if (validator.isNotEmpty(view.get().getEmailError())) {
            view.get().hideEmailError();
        }

        if (validator.isNotEmpty(view.get().getFullNameError())) {
            view.get().hideFullNameError();
        }

        if (validator.isNotEmpty(view.get().getReferralCodeError())) {
            view.get().hideReferralCodeError();
        }
    }

    private boolean isInputValidationOkay() {
        boolean isOkay = true;
        if (!validator.isNotEmpty(view.get().getEmail())) {
            view.get().onErrorEmailEmpty();
            isOkay = false;
        }

        if (!validator.isEmailValid(view.get().getEmail())) {
            view.get().onErrorEmailInvalid();
            isOkay = false;
        }

        if (!validator.isNotEmpty(view.get().getFullName())) {
            view.get().onErrorFullNameEmpty();
            isOkay = false;
        }

        if (!validator.isFullNameValid(view.get().getFullName())) {
            view.get().onErrorFullNameInvalid();
            isOkay = false;
        }

        if (validator.isNotEmpty(view.get().getReferralCode())) {
            if (!validator.isReferralCodeValid(view.get().getReferralCode())) {
                view.get().onErrorReferralCodeInvalid();
                isOkay = false;
            }
        }

        return isOkay;
    }

    private void signUpUser() {
        apiService.signUp(RequestBody.create(
                MediaType.parse("text/plain"),
                createSignUpBody(validator, view.get().getPhoneNumber(),
                        view.get().getFullName(), view.get().getEmail(), view.get().getReferralCode())))
                .enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.isSuccessful()) {
                            if (view != null) {
                                view.get().showSignUpSuccess();
                                onSignUpComplete(response.body());
                            }
                        } else {
                            if (view != null) {
                                view.get().showSignUpFailure();
                            }
                        }
                    }

                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        if (view != null) {
                            view.get().showSignUpFailure();
                        }
                    }
                });
    }

    private void onSignUpComplete(User user) {
        // login this user
        view.get().navigateToLogin(user);
    }
}
