package com.instake.passenger.feature.freeridepromo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.instake.passenger.R;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityEnterPromoCodeBinding;

/**
 * Created by mahmudul.hasan on 4/30/2017.
 */

public class GetFreeRideEnterPromo extends SiriusActivity implements BaseView {

    private ActivityEnterPromoCodeBinding binding;
    private BasePresenter basePresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_enter_promo_code;
    }

    @Override
    protected void initComponents() {
        setToolbar();
        binding = (ActivityEnterPromoCodeBinding) getDataBinding();
        basePresenter = new GetFreeRideEnterPromoPresenter(this);
    }

    @Override
    public Context getContext() {
        return GetFreeRideEnterPromo.this;
    }

    @Override
    public Activity getActivity() {
        return GetFreeRideEnterPromo.this;
    }


    public void setToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_chat);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable upArrow = ContextCompat.getDrawable(GetFreeRideEnterPromo.this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            toolbar.setTitleTextColor(0xFFFFFFFF);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED, resultIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
