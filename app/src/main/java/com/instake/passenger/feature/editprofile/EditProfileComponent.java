package com.instake.passenger.feature.editprofile;

import com.instake.passenger.monowar.invitefriends.InviteFriendsActivity;
import com.instake.passenger.monowar.invitefriends.InviteFriendsModule;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Singleton
@Subcomponent(modules = EditProfileModule.class)
public interface EditProfileComponent {
    void inject(EditProfileActivity activity);
}
