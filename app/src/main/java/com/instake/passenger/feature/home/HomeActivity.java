package com.instake.passenger.feature.home;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.GpsStatus;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.databinding.ActivityHomeBinding;
import com.instake.passenger.feature.nointernetdialog.NoInternetDialogFragment;
import com.instake.passenger.feature.offer.OfferRideActivity;
import com.instake.passenger.feature.payment.PaymentActivity;
import com.instake.passenger.feature.ridehistory.RideHistoryActivity;
import com.instake.passenger.model.User;
import com.instake.passenger.utility.NetworkUtils;
import com.instake.passenger.monowar.profile.ProfileActivity;
import com.instake.passenger.monowar.supportandhelp.a.SupportAndHelpActivity;
import com.instake.passenger.service.GPSTracker;

import javax.inject.Inject;

import de.hdodenhof.circleimageview.CircleImageView;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static android.location.GpsStatus.GPS_EVENT_STARTED;
import static android.location.GpsStatus.GPS_EVENT_STOPPED;

/**
 * Created by Tushar on 4/3/17.
 */

public class HomeActivity extends SiriusActivity implements NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener {

    public int currentScreenType = 0;

    @Inject
    Session session;

    private Class<?> mClass;
    private SelectLocation selectLocation;
    private Subscription busSubscription;

    private NoInternetDialogFragment noInternetDialogFragment;
    private GPSTracker gpsTracker;

    private View navHeader;
    private TextView tvUserName;
    private CircleImageView ivProfileImage;

    public static void open(Activity activity) {
        activity.startActivity(new Intent(activity, HomeActivity.class));
    }

    public static void ClearAllScreenAndOpen(Activity activity) {
        Intent intent = new Intent(activity, HomeActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    private ActivityHomeBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_home;
    }

    @Override
    protected void initComponents() {
        app.getAppComponent().inject(this);
        binding = (ActivityHomeBinding) getDataBinding();
        binding.navView.setNavigationItemSelectedListener(this);
        initNavigationViewComponents();
        getSupportFragmentManager().beginTransaction().add(R.id.container, new HomeFragment()).commit();
    }

    private void initNavigationViewComponents() {
        navHeader = binding.navView.getHeaderView(0);

        tvUserName = (TextView) navHeader.findViewById(R.id.userNameTv);
        ivProfileImage = (CircleImageView) navHeader.findViewById(R.id.iv_profile_image);

        User user = session.getUser();

        tvUserName.setText(user.getFullName());

        Glide.with(getContext().getApplicationContext())
                .load(user.getProfileImageUrl())
                .dontAnimate()
                .error(R.drawable.backgroud_select_image)
                .placeholder(R.drawable.backgroud_select_image)
                .into(ivProfileImage);

        autoUnsubBus();
        rxCallBackGettingUserInfo();

        ((TextView) binding.navView.getMenu()
                .findItem(R.id.menu_advanced_rides).getActionView()
                .findViewById(R.id.badgeTv)).setText(getString(R.string.comment_upcoming));

        navHeader.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mClass = ProfileActivity.class;
                binding.drawerLayout.closeDrawer(GravityCompat.START);
            }
        });

        binding.drawerLayout.addDrawerListener(this);
//        ((TextView) binding.navView.getMenu()
//                .findItem(R.id.menu_offers).getActionView()
//                .findViewById(R.id.badgeTv)).setText(5 + "");

//        binding.test.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Toast.makeText(getContext(), "Fotter Clicked", Toast.LENGTH_SHORT).show();
//            }
//        });

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        locationManager.addGpsStatusListener(new GpsStatus.Listener() {
            public void onGpsStatusChanged(int event) {
                switch (event) {
                    case GPS_EVENT_STARTED:
                        //Toast.makeText(getActivity(), "GPS is on", Toast.LENGTH_LONG).show();
                        break;
                    case GPS_EVENT_STOPPED:
                        //Toast.makeText(getActivity(), "GPS is off", Toast.LENGTH_LONG).show();
                        break;
                }
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (currentScreenType == HomeFragment.PICKUP) {
            //selectLocation.onLocationTypeChange(1);
            //return;
        } else if (currentScreenType == HomeFragment.DESTINATION) {
            selectLocation.onLocationTypeChange(HomeFragment.DESTINATION);
            return;
        } else if (currentScreenType == HomeFragment.CONFIRM_RIDE) {
            selectLocation.onLocationTypeChange(HomeFragment.CONFIRM_RIDE);
            return;
        } else if (currentScreenType == HomeFragment.ACCEPTED_RIDE) {
            selectLocation.onLocationTypeChange(HomeFragment.ACCEPTED_RIDE);
            return;
        } else if (currentScreenType == HomeFragment.ON_RIDE) {
            selectLocation.onLocationTypeChange(HomeFragment.ON_RIDE);
            return;
        }

        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            App.getInstance().disconnectSocket();
            super.onBackPressed();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!NetworkUtils.isConnected(getContext())) {
            noInternetDialogFragment = new NoInternetDialogFragment();
            noInternetDialogFragment.show(getSupportFragmentManager(), NoInternetDialogFragment.TAG);
        }

        gpsTracker = new GPSTracker(getActivity());
        if (!gpsTracker.isGPSEnabled()) {
            gpsTracker.showSettingsAlert();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (noInternetDialogFragment != null) {
            noInternetDialogFragment.dismiss();
            noInternetDialogFragment = null;
        }

        if (gpsTracker != null) {
            gpsTracker = null;
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_advanced_rides:
                break;
            case R.id.menu_history:
                mClass = RideHistoryActivity.class;
                break;
            case R.id.menu_rate_card:
                break;
            case R.id.menu_payment:
                mClass = PaymentActivity.class;
                break;
            case R.id.menu_offers:
                mClass = OfferRideActivity.class;
                break;
            case R.id.menu_support:
                mClass = SupportAndHelpActivity.class;
                break;
        }
        binding.navView.getMenu().findItem(item.getItemId()).setChecked(true);
        binding.drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    public void openSideDrawer() {
        if (!binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.openDrawer(GravityCompat.START);
        }
    }

    public void selectLocation(SelectLocation selectLocation) {
        this.selectLocation = selectLocation;
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (mClass != null) {
            startActivity(mClass, false);
            mClass = null;
        }
        clearNavigationMenu();
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

    private void autoUnsubBus() {
        if (busSubscription != null && !busSubscription.isUnsubscribed()) {
            busSubscription.unsubscribe();
        }
    }

    private void rxCallBackGettingUserInfo() {
        busSubscription = App.getInstance().bus().toObserverable1().observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Action1<Object>() {
                    @Override
                    public void call(Object event) {
                        if (event instanceof User) {
                            User user = (User) event;
                            tvUserName.setText(user.getFullName());

                            Glide.with(getContext().getApplicationContext())
                                    .load(user.getProfileImageUrl())
                                    .dontAnimate()
                                    .error(R.drawable.backgroud_select_image)
                                    .placeholder(R.drawable.backgroud_select_image)
                                    .into(ivProfileImage);
                        }
                    }
                });
    }

    private void clearNavigationMenu() {
        binding.navView.getMenu().findItem(R.id.menu_advanced_rides).setChecked(false);
        binding.navView.getMenu().findItem(R.id.menu_history).setChecked(false);
        binding.navView.getMenu().findItem(R.id.menu_rate_card).setChecked(false);
        binding.navView.getMenu().findItem(R.id.menu_payment).setChecked(false);
        binding.navView.getMenu().findItem(R.id.menu_offers).setChecked(false);
        binding.navView.getMenu().findItem(R.id.menu_support).setChecked(false);
    }

    interface SelectLocation {
        void onLocationTypeChange(int type);
    }
}