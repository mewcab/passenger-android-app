package com.instake.passenger.feature.onboard;

import android.util.Log;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;

import java.lang.ref.WeakReference;

import static com.instake.passenger.feature.onboard.CarFragment.isAlreadySetSecondBackground;
import static com.instake.passenger.feature.onboard.CarFragment.isFirstTimeAnimation;

/**
 * Created by Tushar on 5/12/17.
 */
public class OnBoardingPresenter implements BasePresenter<OnBoardingView> {

    private static final String TAG = OnBoardingPresenter.class.getSimpleName();

    private WeakReference<OnBoardingView> view;
    private ApiService apiService;
    private Session session;

    public OnBoardingPresenter(OnBoardingView view, ApiService apiService, Session session) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {
        if (view != null) {
            if (session.isUserLoggedIn()) {
                Log.d(TAG, session.getUser().getToken());
                view.get().navigateToSplashScreen();
            } else {
                view.get().startPagerRotation();
            }
        }
    }

    @Override
    public void onResumePresenter() {
        // TODO: sync viewpager rotation handler
    }

    @Override
    public void onPausePresenter() {
        // TODO: sync viewpager rotation handler
        isFirstTimeAnimation = true;
        isAlreadySetSecondBackground = true;
    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onClickLetsGo() {
        if (view != null) {
            checkStatus();
        }
    }

    private void checkStatus() {
        if (view.get().isUserLoggedIn()) {
            view.get().navigateToSplashScreen();
        } else {
            view.get().navigateToSignUp();
        }
    }
}
