package com.instake.passenger.feature.onboard;


import com.instake.passenger.R;

/**
 * Created by Adib on 31-Mar-17.
 */

public enum OnboardingObject {

    ONBOARDING_OBJECT_1(0, R.layout.content_onboard_slide_one),
    ONBOARDING_OBJECT_2(1, R.layout.content_onboard_slide_two),
    ONBOARDING_OBJECT_3(1, R.layout.content_onboard_slide_three),
    ONBOARDING_OBJECT_4(1, R.layout.content_onboard_slide_four),
    ONBOARDING_OBJECT_5(1, R.layout.content_onboard_slide_five),
    ONBOARDING_OBJECT_6(1, R.layout.content_onboard_slide_six);


    private int mTitleResId;
    private int mLayoutResId;

    OnboardingObject(int titleResId, int layoutResId) {
        mTitleResId = titleResId;
        mLayoutResId = layoutResId;
    }

    public int getTitleResId() {
        return mTitleResId;
    }

    public int getLayoutResId() {
        return mLayoutResId;
    }

}
