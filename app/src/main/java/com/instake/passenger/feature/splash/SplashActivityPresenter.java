package com.instake.passenger.feature.splash;

import com.instake.passenger.common.base.BasePresenter;

import java.lang.ref.WeakReference;

/**
 * Created by masum on 2/22/17.
 */
public class SplashActivityPresenter implements BasePresenter<SplashView> {
    private WeakReference<SplashView> view;

    public SplashActivityPresenter(SplashView splashView) {
        this.view = new WeakReference<>(splashView);
    }

    @Override
    public void onCreatePresenter() {
        view.get().startAnimation();
        //view.get().initAppData();
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {
        if (view != null) {
            view.get().stopAnimation();
        }
    }

    public void onAppDataInitiateSuccessfully() {
        if (view != null) {
            view.get().navigateToHome();
        }
    }

}
