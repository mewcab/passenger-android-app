package com.instake.passenger.feature.requestdriver;


import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;

import java.lang.ref.WeakReference;

/**
 * Created by masum on 3/29/17.
 */

public class RequestDriverPresenter implements BasePresenter<RequestDriverView>, TimeCounterInteractor {
    // TODO: use weak reference here
    private WeakReference<RequestDriverView> view;
    private TimeCounterImpl timeCounter;
    private Session session;

    public RequestDriverPresenter(RequestDriverView view, TimeCounterImpl interactor, Session session) {
        this.view = new WeakReference<>(view);
        this.timeCounter = interactor;
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {
        getTimer();
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {
        timeCounter.removeTimerHandler();
    }

    private void getTimer() {
        timeCounter.getTime(this);
    }

    @Override
    public void countTimer(String value) {
        if (view.get() != null) {
            view.get().updateTimer(value);
        }

    }

    public void onAcceptRequest(String passengerPhoneNo) {
        if (session.getUser().getPhoneNumber().contains(passengerPhoneNo)) {
            view.get().onAcceptedRequest(true);
        }

    }
}
