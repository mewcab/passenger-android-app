package com.instake.passenger.feature.home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.RelativeLayout;

import com.instake.passenger.R;
import com.instake.passenger.feature.vehicledetails.VehicleDetailsActivity;

import java.util.ArrayList;


public class CarListAdapter extends RecyclerView.Adapter<CarListAdapter.ViewHolder> {
    private ArrayList<String> mDataset = new ArrayList<>();
    private Context context;
    private int selectedPos = 0;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public View viewSelected;
        public RelativeLayout relativeCarInfo;
        public RelativeLayout relativeCarInfoDetails;
        public ImageButton mTextViewTime;

        public ViewHolder(View v) {
            super(v);
            view = (View) v.findViewById(R.id.viewSelected);
            relativeCarInfo = (RelativeLayout) v.findViewById(R.id.relativeCarInfo);
            relativeCarInfoDetails = (RelativeLayout) v.findViewById(R.id.relativeCarInfoDetails);
            viewSelected = (View) v.findViewById(R.id.viewSelected);
        }

    }

    public CarListAdapter(ArrayList<String> myDataset, Context context) {
        mDataset = myDataset;
        this.context = context;
    }

    @Override
    public CarListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_car_info, parent, false);
        // set the view's size, margins, paddings and layout parameters
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        if (selectedPos == position) {
            holder.view.setVisibility(View.VISIBLE);
        } else {
            holder.view.setVisibility(View.GONE);
        }

        holder.relativeCarInfo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                notifyItemChanged(selectedPos);
                selectedPos = holder.getAdapterPosition();
                notifyItemChanged(selectedPos);
                doButtonOneClickActions(position);
                notifyDataSetChanged();

            }

        });

        holder.relativeCarInfoDetails.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, VehicleDetailsActivity.class);
                context.startActivity(intent);
            }

        });

    }

    @Override
    public int getItemCount() {
        return mDataset.size();
    }

    private void doButtonOneClickActions(int position) {
        if (mOnDataChangeListener != null) {
            mOnDataChangeListener.onDataChanged(position);
        }
    }

    OnDataChangeListener mOnDataChangeListener;

    public void setOnDataChangeListener(OnDataChangeListener onDataChangeListener) {
        mOnDataChangeListener = onDataChangeListener;
    }

    public interface OnDataChangeListener {
        void onDataChanged(int position);
    }

}