package com.instake.passenger.feature.splash;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.canelmas.let.AskPermission;
import com.canelmas.let.DeniedPermission;
import com.canelmas.let.Let;
import com.canelmas.let.RuntimePermissionListener;
import com.canelmas.let.RuntimePermissionRequest;
import com.google.android.gms.maps.model.LatLng;
import com.instake.passenger.BuildConfig;
import com.instake.passenger.R;
import com.instake.passenger.common.Animation;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivitySplashBinding;
import com.instake.passenger.dipendency.splash.SplashModule;
import com.instake.passenger.feature.home.HomeActivity;
import com.instake.passenger.service.GPSTracker;

import java.util.List;

import javax.inject.Inject;

import rx.Subscription;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class SplashActivity extends SiriusPresenterActivity<SplashView, SplashActivityPresenter> implements SplashView, RuntimePermissionListener {

    private ActivitySplashBinding binding;

    private Animation animation;

    private Subscription mBusSubscription;

    @Inject
    SplashActivityPresenter presenter;

    public static void open(Activity activity) {
        Intent intent = new Intent(activity, SplashActivity.class);
        activity.startActivity(intent);
    }

    public static void ClearAllScreenAndOpen(Activity activity) {
        Intent intent = new Intent(activity, SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_splash;
    }

    @Override
    protected SplashActivityPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new SplashModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected void initComponents() {
        binding = (ActivitySplashBinding) getDataBinding();
        binding.setVersionName(BuildConfig.VERSION_NAME);
        animation = new Animation(this);
        getCurrentLocation();

    }

    @SuppressWarnings({"MissingPermission"})
    @AskPermission(ACCESS_FINE_LOCATION)
    private void getCurrentLocation() {
        GPSTracker mGps = new GPSTracker(this);
        if (mGps.canGetLocation()) {
            LatLng currentLatLng = new LatLng(mGps.getLatitude(), mGps.getLongitude());
            initAppData();
        } else {
            mGps.showSettingsAlert();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.e("Opps", "d1" + grantResults[0]);
        Let.handle(this, requestCode, permissions, grantResults);
    }

    @Override
    public void onShowPermissionRationale(List<String> permissionList, RuntimePermissionRequest permissionRequest) {
        final StringBuilder sb = new StringBuilder();

        for (String permission : permissionList) {
            sb.append(getRationale(permission));
            sb.append("\n");
        }

        new AlertDialog.Builder(this).setTitle("Permission Required!")
                .setMessage(sb.toString())
                .setCancelable(true)
                .setNegativeButton("No Thanks", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("Try Again", (dialog, which) -> permissionRequest.retry())
                .show();
    }

    @Override
    public void onPermissionDenied(List<DeniedPermission> deniedPermissionList) {
        Log.e("Opps", "d2");
        final StringBuilder sb = new StringBuilder();

        for (DeniedPermission result : deniedPermissionList) {
            if (result.isNeverAskAgainChecked()) {
                sb.append(result.getPermission());
                sb.append("\n");
            }
        }

        if (sb.length() != 0) {
            new AlertDialog.Builder(this).setTitle("Go Settings and Grant Permission")
                    .setMessage(sb.toString())
                    .setCancelable(true)
                    .setPositiveButton("ok", (dialog, which) -> {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1);
                        dialog.dismiss();
                    }).show();
        }
    }

    @Override
    public void startAnimation() {
        animation.flipAnimation(binding.imgSplashLogo).start();
    }

    @Override
    public void stopAnimation() {
        animation.stopAnimation();
    }

    @Override
    public void initAppData() {
        // TODO: get required data (i.e: GPS, API data etc.) to initiate app functionality
        new Handler().postDelayed(() -> presenter.onAppDataInitiateSuccessfully(), 3000);
    }

    @Override
    public void navigateToHome() {
        HomeActivity.open(this);
        finish();
    }

    private String getRationale(String permission) {
        if (permission.equals(Manifest.permission.GET_ACCOUNTS)) {
            return getString(R.string.rationale_accounts);
        } else if (permission.equals(Manifest.permission.RECEIVE_SMS)) {
            return getString(R.string.rationale_receive_sms);
        } else if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return getString(R.string.rationale_location);
        } else {
            return getString(R.string.rationale_other);
        }
    }

}
