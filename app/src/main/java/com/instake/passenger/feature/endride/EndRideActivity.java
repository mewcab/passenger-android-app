package com.instake.passenger.feature.endride;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityEndRideBinding;
import com.instake.passenger.dipendency.endride.EndRideModule;
import com.instake.passenger.model.Rating;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.payment.Tips;
import com.instake.passenger.utility.Utility;

import org.parceler.Parcels;

import javax.inject.Inject;

import static com.instake.passenger.R.id.rel_submit;

// TODO: redefine this mvp model
public class EndRideActivity extends SiriusActivity implements EndRideContract.EndRideView,
        View.OnClickListener {

    private final String TAG = getClass().getSimpleName();

    @Inject
    EndRidePresenterImpl mEndRidePresenter;

    ActivityEndRideBinding binding;

    Trip trip;

    TextView txtViewFare;

    String total;

    Tips tips;

    float scoreRating;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_end_ride;
    }

    @Override
    protected void initComponents() {
        app.getAppComponent()
                .plus(new EndRideModule(this))
                .inject(this);

        binding = (ActivityEndRideBinding) getDataBinding();

        tips = new Tips();
        trip = (Trip) Parcels.unwrap(getIntent().getParcelableExtra("trip_info"));
        Log.d(TAG, "got: " + trip.getFareDetails().getBreakdown().get(0).getTitle());
        total = trip.getTripTotalFare();

        Log.e(TAG, "Connection " + App.getInstance().isConnected());

        mEndRidePresenter.setTripForSavingToSession(trip);
        mEndRidePresenter.saveAppLocalState(App.getInstance().APP_IN_TIPS_UI);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");

        txtViewFare = (TextView) findViewById(R.id.tvCost);
        txtViewFare.setText(trip.getTripTotalFare());

        TextView textViewDriverName = (TextView) findViewById(R.id.textViewDriverName);
        textViewDriverName.setText(trip.getDriverName());
        binding.ratingBar.setOnScoreChanged(new SiriusRatingBar.IRatingBarCallbacks() {
            @Override
            public void scoreChanged(float score) {
                scoreRating = score;
                binding.tvRateMeaning.setText(Utility.ratingText(score));
            }
        });

        binding.buttonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (scoreRating > 5) {
                    scoreRating = 5;
                }
                mEndRidePresenter.saveAppLocalState(App.getInstance().APP_IN_RATING_UI);
                Rating rating = new Rating();
                rating.setText("No special text from user");
                rating.setRating(scoreRating);
                mEndRidePresenter.onPostRating(trip, rating);
                //mEndRidePresenter.openEndRideReviewActivity(trip);
            }
        });

        RelativeLayout relativeFareViewHolder = (RelativeLayout) findViewById(R.id.relativeFareViewHolder);
        relativeFareViewHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(EndRideActivity.this, CashSummeryActivity.class);
                intent.putExtra("trip_info", Parcels.wrap(trip));
                intent.putExtra("tips_info", Parcels.wrap(tips));
                intent.putExtra("total", total);
                startActivity(intent);
            }
        });

        ImageView ivDriverProfile = (ImageView) findViewById(R.id.ivDriverProfile);
        Glide.with(getActivity())
                .load(trip.getDriverPhotoUrl())
                .error(R.drawable.ic_driver_user)
                .placeholder(R.drawable.ic_avatar)
                .into(ivDriverProfile);

        initTipLayout();
    }

    private ProgressDialog progressDialog;

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public Activity getActivity() {
        return this;
    }


    @Override
    public float getRating() {
        return 0;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onAddingTip(float tipAmount) {
        total = (tipAmount + Float.parseFloat(trip.getTripTotalFare())) + "";
        txtViewFare.setText(total);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case rel_submit:
                mEndRidePresenter.onBtnSubmitClick();
                break;
        }
    }

    public void showDialogOtherTipsAmount() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = getLayoutInflater();

        View dialogLayout = inflater.inflate(R.layout.dialog_tips_other_amount, null);
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                hideKeyboard(dialogLayout);
            }
        });


        EditText editTextTipsAmount = (EditText) dialogLayout.findViewById(R.id.editTextTipsAmount);

        Button buttonSend = (Button) dialogLayout.findViewById(R.id.buttonSend);
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideKeyboard(view);

                Tips tips = new Tips();
                tips.setTrip_id(trip.tripId);
                tips.setTips_amount(Float.parseFloat(editTextTipsAmount.getText().toString()));

                mEndRidePresenter.onPostTips(trip, tips);

            }
        });

        Button buttonClose = (Button) dialogLayout.findViewById(R.id.buttonClose);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        App.getInstance().disconnectSocket();
        super.onDestroy();
    }

    @SuppressWarnings("deprecation")
    private void initTipLayout() {

    }
}
