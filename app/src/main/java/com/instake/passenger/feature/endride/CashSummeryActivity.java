package com.instake.passenger.feature.endride;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.nearestdriver.Breakdown;
import com.instake.passenger.model.nearestdriver.FareDetails;
import com.instake.passenger.model.payment.Tips;

import org.parceler.Parcels;

/**
 * Created by Tushar on 6/1/17.
 */

public class CashSummeryActivity extends SiriusActivity {

    private static final String TAG = CashSummeryActivity.class.getName();

    TextView tvTotalPay;

    TextView tvUserName;

    RecyclerView rvFareList;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_cash_summery;
    }

    @Override
    protected void initComponents() {
        Trip trip = (Trip) Parcels.unwrap(getIntent().getParcelableExtra("trip_info"));
        Tips tips = (Tips) Parcels.unwrap(getIntent().getParcelableExtra("tips_info"));
        String total = getIntent().getExtras().getString("total");

        rvFareList = (RecyclerView) findViewById(R.id.rvFareList);

        ImageButton btnClose = (ImageButton) findViewById(R.id.btnClose);
        btnClose.setOnClickListener(v-> {
            finish();
        });

        tvTotalPay = (TextView) findViewById(R.id.tvTotalPay);

        tvTotalPay.setText(total);




        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvFareList.setLayoutManager(mLayoutManager);

        VoucherAdapter voucherAdapter = new VoucherAdapter(this, trip.getFareDetails().getBreakdown());
        rvFareList.setAdapter(voucherAdapter);


    }
}