package com.instake.passenger.feature.onboard;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;


/**
 * Created by Adib on 31-Mar-17.
 */

public class ViewPagerAdapter extends PagerAdapter {

    private Context mContext;

    public ViewPagerAdapter(Context context) {
        mContext = context;
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        switch (position) {
            case 0:
                ViewGroup mViewGroup = CarFragment.newInstance(mContext, collection);
                collection.addView(mViewGroup);
                return mViewGroup;

            default:
                OnboardingObject onboardingObject = OnboardingObject.values()[position];
                LayoutInflater inflater = LayoutInflater.from(mContext);
                ViewGroup layout = (ViewGroup) inflater.inflate(onboardingObject.getLayoutResId(), collection, false);
                collection.addView(layout);
                return layout;

        }
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return OnboardingObject.values().length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        OnboardingObject customPagerEnum = OnboardingObject.values()[position];
        return mContext.getString(customPagerEnum.getTitleResId());
    }

}
