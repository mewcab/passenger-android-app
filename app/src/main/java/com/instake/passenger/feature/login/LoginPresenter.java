package com.instake.passenger.feature.login;

import android.util.Log;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.api.JsonBuilder;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.User;
import com.instake.passenger.monowar.signup.a.SignupCompleteProfilePresenter;
import com.instake.passenger.utility.validation.Validator;

import java.lang.ref.WeakReference;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

/**
 * Created by Tushar on 5/21/17.
 */
public class LoginPresenter implements BasePresenter<LoginView> {
    private WeakReference<LoginView> view;
    private ApiService apiService;
    private Session session;
    private Validator validator;

    public LoginPresenter(LoginView view, ApiService apiService, Session session, Validator validator) {
        this.view = new WeakReference<>(view);
        this.apiService = apiService;
        this.session = session;
        this.validator = validator;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void onLoginClicked() {
        view.get().hideAllErrors();
        if (isInputOkay()) {
            view.get().hideKeyboard();
            view.get().showLoader();
            logIn();
        }
    }

    private boolean isInputOkay() {
        boolean isOkay = true;
        if (!validator.isPhoneNumberValid(view.get().getPhoneNumber()) && !validator.isPhoneNumberLengthValid(view.get().getPhoneNumber())) {
            view.get().showPhoneNumberError();
            isOkay = false;
        }
        if (!validator.isPasswordValid(view.get().getPassword())) {
            view.get().showPasswordError();
            isOkay = false;
        }
        return isOkay;
    }

    private void logIn() {
        apiService.loginNew(getDefaultLoginRequestBody(view.get().getPhoneNumber(), view.get().getPassword()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(new Func1<Throwable, Observable<? extends User>>() {
                    @Override
                    public Observable<? extends User> call(Throwable throwable) {
                        return Observable.error(throwable);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        onLoginFailed(e.getMessage());
                    }

                    @Override
                    public void onNext(User user) {
                        onLoginSuccess(user);
                    }
                });
    }

    private RequestBody getDefaultLoginRequestBody(String mobileNumber, String password) {
        return RequestBody.create(MediaType.parse("application/json"), JsonBuilder.createLoginBodyWithPassword(mobileNumber, password));
    }

    private void onLoginFailed(String error) {
        Log.e("LoginPresenter", "Log In Error: " + error);
        if (view != null) {
            view.get().hideLoader();
            view.get().onLoginFailed(error);
        }
    }

    private void onLoginSuccess(User user) {
        if (user.getMessage() == null || user.getMessage().length() == 0) {
            session.saveUser(user);
            Log.d(SignupCompleteProfilePresenter.class.getSimpleName(), "onSignupSuccess: " + user.getFullName());
            Log.d(SignupCompleteProfilePresenter.class.getSimpleName(), "onSignupSuccess: " + user.getProfileImageUrl());

            if (view != null) {
                view.get().hideLoader();
                view.get().clearAllActivitiesAndNavigateToSplash();
            }
        } else {
            onLoginFailed(user.getMessage());
        }
    }
}
