package com.instake.passenger.feature.helpandsupport;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.model.HelpAndSupportModel;

import java.util.List;

/**
 * Created by mahmudul.hasan on 4/24/2017.
 */

public class HelpAndSupportAdapter extends RecyclerView.Adapter<HelpAndSupportAdapter.MyRecyclerHolder> {

    private LayoutInflater inflater;
    private List<HelpAndSupportModel> list;

    public HelpAndSupportAdapter(Context context, List<HelpAndSupportModel> list) {
        inflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public MyRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyRecyclerHolder(inflater.inflate(R.layout.row_help_and_support, parent, false));
    }

    @Override
    public void onBindViewHolder(MyRecyclerHolder holder, int position) {
        HelpAndSupportModel currentItem = list.get(position);
        holder.tx_row_title.setText(currentItem.getTitle());
        holder.tx_row_detail.setText(currentItem.getDetail());


    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tx_row_collaps, tx_row_title, tx_row_detail;

        MyRecyclerHolder(View itemView) {
            super(itemView);
            tx_row_collaps = (TextView) itemView.findViewById(R.id.tx_row_collaps);
            tx_row_title = (TextView) itemView.findViewById(R.id.tx_row_title);
            tx_row_detail = (TextView) itemView.findViewById(R.id.tx_row_detail);
            tx_row_collaps.setOnClickListener(this);


        }

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.tx_row_collaps:
                    switch (tx_row_collaps.getText().toString()) {
                        case "‒":
                            tx_row_collaps.setText("+");
                            tx_row_detail.setVisibility(View.GONE);
                            break;
                        case "+":
                            tx_row_collaps.setText("‒");
                            tx_row_detail.setVisibility(View.VISIBLE);
                            break;
                    }
                    break;
            }
        }
    }
}
