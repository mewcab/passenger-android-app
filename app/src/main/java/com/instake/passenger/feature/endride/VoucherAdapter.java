package com.instake.passenger.feature.endride;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.model.nearestdriver.Breakdown;

import java.util.List;

public class VoucherAdapter extends BaseRecyclerViewAdapter<BreakdownFare, VoucherAdapter.VoucherViewHolder> {

    private static final int VIEW_TYPE_TIPS = 1;
    private static final int VIEW_TYPE_NORMAL = 2;
    private static final int VIEW_TYPE_SUBTOTAL = 3;
    private static final int VIEW_TYPE_DISCOUNT = 4;

    private List<BreakdownFare> voucher;
    private Context context;

    public VoucherAdapter(Context context, List<BreakdownFare> voucher) {
        super(voucher);
        this.context = context;
        this.voucher = voucher;
        setHasStableIds(true);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return VIEW_TYPE_TIPS;
        } else if (hasDiscount()) {
            if (position == voucher.size() - 1) {
                return VIEW_TYPE_DISCOUNT;
            } else if (position == voucher.size() - 2) {
                return VIEW_TYPE_SUBTOTAL;
            }
        }
        return VIEW_TYPE_NORMAL;
    }

    private boolean hasDiscount() {
        if (voucher.get(voucher.size() - 1).getTitle().toLowerCase().contains("discount")) {
            return true;
        }
        return false;
    }

    @Override
    public VoucherViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;

        if (viewType == VIEW_TYPE_TIPS) {
            layoutId = R.layout.item_vouchar_tips;
        } else if (viewType == VIEW_TYPE_DISCOUNT) {
            layoutId = R.layout.item_vouchar_discount;
        } else if (viewType == VIEW_TYPE_SUBTOTAL) {
            layoutId = R.layout.item_vouchar_subtotal;
        } else {
            layoutId = R.layout.item_vouchar;
        }

        return new VoucherViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(layoutId, parent, false));
    }

    @Override
    public void onBindViewHolder(VoucherViewHolder holder, int position) {
        BreakdownFare fare = getItem(position);

        holder.tvVoucherTitle.setText(fare.getTitle());
        holder.tvAmount.setText(fare.getAmount() + "");

        /*if (getItemViewType(position) == VIEW_TYPE_TIPS && fare.getCalculatedAmount() > 0) {
            holder.tipsProgressBar.setVisibility(View.GONE);
        }*/
    }

    @Override
    public int getItemCount() {
        return voucher.size();
    }

    public class VoucherViewHolder extends RecyclerView.ViewHolder {

        public final View view;

        TextView tvVoucherTitle;

        TextView tvAmount;

        @Nullable
        ProgressBar tipsProgressBar;

        public VoucherViewHolder(View view) {
            super(view);

            tvVoucherTitle = (TextView) view.findViewById(R.id.tvVoucherTitle);
            tvAmount = (TextView) view.findViewById(R.id.tvAmount);

            this.view = view;
        }
    }
}