package com.instake.passenger.feature.home;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.canelmas.let.AskPermission;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.AutocompleteFilter;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.SiriusFragment;
import com.instake.passenger.common.views.InstakeAddressView;
import com.instake.passenger.databinding.FragmentHomeBinding;
import com.instake.passenger.dipendency.home.HomeModule;
import com.instake.passenger.feature.endreview.EndRideReviewActivity;
import com.instake.passenger.feature.endride.EndRideActivity;
import com.instake.passenger.feature.requestdriver.RequestDriverActivity;
import com.instake.passenger.feature.vehicledetails.VehicleDetailsActivity;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.User;
import com.instake.passenger.model.nearestdriver.DriverList;
import com.instake.passenger.model.nearestdriver.NearestDriverResponse;
import com.instake.passenger.model.nearestdriver.NearestJsonParser;
import com.instake.passenger.monowar.CircleTransform;
import com.instake.passenger.monowar.invitefriends.InviteFriendsActivity;
import com.instake.passenger.service.GPSTracker;
import com.instake.passenger.utility.Utility;

import org.json.JSONArray;
import org.json.JSONObject;
import org.parceler.Parcels;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static com.facebook.FacebookSdk.getApplicationContext;
import static com.facebook.FacebookSdk.getCacheDir;
import static com.instake.passenger.R.id.map;

/**
 * Created by Masum on 4/4/17.
 */

public class HomeFragment extends SiriusFragment<HomeView, HomePresenter> implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnCameraMoveStartedListener,
        GoogleMap.OnCameraIdleListener,
        LocationListener,
        HomeView {

    public static int PICKUP = 1;
    public static int DESTINATION = 2;
    public static int CONFIRM_RIDE = 3;
    public static int ACCEPTED_RIDE = 4;
    public static int ON_RIDE = 5;

    private static final int PAYMENT_TYPE_CASH = 1;
    private static final int PAYMENT_TYPE_INSTAKE_PAY = 2;
    private static final int PAYMENT_TYPE_CARD = 3;

    private static int PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP = 1;
    private static int PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION = 2;
    private static int REQUEST_DRIVER_REQUEST_CODE = 104;

    private static final int REQUEST_CODE_PERMISSION = 2;
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 5000;
    private static final String TAG = "HomeFragment";

    /**
     * The fastest rate for active location updates. Exact. Updates will never be more frequent
     * than this value.
     */
    public static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS = UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private GPSTracker mGps;
    private Marker mMarkerPickUp;
    private Marker mMarkerDropOff;
    private FragmentHomeBinding binding;
    private View confirmView;
    private View acceptedView;
    private SupportMapFragment mMapFragment;

    private LatLng mPickUpLatLng;
    private LatLng mDropOffLatLng;
    private String mPickupAddress;
    private String mDropOffAddress;

    protected LocationRequest mLocationRequest;
    protected Location mCurrentLocation;
    protected String mLastUpdateTime;

    private InstakeAddressView mInstakeAddressView;
    private InstakeAddressView instakeTextViewPayment;
    private InstakeAddressView instakeTextViewEta;
    private InstakeAddressView instakeTextCarName;

    private TextView textViewTripTime;
    private TextView textViewTripStatus;
    private TextView textViewDriverName;
    private TextView textViewRatings;
    private TextView textViewCarName;
    private TextView textViewCarId;
    private ImageView imageViewDriverPhoto;

    private Subscription busSubscription;

    // private ArrayList<LatLng> latlngs = new ArrayList<>();

    private MarkerOptions options = new MarkerOptions();

    @Inject
    HomePresenter mHomePresenter;

    public JSONObject jObjNearestDriver;

    private NearestDriverResponse driverListResponse;

    private Trip trip;

    private ProgressDialog progressDialog;

    @Override
    protected HomePresenter bindPresenter() {
        trip = new Trip();
        File cacheFile = new File(getCacheDir(), "responses");

        app.getAppComponent()
                .plus(new HomeModule(this, cacheFile))
                .inject(this);

        jObjNearestDriver = new JSONObject();
        return mHomePresenter;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_home;
    }

    @Override
    protected void initFragmentComponents() {
        binding = (FragmentHomeBinding) getDataBinding();
        binding.setMapPresenter(mHomePresenter);
        mHomePresenter.createSocketConnection();

        initView();

        buildGoogleApiClient();

        getBackScreen();

        autoUnsubBus();

        rxCallBackGettingNearestDriverList();

    }

    protected synchronized void buildGoogleApiClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
            //createLocationRequest();
        }
    }

    private void getBackScreen() {
        ((HomeActivity) getActivity()).selectLocation(currentScreen -> {
            if (currentScreen == CONFIRM_RIDE) {
                setHomeUIWhenBack();
            }
        });
    }

    private void autoUnsubBus() {
        if (busSubscription != null && !busSubscription.isUnsubscribed()) {
            busSubscription.unsubscribe();
        }
    }

    private void rxCallBackGettingNearestDriverList() {
        busSubscription = App.getInstance().bus().toObserverable1().observeOn(AndroidSchedulers.mainThread())
                .subscribe(event -> {
                    if (event instanceof User) {
                        setBtnMenuImage(((User) event).getProfileImageUrl());
                    }
                });
    }

    private void manageFullTrip(Object event) {
        if (event instanceof Trip) {
            manageTripStartAndEnd((Trip) event);
        }
    }

    private void manageTripStartAndEnd(Trip trip) {
        this.trip = trip;
        new Thread() {
            public void run() {
                Message msg1 = new Message();
                msg1.arg1 = 2;
                msg1.obj = trip;
                handlerUpdatedLocationData.sendMessage(msg1);
            }
        }.start();

    }

    private void updateBigMessage(Trip trip) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {

                if (acceptedView == null) {
                    loadAcceptedView(R.layout.layout_request_accepted_driver_info);
                    initAcceptUIComponentsListeners();
                }

                textViewTripTime.setText(trip.getTripBigMessage());
                textViewTripStatus.setText(trip.getTripSmallMessage());
                acceptedView.invalidate();
            }
        }, 10);

    }

    @Override
    public void openEndTripReviewActivity(Trip trip) {
        try {
            Intent intent = new Intent(getApplicationContext(), EndRideReviewActivity.class);
            intent.putExtra("trip_info", Parcels.wrap(trip));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "openEndTripReviewActivity: " + e.getMessage());
        }

    }

    @Override
    public void openEndTripActivity(Trip trip) {
        try {
            Intent intent = new Intent(getApplicationContext(), EndRideActivity.class);
            intent.putExtra("trip_info", Parcels.wrap(trip));
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } catch (Exception e) {
            Log.e(TAG, "openEndTripActivity: " + e.getMessage());
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    @Override
    public void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onDestroy() {
        if (mGps != null) {
            mHomePresenter.onStop();
            mGps.stopUsingGPS();
        }
        super.onDestroy();
    }

    @Override
    public void onDestroyView() {
        mHomePresenter.onStop();
        //mHomePresenter.mSocketService.disconnectSocket();
        super.onDestroyView();
    }

    @Override
    public void onDrawPath(PolylineOptions polylineOptions) {
        //mMap.clear();
        mMap.addPolyline(polylineOptions);
    }

    @Override
    public void setFareEstimation(String fare) {
        mInstakeAddressView.setText(fare);
        mInstakeAddressView.setSmallDecoratedText("TK", 0.60f);
        confirmView.invalidate();

    }

    @Override
    public void setPaymentName(String paymentMethod) {
        instakeTextViewPayment.setText(paymentMethod);
        confirmView.invalidate();
    }

    @Override
    public void loadConfirmRide() {
        setCurrentScreenIsConfirmRide();

        setDefaultPaymentAsCash();

        stopNearestDriverSearchingThread();

        loadView(R.layout.layout_ride_detail_info);

        initConfirmRideComponentAndListeners();

        setComponentsDefaultValue();

        setUpConfirmRideUI();

        positioningMap();

        changeMapMoveableStatus();

        addMarkerAndSetPickUpAndDropOff();

        setMapLatLonBound();

        getDistanceFromPickUpToDropOff();
    }

    private void setCurrentScreenIsConfirmRide() {
        ((HomeActivity) getActivity()).currentScreenType = CONFIRM_RIDE;
    }

    private void setDefaultPaymentAsCash() {
        trip.setPaymentType("Cash");
    }

    private void stopNearestDriverSearchingThread() {
        mHomePresenter.removeGetUpdatedLocationThread();
    }

    private void initConfirmRideComponentAndListeners() {
        mInstakeAddressView = (InstakeAddressView) confirmView.findViewById(R.id.textViewFareEstimation);
        instakeTextViewPayment = (InstakeAddressView) confirmView.findViewById(R.id.instakeTextViewPayment);
        instakeTextViewEta = (InstakeAddressView) confirmView.findViewById(R.id.instakeTextViewEta);
        instakeTextCarName = (InstakeAddressView) confirmView.findViewById(R.id.instakeTextCarName);
        RelativeLayout paymentLayout = (RelativeLayout) confirmView.findViewById(R.id.paymentLayout);
        binding.relativeBottomViewHolder.addView(confirmView);

        BottomSheetDialog bottomSheetDialog = initBottomSheetPaymentDialog();

        paymentLayout.setOnClickListener(v -> {
            bottomSheetDialog.show();
        });

        binding.btnConfirmRide.setOnClickListener(v -> {
            showLoader();
            mHomePresenter.confirmRideStatus(true);
            mHomePresenter.getLatestDriverLocationList();
            mHomePresenter.removeGetUpdatedLocationThread();

        });

    }

    private void setComponentsDefaultValue() {
        if (driverListResponse == null) {
            return;
        }
        Random ran = new Random();
        int random = 8 + ran.nextInt(12 - 8 + 1);

        if (!driverListResponse.isEmptyDriverList()) {
            instakeTextViewEta.setText(random + "mins");
            instakeTextCarName.setText2(driverListResponse.getDriverList().get(0).getVehicleInfo().getSeats() + " seats");
            instakeTextCarName.setText(driverListResponse.getDriverList().get(0).getDriver().getName().toUpperCase());
        }

    }

    @NonNull
    private String getUnit() {
        String unit;
        if (driverListResponse.getDriverList().get(0).getDistance().getDurationUnit().toLowerCase().contains("min")) {
            unit = "mins";
        } else {
            unit = "hours";
        }
        return unit;
    }

    private void setUpConfirmRideUI() {
        hideOtherComponents();

        binding.btnConfirmRide.setVisibility(View.VISIBLE);
        binding.btnBack.setVisibility(View.VISIBLE);

        binding.btnBack.setOnClickListener(v -> {
            setHomeUIWhenBack();
        });

    }

    private void positioningMap() {
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) binding.relativeMap.getLayoutParams();
        params.addRule(RelativeLayout.ABOVE, R.id.relativeBottomViewHolder);
    }

    private void changeMapMoveableStatus() {
        if (getCurrentScreenFlag() == DESTINATION || getCurrentScreenFlag() == CONFIRM_RIDE
                || getCurrentScreenFlag() == ACCEPTED_RIDE) {
            removeCemeraListener();
        } else {
            addMapMoveableListener(this);
        }

    }

    private void removeCemeraListener() {
        addMapMoveableListener(null);
    }

    private void addMapMoveableListener(HomeFragment onCameraIdleListener) {
        mMap.setOnCameraIdleListener(onCameraIdleListener);
        mMap.setOnCameraMoveStartedListener(onCameraIdleListener);
    }

    private void addMarkerAndSetPickUpAndDropOff() {
        mMarkerPickUp = getMarker(mPickUpLatLng, "Pickup", mPickupAddress);
        mMarkerDropOff = getMarker(mDropOffLatLng, "Destination", mDropOffAddress);
    }

    private void setMapLatLonBound() {
        binding.relativeMap.postDelayed(new Runnable() {

            @Override
            public void run() {
                binding.relativeMap.invalidate();

                int width = binding.relativeMap.getWidth();
                int height = binding.relativeMap.getHeight();

                int padding = (int) (height * 0.20); // offset from edges of the map 10% of screen

                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(
                        Utility.getLatLngBounds(mPickUpLatLng, mDropOffLatLng), width, height - 100, padding);

                mMap.animateCamera(cu);

            }
        }, 10);

    }

    private void getDistanceFromPickUpToDropOff() {
        mHomePresenter.getDistanceInfoFromGoogleApi(mPickUpLatLng, mDropOffLatLng);
    }

    public void loadUIAfterAcceptRequest() {

        ((HomeActivity) getActivity()).currentScreenType = ACCEPTED_RIDE;

        loadAcceptedView(R.layout.layout_request_accepted_driver_info);

        initAcceptUIComponentsListeners();

        addViewToBottomHolder(acceptedView);

        hideBackButtonAndConfirmRide();

        hideOtherComponents();

    }

    private void removeConfirmRideView() {
        binding.relativeBottomViewHolder.removeView(confirmView);
    }

    private void removeAcceptedView() {
        binding.relativeBottomViewHolder.removeView(acceptedView);
    }

    private void loadView(int layout) {
        confirmView = getActivity().getLayoutInflater().inflate(layout, null);
    }

    private void loadAcceptedView(int layout) {
        acceptedView = getActivity().getLayoutInflater().inflate(layout, null);
    }

    private void initAcceptUIComponentsListeners() {
        Button btnCancelRide = (Button) acceptedView.findViewById(R.id.btnCancelRide);

        textViewTripTime = (TextView) acceptedView.findViewById(R.id.textViewTripTime);
        textViewTripStatus = (TextView) acceptedView.findViewById(R.id.textViewTripStatus);
        textViewDriverName = (TextView) acceptedView.findViewById(R.id.textViewDriverName);
        textViewRatings = (TextView) acceptedView.findViewById(R.id.textViewRatings);
        textViewCarName = (TextView) acceptedView.findViewById(R.id.textViewCarName);
        textViewCarId = (TextView) acceptedView.findViewById(R.id.textViewCarId);
        imageViewDriverPhoto = (ImageView) acceptedView.findViewById(R.id.imageViewDriverUser);

        Button btnDialer = (Button) acceptedView.findViewById(R.id.btnDialer);

        btnDialer.setOnClickListener(v -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + trip.getDriverPhoneNo()));
            startActivity(intent);

        });

        btnCancelRide.setOnClickListener(v -> {
            showDialogCancelRide();

        });
    }

    private void addViewToBottomHolder(View view) {
        binding.relativeBottomViewHolder.addView(view);
    }

    private void hideBackButtonAndConfirmRide() {
        binding.btnBack.setVisibility(View.GONE);
        binding.btnConfirmRide.setVisibility(View.GONE);
    }

    private void hideOtherComponents() {
        binding.btnMenu.setVisibility(View.GONE);
        binding.btnOffers.setVisibility(View.GONE);

        binding.btnSetPickUp.setVisibility(View.GONE);
        binding.relativeButtonMyLocation.setVisibility(View.GONE);
        binding.relativieButtonWhereToGo.setVisibility(View.GONE);

        binding.layoutPickupLocationPointer.relativeHolderPointer.setVisibility(View.GONE);

    }

    @Override
    @SuppressWarnings({"MissingPermission"})
    @AskPermission(ACCESS_FINE_LOCATION)
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.setMyLocationEnabled(true);
            mMap.getUiSettings().setMapToolbarEnabled(false);
            //mMap.setOnMarkerClickListener(this);

            mGoogleApiClient.connect();

            changeMapMoveableStatus();

        }
    }

   /* @Override
    public boolean onMarkerClick(Marker marker) {
        if (marker.equals(mMarkerPickUp)) {
            openGooglePlacePicker(PICKUP);
        } else {
            openGooglePlacePicker(DESTINATION);
        }

        return false;

    }*/

    @Override
    public void onCameraIdle() {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (isPickUpAddressFromPicker) {
                    isPickUpAddressFromPicker = false;
                    return;
                }

                binding.relativeLayoutGuardMarker.setVisibility(View.GONE);
                binding.layoutPickupLocationPointer.relativeHolderPointer.setVisibility(View.VISIBLE);
                mPickUpLatLng = new LatLng(mMap.getCameraPosition().target.latitude, mMap.getCameraPosition().target.longitude);
                binding.layoutPickupLocationPointer.textViewPickupAddress.setText("Updating...");
                mHomePresenter.getAddressNameFromLatLon(mPickUpLatLng);
                //mHomePresenter.resumeGetUpdatedLocationThread();
            }
        }, 10);

    }

    @Override
    public void onCameraMoveStarted(int reason) {
        new Handler(Looper.getMainLooper()).postDelayed(new Runnable() {
            @Override
            public void run() {
                if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_GESTURE) {
                    binding.relativeLayoutGuardMarker.setVisibility(View.VISIBLE);
                    binding.layoutPickupLocationPointer.relativeHolderPointer.setVisibility(View.GONE);
                    //mHomePresenter.removeGetUpdatedLocationThread();
                } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_API_ANIMATION) {

                } else if (reason == GoogleMap.OnCameraMoveStartedListener.REASON_DEVELOPER_ANIMATION) {

                }
            }
        }, 10);


    }

    @Override
    public void setPickupAddress(String pickupAddress) {
        binding.layoutPickupLocationPointer.textViewPickupAddress.setText(pickupAddress);

    }

    @Override
    public String getPickUpAddress() {
        mPickupAddress = binding.layoutPickupLocationPointer.textViewPickupAddress.getText().toString();
        return mPickupAddress;
    }

    @Override
    public String getDropOfAddress() {
        return binding.textViewDropOffAddress.getText().toString();
    }

    @Override
    public void openAddressSearch() {
        openGooglePlacePicker(PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION);

    }

    @Override
    public Context getContext() {
        return getActivity();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showToast(String text) {
        //binding.editText2.setText(text);
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }

    @Override
    public void setDriverList(List<DriverList> driverList) {
        new Handler(Looper.getMainLooper()).postDelayed(() ->
                plotNearestVehicleImage(extractLatLngsFromDriver(driverList)), 100);

    }

    private ArrayList<LatLng> extractLatLngsFromDriver(List<DriverList> locations) {
        ArrayList<LatLng> latlngs = new ArrayList<>();
        for (DriverList result : locations) {

            Log.i(TAG, "driver_phone: " + result.getDriver().getEmail());
            Log.i(TAG, "Loc ====>: " + result.getDistance().getDuration());
            Log.i(TAG, "Base fare: " + result.getVehicleInfo().getBaseFare());
            Log.i(TAG, " fare: " + driverListResponse.getDriverList().get(0).getVehicleInfo().getBaseFare());

            latlngs.add(new LatLng(result.getDriverLocation().getLatitude(),
                    result.getDriverLocation().getLongitude()));

        }

        return latlngs;
    }

    private void plotNearestVehicleImage(ArrayList<LatLng> latlngs) {
        addVehiclesMarker(latlngs);
    }

    HashMap<Integer, Marker> markerHashMap;

    private void addVehiclesMarker(ArrayList<LatLng> latlngs) {
        markerHashMap = new HashMap<>();
        plottingVehicleMarker(latlngs);
    }

    private void plottingVehicleMarker(ArrayList<LatLng> latlngs) {
        int markerKeyCount = 0;
        for (LatLng point : latlngs) {
            options.position(point);
            options.title("Bike X");
            options.snippet("Have a happy ride");

            addMarkerIconBasedOnVehicleType(markerKeyCount);

            Marker marker = mMap.addMarker(options);

            markerHashMap.put(markerKeyCount, marker);
            markerKeyCount++;

        }
    }

    private void addMarkerIconBasedOnVehicleType(int markerKeyCount) {
        if (driverListResponse.getDriverList().get(markerKeyCount).getVehicleInfo()
                .getName().equalsIgnoreCase("cng")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.cng));
        } else if (driverListResponse.getDriverList().get(markerKeyCount).getVehicleInfo()
                .getName().equalsIgnoreCase("car")) {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.car));
        } else {
            options.icon(BitmapDescriptorFactory.fromResource(R.drawable.bike));
        }
    }

    private boolean isEmpty(List<DriverList> locations) {
        return locations.size() == 0;
    }

    @Override
    public LatLng getPickUpLocation() {
        return mPickUpLatLng;
    }

    @Override
    public int getBaseFare() {
        return driverListResponse.getDriverList().get(0).getVehicleInfo().getBaseFare();
    }

    @Override
    public int getFarePerKilo() {
        return driverListResponse.getDriverList().get(0).getVehicleInfo().getKillometre();
    }

    @Override
    public int getWaitingChargePerMin() {
        return driverListResponse.getDriverList().get(0).getVehicleInfo().getPerMinit();
    }

    @Override
    public void setBtnMenuImage(String imageURL) {
        Glide.with(getActivity()).load(imageURL)
                .asBitmap().centerCrop()
                .error(R.drawable.ic_user_image)
                .placeholder(R.drawable.ic_user_image)
                .into(binding.btnMenu);
    }

    @Override
    public void acceptRequest(Trip trip) {
        try {
            ((HomeActivity) getActivity()).currentScreenType = ACCEPTED_RIDE;
            this.trip = trip;

            mPickUpLatLng = trip.getPickUpLocation();
            mPickupAddress = trip.getPickUpAddress();

            mDropOffLatLng = trip.getDropOffLocation();
            mDropOffAddress = trip.getDropOffAddress();

            removeConfirmRideView();

            loadUIAfterAcceptRequest();
            initAcceptUIComponentsListeners();

            positioningMap();

            changeMapMoveableStatus();

            addMarkerAndSetPickUpAndDropOff();

            setMapLatLonBound();

            updateTripInfoUI(trip);

            binding.btnSetPickUp.setVisibility(View.GONE);


        } catch (Exception e) {
            Log.e("HomeFragment", "Accept Request: " + e.getMessage());

        }
    }

    @Override
    public void startTrip(Trip trip) {
        this.trip = trip;
        ((HomeActivity) getActivity()).currentScreenType = ON_RIDE;

        mPickUpLatLng = trip.getPickUpLocation();
        mPickupAddress = trip.getPickUpAddress();

        mDropOffLatLng = trip.getDropOffLocation();
        mDropOffAddress = trip.getDropOffAddress();


        loadUIAfterAcceptRequest();
        initAcceptUIComponentsListeners();
        positioningMap();

        changeMapMoveableStatus();

        addMarkerAndSetPickUpAndDropOff();

        setMapLatLonBound();

        updateTripInfoUI(trip);

        mHomePresenter.getSingleDriverLocation(trip.getDriverPhoneNo());
    }

    @Override
    public void endTrip(Trip trip) {

    }

    @Override
    public void onUpdateDriverList(JSONObject jsonObject) {
        clearMapsForRePlottingMarkerUntilConfirmRide();

        manageVehicleMarkerWhenScreenConfirmRide();

        manageNearestLocationSearch(jsonObject);

    }

    @Override
    public void onUpdateVeryLatestDriverList(JSONObject jsonObject) {
        manageVehicleMarkerWhenScreenConfirmRide();
        executeRideRequest(jsonObject);
    }

    @Override
    public void cancelRide() {
        hideLoader();
        ((HomeActivity) getActivity()).currentScreenType = PICKUP;
        setHomeUIWhenBack();
    }

    @Override
    public void startEndTrip(Object object) {
        manageFullTrip(object);
    }

    @Override
    @AskPermission(ACCESS_FINE_LOCATION)
    @SuppressWarnings({"MissingPermission"})
    public void onConnected(@Nullable Bundle bundle) {
        mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mCurrentLocation != null) {
            mPickUpLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            cameraAnimatedWithZoom(mPickUpLatLng);
            mHomePresenter.getNearestDriverFromSocket(mPickUpLatLng);
            setMapDetails();

        } else {
            connectToLocationManager();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        Toast.makeText(getActivity(), "Location change - \nLat: "
                + location.getLatitude()
                + "\nLong: "
                + location.getLongitude(), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP) {
            managePickUpPickerPlace(resultCode, data);

        } else if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE_DESTINATION) {
            manageDropOffPickerPlace(resultCode, data);

        } else
            //When request accepted by the driver
            if (requestCode == REQUEST_DRIVER_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    manageAcceptRequest(data);
                    Log.i(TAG, "onActivityResult: Accepted request!!!!" + data.getExtras().getString("data"));

                }

            }
    }

    private void manageDropOffPickerPlace(int resultCode, Intent data) {
        binding.btnSetPickUp.setVisibility(View.VISIBLE);

        if (resultCode == RESULT_OK) {
            Place place = PlaceAutocomplete.getPlace(getActivity(), data);
            setDropOff(place);

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(getActivity(), data);
            Log.i(TAG, status.getStatusMessage());

        } else if (resultCode == RESULT_CANCELED) {
            // The user canceled the operation.
        }
    }

    private void managePickUpPickerPlace(int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Place place = PlaceAutocomplete.getPlace(getActivity(), data);
            setPickup(place);

        } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
            Status status = PlaceAutocomplete.getStatus(getActivity(), data);
            Log.i(TAG, status.getStatusMessage());

        } else if (resultCode == RESULT_CANCELED) {
            // The user canceled the operation.
        }
    }

    private void manageAcceptRequest(Intent data) {
        mHomePresenter.acceptRequestFirstTime(data.getExtras().getString("data"));
    }

    private void updateTripInfoUI(Trip trip) {

        updateBigMessage(trip);

        textViewDriverName.setText(trip.getDriverName().toUpperCase());
        textViewCarName.setText(trip.getVehicleName());
        textViewCarId.setText(trip.getVehicleId());
        textViewRatings.setText(String.valueOf(trip.getDriverRating()));
        Log.e(TAG, "updateTripInfcoUI: " + trip.getDriverPhotoUrl());

        Glide.with(getActivity())
                .load(trip.getDriverPhotoUrl())
                .transform(new CircleTransform(getActivity()))
                .error(R.drawable.ic_driver_user)
                .placeholder(R.drawable.ic_avatar)
                .into(imageViewDriverPhoto);

    }

    private void setDropOff(Place place) {
        mDropOffLatLng = place.getLatLng();
        mDropOffAddress = place.getName().toString();

        binding.textViewDropOffAddress.setVisibility(View.VISIBLE);
        binding.textViewDropOffAddress.setText(mDropOffAddress);
    }

    boolean isPickUpAddressFromPicker = false;

    private void setPickup(Place place) {
        isPickUpAddressFromPicker = true;
        Log.i(TAG, "Place: " + getPickUpAddress());
        mPickUpLatLng = place.getLatLng();

        setPickupAddress(place.getName().toString());

        binding.layoutPickupLocationPointer.textViewPickupAddress.setVisibility(View.VISIBLE);
        binding.textViewDropOffAddress.setVisibility(View.VISIBLE);
        binding.layoutPickupLocationPointer.textViewPickupAddress.setText(getPickUpAddress());

        mHomePresenter.drawPath(mPickUpLatLng, place.getLatLng());

        cameraAnimatedWithZoom(mPickUpLatLng);

    }

    private void initView() {
        presenter.setBtnMenuImage();

        mMapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(map);
        mMapFragment.getMapAsync(this);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");

        CarSelectionDialog carSelectionDialog = new CarSelectionDialog(getActivity());

        carSelectionDialog.selectBike();

        carSelectionDialog.relativeLayoutCNG.setOnClickListener(v -> {

            placeSelectedCarToRoundHolder(R.drawable.ic_cng_popup, R.color.hot_pink, "CNG");

            carSelectionDialog.mBottomSheetDialog.cancel();

            carSelectionDialog.selectCng();

        });

        carSelectionDialog.relativeLayoutBike.setOnClickListener(v -> {

            placeSelectedCarToRoundHolder(R.drawable.ic_bike_popup, R.color.hot_pink, "Bike");

            carSelectionDialog.mBottomSheetDialog.cancel();

            carSelectionDialog.selectBike();

        });

        carSelectionDialog.relativeLayoutCar.setOnClickListener(v -> {

            placeSelectedCarToRoundHolder(R.drawable.ic_taxi_popup, R.color.hot_pink, "Car");

            carSelectionDialog.mBottomSheetDialog.cancel();

            carSelectionDialog.selectCar();

        });

        carSelectionDialog.carInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), VehicleDetailsActivity.class);
            intent.putExtra("EXTRA_FLAG", VehicleDetailsActivity.CAR);
            getActivity().startActivity(intent);
            carSelectionDialog.mBottomSheetDialog.cancel();
        });

        carSelectionDialog.bikeInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), VehicleDetailsActivity.class);
            intent.putExtra("EXTRA_FLAG", VehicleDetailsActivity.BIKE);
            getActivity().startActivity(intent);
            carSelectionDialog.mBottomSheetDialog.cancel();
        });

        carSelectionDialog.cngInfo.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), VehicleDetailsActivity.class);
            intent.putExtra("EXTRA_FLAG", VehicleDetailsActivity.CNG);
            getActivity().startActivity(intent);
            carSelectionDialog.mBottomSheetDialog.cancel();
        });

        binding.relativeLayoutCarSelection.setOnClickListener(v -> {
            carSelectionDialog.mBottomSheetDialog.show();
        });

        binding.relativeButtonMyLocation.setOnClickListener(v -> {
            setMyCurrentLocation();
        });

        binding.btnMenu.setOnClickListener(v -> {
            ((HomeActivity) getActivity()).openSideDrawer();

        });

        binding.btnOffers.setOnClickListener(v -> {
            ((HomeActivity) getActivity()).startActivity(InviteFriendsActivity.class, false);
        });

        binding.layoutPickupLocationPointer.linearLayoutPickupAddressHolder.setOnClickListener(v -> {
            openGooglePlacePicker(PLACE_AUTOCOMPLETE_REQUEST_CODE_PICKUP);
        });

    }

    private void placeSelectedCarToRoundHolder(int transportIcon, int holderBackoground, String transportName) {
        binding.imageViewTransportIcon.setImageResource(transportIcon);
        binding.imageViewTransportIcon.setColorFilter(ContextCompat.getColor(getActivity(), holderBackoground));
        binding.textViewTransportName.setText(transportName);
    }

    @SuppressWarnings({"MissingPermission"})
    protected void startLocationUpdates() {
        // Create the location request
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL_IN_MILLISECONDS)
                .setFastestInterval(UPDATE_INTERVAL_IN_MILLISECONDS);
        // Request location updates
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
    }

    private void setMapDetails() {
        if (mCurrentLocation != null) {
            LatLng currentLatLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
            getAddressNameFromLocation(currentLatLng);
            //startLocationUpdates(); // start updating current location

        } else {
            connectToLocationManager();

        }
    }

    private void getAddressNameFromLocation(LatLng latLng) {
        mHomePresenter.getAddressNameFromLatLon(latLng);
    }

    private void connectToLocationManager() {
        mGps = new GPSTracker(getActivity());
        // check if GPS enabled
        if (mGps.canGetLocation()) {
            mPickUpLatLng = new LatLng(mGps.getLatitude(), mGps.getLongitude());
            getAddressNameFromLocation(mPickUpLatLng);
            mHomePresenter.getNearestDriverFromSocket(mPickUpLatLng);
            setMapDetails();

            Toast.makeText(getActivity(), "LocationManager is - \n Lat: " +
                    mGps.getLatitude() + "\n Long: " + mGps.getLongitude(), Toast.LENGTH_LONG).show();
        } else {
            mGps.showSettingsAlert();
        }
    }

    private void setHomeUIWhenBack() {
        mMap.clear();

        binding.btnSetPickUp.setVisibility(View.GONE);

        mHomePresenter.resumeGetUpdatedLocationThread();
        mHomePresenter.getLatestDriverLocationList(); //get instant driver location list

        removeConfirmRideView();
        removeAcceptedView();

        setCurrentScreenFlag(PICKUP);

        visibleSomeHomeComponents();

        hideBackButtonAndConfirmRide();

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) binding.relativeMap.getLayoutParams();
        params.addRule(RelativeLayout.ABOVE, R.id.layoutActionButton);

        changeMapMoveableStatus();

        cameraAnimatedWithZoom(mPickUpLatLng);


    }

    private void setCurrentScreenFlag(int flag) {
        ((HomeActivity) getActivity()).currentScreenType = flag;
    }

    private void visibleSomeHomeComponents() {
        binding.btnSetPickUp.setVisibility(View.VISIBLE);

        binding.btnOffers.setVisibility(View.VISIBLE);
        binding.btnMenu.setVisibility(View.VISIBLE);
        binding.relativeButtonMyLocation.setVisibility(View.VISIBLE);
        binding.relativieButtonWhereToGo.setVisibility(View.VISIBLE);
        binding.layoutPickupLocationPointer.relativeHolderPointer.setVisibility(View.VISIBLE);
    }


    private int getCurrentScreenFlag() {
        return ((HomeActivity) getActivity()).currentScreenType;
    }

    private void cameraAnimatedWithZoom(LatLng latLng) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f));
    }

    private Marker getMarker(LatLng latLng, String title, String address) {
        Marker marker = mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(new Utility(getActivity())
                        .getMarkerBitmapFromView(R.drawable.ic_user, title, address))));

        return marker;
    }

    private void showWaitingRideRequestActivity(Trip tripInfo) {
        Intent intent = new Intent(getActivity(), RequestDriverActivity.class);
        intent.putExtra("trip_info", Parcels.wrap(tripInfo));
        startActivityForResult(intent, REQUEST_DRIVER_REQUEST_CODE);

    }

    private BottomSheetDialog initBottomSheetPaymentDialog() {

        PaymentDialogBottomSheet paymentDialogBottomSheet = new PaymentDialogBottomSheet(getActivity());

        paymentDialogBottomSheet.buttonCash.setOnClickListener(v -> {
            makeSelectPaymentType(paymentDialogBottomSheet, PAYMENT_TYPE_CASH);
            paymentDialogBottomSheet.dialog.cancel();

        });

        paymentDialogBottomSheet.buttonInstakePay.setOnClickListener(v -> {
            makeSelectPaymentType(paymentDialogBottomSheet, PAYMENT_TYPE_INSTAKE_PAY);
            paymentDialogBottomSheet.dialog.cancel();
        });

        paymentDialogBottomSheet.buttonCardPay.setOnClickListener(v -> {
            makeSelectPaymentType(paymentDialogBottomSheet, PAYMENT_TYPE_CARD);
            paymentDialogBottomSheet.dialog.cancel();
        });

        return paymentDialogBottomSheet.dialog;
    }

    private void makeSelectPaymentType(PaymentDialogBottomSheet paymentDialogBottomSheet, int paymentType) {
        resetAllPaymentButton(paymentDialogBottomSheet);

        mHomePresenter.setPaymentName(paymentType);

        switch (paymentType) {
            case PAYMENT_TYPE_CASH:
                paymentDialogBottomSheet.buttonCash
                        .setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_payment_cash, 0, R.drawable.ic_selection_ok, 0);
                trip.setPaymentType("Cash");
                break;

            case PAYMENT_TYPE_INSTAKE_PAY:
                trip.setPaymentType("Instake Pay");
                paymentDialogBottomSheet.buttonInstakePay
                        .setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_payment_instake, 0, R.drawable.ic_selection_ok, 0);
                break;

            case PAYMENT_TYPE_CARD:
                trip.setPaymentType("Card");
                paymentDialogBottomSheet.buttonCardPay
                        .setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_payment_visa, 0, R.drawable.ic_selection_ok, 0);
                break;

            default:
                break;

        }

    }

    private void resetAllPaymentButton(PaymentDialogBottomSheet paymentDialogBottomSheet) {
        paymentDialogBottomSheet.buttonCash.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_payment_cash, 0, 0, 0);
        paymentDialogBottomSheet.buttonInstakePay.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_payment_instake, 0, 0, 0);
        paymentDialogBottomSheet.buttonCardPay.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_payment_visa, 0, 0, 0);
    }

    private void openGooglePlacePicker(int locationType) {
        try {
            AutocompleteFilter autocompleteFilter = new AutocompleteFilter.Builder()
                    .setTypeFilter(Place.TYPE_COUNTRY)
                    .setCountry("BD")
                    .build();
            Intent intent = new PlaceAutocomplete.IntentBuilder(PlaceAutocomplete.MODE_OVERLAY)
                    .setFilter(autocompleteFilter)
                    .build(getActivity());
            startActivityForResult(intent, locationType);
        } catch (GooglePlayServicesRepairableException e) {
            // TODO: Handle the error.
        } catch (GooglePlayServicesNotAvailableException e) {
            // TODO: Handle the error.
        }
    }

    private void setMyCurrentLocation() {
        LatLng latLng = new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(latLng, 16f);
        mMap.animateCamera(cameraUpdate);
    }

    private void plottingVehiclesMarker(NearestDriverResponse driverListResponse) {
        setDriverList(driverListResponse.getDriverList());
    }

    public void sendRideRequest(Trip tripInfo, JSONObject jObj) {
        mHomePresenter.sendDriverRequest(tripInfo, jObj);
    }

    private void manageVehicleMarkerWhenScreenConfirmRide() {
        if (isScreenConfirmRide()) {
            removeAllVehicleMarker();

        }
    }

    @Nullable
    private void removeAllVehicleMarker() {
        if (markerHashMap != null) {
            for (int i = 0; markerHashMap.size() > i; i++) {
                Marker marker1 = markerHashMap.get(i);
                if (marker1 != null) {
                    marker1.remove();
                }

                markerHashMap.remove(i);
            }
        }

    }

    private void clearMapsForRePlottingMarkerUntilConfirmRide() {
        //mMap.clear();
        removeAllVehicleMarker();
    }

    private boolean isScreenConfirmRide() {
        if (((HomeActivity) getActivity()) != null) {
            return ((HomeActivity) getActivity()).currentScreenType == CONFIRM_RIDE;
        }
        return false;
    }

    private void manageNearestLocationSearch(JSONObject event) {
        JSONObject jsonObject = event;

        driverListResponse = new NearestJsonParser(new Gson()).getResponseToLocationList(jsonObject.toString());

        if (!isScreenConfirmRide()) {
            plottingVehiclesMarker(driverListResponse);
        }

        Log.i(TAG, "new loc: " + jsonObject.toString());
    }

    private void executeRideRequest(JSONObject jsonObject) {
        hideLoader();
        buildTripRequest(jsonObject);

        mHomePresenter.removeGetUpdatedLocationThread();
        mHomePresenter.confirmRideStatus(false);

        showWaitingRideRequestActivity(trip);
        sendRideRequest(trip, jObjNearestDriver);
    }

    private boolean rideStatus(Trip trip, String started) {
        return trip.currentRideStatus.equalsIgnoreCase(started);
    }

    //@TODO this method need to transfer to presenter
    private void buildTripRequest(JSONObject jsonObject) {
        try {
            Log.e(TAG, "SEND REQUEST");

            JSONArray jsonArrayDriverList = jsonObject.getJSONArray("driver_list");

            if (!isEmptyDriver()) {
                jObjNearestDriver = jsonArrayDriverList.getJSONObject(0);
                trip.tripId = "start-trip-id-888";
                trip.setCurrentRideStatus("requesting");
                trip.setDriverPhoneNo(driverListResponse.getDriverList().get(0).getDriver().getPhoneNo());
                trip.setPickUpLocation(mPickUpLatLng);
                trip.setPickUpAddress(getPickUpAddress());
                trip.setDropOffAddress(mDropOffAddress);
                trip.setDropOffLocation(mDropOffLatLng);

            } else {
                trip.tripId = "start-trip-id-000";
                trip.currentRideStatus = "no_driver";

            }

        } catch (Exception e) {
            Log.e(this.getClass().getSimpleName(), "buildTripRequest:: " + e.getMessage());
        }
    }

    public void showDialogCancelRide() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = getActivity().getLayoutInflater();

        View dialogLayout = inflater.inflate(R.layout.dialog_ride_cancel, null);
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();

        Button textButtonSubmit = (Button) dialogLayout.findViewById(R.id.text_button_submit);
        textButtonSubmit.setOnClickListener(view1 -> {
            dialog.dismiss();
            showLoader();
            mHomePresenter.cancelRide(trip.tripId);
        });


    }

    private boolean isEmptyDriver() {
        if (driverListResponse == null) {
            return true;
        }

        if (driverListResponse.getDriverList().size() == 0) {
            return true;
        }

        return false;
    }

    Handler handlerUpdatedLocationData = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            synchronized (this) {
                if (msg.arg1 == 2) {
                    Trip trip = (Trip) msg.obj;
                    if (rideStatus(trip, "started")) {
                        updateBigMessage(trip);

                    } else if (rideStatus(trip, "end")) {
                        openEndTripActivity(trip);
                    }
                }
            }

            return false;
        }
    });

}
