package com.instake.passenger.feature.editprofile;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.monowar.invitefriends.InviteFriendsView;
import com.instake.passenger.monowar.invitefriends.InviteFriendsViewPresenter;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Mostafa Monowar on 15-Aug-17.
 */

@Module
public class EditProfileModule {

    private EditProfileView view;

    public EditProfileModule(EditProfileView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public EditProfilePresenter provideEditProfileViewPresenter(ApiService apiService, Session session, Validator validator) {
        return new EditProfilePresenter(view, apiService, session, validator);
    }
}
