package com.instake.passenger.feature.advancebook;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;

/**
 * Created by Tushar on 4/10/17.
 */

public class AdvancedBookingActivity extends SiriusActivity {

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_container;
    }

    @Override
    protected void initComponents() {
        getSupportFragmentManager().beginTransaction().add(R.id.container,
                new AdvancedBookingFragment()).commit();
    }
}
