package com.instake.passenger.feature.signup.confirmaccount;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.User;

/**
 * Created by Tushar on 5/12/17.
 */

public interface ConfirmAccountView extends BaseView {

    void initFbLoginComponent();

    void setNameOnView(String fbName);

    void setEmailOnView(String fbEmail);

    String getPhoneNumber();

    String getEmail();

    String getFullName();

    String getReferralCode();

    void onErrorEmailEmpty();

    void onErrorEmailInvalid();

    void onErrorFullNameEmpty();

    void onErrorFullNameInvalid();

    void onErrorReferralCodeInvalid();

    CharSequence getEmailError();

    CharSequence getFullNameError();

    CharSequence getReferralCodeError();

    void hideEmailError();

    void hideFullNameError();

    void hideReferralCodeError();
    void startHomeActivity();

    void showSignUpSuccess();

    void showSignUpFailure();

    void navigateToLogin(User user);
}
