package com.instake.passenger.feature.confirmride;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityConfirmAccountBinding;

import static com.instake.passenger.R.id.map;

public class ConfirmRideActivity extends SiriusActivity implements OnMapReadyCallback,
        GoogleMap.OnMapLongClickListener,
        GoogleMap.OnMapClickListener {

    private ActivityConfirmAccountBinding binding;
    private GoogleMap mMap;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_confirm_ride;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityConfirmAccountBinding) getDataBinding();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Your title");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        SupportMapFragment mMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(map);
        mMapFragment.getMapAsync(this);
    }

    @Override
    public void onMapClick(LatLng latLng) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {

    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.setOnMapLongClickListener(this);
            mMap.setOnMapClickListener(this);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setMapToolbarEnabled(false);
        }

        LatLng latLng = new LatLng(23.758199, 90.367149);
        setMarkerInfo(latLng, "Destination", "Greed Road");


        LatLng latLng1 = new LatLng(23.805915, 90.364447);
        setMarkerInfo(latLng1, "Pickup", "Rampura Bridge");

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        builder.include(latLng);
        builder.include(latLng1);
        LatLngBounds bounds = builder.build();

        mMap.setPadding(0, 250, 0, 0);
        mMap.animateCamera(CameraUpdateFactory.newLatLngBounds(bounds, 0));
    }

    private void setMarkerInfo(LatLng latLng, String title, String address) {
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .icon(BitmapDescriptorFactory.fromBitmap(getMarkerBitmapFromView(R.drawable.ic_user, title, address))));
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16f));
        Toast.makeText(this, "Location:" + latLng.latitude + ", " + latLng.longitude, Toast.LENGTH_SHORT).show();

    }

    private Bitmap getMarkerBitmapFromView(@DrawableRes int resId, String title, String address) {

        View customMarkerView = ((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE)).
                inflate(R.layout.layout_custom_map_marker, null);
        TextView markerImageView = (TextView) customMarkerView.findViewById(R.id.imageViewLavel);
        TextView textViewPickupAddress = (TextView) customMarkerView.findViewById(R.id.textViewPickupAddress);
        markerImageView.setText(title);
        textViewPickupAddress.setText(address);

        customMarkerView.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
        customMarkerView.layout(0, 0, customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight());
        customMarkerView.buildDrawingCache();
        Bitmap returnedBitmap = Bitmap.createBitmap(customMarkerView.getMeasuredWidth(), customMarkerView.getMeasuredHeight(),
                Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(returnedBitmap);
        canvas.drawColor(Color.WHITE, PorterDuff.Mode.SRC_IN);
        Drawable drawable = customMarkerView.getBackground();
        if (drawable != null)
            drawable.draw(canvas);
        customMarkerView.draw(canvas);
        return returnedBitmap;
    }

}


