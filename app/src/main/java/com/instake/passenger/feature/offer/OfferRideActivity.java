package com.instake.passenger.feature.offer;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityOfferBinding;
import com.instake.passenger.dipendency.offer.OfferModule;
import com.instake.passenger.dipendency.ridehistory.RideHistoryModule;
import com.instake.passenger.model.Offer;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by mahmudul.hasan on 4/23/2017.
 */

public class OfferRideActivity extends SiriusPresenterActivity<OfferRideView, OfferRidePresenter> implements OfferRideView {

    ActivityOfferBinding binding;

    @Inject
    OfferRidePresenter offerRidePresenter;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_offer;
    }

    @Override
    protected OfferRidePresenter bindPresenter() {
        app.getAppComponent().plus(new OfferModule(this)).inject(this);
        return offerRidePresenter;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityOfferBinding) getDataBinding();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_chat);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable upArrow = ContextCompat.getDrawable(OfferRideActivity.this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            toolbar.setTitleTextColor(0xFFFFFFFF);
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.rv_offer);
        OfferRecyclerAdapter recyclerAdapter = new OfferRecyclerAdapter(OfferRideActivity.this, getData());
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false));
//        recyclerView.addItemDecoration(new DividerItemDecoration(this, android.R.drawable.divider_horizontal_bright));
        recyclerView.setAdapter(recyclerAdapter);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:

                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED, resultIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private List<Offer> getData() {
        List<Offer> myList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            Offer offer = new Offer();
            myList.add(offer);
        }
        return myList;
    }

    @Override
    public void showToast(String message) {

    }

}
