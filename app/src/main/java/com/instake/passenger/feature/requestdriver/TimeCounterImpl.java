package com.instake.passenger.feature.requestdriver;

import android.os.Handler;

/**
 * Created by masum on 3/30/17.
 */

public class TimeCounterImpl {
    private int count = 30;
    private  TimeCounterInteractor timeCounterInteractor;
    private Handler handler;
    private Runnable r;

    public void getTime(TimeCounterInteractor timeCounterInteractor) {
        this.timeCounterInteractor = timeCounterInteractor;

        handler = new Handler();
        r = new Runnable() {
            @Override
            public void run() {
                if (count == 0) {
                    handler.removeCallbacks(this);
                } else {
                    count--;
                    handler.postDelayed(this, 1000);
                    timeCounterInteractor.countTimer(String.valueOf(count));
                }
            }
        };
        handler.postAtTime(r, 1000);
    }

    public void removeTimerHandler() {
        handler.removeCallbacks(r);
    }

}
