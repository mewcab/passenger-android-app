package com.instake.passenger.feature.editprofile;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.User;

/**
 * Created by Mostafa Monowar on 24-Aug-17.
 */

public interface EditProfileView extends BaseView {
    void setUserInfo(User user);

    void initFacebookComponent();

    String getFullName();

    String getEmail();

    String getPhoneNumber();

    void setFbProfileImageUrl(String imageUrl);

    String getFbProfileImageUrl();

    String getImageUri();

    void showFullNameError();

    void showEmailError(String error);

    void showPhoneNumberError();

    void hideAllErrors();

    void editProfileFailed(String error);

    void goBackToProfileActivity();

    void hideKeyboard();
}
