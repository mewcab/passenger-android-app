package com.instake.passenger.feature.helpandsupport;

import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityHelpAndSupportBinding;
import com.instake.passenger.model.HelpAndSupportModel;

import java.util.ArrayList;

/**
 * Created by mahmudul.hasan on 5/7/2017.
 */

public class HelpAndSupportActivity extends SiriusActivity {

    ActivityHelpAndSupportBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_help_and_support;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityHelpAndSupportBinding) getDataBinding();

        setSupportActionBar(binding.animToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        binding.collapsingToolbar.setTitle("Support/Help");
        binding.collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        HelpAndSupportAdapter adapter = new HelpAndSupportAdapter(HelpAndSupportActivity.this, prePareData());
        binding.recyclerView.setAdapter(adapter);
    }

    public ArrayList<HelpAndSupportModel> prePareData() {
        ArrayList<HelpAndSupportModel> dataList = new ArrayList<>();

        HelpAndSupportModel model;

        model = new HelpAndSupportModel();
        model.setId("1");
        model.setTitle("How to identity a driver and  vehicle?");
        model.setDetail("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since !");
        dataList.add(model);

        model = new HelpAndSupportModel();
        model.setId("1");
        model.setTitle("How to remove account?");
        model.setDetail("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since !");
        dataList.add(model);

        model = new HelpAndSupportModel();
        model.setId("1");
        model.setTitle("Do riders pay tolls or charge?");
        model.setDetail("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since !");
        dataList.add(model);

        model = new HelpAndSupportModel();
        model.setId("1");
        model.setTitle("How to remove account?");
        model.setDetail("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since !");
        dataList.add(model);

        model = new HelpAndSupportModel();
        model.setId("1");
        model.setTitle("Do riders pay tolls or charge?");
        model.setDetail("Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since !");
        dataList.add(model);
        return dataList;
    }
}
