package com.instake.passenger.feature.endride;

import android.content.Intent;
import android.util.Log;

import com.android.volley.NetworkError;
import com.instake.passenger.Rx.RxService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.endreview.EndRideReviewActivity;
import com.instake.passenger.feature.splash.SplashActivity;
import com.instake.passenger.model.Rating;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.payment.Tips;

import org.parceler.Parcels;

import okhttp3.ResponseBody;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by The Game on 4/2/17.
 */

public class EndRidePresenterImpl implements EndRideContract.EndRidePresenter {
    private static final String TAG = "EndRidePresenterImpl";
    EndRideContract.EndRideView mEndRideView;
    Session session;

    public EndRidePresenterImpl(EndRideContract.EndRideView rideView, Session session) {
        mEndRideView = rideView;
        this.session = session;
    }

    public void setTripForSavingToSession(Trip trip) {
        session.saveObject("trip", trip);
    }

    @Override
    public void onPostRating(Trip trip, Rating rating) {
        mEndRideView.showLoader();
        CompositeSubscription subscriptions = new CompositeSubscription();
        Subscription subscription = new RxService(session).postRating(trip, rating, new RxService.postApiCallBack() {
            @Override
            public void onSuccess(ResponseBody response) {
                saveAppLocalState(App.APP_IN_HOME_UI);

                mEndRideView.hideLoader();
                mEndRideView.showMessage("Thanks for using mewcab!");

                openSplashScreenAfterARideEnd();

            }

            @Override
            public void onError(NetworkError networkError) {
                Log.e(TAG, "onError: " + networkError.getMessage());
            }
        });

        subscriptions.add(subscription);

    }

    private void openSplashScreenAfterARideEnd() {
        Intent intent = new Intent(mEndRideView.getContext(), SplashActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mEndRideView.getContext().startActivity(intent);
    }

    public void saveAppLocalState(int app_state) {
        Log.e(TAG, "app local state : ====> state " + app_state + " ==>bool " + session.saveLocalAppState(app_state));
        session.saveLocalAppState(app_state);
    }

    @Override
    public void onPostTips(Trip trip, Tips tips) {
        mEndRideView.showLoader();
        try {
            App.getInstance().sendTips(tips, trip.getDriverPhoneNo());
            mEndRideView.hideLoader();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBtnSubmitClick() {
        if (mEndRideView != null) {

            if (mEndRideView.getRating() <= 0f) {
                // EducationDetails message. We will change that
                mEndRideView.showMessage("Please give your rating");
            } else {
                // User already gave his rating. Do whatever you want
                mEndRideView.showMessage("Congratulation. You click button submit.");

            }
        }
    }

    public void openEndRideReviewActivity(Trip trip) {
        Log.e("--->", "manageTripWhenStateFinish: ====> state " + session.getAppLocalState() + " pass " + App.APP_IN_RATING_UI);
        Intent intent = new Intent(mEndRideView.getContext(), EndRideReviewActivity.class);
        intent.putExtra("trip_info", Parcels.wrap(trip));
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        mEndRideView.getContext().startActivity(intent);
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {
        App.getInstance().disconnectSocket();
        mEndRideView = null;

    }
}
