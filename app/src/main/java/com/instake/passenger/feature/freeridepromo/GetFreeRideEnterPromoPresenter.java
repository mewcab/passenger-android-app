package com.instake.passenger.feature.freeridepromo;

import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.base.BaseView;

/**
 * Created by mahmudul.hasan on 4/30/2017.
 */

public class GetFreeRideEnterPromoPresenter implements BasePresenter {
    BaseView baseView;

    public GetFreeRideEnterPromoPresenter(BaseView baseView) {
        this.baseView = baseView;
    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}
