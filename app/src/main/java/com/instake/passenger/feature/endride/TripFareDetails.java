package com.instake.passenger.feature.endride;

import org.parceler.Parcel;

import java.util.List;

@Parcel
public class TripFareDetails {
    public float total;

    public List<BreakdownFare> breakdown = null;

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

    public List<BreakdownFare> getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(List<BreakdownFare> breakdown) {
        this.breakdown = breakdown;
    }

}