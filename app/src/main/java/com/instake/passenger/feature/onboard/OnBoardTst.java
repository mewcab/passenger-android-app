package com.instake.passenger.feature.onboard;

import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

import com.instake.passenger.R;

/**
 * Created by mahmudul.hasan on 6/15/2017.
 */

public class OnBoardTst extends AppCompatActivity {

    private ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tst_onbrd);

        final HorizontalScrollView scrollView = (HorizontalScrollView) findViewById(R.id.scroll_view);
        final ImageView imageView = (ImageView) findViewById(R.id.background);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.view_pager);

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
                int x = (int) ((viewPager.getWidth() * position + positionOffsetPixels) * computeFactor());
                scrollView.scrollTo(x, 0);
            }

            @Override
            public void onPageSelected(int position) {

            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }

            private float computeFactor() {
                return (imageView.getWidth() - viewPager.getWidth()) /
                        (float)(viewPager.getWidth() * (viewPager.getAdapter().getCount() - 1));
            }
        });
        viewPagerAdapter = new ViewPagerAdapter(this);

        viewPager.setAdapter(viewPagerAdapter);
    }
}
