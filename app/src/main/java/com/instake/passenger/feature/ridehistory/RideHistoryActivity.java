package com.instake.passenger.feature.ridehistory;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityRideHistoryBinding;
import com.instake.passenger.dipendency.ridehistory.RideHistoryModule;
import com.instake.passenger.model.historydata.Trip;
import com.instake.passenger.monowar.EndlessRecyclerViewScrollListener;
import com.instake.passenger.monowar.EqualSpacingItemDecoration;
import com.instake.passenger.monowar.RecyclerTouchListener;
import com.instake.passenger.monowar.supportandhelp.c.TripsHelpDetailsActivity;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

public class RideHistoryActivity extends SiriusPresenterActivity<RideHistoryView, RideHistoryPresenter> implements RideHistoryView {

    private static final int SCROLL_DIRECTION_UP = -1;
    public int page = 1;
    private boolean isLoadMore = true;

    @Inject
    RideHistoryPresenter presenter;

    private RideHistoryAdapter adapter;
    private List<Trip> tripList;

    private ActivityRideHistoryBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_ride_history;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityRideHistoryBinding) getDataBinding();
        setSupportActionBar(binding.toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        binding.collapsingToolbarLayout.setTitle(getString(R.string.title_activity_ride_history));
        Typeface expandedFont = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Black.ttf");
        binding.collapsingToolbarLayout.setExpandedTitleTypeface(expandedFont);

        initRecyclerView();
    }

    @Override
    protected RideHistoryPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new RideHistoryModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    public int getPageNumber() {
        return page;
    }

    @Override
    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onLoadDataIntoAdapter(List<Trip> model_list) {
        if (model_list.size() == 0) {
            isLoadMore = false;
        } else {
            page++;
            if (adapter == null) {
                tripList = model_list;
                adapter = new RideHistoryAdapter(getActivity(), tripList);
                binding.recyclerView.setAdapter(adapter);
            } else {
                tripList.addAll(model_list);
                adapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void showLoading() {
        binding.layoutProgress.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        binding.layoutProgress.setVisibility(View.GONE);
    }

    private void initRecyclerView() {
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        binding.recyclerView.setLayoutManager(mLayoutManager);
        binding.recyclerView.setItemAnimator(new DefaultItemAnimator());
        int defaultGap = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_4dp), getResources().getDisplayMetrics());
        binding.recyclerView.addItemDecoration(new EqualSpacingItemDecoration(defaultGap, EqualSpacingItemDecoration.VERTICAL));

        int elevation = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, getResources().getDimension(R.dimen.size_2dp), getResources().getDisplayMetrics());
        EndlessRecyclerViewScrollListener scrollListener = new EndlessRecyclerViewScrollListener(mLayoutManager) {
            @Override
            public void onLoadMore(int nextPage, int totalItemsCount, RecyclerView view) {
                if (isLoadMore) {
                    presenter.getApiData(page);
                }
            }

            @Override
            public void onScrolled(RecyclerView view, int dx, int dy) {
                super.onScrolled(view, dx, dy);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (binding.recyclerView.canScrollVertically(SCROLL_DIRECTION_UP)) {
                        binding.appbar.setElevation(elevation);
                    } else {
                        binding.appbar.setElevation(0);
                    }
                }
            }
        };
        binding.recyclerView.addOnScrollListener(scrollListener);

        binding.recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getContext(), binding.recyclerView, new RecyclerTouchListener.OnRecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                view.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Trip trip = tripList.get(position);
                        Intent intent = new Intent(getActivity(), TripsHelpDetailsActivity.class);
                        intent.putExtra("EXTRA_RIDE_HISTORY", Parcels.wrap(trip));
                        startActivity(intent);
                    }
                }, 200);
            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
    }
}
