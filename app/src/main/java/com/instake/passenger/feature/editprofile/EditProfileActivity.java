package com.instake.passenger.feature.editprofile;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityEditProfileBinding;
import com.instake.passenger.model.User;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.Arrays;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 5/7/17.
 */

public class EditProfileActivity extends SiriusPresenterActivity<EditProfileView, EditProfilePresenter> implements EditProfileView {

    private static final String TAG = "EditProfileActivity";

    private CallbackManager mCallbackManager;

    private ActivityEditProfileBinding binding;

    @Inject
    EditProfilePresenter presenter;

    private String fbImageUrl, imageUri;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_edit_profile;
    }

    @Override
    protected EditProfilePresenter bindPresenter() {
        app.getAppComponent()
                .plus(new EditProfileModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityEditProfileBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        binding.ivProfileImage.setOnClickListener(v -> presenter.onImageClick());

        binding.importFromFacebook.setOnClickListener(v -> {
            LoginManager.getInstance().logInWithReadPermissions(EditProfileActivity.this, Arrays.asList("public_profile", "email"));
        });
    }

    @Override
    public void setUserInfo(User user) {
        binding.etFullName.setText(user.getFullName());
        binding.etEmail.setText(user.getEmail());
        binding.etPhoneNumber.setText(user.getPhoneNumber().substring(3));
        Glide.with(getContext().getApplicationContext())
                .load(user.getProfileImageUrl())
                .dontAnimate()
                .error(R.drawable.backgroud_select_image)
                .placeholder(R.drawable.backgroud_select_image)
                .into(binding.ivProfileImage);
    }

    @Override
    public String getFullName() {
        return binding.etFullName.getText().toString();
    }

    @Override
    public String getEmail() {
        return binding.etEmail.getText().toString();
    }

    @Override
    public String getPhoneNumber() {
        return binding.tvCountryCode.getText().toString() + binding.etPhoneNumber.getText().toString();
    }

    @Override
    public void setFbProfileImageUrl(String imageUrl) {
        fbImageUrl = imageUrl;
        Glide.with(getContext().getApplicationContext())
                .load(fbImageUrl)
                .dontAnimate()
                .error(R.drawable.backgroud_select_image)
                .placeholder(R.drawable.backgroud_select_image)
                .into(binding.ivProfileImage);
    }

    @Override
    public String getFbProfileImageUrl() {
        return fbImageUrl;
    }

    @Override
    public String getImageUri() {
        return imageUri;
    }

    @Override
    public void showFullNameError() {
        binding.etFullName.setError(getString(R.string.error_full_name));
    }

    @Override
    public void showEmailError(String error) {
        binding.etEmail.setError(error);
    }

    @Override
    public void showPhoneNumberError() {
        binding.etPhoneNumber.setError(getString(R.string.please_enter_a_valid_phone_number));
    }

    @Override
    public void hideAllErrors() {

    }

    @Override
    public void initFacebookComponent() {
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.e("****", "***Twice****");

                        presenter.onFbLoginSuccess(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.e(TAG, exception.toString());
                        LoginManager.getInstance().logOut();
                    }
                });
    }

    @Override
    public void editProfileFailed(String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(EditProfileActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void goBackToProfileActivity() {
        onBackPressed();
    }

    @Override
    public void hideKeyboard() {
        hideKeyboard(binding.etPhoneNumber);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.save, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.action_save:
                presenter.onSaveClick();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri resultUri = result.getUri();
                imageUri = resultUri.getPath();
                Log.i(TAG, "Image info: " + resultUri.toString());
                Glide.with(getContext().getApplicationContext())
                        .load(resultUri)
                        .dontAnimate()
                        .error(R.drawable.backgroud_select_image)
                        .placeholder(R.drawable.backgroud_select_image)
                        .into(binding.ivProfileImage);
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
                error.printStackTrace();
            }
        } else {
            mCallbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }
}
