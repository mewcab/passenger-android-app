package com.instake.passenger.feature.advancebook;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityAdvanceBookingReportBinding;

/**
 * Created by Tushar on 4/21/17.
 */

public class AdvanceBookingReportActivity extends SiriusActivity {

    ActivityAdvanceBookingReportBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_advance_booking_report;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityAdvanceBookingReportBinding) getDataBinding();

        binding.btnEdit.setOnClickListener(v -> AdvanceBookingReportActivity.this.finish());

        binding.btnConfirm.setOnClickListener(v -> AdvanceBookingReportActivity.this.finish());

        binding.btnClose.setOnClickListener(v -> AdvanceBookingReportActivity.this.finish());
    }
}
