package com.instake.passenger.feature.home;

import com.google.android.gms.maps.model.PolylineOptions;

/**
 * Created by masum on 3/12/17.
 */

public interface PathListener {
    void onSuccessPathDraw(PolylineOptions polylineOptions);
}
