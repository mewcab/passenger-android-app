package com.instake.passenger.feature.advancebook;

import android.content.Intent;
import android.support.design.widget.BottomSheetDialogFragment;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.instake.passenger.R;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.base.SiriusFragment;
import com.instake.passenger.databinding.FragmentAdvancedBookingBinding;
import com.instake.passenger.feature.vehicledetails.VehicleDetailsActivity;

/**
 * Created by Tushar on 4/9/17.
 */
public class AdvancedBookingFragment extends SiriusFragment implements OnMapReadyCallback {

    private FragmentAdvancedBookingBinding binding;
    private SupportMapFragment mMapFragment;
    private GoogleMap mMap;

    @Override
    protected BasePresenter bindPresenter() {
        return null;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.fragment_advanced_booking;
    }

    @Override
    protected void initFragmentComponents() {
        binding = (FragmentAdvancedBookingBinding) getDataBinding();
        mMapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mMapFragment.getMapAsync(this);

        binding.rideInfoLayout1.vehiclePreview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), VehicleDetailsActivity.class));
            }
        });

        binding.rideInfoLayout1.tvEdit.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), DateTimePickerActivity.class));
        });

        binding.rideInfoLayout2.paymentLayout.setOnClickListener(v -> {
            BottomSheetDialogFragment bottomSheetDialogFragment = new PaymentMethodBottomSheetDialogFragment();
            bottomSheetDialogFragment.show(getActivity().getSupportFragmentManager(), bottomSheetDialogFragment.getTag());
        });

        binding.actionNextBtn.setOnClickListener(v -> {
            startActivity(new Intent(getActivity(), AdvanceBookingReportActivity.class));
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (mMap != null) {
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            mMap.getUiSettings().setCompassEnabled(false);
            mMap.getUiSettings().setMyLocationButtonEnabled(false);
            mMap.getUiSettings().setMapToolbarEnabled(false);
        }
    }
}