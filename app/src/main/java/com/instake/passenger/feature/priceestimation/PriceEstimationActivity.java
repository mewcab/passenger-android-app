package com.instake.passenger.feature.priceestimation;

import android.support.v4.content.ContextCompat;
import android.view.MenuItem;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityPriceEstimateBinding;

/**
 * Created by user on 5/8/2017.
 */

public class PriceEstimationActivity extends SiriusActivity {

    private ActivityPriceEstimateBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_price_estimate;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityPriceEstimateBinding) getDataBinding();
        setSupportActionBar(binding.animToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        binding.collapsingToolbar.setTitle("Price estimation");
        binding.collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
