package com.instake.passenger.feature.requestdriver;

/**
 * Created by masum on 3/30/17.
 */

public interface TimeCounterInteractor {
    void countTimer(String value);
}
