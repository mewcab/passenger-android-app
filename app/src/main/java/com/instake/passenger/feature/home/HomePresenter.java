package com.instake.passenger.feature.home;

import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.util.Log;

import com.android.volley.NetworkError;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;
import com.instake.passenger.Rx.RxService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.dipendency.home.DaggerHomePresenterComponent;
import com.instake.passenger.dipendency.home.HomePresenterModule;
import com.instake.passenger.feature.endride.BreakdownFare;
import com.instake.passenger.feature.endride.TripFareDetails;
import com.instake.passenger.model.GeoCoder;
import com.instake.passenger.model.LocationInfo;
import com.instake.passenger.model.Path;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.geocoder.GeoCoderPlaceResponse;
import com.instake.passenger.model.mapdistance.DistanceResponse;
import com.instake.passenger.model.nearestdriver.AcceptedTripInfoJsonParser;
import com.instake.passenger.model.nearestdriver.AcceptedTripResponse;
import com.instake.passenger.model.nearestdriver.Breakdown;
import com.instake.passenger.model.nearestdriver.EndTripInfoJsonParser;
import com.instake.passenger.model.nearestdriver.EndTripResponse;
import com.instake.passenger.model.payment.Fare;
import com.instake.passenger.model.payment.Payment;
import com.instake.passenger.model.state.ApplicationStateJsonParser;
import com.instake.passenger.model.state.Response;
import com.instake.passenger.socket.SocketCallbackInterface;
import com.instake.passenger.utility.Utility;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by masum on 2/28/17.
 */

public class HomePresenter implements GeoCoderListener,
        PathListener, SocketCallbackInterface, BasePresenter<HomeView> {
    private static final String TAG = "HomePresenter";
    private WeakReference<HomeView> mapView;
    private GeoCoder mGeoCoder;
    private RxService rxService;
    private static Handler handler = new Handler();
    public boolean isCloseApplication = false;

    Trip trip = new Trip();

    @Inject
    CompositeSubscription subscriptions;

    @Inject
    Fare fare;

    Session session = null;

    public HomePresenter(HomeView mapView, Session session) {
        DaggerHomePresenterComponent.builder().homePresenterModule(
                new HomePresenterModule()).build().inject(this);

        App.getInstance().bindCallBackInterface(this);

        this.session = session;
        this.mapView = new WeakReference<>(mapView);
        this.mGeoCoder = new GeoCoder(this.mapView.get().getContext(), this);
        this.rxService = new RxService(session);

        Log.d(TAG, "HomePresenter: User Name" + session.getUser().getPhoneNumber());
        Log.i(TAG, "Token: " + session.getToken());

    }

    @Override
    public void onGotAddress(LocationInfo locationInfo) {
        if (mapView.get() != null) mapView.get().setPickupAddress(locationInfo.getAddress());
    }

    @Override
    public void onSuccessPathDraw(PolylineOptions polylineOptions) {
        if (mapView != null) mapView.get().onDrawPath(polylineOptions);

    }

    @Override
    public void onCreatePresenter() {

    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {
        Log.e("Close", "Fragment***");
    }

    @Override
    public void onConnect(Object... args) {
        Log.d(TAG, "Connected");
    }

    @Override
    public void onUpdatedDriverList(Object... updatedNearestDriverList) {
        JSONObject data = (JSONObject) updatedNearestDriverList[0];
        new Thread() {
            public void run() {
                Message msg1 = new Message();
                msg1.arg1 = 2;
                msg1.obj = data;
                handlerUpdatedLocationData.sendMessage(msg1);
            }
        }.start();

    }

    @Override
    public void onRideAccepted(Object... data) {
        try {
            JSONObject jsonRideAccepted = (JSONObject) data[0];
            Log.i(TAG, "call: " + "After accepted!!!" + jsonRideAccepted.toString());
            App.getInstance().bus().send(jsonRideAccepted.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onRideStarted(Object... args) {

        try {
            JSONObject jsonRideStarted = (JSONObject) args[0];
            Log.i(TAG, "onRideStarted: " + jsonRideStarted.toString());

            if (isOriginalPassenger(jsonRideStarted)) {
                Trip trip = buildStartedTrip(jsonRideStarted);
                //App.getInstance().bus().send(trip);
                new Thread() {
                    public void run() {
                        Message msg1 = new Message();
                        msg1.arg1 = 3;
                        msg1.obj = trip;
                        handlerStartAndEndTrip.sendMessage(msg1);
                    }
                }.start();
            }

        } catch (Exception e) {
            Log.e("Start RIDE", "Exception " + e.getMessage());
        }

    }

    @Override
    public void onRideEnd(Object... data) {
        try {
            JSONObject jsonRideStarted = (JSONObject) data[0];
            Log.i(TAG, "End: Hope you had a nice journey!!!" + jsonRideStarted.toString());

            if (isOriginalPassenger(jsonRideStarted)) {
                Trip trip = buildEndTrip(jsonRideStarted);
                //App.getInstance().bus().send(trip);
                Message msg1 = new Message();
                msg1.arg1 = 3;
                msg1.obj = trip;
                handlerStartAndEndTrip.sendMessage(msg1);
            }

        } catch (Exception e) {
            Log.e(TAG, "On Ride EndException " + e.getMessage());
        }

    }

    private Trip buildStartedTrip(JSONObject jsonRideStarted) throws JSONException {
        //Trip tripInfo = new Trip();
        trip.setCurrentRideStatus("started");
        trip.setTripId(jsonRideStarted.getJSONObject("trip").getString("_id"));
        trip.setDriverPhoneNo(jsonRideStarted.getJSONObject("driver").getString("phone_no"));
        trip.setTripBigMessage("ON RIDE");
        trip.setTripSmallMessage("Go safer, faster and together");
        return trip;
    }

    private boolean isOriginalPassenger(JSONObject jsonRideStarted) throws JSONException {
        return session.getUser().getPhoneNumber().contains(jsonRideStarted.getJSONObject("passenger").getString("phone_no"));
    }

    private Trip buildEndTrip(JSONObject jsonRideStarted) throws JSONException {

        EndTripResponse endTripResponse = new EndTripInfoJsonParser(new Gson())
                .getAcceptedTripInfo(jsonRideStarted.toString());

        trip.setCurrentRideStatus("end");
        trip.setTripId(endTripResponse.getTrip().getId());
        trip.setDriverPhoneNo(endTripResponse.getDriver().getPhoneNo());
        trip.setPassengerPhoneNo(endTripResponse.getPassenger().getPhoneNo());
        trip.setTripTotalFare(endTripResponse.getFareDetails().getTotal() + "");

        List<BreakdownFare> breakdownFares = new ArrayList<>();

        for (Breakdown fare : endTripResponse.getFareDetails().getBreakdown()) {
            BreakdownFare breakdownFare = new BreakdownFare();
            breakdownFare.setTitle(fare.getTitle());
            breakdownFare.setAmount(fare.getAmount());
            breakdownFares.add(breakdownFare);
        }

        TripFareDetails tripFareDetails = new TripFareDetails();
        tripFareDetails.setTotal(Integer.parseInt(trip.getTripTotalFare()));
        tripFareDetails.setBreakdown(breakdownFares);

        trip.setFareDetails(tripFareDetails);


        return trip;
    }

    @Override
    public void onDisconnect(Object... args) {
        Log.i(TAG, "Disconnect Socket");
        if (isCloseApplication) {
            Log.i(TAG, "Disconnect Socket");
        } else {
            //App.getInstance().connectSocket(session.getUser().getPhoneNumber());
        }

    }

    public void setBtnMenuImage() {
        mapView.get().setBtnMenuImage(session.getUser().getProfileImageUrl());
    }

    @Override
    public void setAppState(String appStateAsString) {
        Log.d(TAG, "App State Calling twice: ");
        Log.d(TAG, "state json: " + appStateAsString);
        new Thread() {
            public void run() {
                Message msg = new Message();
                msg.arg1 = 1;
                msg.obj = appStateAsString;
                handlerManageAppState.sendMessage(msg);
            }
        }.start();

    }

    @Override
    public void onSendRideRequestSuccess(Object... args) {
        JSONObject jsonObjectRequestSendSuccess = (JSONObject) args[0];
        Log.d(TAG, "Ride Request Success: " + jsonObjectRequestSendSuccess);
    }

    public void acceptRequestFirstTime(String data) {
        AcceptedTripResponse acceptedTripResponse = getAcceptedTripResponse(data);
        createTrip(acceptedTripResponse);
        getTripLocationFromAcceptedResponse(acceptedTripResponse);
        getSingleDriverLocation(trip.getDriverPhoneNo());
        mapView.get().acceptRequest(trip);

    }

    private void createTrip(AcceptedTripResponse acceptedTripResponse) {
        trip.setTripId(acceptedTripResponse.getTrip().getId());
        trip.setDriverName(acceptedTripResponse.getDriver().getName());
        trip.setDriverPhoneNo(acceptedTripResponse.getDriver().getPhoneNo());
        trip.setDistance(acceptedTripResponse.getDistance().getDistance());
        trip.setCurrentRideStatus("accepted");
        trip.setTripBigMessage(acceptedTripResponse.getDistance().getDistance() + "M/KM");
        trip.setTripSmallMessage("Your captain is coming toward you");
        trip.setVehicleId(acceptedTripResponse.getVehicleInfo().getId().substring(0, 5));
        trip.setVehicleName(acceptedTripResponse.getVehicleInfo().getName());

        if (acceptedTripResponse.getDriver().getAvatar() != null) {
            trip.setDriverPhotoUrl(acceptedTripResponse.getDriver().getAvatar().get50X50());
        }

        if (acceptedTripResponse.getDriver().getRating() == null) {
            trip.setDriverRating(5.0);
        } else {
            trip.setDriverRating(acceptedTripResponse.getDriver().getRating());
        }
    }

    private AcceptedTripResponse getAcceptedTripResponse(String data) {
        return new AcceptedTripInfoJsonParser(new Gson()).getAcceptedTripInfo(data);
    }


    public void getAddressNameFromLatLon(LatLng latLng) {
        //mGeoCoder.getAddressFromLocation(latLng.latitude, latLng.longitude);
        getGeoCoderPlaceName(latLng);

    }

    public void drawPath(LatLng pickUp, LatLng dropOff) {
        Path path = new Path(this);
        path.drawPath(pickUp, dropOff);
    }

    public void openDropOffAddress() {
        mapView.get().openAddressSearch();
    }

    public void getDistanceInfoFromGoogleApi(LatLng pickUpLatLng, LatLng dropOffLatLng) {
        String path = pickUpLatLng.latitude + "," + pickUpLatLng.longitude + "&" +
                "destination=" + dropOffLatLng.latitude + "," + dropOffLatLng.longitude +
                "&sensor=false&units=metric&mode=driving";

        Log.d("Path", "" + path);
        Subscription subscription = rxService.getDistanceInfo(pickUpLatLng, dropOffLatLng,
                new RxService.getDistanceInfoCallback() {

                    @Override
                    public void onSuccess(DistanceResponse distanceInfoList) throws Exception {
                        fare.setBaseFare(mapView.get().getBaseFare());
                        fare.setEstimatedTime(distanceInfoList.getEstimatedTime());
                        fare.setFarePerKilo(mapView.get().getFarePerKilo());
                        fare.setDistance(distanceInfoList.getDistance());
                        fare.setWaitingCharge(mapView.get().getWaitingChargePerMin());

                        mapView.get().setFareEstimation(fare.calculateFare().getEstimatedFare() + "TK");

                    }

                    @Override
                    public void onError(NetworkError networkError) {
                        //view.onFailure(networkError.getAppErrorMessage());
                    }

                });

        subscriptions.add(subscription);
    }

    public void getGeoCoderPlaceName(LatLng pickUpLatLng) {
        Subscription subscription = rxService.getGeoCoderPlace(pickUpLatLng, new RxService.getGeoCoderPlaceNameCallback() {
            @Override
            public void onSuccess(GeoCoderPlaceResponse response) {
                LocationInfo locationInfo = new LocationInfo();

                if (response.getResults().get(0).getFormattedAddress().contains(",")) {
                    List<String> items = Arrays.asList(response.getResults()
                            .get(0).getFormattedAddress().split("\\s*,\\s*"));
                    locationInfo.setAddress(items.get(0));

                } else {
                    locationInfo.setAddress(response.getResults().get(0).getFormattedAddress());

                }

                if (mapView.get() != null)
                    mapView.get().setPickupAddress(locationInfo.getAddress());
            }

            @Override
            public void onError(NetworkError networkError) {

            }
        });

        subscriptions.add(subscription);
    }

    public void onStop() {
        isCloseApplication = true;
        //handler.removeCallbacks(getDriverList);
        subscriptions.unsubscribe();
    }

    public void setPaymentName(int payment) {
        mapView.get().setPaymentName(Payment.paymentTypeToString(payment));
    }

    public void createSocketConnection() {
        App.getInstance().connectSocket(session.getUser().getPhoneNumber(), session.getToken());

    }

    public boolean validation() {
        if (mapView.get().getPickUpAddress().contains("Undefined")) {
            mapView.get().showToast("Pickup location invalid");
            return false;
        }

        if (mapView.get().getDropOfAddress().equalsIgnoreCase("Where to go?")) {
            mapView.get().showToast("Enter where to go?");
            return false;
        }

        return true;
    }

    public void loadConfirmRide() {
        if (validation()) {
            mapView.get().loadConfirmRide();
        }

    }

    public void sendDriverRequest(Trip trip, JSONObject jsonObject) {

        try {
            buildRideRequestJson(trip, jsonObject);

            if (App.getInstance() != null) {
                Log.i(TAG, "sendRideRequest: Yes it wroks");
                App.getInstance().sendDriverRequest(mapView.get().getPickUpLocation(), trip.getDriverPhoneNo(), jsonObject);
            }

        } catch (Exception e) {
            mapView.get().showToast("Ride request is not available now. Please try again");
        }


    }

    private void buildRideRequestJson(Trip trip, JSONObject jsonObject) throws JSONException {
        JSONObject passenger = new JSONObject()
                .put("phone_no", session.getUser().getPhoneNumber())
                .put("full_name", session.getUser().getFullName())
                .put("avatar", session.getUser().getProfileImageUrl())
                .put("rating", 4.5)
                .put("text", "A ride request from a passenger");

        JSONObject addressPickUp = new JSONObject()
                .put("name", trip.getPickUpAddress())
                .put("latitude", trip.getPickUpLocation().latitude)
                .put("longitude", trip.getPickUpLocation().longitude);

        JSONObject addressDropOff = new JSONObject()
                .put("name", trip.getDropOffAddress())
                .put("latitude", trip.getDropOffLocation().latitude)
                .put("longitude", trip.getDropOffLocation().longitude);

        jsonObject.put("passenger", passenger)
                .put("address_pickup", addressPickUp)
                .put("address_dropoff", addressDropOff);

        jsonObject.put("payment_method", trip.getPaymentType());
    }

    public void getLatestDriverLocationList() {
        App.getInstance().getLocation(mapView.get().getPickUpLocation());
    }

    public void getNearestDriverFromSocket(LatLng latLng) {
        if (App.getInstance() != null) {
            App.getInstance().getLocation(latLng);
        }
    }

    public void getSingleDriverLocation(String driverPhoneNo) {
        App.getInstance().getSingleDriverLocation(driverPhoneNo);
    }

    public void cancelRide(String tripId) {
        App.getInstance().cancelRide(tripId);
        mapView.get().cancelRide();
    }

    private boolean confirmRideStatus;

    public boolean confirmRideStatus(boolean confirmRideStatus) {
        return this.confirmRideStatus = confirmRideStatus;
    }

    public boolean isEnableConfirmRide() {
        return this.confirmRideStatus;
    }

    public void removeGetUpdatedLocationThread() {
        // handler.removeCallbacks(getDriverList);
    }

    public void resumeGetUpdatedLocationThread() {
        //handler.post(getDriverList);
    }

    private void manageTripWhenStateOnRide(Response response, String status) {
        if (mapView.get() != null) {
            Trip trip = getTripLocationDetails(response);
            trip.setCurrentRideStatus(status.toLowerCase());
            trip.setTripSmallMessage("Go faster, Safer, Together");
            trip.setTripBigMessage("ON RIDE");
            Log.d(TAG, "ride end: " + trip.getDriverName());
            mapView.get().startTrip(trip);
        }
    }

    private void manageTripWhenStateAccepted(Response response, String status) {
        if (mapView.get() != null) {
            Log.d(TAG, "setAppState: from state save: " + response.getStatus());
            Trip trip = getTripLocationDetails(response);
            trip.setCurrentRideStatus(status.toLowerCase());
            String distance = Utility.twoDecimalFormat((float) (trip.getDistance() / 1000));
            trip.setTripBigMessage("" + distance + " km");
            mapView.get().acceptRequest(trip);
        }
    }

    @NonNull
    private Trip getTripLocationDetails(Response response) {
        Trip trip = getTripStatusForSetCurrentState(response);
        LatLng latLng = new LatLng(response.getDepartureLocation().get(1), response.getDepartureLocation().get(0));
        trip.setPickUpLocation(latLng);
        trip.setPickUpAddress(response.getDepartureLocationName());

        LatLng latLng1 = new LatLng(response.getDestinationLocation().get(1), response.getDestinationLocation().get(0));
        trip.setDropOffLocation(latLng1);
        trip.setDropOffAddress(response.getDestinationLocationName());

        return trip;
    }

    @NonNull
    private Trip getTripLocationFromAcceptedResponse(AcceptedTripResponse response) {
        LatLng latLng = new LatLng(response.getAddressPickup().getLatitude(), response.getAddressPickup().getLongitude());
        trip.setPickUpLocation(latLng);
        trip.setPickUpAddress(response.getAddressPickup().getName());

        LatLng latLng1 = new LatLng(response.getAddressDropoff().getLatitude(), response.getAddressDropoff().getLongitude());
        trip.setDropOffLocation(latLng1);
        trip.setDropOffAddress(response.getAddressDropoff().getName());

        return trip;
    }

    private void manageTripWhenStateFinish() {
        Log.d(TAG, "manageTripWhenStateFinish: ====> state " + session.getAppLocalState());

    }

    private void manageTripWhenInRatingUI() {
        Trip trip = (Trip) session.getObject("trip", Trip.class);
        if (mapView.get() != null) {
            mapView.get().openEndTripReviewActivity(trip);
        }

    }

    private void manageTripWhenInTipsUI(Response response) {
        Trip trip = (Trip) session.getObject("trip", Trip.class);
        Log.d(TAG, "manageTripWhenInTipsUI: " + response.getTotalFare());


        List<BreakdownFare> breakdownFares = new ArrayList<>();

        for (com.instake.passenger.model.state.Breakdown fare : response.getFareDetails().getBreakdown()) {
            BreakdownFare breakdownFare = new BreakdownFare();
            breakdownFare.setTitle(fare.getTitle());
            breakdownFare.setAmount(fare.getAmount());
            breakdownFares.add(breakdownFare);
        }

        TripFareDetails tripFareDetails = new TripFareDetails();
        tripFareDetails.setBreakdown(breakdownFares);

        trip = getTripStatusForSetCurrentState(response);
        trip.setTripTotalFare(response.getFareDetails().getTotal() + "");
        trip.setFareDetails(tripFareDetails);

        Log.d(TAG, "Total Fare: " + trip.getTripTotalFare());
        Log.d(TAG, "Fare detail: " + trip.getFareDetails().getBreakdown().get(0).getTitle());


        if (mapView.get() != null) {
            mapView.get().openEndTripActivity(trip);
        }

    }

    @NonNull
    private Trip getTripStatusForSetCurrentState(Response response) {
        trip.setTripId(response.getId());
        trip.setDriverName(response.getDriverId().getFullName());
        trip.setDriverPhoneNo(response.getDriverId().getPhoneNo());

        trip.setDistance(response.getDistance().getDistanceValue());
        trip.setCurrentRideStatus(response.getStatus());
        trip.setTripSmallMessage("Your captain is coming toward you");
        trip.setVehicleId(response.getDriverId().getId().substring(0, 5));
        trip.setVehicleName(response.getDriverId().getVehicleType());

        if (response.getDriverId().getProfileImages() != null) {
            trip.setDriverPhotoUrl(response.getDriverId().getProfileImages().get50X50());
        }

        if (response.getDriverId().getTotalRating() == null) {
            trip.setDriverRating(5.0);
        } else {
            trip.setDriverRating(response.getDriverId().getTotalRating());
        }

        Log.d(TAG, "Driver Name : " + trip.getDriverName());
        Log.d(TAG, "Photo Url: " + trip.getDriverPhotoUrl());

        return trip;
    }

    private boolean isStateAccepted(String status) {
        return status.equalsIgnoreCase("accept");
    }

    private boolean isStateFinish(String status) {
        return status.equalsIgnoreCase("finish");
    }

    private boolean isStateOnRide(String status) {
        return status.equalsIgnoreCase("onride");
    }

    private boolean isStateEndRide(String status) {
        return status.equalsIgnoreCase("endride");
    }

    Handler handlerManageAppState = new Handler(new Handler.Callback() {

        @Override
        public boolean handleMessage(Message msg) {
            synchronized (this) {
                if (msg.arg1 == 1) {
                    try {
                        Response response = new ApplicationStateJsonParser(new Gson()).getStateInfo((String) msg.obj);
                        String status = response.getStatus();

                        //status = "finish";
                        //Log.d(TAG, "setAppState: " + response.getStatus());

                        if (isStateAccepted(status)) {
                            manageTripWhenStateAccepted(response, status);

                        } else if (isStateOnRide(status)) {
                            manageTripWhenStateOnRide(response, status);

                        } else if (isStateEndRide(status)) {

                            if (session.getAppLocalState() == App.APP_IN_RATING_UI) {
                                manageTripWhenInRatingUI();
                            } else {
                                manageTripWhenInTipsUI(response);

                            }

                        } else if (isStateFinish(status)) {

                            manageTripWhenStateFinish();

                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e(TAG, "state parsing exception: " + e.getMessage());
                    }
                }
            }
            return false;
        }
    });

    Handler handlerUpdatedLocationData = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            synchronized (this) {
                if (msg.arg1 == 2) {
                    if (mapView.get() != null) {
                        if (isEnableConfirmRide()) {
                            mapView.get().onUpdateVeryLatestDriverList((JSONObject) msg.obj);
                        } else {
                            Log.d(TAG, "update driver list: " + msg.obj.toString());
                            mapView.get().onUpdateDriverList((JSONObject) msg.obj);
                        }

                    }

                }
            }

            return false;
        }
    });

    Handler handlerStartAndEndTrip = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            synchronized (this) {
                if (msg.arg1 == 3) {
                    if (mapView.get() != null) {
                        mapView.get().startEndTrip(msg.obj);
                    }

                }
            }

            return false;
        }
    });


}