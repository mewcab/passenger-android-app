package com.instake.passenger.feature.login;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.text.InputType;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityLoginBinding;
import com.instake.passenger.dipendency.Login.LoginModule;
import com.instake.passenger.feature.splash.SplashActivity;

import javax.inject.Inject;

/**
 * Created by Mostafa Monowar on 5/21/17.
 */

public class LoginActivity extends SiriusPresenterActivity<LoginView, LoginPresenter> implements LoginView {

    @Inject
    LoginPresenter presenter;

    private ActivityLoginBinding binding;

    private Bundle bundle;

    private boolean isPassword = true;

    public static void open(Activity activity, Bundle bundle) {
        Intent intent = new Intent(activity, LoginActivity.class);
        intent.putExtras(bundle);
        activity.startActivity(intent);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_login;
    }

    @Override
    protected LoginPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new LoginModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityLoginBinding) getDataBinding();

        binding.toolbar.setTitle("");
        setSupportActionBar(binding.toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeButtonEnabled(true);
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        bundle = getIntent().getExtras();

        binding.tilPhoneNumber.getEditText().setText(bundle.getString("BUNDLE_PHONE_NUMBER"));

        binding.buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onLoginClicked();
            }
        });

        binding.tvShow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText tempEditText = binding.tilPassword.getEditText();
                if (isPassword) {
                    isPassword = false;
                    tempEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
                    binding.tvShow.setText(R.string.hide);
                } else {
                    isPassword = true;
                    tempEditText.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    binding.tvShow.setText(R.string.show);
                }
                tempEditText.setSelection(tempEditText.getText().length());
            }
        });
    }

    @Override
    public String getPhoneNumber() {
        return binding.tilPhoneNumber.getEditText().getText().toString();
    }

    @Override
    public String getPassword() {
        return binding.tilPassword.getEditText().getText().toString();
    }

    @Override
    public void clearAllActivitiesAndNavigateToSplash() {
        SplashActivity.ClearAllScreenAndOpen(this);
    }

    @Override
    public void showPasswordError() {
        binding.tilPassword.getEditText().setError(getString(R.string.error_invalid_password));
    }

    @Override
    public void showPhoneNumberError() {
        binding.tilPhoneNumber.getEditText().setError(getString(R.string.please_enter_a_valid_phone_number));
    }

    @Override
    public void hideAllErrors() {
        binding.tilPhoneNumber.setErrorEnabled(false);
        binding.tilPassword.setErrorEnabled(false);
    }

    @Override
    public void onLoginFailed(String error) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(LoginActivity.this, error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void hideKeyboard() {
        hideKeyboard(binding.tilPassword.getEditText());
    }
}
