package com.instake.passenger.feature.onboard;

import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.instake.passenger.R;
import com.transitionseverywhere.ChangeBounds;
import com.transitionseverywhere.Transition;
import com.transitionseverywhere.TransitionManager;


/**
 * Created by Mostafa Monowar on 6/20/2017.
 */

public class CarFragment {


    static boolean isFirstTimeAnimation = true;
    static boolean isAlreadySetSecondBackground = true;

    static ViewGroup newInstance(Context mContext, ViewGroup collection) {
        OnboardingObject onboardingObject = OnboardingObject.values()[0];
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(onboardingObject.getLayoutResId(), collection, false);
        getTranslation(layout, mContext);
        return layout;

    }

    private static void getTranslation(ViewGroup viewGroup, Context mContext) {
        ImageView img_animation = (ImageView) viewGroup.findViewById(R.id.im_car_animation);
        Animation animSlide;

        if (isFirstTimeAnimation) {
            // Load the animation like this
//            animSlide = AnimationUtils.loadAnimation(mContext,
//                    R.anim.slide);
//            img_animation.setBackgroundResource(R.drawable.car_onb);
//
//            // Start the animation like this
//            img_animation.startAnimation(animSlide);
//            img_animation.setStateListAnimator(new StateListAnimator());
//            img_animation.setTag(R.drawable.car_onb);

            FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) img_animation.getLayoutParams();
            params.gravity = Gravity.LEFT | Gravity.CENTER;
            img_animation.setLayoutParams(params);

            img_animation.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Transition transition = new ChangeBounds();
                    transition.setDuration(2000);
                    transition.setInterpolator(new LinearInterpolator());
                    transition.setStartDelay(100);
                    TransitionManager.beginDelayedTransition(viewGroup, transition);

                    FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) img_animation.getLayoutParams();
                    params.gravity = Gravity.CENTER;
                    img_animation.setLayoutParams(params);
                }
            }, 200);

            isFirstTimeAnimation = false;
        }
    }
}

