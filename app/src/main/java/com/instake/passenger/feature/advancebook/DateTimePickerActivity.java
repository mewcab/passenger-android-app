package com.instake.passenger.feature.advancebook;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.view.ViewGroup;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityDateTimePickerBinding;

/**
 * Created by Tushar on 4/21/17.
 */

public class DateTimePickerActivity extends SiriusActivity {

    ActivityDateTimePickerBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_date_time_picker;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityDateTimePickerBinding) getDataBinding();
        setupViewPager();
    }

    private void setupViewPager() {
        binding.pager.setAdapter(new PagerAdapter() {

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                return binding.pager.getChildAt(position);
            }

            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return "Date";
                    case 1:
                        return "Time";
                }
                return super.getPageTitle(position);
            }

            @Override
            public int getCount() {
                return binding.pager.getChildCount();
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }
        });

        binding.tabs.setupWithViewPager(binding.pager);

        binding.pager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                if (position == 0) {
                    binding.btnPositive.setText(getText(R.string.next));
                } else {
                    binding.btnPositive.setText(getText(R.string.confirm));
                }
            }
        });

        binding.btnPositive.setOnClickListener(v -> {
            if (binding.pager.getCurrentItem() == 0) {
                binding.pager.setCurrentItem(1, true);
            } else if (binding.pager.getCurrentItem() == 1) {
                if (inputOkay()) {
                    DateTimePickerActivity.this.finish();
                }
            }
        });

        binding.btnCancel.setOnClickListener(v -> DateTimePickerActivity.this.finish());
    }

    private boolean inputOkay() {
        return true;
    }
}
