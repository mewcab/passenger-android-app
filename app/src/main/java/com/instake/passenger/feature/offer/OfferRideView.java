package com.instake.passenger.feature.offer;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by GoInstake on 10/21/2017.
 */

public interface OfferRideView extends BaseView {
    void showToast(String message);
}
