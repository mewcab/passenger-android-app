package com.instake.passenger.feature.ridehistory;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.historydata.Trip;

import java.util.List;

/**
 * Created by mahmudul.hasan on 5/24/2017.
 */

public interface RideHistoryView extends BaseView {

    int getPageNumber();

    void showToast(String message);

    void onLoadDataIntoAdapter(List<Trip> model_list);

    void showLoading();

    void hideLoading();
}
