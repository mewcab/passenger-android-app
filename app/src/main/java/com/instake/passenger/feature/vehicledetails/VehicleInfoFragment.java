package com.instake.passenger.feature.vehicledetails;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.views.InstakeCircularSlideLayout;
import com.instake.passenger.model.driver.CarType;
import com.instake.passenger.utility.Utility;

import java.util.List;
import java.util.Locale;

public class VehicleInfoFragment extends Fragment {

    private static final String POSITON = "position";
    private static final String SCALE = "scale";
    private static final String DRAWABLE_RESOURE = "resource";

    private int screenWidth;
    private int screenHeight;

    public static Fragment newInstance(Context context, CarType vehicleDetails, int pos, float scale) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("ARGUMENT_VEHICLE_DETAILS", vehicleDetails);
        bundle.putInt(POSITON, pos);
        bundle.putFloat(SCALE, scale);

        return Fragment.instantiate(context, VehicleInfoFragment.class.getName(), bundle);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getScreenSize();
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if (container == null) {
            return null;
        }

        CarType vehicleDetails = getArguments().getParcelable("ARGUMENT_VEHICLE_DETAILS");
        final int position = this.getArguments().getInt(POSITON);
        float scale = this.getArguments().getFloat(SCALE);

        int pageMargin = ((Resources.getSystem().getDisplayMetrics().widthPixels / 10) * 2);
        int pageMargin1 = ((Resources.getSystem().getDisplayMetrics().heightPixels / 8) * 2);

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(screenWidth - pageMargin, screenHeight - pageMargin1);
        LinearLayout linearLayout = (LinearLayout) inflater.inflate(R.layout.fragment_car_info, container, false);

        InstakeCircularSlideLayout root = (InstakeCircularSlideLayout) linearLayout.findViewById(R.id.single_item_vehicle_details);
        LinearLayout imageView = (LinearLayout) linearLayout.findViewById(R.id.wrapper_main);
        imageView.setLayoutParams(layoutParams);

        TextView textViewVehicleName = (TextView) linearLayout.findViewById(R.id.textViewVehicleName);
        ImageView carImage = (ImageView) linearLayout.findViewById(R.id.imageCar);
        TextView textViewSeatAmount = (TextView) linearLayout.findViewById(R.id.textSeatAmount);
        TextView textFareCost = (TextView) linearLayout.findViewById(R.id.textFareCost);
        TextView textPerKiloCost = (TextView) linearLayout.findViewById(R.id.textPerKiloCost);
        TextView textPerMinCost = (TextView) linearLayout.findViewById(R.id.textPerMinCost);

        if (vehicleDetails != null) {
            textViewVehicleName.setText(vehicleDetails.getName());
            textViewSeatAmount.setText(String.valueOf(vehicleDetails.getSeats()));
            textFareCost.setText(Utility.getTextStyleSlightly(getString(R.string.txt_currency_after_amount, String.valueOf(vehicleDetails.getBaseFare().intValue())), "tk"));
            textPerKiloCost.setText(Utility.getTextStyleSlightly(getString(R.string.txt_currency_after_amount, String.valueOf(vehicleDetails.getKillometre().intValue())), "tk"));
            textPerMinCost.setText(Utility.getTextStyleSlightly(getString(R.string.txt_currency_after_amount, String.valueOf(vehicleDetails.getPerMinit().intValue())), "tk"));

            Glide.with(getActivity())
                    .load(App.BASE_URL + vehicleDetails.getImage().get150X150())
                    .error(R.drawable.ic_corolla)
                    .into(carImage);
        }

        root.setScale(scale);

        return linearLayout;
    }

    private void getScreenSize() {
        screenHeight = Resources.getSystem().getDisplayMetrics().heightPixels;
        screenWidth = Resources.getSystem().getDisplayMetrics().widthPixels;
    }
}
