package com.instake.passenger.feature.home;

import android.content.Context;
import android.content.Intent;

import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.JsonObject;
import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.LocationInfo;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.nearestdriver.DriverList;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by masum on 2/28/17.
 */

public interface HomeView extends BaseView {
    void setPickupAddress(String locationInfo);
    String getPickUpAddress();
    String getDropOfAddress();
    void openAddressSearch();
    void onDrawPath(PolylineOptions polylineOptions);
    void setFareEstimation(String fare);
    void setPaymentName(String paymentMethod);
    void loadConfirmRide();
    Context getContext();
    void openEndTripReviewActivity(Trip trip);
    void openEndTripActivity(Trip trip);
    void showToast(String text);
    void setDriverList(List<DriverList> locations);
    LatLng getPickUpLocation();
    int getBaseFare();
    int getFarePerKilo();
    int getWaitingChargePerMin();
    void setBtnMenuImage(String imageURL);
    void acceptRequest(Trip trip);
    void startTrip(Trip trip);
    void endTrip(Trip trip);
    void onUpdateDriverList(JSONObject jsonObject);
    void onUpdateVeryLatestDriverList(JSONObject jsonObject);
    void cancelRide();
    void startEndTrip(Object object);
}
