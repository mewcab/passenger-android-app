package com.instake.passenger.feature.home;

import android.app.Activity;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.instake.passenger.R;

/**
 * Created by masum on 7/8/17.
 */

public class CarSelectionDialog {

    BottomSheetDialog mBottomSheetDialog;

    RelativeLayout relativeLayoutCar;
    RelativeLayout relativeLayoutBike;
    RelativeLayout relativeLayoutCNG;

    TextView tvCar;
    TextView tvBike;
    TextView tvCng;

    View carInfo;
    View bikeInfo;
    View cngInfo;

    Activity activity;

    CarSelectionDialog(Activity activity) {
        this.activity = activity;
        mBottomSheetDialog = new BottomSheetDialog(activity);
        final View sheetView = activity.getLayoutInflater().inflate(R.layout.dialog_car_list_bottom_sheet, null);
        mBottomSheetDialog.setContentView(sheetView);
        mBottomSheetDialog.setCancelable(true);
        mBottomSheetDialog.setCanceledOnTouchOutside(true);
        mBottomSheetDialog.getWindow().setDimAmount((float) 0.9);

        relativeLayoutCar = (RelativeLayout) sheetView.findViewById(R.id.button_select_car);
        relativeLayoutBike = (RelativeLayout) sheetView.findViewById(R.id.button_select_bike);
        relativeLayoutCNG = (RelativeLayout) sheetView.findViewById(R.id.button_select_cng);

        tvCar = (TextView) sheetView.findViewById(R.id.tv_car);
        tvBike = (TextView) sheetView.findViewById(R.id.tv_bike);
        tvCng = (TextView) sheetView.findViewById(R.id.tv_cng);

        carInfo = sheetView.findViewById(R.id.view_car_details);
        bikeInfo = sheetView.findViewById(R.id.view_bike_details);
        cngInfo = sheetView.findViewById(R.id.view_cng_details);
    }

    void selectCar() {
        tvCar.setTextColor(ContextCompat.getColor(activity, R.color.hot_pink));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            carInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.hot_pink)));
        } else {
            carInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.hot_pink)));
        }

        tvBike.setTextColor(ContextCompat.getColor(activity, R.color.warm_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            bikeInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        } else {
            carInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        }

        tvCng.setTextColor(ContextCompat.getColor(activity, R.color.warm_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            cngInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        } else {
            cngInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        }
    }

    void selectBike() {
        tvCar.setTextColor(ContextCompat.getColor(activity, R.color.warm_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            carInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        } else {
            carInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        }

        tvBike.setTextColor(ContextCompat.getColor(activity, R.color.hot_pink));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            bikeInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.hot_pink)));
        } else {
            carInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.hot_pink)));
        }

        tvCng.setTextColor(ContextCompat.getColor(activity, R.color.warm_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            cngInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        } else {
            cngInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        }
    }

    void selectCng() {
        tvCar.setTextColor(ContextCompat.getColor(activity, R.color.warm_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            carInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        } else {
            carInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        }

        tvBike.setTextColor(ContextCompat.getColor(activity, R.color.warm_gray));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            bikeInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        } else {
            carInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.warm_gray)));
        }

        tvCng.setTextColor(ContextCompat.getColor(activity, R.color.hot_pink));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            cngInfo.setBackground(changeDrawableColor(ContextCompat.getColor(activity, R.color.hot_pink)));
        } else {
            cngInfo.setBackgroundDrawable(changeDrawableColor(ContextCompat.getColor(activity, R.color.hot_pink)));
        }
    }

    private Drawable changeDrawableColor(int color) {
        Drawable myDrawable = ContextCompat.getDrawable(activity, R.drawable.ic_info_warm_grey);
        myDrawable.setColorFilter(color, PorterDuff.Mode.SRC_IN);
        return myDrawable;
    }

}
