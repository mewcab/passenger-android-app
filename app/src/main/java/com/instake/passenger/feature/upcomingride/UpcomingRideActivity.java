package com.instake.passenger.feature.upcomingride;

import android.support.v4.content.ContextCompat;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityUpcomingRideBinding;

public class UpcomingRideActivity extends SiriusActivity {

    private ActivityUpcomingRideBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_upcoming_ride;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityUpcomingRideBinding) getDataBinding();
        setSupportActionBar(binding.animToolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        binding.collapsingToolbar.setTitle("Upcoming Ride");
        binding.collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
    }
}
