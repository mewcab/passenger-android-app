package com.instake.passenger.feature.login;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by Tushar on 5/21/17.
 */

public interface LoginView extends BaseView {
    String getPhoneNumber();
    String getPassword();
    void clearAllActivitiesAndNavigateToSplash();
    void showPasswordError();
    void showPhoneNumberError();
    void hideAllErrors();
    void onLoginFailed(String error);
    void hideKeyboard();
}
