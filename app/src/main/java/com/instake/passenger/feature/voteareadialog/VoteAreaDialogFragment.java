package com.instake.passenger.feature.voteareadialog;

import android.support.v7.app.AlertDialog;
import android.view.Window;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusDialogFragment;
import com.instake.passenger.databinding.DialogVoteForMyAreaBinding;

/**
 * Created by Tushar on 5/10/17.
 */

public class VoteAreaDialogFragment extends SiriusDialogFragment {

    public static final String TAG = VoteAreaDialogFragment.class.getName();

    private DialogVoteForMyAreaBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.dialog_vote_for_my_area;
    }

    @Override
    protected AlertDialog initDialogComponents() {
        binding = (DialogVoteForMyAreaBinding) getDataBinding();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity(), R.style.CustomDialog);
        final AlertDialog dialog = builder.create();
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(getView());
        binding.close.setOnClickListener(view -> dialog.dismiss());
        binding.txVoteNow.setOnClickListener(v -> {
            new VoteAreaCategoryDialogFragment().show(getActivity().getSupportFragmentManager(),
                    VoteAreaCategoryDialogFragment.TAG);
            dismiss();
        });
        dialog.show();
        return dialog;
    }
}
