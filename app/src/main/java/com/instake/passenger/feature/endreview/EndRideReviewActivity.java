package com.instake.passenger.feature.endreview;

import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityEndrideReviewBinding;
import com.instake.passenger.dipendency.endride.EndRideModule;
import com.instake.passenger.feature.endride.EndRideContract;
import com.instake.passenger.feature.endride.EndRidePresenterImpl;
import com.instake.passenger.feature.endride.SiriusRatingBar;
import com.instake.passenger.model.Rating;
import com.instake.passenger.model.Trip;
import com.instake.passenger.utility.Utility;

import org.parceler.Parcels;

import javax.inject.Inject;

//TODO: redefine this mvp model
public class EndRideReviewActivity extends SiriusActivity implements EndRideContract.EndRideView {
    public String TAG = EndRideReviewActivity.class.getSimpleName();

    public enum State {
        EXPANDED,
        COLLAPSED,
        IDLE
    }

    public float ratingScore;
    private ActivityEndrideReviewBinding binding;
    private boolean collapsed = false;

    @Inject
    EndRidePresenterImpl mEndRidePresenter;


    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_endride_review;
    }

    @Override
    protected void initComponents() {
        Trip trip = (Trip) Parcels.unwrap(getIntent().getParcelableExtra("trip_info"));

        Log.e(TAG, "initComponents: " + trip.getTripId());

        binding = (ActivityEndrideReviewBinding) getDataBinding();


        app.getAppComponent()
                .plus(new EndRideModule(this))
                .inject(this);

        binding.collapsingToolbar.setTitle(trip.getDriverName());
        binding.collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);

        Glide.with(getActivity())
                .load(trip.getDriverPhotoUrl())
                .error(R.drawable.ic_driver_user)
                .placeholder(R.drawable.ic_avatar)
                .into(binding.expandedImage);

        setSupportActionBar(binding.toolbar);
        binding.collapsingToolbar.setTitle("Rating");
        binding.collapsingToolbar.setExpandedTitleColor(ContextCompat.getColor(this, R.color.dark_gray));
        binding.collapsingToolbar.setCollapsedTitleTextColor(ContextCompat.getColor(this, R.color.black_2));
        binding.collapsingToolbar.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);


        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");

        final AppBarLayout appBarLayout = (AppBarLayout) findViewById(R.id.app_bar_layout);

        binding.rowBottomEndrideReview.txSubmitText.setVisibility(View.GONE);
        binding.rowBottomEndrideReview.relSubmit.setVisibility(View.GONE);
        binding.rowBottomEndrideReview.relSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ratingScore > 5) {
                    ratingScore = 5;
                }
                Rating rating = new Rating();
                rating.setText(binding.rowBottomEndrideReview.txSubmitText.getText().toString());
                rating.setRating(ratingScore);
                mEndRidePresenter.onPostRating(trip, rating);
            }
        });

        binding.rowMiddleEndrideReview.ratingBar.setOnScoreChanged(new SiriusRatingBar.IRatingBarCallbacks() {
            @Override
            public void scoreChanged(float score) {
                ratingScore = score;
                makePositionChangeAnimation(binding.rowBottomEndrideReview.txSubmitText,
                        binding.rowBottomEndrideReview.relSubmit, appBarLayout);
                //Toast.makeText(EndRideReviewActivity.this, "" + score, Toast.LENGTH_SHORT).show();
                binding.rowMiddleEndrideReview.tvRateMeaning.setText(Utility.ratingText(ratingScore));

            }
        });

        appBarLayout.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Log.d("STATE", state.name());
                collapsed = !state.name().equalsIgnoreCase(State.EXPANDED.toString());
            }
        });

    }

    private void makePositionChangeAnimation(EditText editText, RelativeLayout relativeLayout, final AppBarLayout appBarLayout) {
        if (!collapsed) {
            collapsed = true;

            editText.setVisibility(View.VISIBLE);
            relativeLayout.setVisibility(View.VISIBLE);

            CoordinatorLayout.LayoutParams params = (CoordinatorLayout.LayoutParams) appBarLayout.getLayoutParams();
            final AppBarLayout.Behavior behavior = (AppBarLayout.Behavior) params.getBehavior();
            if (behavior != null) {
                ValueAnimator valueAnimator = ValueAnimator.ofInt();
                valueAnimator.setInterpolator(new DecelerateInterpolator());
                valueAnimator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        behavior.setTopAndBottomOffset((Integer) animation.getAnimatedValue());
                        appBarLayout.requestLayout();
                    }
                });
                valueAnimator.setIntValues(0, -900);
                valueAnimator.setDuration(2000);
                valueAnimator.start();
            }


        }
    }

    @Override
    public Context getContext() {
        return getApplicationContext();
    }

    @Override
    public Activity getActivity() {
        return this;
    }


    @Override
    public float getRating() {
        return 0;
    }

    @Override
    public void showMessage(String message) {
        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
        finish();
    }

    @Override
    public void onAddingTip(float tipAmount) {

    }

    public abstract class AppBarStateChangeListener implements AppBarLayout.OnOffsetChangedListener {

        private State mCurrentState = State.IDLE;

        @Override
        public final void onOffsetChanged(AppBarLayout appBarLayout, int i) {
            if (i == 0) {
                if (mCurrentState != State.EXPANDED) {
                    onStateChanged(appBarLayout, State.EXPANDED);
                }
                mCurrentState = State.EXPANDED;
            } else if (Math.abs(i) >= appBarLayout.getTotalScrollRange()) {
                if (mCurrentState != State.COLLAPSED) {
                    onStateChanged(appBarLayout, State.COLLAPSED);
                }
                mCurrentState = State.COLLAPSED;
            } else {
                if (mCurrentState != State.IDLE) {
                    onStateChanged(appBarLayout, State.IDLE);
                }
                mCurrentState = State.IDLE;
            }
        }

        public abstract void onStateChanged(AppBarLayout appBarLayout, State state);
    }

}