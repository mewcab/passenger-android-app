package com.instake.passenger.feature.ridehistory;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.model.historydata.Trip;

import java.util.List;
import java.util.Locale;

/**
 * Created by mahmudul.hasan on 4/24/2017.
 */

public class RideHistoryAdapter extends RecyclerView.Adapter<RideHistoryAdapter.MyRecyclerHolder> {

    private LayoutInflater inflater;
    private List<Trip> list;
    private Context context;

    public RideHistoryAdapter(Context context, List<Trip> list) {
        inflater = LayoutInflater.from(context);
        this.context = context;
        this.list = list;
    }

    @Override
    public MyRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyRecyclerHolder(inflater.inflate(R.layout.row_ride_history, parent, false));
    }

    @Override
    public void onBindViewHolder(MyRecyclerHolder holder, int position) {
        Trip currentItem = list.get(position);

        holder.textViewDateTime.setText(currentItem.getStartTime());
        holder.textViewFareAmount.setText(String.format(Locale.ENGLISH, "%.2fTk", currentItem.getSubTotal()));
        holder.textViewPickupLocation.setText(currentItem.getPickupName());
        holder.textViewDestination.setText(currentItem.getDropoffName());
        holder.textViewPaymentType.setText(currentItem.getPaymentMethod());
        holder.textViewDriverName.setText(currentItem.getDriverId().getFullName().toUpperCase());
        holder.textViewCarType.setText(currentItem.getDriverId().getVehicleId().getVehicleTypeName() + "");
        holder.textViewRatings.setText(String.format(Locale.getDefault(), "%.01f", currentItem.getDriverId().getTotalRating() / currentItem.getDriverId().getRatingCount()));
        holder.textViewCarId.setText(currentItem.getDriverId().getVehicleId().getVehicleType() + "");

        Glide.with(context)
                .load(App.BASE_URL + currentItem.getDriverId().getProfileImages().get150X150())
                .dontAnimate()
                .error(R.drawable.ic_driver_user)
                .placeholder(R.drawable.ic_driver_user)
                .into(holder.imageViewDriverImage);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyRecyclerHolder extends RecyclerView.ViewHolder {

        TextView textViewDateTime;
        TextView textViewFareAmount;
        TextView textViewPaymentType;
        TextView textViewPickupLocation;
        TextView textViewDestination;
        TextView textViewDriverName;
        TextView textViewCarType;
        TextView textViewRatings;
        TextView textViewCarId;
        ImageView imageViewDriverImage;

        MyRecyclerHolder(View itemView) {
            super(itemView);

            textViewDateTime = (TextView) itemView.findViewById(R.id.textViewDateTime);
            textViewFareAmount = (TextView) itemView.findViewById(R.id.textViewFareAmount);
            textViewPaymentType = (TextView) itemView.findViewById(R.id.textViewPaymentType);
            textViewPickupLocation = (TextView) itemView.findViewById(R.id.textViewPickupLocation);
            textViewDestination = (TextView) itemView.findViewById(R.id.textViewDestination);
            textViewDriverName = (TextView) itemView.findViewById(R.id.textViewDriverName);
            textViewCarType = (TextView) itemView.findViewById(R.id.textViewCarType);
            textViewRatings = (TextView) itemView.findViewById(R.id.textViewRatings);
            textViewCarId = (TextView) itemView.findViewById(R.id.textViewCarId);
            imageViewDriverImage = (ImageView) itemView.findViewById(R.id.imageViewDriverImage);
        }
    }
}
