package com.instake.passenger.feature.endride;

import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.Rating;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.payment.Tips;

/**
 * Created by The Game on 4/2/17.
 */

public interface EndRideContract {

    interface EndRideView extends BaseView {

        float getRating();

        void showMessage(String message);

        void onAddingTip(float tipAmount);

    }

    interface EndRidePresenter extends BasePresenter {
        void onBtnSubmitClick();
        void onPostRating(Trip trip, Rating rating);
        void onPostTips(Trip trip, Tips tips);
    }
}
