package com.instake.passenger.feature.requestdriver;

/**
 * Created by masum on 3/29/17.
 */

public interface RequestDriverView {
    void updateTimer(String value);

    void onAcceptedRequest();

    void onAcceptedRequest(boolean isOriginalDriver);
}
