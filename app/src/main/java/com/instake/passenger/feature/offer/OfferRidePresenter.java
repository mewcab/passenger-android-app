package com.instake.passenger.feature.offer;

import android.util.Log;

import com.android.volley.NetworkError;
import com.instake.passenger.Rx.RxService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.ridehistory.RideHistoryView;
import com.instake.passenger.model.historydata.HistoryResp;
import com.instake.passenger.model.offers.OfferResponse;

import java.lang.ref.WeakReference;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by GoInstake on 10/21/2017.
 */

public class OfferRidePresenter implements BasePresenter<OfferRideView> {
    private static String TAG = "OfferRidePresenter";
    private Session session;
    private WeakReference<OfferRideView> offerRideView;
    private RxService rxService;

    public OfferRidePresenter(OfferRideView view, Session session, RxService rxService) {
        this.offerRideView = new WeakReference<>(view);
        this.session = session;
        this.rxService = rxService;
    }

    @Override
    public void onCreatePresenter() {
        getOffers();
    }

    private void getOffers() {
        offerRideView.get().showLoader();

        CompositeSubscription subscriptions = new CompositeSubscription();
        Subscription subscription = rxService.getOffer(new RxService.getOfferCallBack() {
            @Override
            public void onSuccess(OfferResponse response) {
                Log.e(TAG, "getOfferType: " + response.getOfferType());
                offerRideView.get().hideLoader();
            }

            @Override
            public void onError(NetworkError networkError) {
                offerRideView.get().hideLoader();
            }
        });

        subscriptions.add(subscription);
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }
}
