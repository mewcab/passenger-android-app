package com.instake.passenger.feature.onboard;

import android.Manifest;
import android.content.Intent;
import android.net.Uri;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.Toast;

import com.canelmas.let.AskPermission;
import com.canelmas.let.DeniedPermission;
import com.canelmas.let.Let;
import com.canelmas.let.RuntimePermissionListener;
import com.canelmas.let.RuntimePermissionRequest;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityOnBoardingBinding;
import com.instake.passenger.dipendency.onboard.OnBoardingModule;
import com.instake.passenger.feature.login.LoginActivity;
import com.instake.passenger.feature.splash.SplashActivity;
import com.instake.passenger.model.User;
import com.instake.passenger.monowar.mobilenumbervalidation.a.SignupGetStartedActivity;

import java.util.List;

import javax.inject.Inject;

public class OnBoardingActivity extends SiriusPresenterActivity<OnBoardingView, OnBoardingPresenter>
        implements OnBoardingView, RuntimePermissionListener {

    @Inject
    OnBoardingPresenter presenter;
    boolean isFirst = true;
    private ActivityOnBoardingBinding binding;
    private ViewPagerAdapter viewPagerAdapter;
    private int current_position = 0;
    @NonNull
    private final ViewPager.SimpleOnPageChangeListener mOnPageChangeListener = new ViewPager.SimpleOnPageChangeListener() {

        @Override
        public void onPageScrolled(final int position, final float positionOffset, final int positionOffsetPixels) {
            switch (position) {
                case 0:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_four)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_five)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_six)).setChecked(false);
                    break;
                case 1:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_four)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_five)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_six)).setChecked(false);
                    break;
                case 2:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rb_four)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_five)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_six)).setChecked(false);
                    break;
                case 3:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_four)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rb_five)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_six)).setChecked(false);
                    break;
                case 4:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_four)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_five)).setChecked(true);
                    ((RadioButton) findViewById(R.id.rb_six)).setChecked(false);
                    break;
                case 5:
                    ((RadioButton) findViewById(R.id.rb_one)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_two)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_three)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_four)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_five)).setChecked(false);
                    ((RadioButton) findViewById(R.id.rb_six)).setChecked(true);
                    break;
            }

            current_position = position;
//            int x = (int) ((binding.viewPagerOnBoarding.getWidth() * position + positionOffsetPixels) * computeFactor());
//            binding.scrollView.scrollTo(x, 0);
        }

//        private float computeFactor() {
//            return (binding.background.getWidth() - binding.viewPagerOnBoarding.getWidth()) /
//                    (float) (binding.viewPagerOnBoarding.getWidth() * (binding.viewPagerOnBoarding.getAdapter().getCount() - 1));
//        }
    };
    private String initialStateParam;
    private Handler handler = new Handler();
    Runnable runnable = this::afficher;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_on_boarding;
    }

    @Override
    protected OnBoardingPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new OnBoardingModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityOnBoardingBinding) getDataBinding();
        setupViewPager();

        binding.viewPagerOnBoarding.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.getParent().requestDisallowInterceptTouchEvent(true);
                switch (event.getAction()) {

                    case MotionEvent.ACTION_CANCEL:
                        break;

                    case MotionEvent.ACTION_UP:
                        // calls when touch release on ViewPager
                        startPagerRotation();
                        break;

                    case MotionEvent.ACTION_MOVE:
                        // calls when ViewPager touch
                        handler.removeCallbacks(runnable);
                        break;
                }
                return false;
            }
        });
    }

    private void setupViewPager() {
        viewPagerAdapter = new ViewPagerAdapter(this);
        binding.viewPagerOnBoarding.setAdapter(viewPagerAdapter);
        binding.viewPagerOnBoarding.addOnPageChangeListener(mOnPageChangeListener);
        binding.btnLetsGo.setOnClickListener(v -> onClickGetStarted());
    }

    @Override
    public void startPagerRotation() {
        runnable.run();
    }

    public void afficher() {
        if (isFirst) {
            handler.postDelayed(runnable, 5000);
            isFirst = false;
            return;
        }
        if (current_position == 5) {
            current_position = 0;
        } else {
            current_position++;
        }

        if (current_position == 0) {
            binding.viewPagerOnBoarding.setCurrentItem(current_position, false);
        } else {
            binding.viewPagerOnBoarding.setCurrentItem(current_position, true);
        }
        handler.postDelayed(runnable, 5000);
    }

    @Override
    public boolean isUserLoggedIn() {
        // TODO: replace this condition with our server token key
        return false;
    }


    @Override
    public void navigateToSignUp() {
        SignupGetStartedActivity.open(this);
        finish();
    }

    @AskPermission({Manifest.permission.GET_ACCOUNTS,
            Manifest.permission.RECEIVE_SMS,
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.CAMERA,
            Manifest.permission.WRITE_EXTERNAL_STORAGE})
    private void onClickGetStarted() {
        presenter.onClickLetsGo();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Let.handle(this, requestCode, permissions, grantResults);
    }

    @Override
    public void onShowPermissionRationale(List<String> permissionList, RuntimePermissionRequest permissionRequest) {
        final StringBuilder sb = new StringBuilder();

        for (String permission : permissionList) {
            sb.append(getRationale(permission));
            sb.append("\n");
        }

        new AlertDialog.Builder(this).setTitle("Permission Required!")
                .setMessage(sb.toString())
                .setCancelable(true)
                .setNegativeButton("No Thanks", (dialog, which) -> dialog.dismiss())
                .setPositiveButton("Try Again", (dialog, which) -> permissionRequest.retry())
                .show();
    }

    @Override
    public void onPermissionDenied(List<DeniedPermission> deniedPermissionList) {
        final StringBuilder sb = new StringBuilder();

        for (DeniedPermission result : deniedPermissionList) {

            if (result.isNeverAskAgainChecked()) {
                sb.append(result.getPermission());
                sb.append("\n");
            }
        }

        if (sb.length() != 0) {
            new AlertDialog.Builder(this).setTitle("Go Settings and Grant Permission")
                    .setMessage(sb.toString())
                    .setCancelable(true)
                    .setPositiveButton("ok", (dialog, which) -> {
                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                        Uri uri = Uri.fromParts("package", getPackageName(), null);
                        intent.setData(uri);
                        startActivityForResult(intent, 1);
                        dialog.dismiss();
                    }).show();
        }
    }

    private String getRationale(String permission) {
        if (permission.equals(Manifest.permission.GET_ACCOUNTS)) {
            return getString(R.string.rationale_accounts);
        } else if (permission.equals(Manifest.permission.RECEIVE_SMS)) {
            return getString(R.string.rationale_receive_sms);
        } else if (permission.equals(Manifest.permission.ACCESS_FINE_LOCATION)) {
            return getString(R.string.rationale_location);
        } else {
            return getString(R.string.rationale_other);
        }
    }

    @Override
    public void navigateToSplashScreen() {
        SplashActivity.open(this);
        finish();
    }
}
