package com.instake.passenger.feature.instakerechange;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.sslcommerz.library.payment.Classes.PayUsingSSLCommerz;
import com.sslcommerz.library.payment.Listener.OnPaymentResultListener;
import com.sslcommerz.library.payment.Util.ConstantData.BankName;
import com.sslcommerz.library.payment.Util.ConstantData.CurrencyType;
import com.sslcommerz.library.payment.Util.ConstantData.ErrorKeys;
import com.sslcommerz.library.payment.Util.ConstantData.SdkCategory;
import com.sslcommerz.library.payment.Util.ConstantData.SdkType;
import com.sslcommerz.library.payment.Util.JsonModel.TransactionInfo;
import com.sslcommerz.library.payment.Util.Model.MandatoryFieldModel;

/**
 * Created by mahmudul.hasan on 4/9/2017.
 */

public class RechargeInstakeActivity extends SiriusActivity implements View.OnClickListener {


    LinearLayout lin_2000_tk, lin_300_tk, lin_500_tk, lin_1000_tk;
    TextView tx_300_sign, tx_500_sign, tx_1000_sign, tx_2000_sign;
    TextView tx_300, tx_500, tx_1000, tx_2000;
    EditText et_amount;
    RadioButton rb_bkash, rb_rocket, rb_card;
    ImageView im_bkash, im_rocket;
    TextView tx_card;
    TextView tx_submit;
    TextInputLayout til_amount;
    String TAG = "ssl";
    OnPaymentResultListener onPaymentResultListener = new OnPaymentResultListener() {
        @Override
        public void transactionSuccess(TransactionInfo transactionInfo) {
            if (transactionInfo.getRiskLevel().equals("0")) {
                Log.d(TAG, "Transaction Successfully completed");
                showDialogForSimpleMessage("Transaction Successfully completed", "Transaction Successfully completed");

            } else {
                showRetryDialogForSimpleMessage("Transaction Fail", "Transaction in risk. Risk Title : " + transactionInfo.getRiskTitle().toString());

                Log.d(TAG, "Transaction in risk. Risk Title : " + transactionInfo.getRiskTitle().toString());
            }
        }

        @Override
        public void transactionFail(TransactionInfo transactionInfo) {
            Log.e(TAG, "Transaction Fail");
            showRetryDialogForSimpleMessage("Transaction Fail", "Transaction Fail");

        }

        @Override
        public void error(int errorCode) {

            switch (errorCode) {
                case ErrorKeys.USER_INPUT_ERROR:
                    Log.e(TAG, "User Input Error");
                    showDialogForSimpleMessage("User Input Error", "User Input Error");
                    break;
                case ErrorKeys.INTERNET_CONNECTION_ERROR:
                    Log.e(TAG, "Internet Connection Error");
                    showRetryDialogForSimpleMessage("Internet Connection Error", "Internet Connection Error");

                    break;
                case ErrorKeys.DATA_PARSING_ERROR:
                    Log.e(TAG, "Data Parsing Error");
                    showDialogForSimpleMessage("Data Parsing Error", "Data Parsing Error");

                    break;
                case ErrorKeys.CANCEL_TRANSACTION_ERROR:
                    Log.e(TAG, "User Cancel The Transaction");
                    showDialogForSimpleMessage("User Cancel The Transaction", "User Cancel The Transaction");

                    break;
                case ErrorKeys.SERVER_ERROR:
                    Log.e(TAG, "Server Error");
                    showRetryDialogForSimpleMessage("Server Error", "Server Error");

                    break;
                case ErrorKeys.NETWORK_ERROR:
                    Log.e(TAG, "Network Error");
                    showRetryDialogForSimpleMessage("Network Error", "Network Error");

                    break;
            }
        }
    };


    public static void hideKeyboard(Activity activity) {
        if (isKeyboardVisible(activity)) {
            InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    public static boolean isKeyboardVisible(Activity activity) {
        ///This method is based on the one described at http://stackoverflow.com/questions/4745988/how-do-i-detect-if-software-keyboard-is-visible-on-android-device
        Rect r = new Rect();
        View contentView = activity.findViewById(android.R.id.content);
        contentView.getWindowVisibleDisplayFrame(r);
        int screenHeight = contentView.getRootView().getHeight();

        int keypadHeight = screenHeight - r.bottom;

        return
                (keypadHeight > screenHeight * 0.15);
    }

    public void initView() {

        im_bkash = (ImageView) findViewById(R.id.im_bkash);
        im_rocket = (ImageView) findViewById(R.id.im_rocket);
        tx_card = (TextView) findViewById(R.id.tx_card);
        tx_submit = (TextView) findViewById(R.id.tx_submit);
        til_amount = (TextInputLayout) findViewById(R.id.til_amount);


        rb_bkash = (RadioButton) findViewById(R.id.rb_bkash);
        rb_rocket = (RadioButton) findViewById(R.id.rb_rocket);
        rb_card = (RadioButton) findViewById(R.id.rb_card);

        lin_2000_tk = (LinearLayout) findViewById(R.id.lin_2000_tk);
        lin_300_tk = (LinearLayout) findViewById(R.id.lin_300_tk);
        lin_500_tk = (LinearLayout) findViewById(R.id.lin_500_tk);
        lin_1000_tk = (LinearLayout) findViewById(R.id.lin_1000_tk);


        tx_300_sign = (TextView) findViewById(R.id.tx_300_sign);
        tx_500_sign = (TextView) findViewById(R.id.tx_500_sign);
        tx_1000_sign = (TextView) findViewById(R.id.tx_1000_sign);
        tx_2000_sign = (TextView) findViewById(R.id.tx_2000_sign);

        tx_300 = (TextView) findViewById(R.id.tx_300);
        tx_500 = (TextView) findViewById(R.id.tx_30);
        tx_1000 = (TextView) findViewById(R.id.tx_1000);
        tx_2000 = (TextView) findViewById(R.id.tx_2000);


        et_amount = (EditText) findViewById(R.id.et_amount);

        im_bkash.setOnClickListener(this);
        im_rocket.setOnClickListener(this);
        tx_card.setOnClickListener(this);
        tx_submit.setOnClickListener(this);

        lin_1000_tk.setOnClickListener(this);
        lin_300_tk.setOnClickListener(this);
        lin_500_tk.setOnClickListener(this);
        lin_2000_tk.setOnClickListener(this);

        rb_card.setOnClickListener(this);
        rb_rocket.setOnClickListener(this);
        rb_bkash.setOnClickListener(this);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_chat);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable upArrow = ContextCompat.getDrawable(RechargeInstakeActivity.this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            toolbar.setTitleTextColor(0xFFFFFFFF);

        }

        textWatcherForAmount();

    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_recharge_instake;
    }

    @Override
    protected void initComponents() {
        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                try {
                    hideKeyboard(RechargeInstakeActivity.this);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED, resultIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.lin_300_tk:
                setImageOnTakaAmount("300");
                break;
            case R.id.lin_500_tk:
                setImageOnTakaAmount("500");

                break;
            case R.id.lin_1000_tk:
                setImageOnTakaAmount("1000");

                break;
            case R.id.lin_2000_tk:
                setImageOnTakaAmount("2000");
                break;

            case R.id.rb_bkash:
            case R.id.im_bkash:
                setCheckedOrUnCheked("rb_bkash");
                break;

            case R.id.rb_rocket:
            case R.id.im_rocket:
                setCheckedOrUnCheked("rb_rocket");
                break;

            case R.id.rb_card:
            case R.id.tx_card:
                setCheckedOrUnCheked("rb_card");
                break;
            case R.id.tx_submit:
                checkAndSubmit();
                break;
        }

    }

/*    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        String message = "";
        if (REQUEST_CODE == requestCode) {
            if (resultCode == RESULT_OK) {
                // If the Result is ok
                Toast.makeText(getApplicationContext(), "payment success ", Toast.LENGTH_SHORT).show();
            } else if (resultCode == RESULT_CANCELED) {
                // If the transaction cancled.
                if (data.getIntExtra("error_key", 0) == ErrorKeys.USER_INPUT_ERROR) {
                    message = "User Input Error";
                    Log.e("Check Error", "User Input Error");
                } else if (data.getIntExtra("error_key", 0) == ErrorKeys.CANCEL_TRANSACTION_ERROR) {
                    Log.e("Check Error", "Transaction Canceled");
                    message = "Transaction Canceled";

                } else if (data.getIntExtra("error_key", 0) == ErrorKeys.DATA_PARSING_ERROR) {
                    Log.e("Check Error", "Data Parsing Error");
                    message = "Data Parsing Error";

                } else if (data.getIntExtra("error_key", 0) == ErrorKeys.INTERNET_CONNECTION_ERROR) {
                    Log.e("Check Error", "Internet Not Connected");
                    message = "Internet Not Connected";

                }

                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

            }
        }
    }*/

    public void textWatcherForAmount() {
        et_amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                til_amount.setErrorEnabled(false);

            }
        });
    }

    public void checkAndSubmit() {

        try {

            hideKeyboard(RechargeInstakeActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (et_amount.getText().toString().length() == 0) {
            til_amount.setErrorEnabled(true);
            til_amount.setError("Please enter your desire amount");

            return;
        }

        if (Integer.parseInt(et_amount.getText().toString()) < 100) {

            til_amount.setError("Your amount must be above 100 TK");

            return;
        }

        if (rb_bkash.isChecked() || rb_rocket.isChecked() || rb_card.isChecked()) {


        } else {
            rb_bkash.setError("Select Item");
            Toast.makeText(RechargeInstakeActivity.this, "Please select any one of payment type", Toast.LENGTH_SHORT).show();
            return;
        }

        /* Make payment */
      /*  Intent intent = new Intent(RechargeInstakeActivity.this, BankList.class);
        intent.putExtra(InputKeys.STORE_ID, "testbox");
        intent.putExtra(InputKeys.STORE_PASSWORD, "qwerty");
        intent.putExtra(InputKeys.TOTAL_AMOUNT, et_amount.getText().toString());
        intent.putExtra(InputKeys.CURRENCY, "BDT");
        intent.putExtra(InputKeys.TRANSACTION_ID, "124587");
        // For Testbox version use this line
        intent.putExtra(InputKeys.SDK_TYPE, "TESTBOX");
        // For Live version use this line
//        intent.putExtra(InputKeys.SDK_TYPE, "LIVE");
        // For Bank List Page
        intent.putExtra(InputKeys.SDK_CATEGORY, InputKeys.SDK_BANK_LIST);
        startActivityForResult(intent, REQUEST_CODE);*/

/*

        Intent intent = new Intent(RechargeInstakeActivity.this, BankListActivity.class);
        intent.putExtra(InputKeys.STORE_ID, "testbox");
        intent.putExtra(InputKeys.STORE_PASSWORD, "qwerty");
        intent.putExtra(InputKeys.TOTAL_AMOUNT, "1000");
        intent.putExtra(InputKeys.CURRENCY, "BDT");
        intent.putExtra(InputKeys.TRANSACTION_ID, "1245");
        intent.putExtra(InputKeys.SDK_TYPE, "TESTBOX");
        intent.putExtra(InputKeys.SDK_CATEGORY, InputKeys.SDK_BANK_LIST);
        startActivityForResult(intent, REQUEST_CODE);
*/

        MandatoryFieldModel mandatoryFieldModel = null;
        if (rb_bkash.isChecked())
            mandatoryFieldModel = new MandatoryFieldModel("test", "qwerty", et_amount.getText().toString(), "1012", CurrencyType.BDT, SdkType.TESTBOX, SdkCategory.BANK_PAGE, BankName.BKASH);
        else if (rb_rocket.isChecked())
            mandatoryFieldModel = new MandatoryFieldModel("test", "qwerty", et_amount.getText().toString(), "1012", CurrencyType.BDT, SdkType.TESTBOX, SdkCategory.BANK_PAGE, BankName.DBBL_VISA);
        else if (rb_card.isChecked())
            mandatoryFieldModel = new MandatoryFieldModel("test", "qwerty", et_amount.getText().toString(), "1012", CurrencyType.BDT, SdkType.TESTBOX, SdkCategory.BANK_LIST);

        if (mandatoryFieldModel != null)
       /* *Call for the payment*/
            PayUsingSSLCommerz.getInstance().setData(this, mandatoryFieldModel, null, null, null, onPaymentResultListener);
    }

    public void setImageOnTakaAmount(String amount) {
        et_amount.setText(amount);

        switch (amount) {
            case "300":

                lin_300_tk.setBackgroundResource(R.drawable.gradient_full_curve_plain_color);
//                lin_300_tk.setBackground(ContextCompat.getDrawable(RechargeInstakeActivity.this, R.drawable.gradient_full_curve_plain_color));

                lin_500_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_1000_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_2000_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);


                tx_300_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));
                tx_500_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_1000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_2000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));


                tx_300.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));
                tx_500.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_1000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_2000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                break;
            case "500":


                lin_300_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_500_tk.setBackgroundResource(R.drawable.gradient_full_curve_plain_color);
                lin_1000_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_2000_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);


                tx_300_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_500_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));
                tx_1000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_2000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));

                tx_300.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_500.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));
                tx_1000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_2000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                break;
            case "1000":


                lin_300_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_500_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_1000_tk.setBackgroundResource(R.drawable.gradient_full_curve_plain_color);
                lin_2000_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);


                tx_300_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_500_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_1000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));
                tx_2000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));

                tx_300.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_500.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_1000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));
                tx_2000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                break;
            case "2000":


                lin_300_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_500_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_1000_tk.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                lin_2000_tk.setBackgroundResource(R.drawable.gradient_full_curve_plain_color);


                tx_300_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_500_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_1000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_2000_sign.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));

                tx_300.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_500.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_1000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.dark));
                tx_2000.setTextColor(ContextCompat.getColor(RechargeInstakeActivity.this, R.color.white));
                break;

        }
        int padding = getResources().getDimensionPixelOffset(R.dimen.value_14sp);
        lin_300_tk.setPadding(padding, padding, padding, padding);
        lin_500_tk.setPadding(padding, padding, padding, padding);
        lin_1000_tk.setPadding(padding, padding, padding, padding);
        lin_2000_tk.setPadding(padding, padding, padding, padding);

        try {
            hideKeyboard(RechargeInstakeActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setCheckedOrUnCheked(String button_type) {
        switch (button_type) {
            case "rb_bkash":
                rb_bkash.setChecked(true);
                rb_rocket.setChecked(false);
                rb_card.setChecked(false);
                break;
            case "rb_rocket":
                rb_bkash.setChecked(false);
                rb_rocket.setChecked(true);
                rb_card.setChecked(false);
                break;
            case "rb_card":
                rb_bkash.setChecked(false);
                rb_rocket.setChecked(false);
                rb_card.setChecked(true);
                break;

        }

        try {
            rb_bkash.setError(null);
            rb_rocket.setError(null);
            rb_card.setError(null);
            hideKeyboard(RechargeInstakeActivity.this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void showDialogForSimpleMessage(String title, String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(RechargeInstakeActivity.this);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setPositiveButton("Close", (dialogInterface, i) -> {

        }).show();
    }

    public void showRetryDialogForSimpleMessage(String title, String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(RechargeInstakeActivity.this);
        alert.setTitle(title);
        alert.setMessage(message);
        alert.setPositiveButton("Retry", (dialogInterface, i) -> {
            checkAndSubmit();
        }).setNegativeButton("Close", (dialogInterface, i) -> {

        }).show();
    }
}
