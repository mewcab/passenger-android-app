package com.instake.passenger.feature.home;

import com.instake.passenger.model.LocationInfo;

/**
 * Created by masum on 3/1/17.
 */

public interface GeoCoderListener {

    void onGotAddress(LocationInfo locationInfo);
}
