package com.instake.passenger.feature.ridehistory;

import android.util.Log;

import com.android.volley.NetworkError;
import com.instake.passenger.Rx.RxService;
import com.instake.passenger.common.base.BasePresenter;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.model.historydata.HistoryResp;

import java.lang.ref.WeakReference;

import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by mahmudul.hasan on 5/24/2017.
 */

public class RideHistoryPresenter implements BasePresenter<RideHistoryView> {
    private static String TAG = "RideHistoryPresenter";
    private WeakReference<RideHistoryView> view;
    private Session session;

    public RideHistoryPresenter(RideHistoryView view, Session session) {
        this.view = new WeakReference<>(view);
        this.session = session;
    }

    @Override
    public void onCreatePresenter() {
        getApiData(view.get().getPageNumber());
    }

    @Override
    public void onResumePresenter() {

    }

    @Override
    public void onPausePresenter() {

    }

    @Override
    public void onDestroyPresenter() {

    }

    public void getApiData(int page) {
        view.get().showLoading();

        CompositeSubscription subscriptions = new CompositeSubscription();
        Subscription subscription = new RxService(session).getHistory(String.valueOf(page), new RxService.getHistoryCallBack() {
            @Override
            public void onSuccess(HistoryResp response) {
                view.get().hideLoading();
                view.get().onLoadDataIntoAdapter(response.getTrips());

            }

            @Override
            public void onError(NetworkError networkError) {
                networkError.printStackTrace();
                view.get().hideLoading();
                Log.e(TAG, "History onError: " + networkError.getMessage());
            }
        });

        subscriptions.add(subscription);

    }
}
