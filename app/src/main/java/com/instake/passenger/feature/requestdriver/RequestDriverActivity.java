package com.instake.passenger.feature.requestdriver;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.Toast;

import com.google.gson.Gson;
import com.instake.passenger.R;
import com.instake.passenger.common.App;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityDriverRequestBinding;
import com.instake.passenger.dipendency.requestdriver.RequestDriverModule;
import com.instake.passenger.model.Trip;
import com.instake.passenger.model.nearestdriver.AcceptedTripInfoJsonParser;
import com.instake.passenger.model.nearestdriver.AcceptedTripResponse;

import org.parceler.Parcels;

import javax.inject.Inject;

import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;


public class RequestDriverActivity extends SiriusPresenterActivity<RequestDriverView, RequestDriverPresenter>
        implements RequestDriverView {

    private ActivityDriverRequestBinding binding;

    @Inject
    RequestDriverPresenter presenter;

    private Trip trip;

    String acceptedJson = "";

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_driver_request;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityDriverRequestBinding) getDataBinding();
        binding.rippleEffect.startRippleAnimation();
        trip = (Trip) Parcels.unwrap(getIntent().getParcelableExtra("trip_info"));
       // Toast.makeText(this, "" + trip.getPickUpLocation().longitude, Toast.LENGTH_SHORT).show();

        binding.linearLayoutRetry.setVisibility(View.GONE);
        binding.btnRetryRideRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        binding.btnCancelRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialogCancelRide();
            }
        });

        App.getInstance().bus().toObserverable().observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Subscriber<String>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(String jsonAcceptedString) {
                        try {
                            acceptedJson = jsonAcceptedString;
                            //JSONObject jsonObject = new JSONObject(jsonAcceptedString);
                            Log.d("getInRequest", "****" + jsonAcceptedString);
                            AcceptedTripResponse driverListResponse = new AcceptedTripInfoJsonParser(new Gson()).getAcceptedTripInfo(jsonAcceptedString.toString());
                            Log.e("Request", "onNext: " +  driverListResponse.getDriver().getPhoneNo());
                            Log.e("Request", "onNext: " +  driverListResponse.getVehicleInfo().getName());
                            Log.e("Request", "onNext: " +  driverListResponse.getPassenger().getPhoneNo());
                            presenter.onAcceptRequest(driverListResponse.getPassenger().getPhoneNo());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });


    }

    @Override
    public void updateTimer(String value) {
        binding.textViewTimer.setText(value);

        if (binding.textViewTimer.getText().toString().equalsIgnoreCase("0")) {

            binding.linearLayoutRetry.setVisibility(View.VISIBLE);

            binding.linearLayoutCancelButtonHolder.setVisibility(View.GONE);

            Toast.makeText(this, "All drivers are busy", Toast.LENGTH_SHORT).show();

        }

    }

    @Override
    public void onAcceptedRequest() {

    }

    @Override
    public void onAcceptedRequest(boolean isOriginalDriver) {
        if(isOriginalDriver) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("data", acceptedJson);
            setResult(Activity.RESULT_OK, returnIntent);
            finish();
        }
    }

    @Override
    protected RequestDriverPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new RequestDriverModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroyPresenter();
        super.onDestroy();

    }

    public void showDialogCancelRide() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.CustomDialog);
        final AlertDialog dialog = builder.create();
        LayoutInflater inflater = getLayoutInflater();

        View dialogLayout = inflater.inflate(R.layout.dialog_ride_cancel, null);
        dialog.setView(dialogLayout);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.show();


        Button textButtonSubmit = (Button) dialogLayout.findViewById(R.id.text_button_submit);
        textButtonSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                App.getInstance().cancelRideWhenNotAccepted(App.getInstance().tripId, trip.getDriverPhoneNo());
                dialog.dismiss();
                finish();
            }
        });


    }
}
