package com.instake.passenger.feature.onboard;

import com.instake.passenger.common.base.BaseView;
import com.instake.passenger.model.User;

/**
 * Created by Tushar on 5/12/17.
 */

public interface OnBoardingView extends BaseView {

    void startPagerRotation();

    boolean isUserLoggedIn();

    void navigateToSignUp();

    void navigateToSplashScreen();
}
