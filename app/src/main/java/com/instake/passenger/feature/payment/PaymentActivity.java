package com.instake.passenger.feature.payment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.feature.instakerechange.RechargeInstakeActivity;

/**
 * Created by mahmudul.hasan on 4/9/2017.
 */

public class PaymentActivity extends SiriusActivity implements View.OnClickListener {

    TextView tk_amount_top;
    Button buttonRecharge;
    CardView crd_select_instake, crd_select_cash, crd_select_card_pay;
    ImageView img_crdview_instake_pay, img_crdview_card_pay, img_crdview_cash_pay;

    public void initView() {

        tk_amount_top = (TextView) findViewById(R.id.tk_amount_top);
        buttonRecharge = (Button) findViewById(R.id.button_recharge);

        crd_select_instake = (CardView) findViewById(R.id.crd_select_instake);
        crd_select_cash = (CardView) findViewById(R.id.crd_select_cash);
        crd_select_card_pay = (CardView) findViewById(R.id.crd_select_card_pay);


        img_crdview_instake_pay = (ImageView) findViewById(R.id.img_crdview_instake_pay);
        img_crdview_card_pay = (ImageView) findViewById(R.id.img_crdview_card_pay);
        img_crdview_cash_pay = (ImageView) findViewById(R.id.img_crdview_cash_pay);

        tk_amount_top.setOnClickListener(this);
        buttonRecharge.setOnClickListener(this);
        crd_select_instake.setOnClickListener(this);
        crd_select_cash.setOnClickListener(this);
        crd_select_card_pay.setOnClickListener(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Drawable upArrow = ContextCompat.getDrawable(PaymentActivity.this, R.drawable.ic_arrow_back_black_24dp);
            getSupportActionBar().setHomeAsUpIndicator(upArrow);
            toolbar.setTitleTextColor(0xFFFFFFFF);
        }

        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar_layout);
        collapsingToolbarLayout.setTitle(getString(R.string.title_activity_payment_method));
        Typeface expandedFont = Typeface.createFromAsset(getAssets(), "fonts/CircularStd-Black.ttf");
        collapsingToolbarLayout.setExpandedTitleTypeface(expandedFont);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_payment;
    }

    @Override
    protected void initComponents() {
        initView();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home buttonRecharge
            case android.R.id.home:
                Intent resultIntent = new Intent();
                setResult(RESULT_CANCELED, resultIntent);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tk_amount_top:
                if (tk_amount_top.getText().toString().equals("0")) {
                    Intent cash_in_intent = new Intent(PaymentActivity.this, RechargeInstakeActivity.class);
                    cash_in_intent.putExtra("comes_from", "payment_page");
                    cash_in_intent.putExtra("extra", "sample");
                    startActivity(cash_in_intent);
                }
                break;
            case R.id.button_recharge:
                Intent cash_in_intent = new Intent(PaymentActivity.this, RechargeInstakeActivity.class);
                cash_in_intent.putExtra("comes_from", "payment_page");
                cash_in_intent.putExtra("extra", "sample");
                startActivity(cash_in_intent);
                break;
            case R.id.crd_select_instake:
                selectedPaymentAction("instake_pay");
                break;
            case R.id.crd_select_cash:
                selectedPaymentAction("cash");
                break;
            case R.id.crd_select_card_pay:
                AlertDialog.Builder message = new AlertDialog.Builder(PaymentActivity.this);
                message.setMessage("We are working in it").setPositiveButton("Close", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                }).show();
//                selectedPaymentAction("card");
                break;

        }

    }

    public void selectedPaymentAction(String payment_type) {
        switch (payment_type) {
            case "instake_pay":
                img_crdview_instake_pay.setImageResource(R.drawable.shape_1_copy);
                img_crdview_card_pay.setImageDrawable(null);
                img_crdview_cash_pay.setImageDrawable(null);

//                crd_select_instake.setBackgroundResource(R.drawable.highlight_card);
//                crd_select_cash.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
//                crd_select_card_pay.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                break;
            case "cash":

                img_crdview_instake_pay.setImageDrawable(null);
                img_crdview_card_pay.setImageDrawable(null);
                img_crdview_cash_pay.setImageResource(R.drawable.shape_1_copy);


//                crd_select_instake.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
//                crd_select_cash.setBackgroundResource(R.drawable.highlight_card);
//                crd_select_card_pay.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
                break;
            case "card":

                img_crdview_instake_pay.setImageDrawable(null);
                img_crdview_card_pay.setImageResource(R.drawable.shape_1_copy);
                img_crdview_cash_pay.setImageDrawable(null);


//                crd_select_instake.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
//                crd_select_cash.setBackgroundResource(R.drawable.bg_white_stroke_grey_4_rounded);
//                crd_select_card_pay.setBackgroundResource(R.drawable.highlight_card);
                break;
        }
    }
}
