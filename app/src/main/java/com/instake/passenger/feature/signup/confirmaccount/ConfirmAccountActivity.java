package com.instake.passenger.feature.signup.confirmaccount;

import android.app.Activity;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.View;

import com.f2prateek.dart.InjectExtra;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.instake.passenger.R;
import com.instake.passenger.common.base.SiriusPresenterActivity;
import com.instake.passenger.databinding.ActivityConfirmAccountBinding;
import com.instake.passenger.dipendency.signup.confirmaccount.ConfirmAccountModule;
import com.instake.passenger.feature.login.LoginActivity;
import com.instake.passenger.model.User;
import com.instake.passenger.feature.home.HomeActivity;

import java.util.Arrays;

import javax.inject.Inject;

import static com.instake.passenger.utility.Utility.getSpannableTextSignupBottom;

/**
 * Created by Tushar on 5/3/17.
 */
public class ConfirmAccountActivity extends SiriusPresenterActivity<ConfirmAccountView, ConfirmAccountPresenter> implements ConfirmAccountView {

    @Nullable
    @InjectExtra("fb_id")
    String fbId;

    @Nullable
    @InjectExtra("phone_number")
    String phoneNumber;

    @Inject
    ConfirmAccountPresenter presenter;

    private CallbackManager mCallbackManager;

    public static void open(Activity activity, String fbId, String phoneNumber) {
        Intent confirmAccountIntent = new Intent(activity, ConfirmAccountActivity.class);
        confirmAccountIntent.putExtra("fb_id", fbId);
        confirmAccountIntent.putExtra("phone_number", phoneNumber);
        activity.startActivity(confirmAccountIntent);
    }

    private ActivityConfirmAccountBinding binding;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_confirm_account;
    }

    @Override
    protected ConfirmAccountPresenter bindPresenter() {
        app.getAppComponent()
                .plus(new ConfirmAccountModule(this))
                .inject(this);
        return presenter;
    }

    @Override
    protected void initComponents() {
        binding = (ActivityConfirmAccountBinding) getDataBinding();
        binding.tosTv.setText(getSpannableTextSignupBottom(getString(R.string.txt_register_bottom_text), "Terms of Services",
                ContextCompat.getColor(this, R.color.colorAccent)));

        binding.fbSignupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginManager.getInstance().logInWithReadPermissions(ConfirmAccountActivity.this,
                        Arrays.asList("public_profile", "email"));
            }
        });

        binding.btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                presenter.onConfirmClick();
            }
        });
    }

    @Override
    public void initFbLoginComponent() {
        AppEventsLogger.activateApp(app);
        mCallbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(mCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        presenter.onFbLoginSuccess(loginResult.getAccessToken());
                    }

                    @Override
                    public void onCancel() {

                    }

                    @Override
                    public void onError(FacebookException exception) {

                    }
                });
    }

    @Override
    public void setNameOnView(String fbName) {
        runOnUiThread(() -> {
            binding.etFullName.setText(fbName);
            binding.etFullName.setSelection(fbName.length());
        });
    }

    @Override
    public void setEmailOnView(String fbEmail) {
        runOnUiThread(() -> {
            binding.etEmail.setText(fbEmail);
            binding.etEmail.setSelection(fbEmail.length());
        });
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String getEmail() {
        return binding.etEmail.getText().toString();
    }

    @Override
    public String getFullName() {
        return binding.etFullName.getText().toString();
    }

    @Override
    public String getReferralCode() {
        return binding.etReferralCode.getText().toString();
    }

    @Override
    public void onErrorEmailEmpty() {
        binding.etEmail.setError(getString(R.string.error_email_empty));
    }

    @Override
    public void onErrorEmailInvalid() {
        binding.etEmail.setError(getString(R.string.error_email_invalid));
    }

    @Override
    public void onErrorFullNameEmpty() {
        binding.etFullName.setError(getString(R.string.error_full_name_empty));
    }

    @Override
    public void onErrorFullNameInvalid() {
        binding.etFullName.setError(getString(R.string.error_full_name_invalid));
    }

    @Override
    public void onErrorReferralCodeInvalid() {
        binding.etReferralCode.setError(getString(R.string.error_referral_code_invalid));
    }

    @Override
    public CharSequence getEmailError() {
        return binding.etEmail.getError();
    }

    @Override
    public CharSequence getFullNameError() {
        return binding.etFullName.getError();
    }

    @Override
    public CharSequence getReferralCodeError() {
        return binding.etReferralCode.getError();
    }

    @Override
    public void hideEmailError() {
        binding.etEmail.setError(null);
    }

    @Override
    public void hideFullNameError() {
        binding.etFullName.setError(null);
    }

    @Override
    public void hideReferralCodeError() {
        binding.etReferralCode.setError(null);
    }

    @Override
    public void showSignUpSuccess() {

    }

    @Override
    public void showSignUpFailure() {

    }

    @Override
    public void navigateToLogin(User user) {
//        getActivity().runOnUiThread(() ->
//                LoginActivity.open(ConfirmAccountActivity.this, user));
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void startHomeActivity() {
        startActivity(HomeActivity.class, false);
    }
}
