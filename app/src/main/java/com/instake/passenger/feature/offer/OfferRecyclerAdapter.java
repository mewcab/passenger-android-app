package com.instake.passenger.feature.offer;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.instake.passenger.R;
import com.instake.passenger.model.Offer;

import java.util.List;

/**
 * Created by mahmudul.hasan on 4/24/2017.
 */

public class OfferRecyclerAdapter extends RecyclerView.Adapter<OfferRecyclerAdapter.MyRecyclerHolder> {

    private LayoutInflater inflater;
    private List<Offer> list;

    public OfferRecyclerAdapter(Context context, List<Offer> list) {
        inflater = LayoutInflater.from(context);
        this.list = list;
    }

    @Override
    public MyRecyclerHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyRecyclerHolder(inflater.inflate(R.layout.row_offer_middle, parent, false));
    }

    @Override
    public void onBindViewHolder(MyRecyclerHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class MyRecyclerHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        private ImageView imageview;

        MyRecyclerHolder(View itemView) {
            super(itemView);
            imageview = (ImageView) itemView.findViewById(R.id.im_back_offer);
            imageview.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

        }
    }
}
