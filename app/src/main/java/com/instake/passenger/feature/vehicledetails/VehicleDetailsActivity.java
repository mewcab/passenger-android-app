package com.instake.passenger.feature.vehicledetails;

import android.content.res.Resources;
import android.util.Log;

import com.f2prateek.dart.InjectExtra;
import com.instake.passenger.R;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.base.SiriusActivity;
import com.instake.passenger.databinding.ActivityVehicleDetailsBinding;
import com.instake.passenger.model.driver.CarType;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class VehicleDetailsActivity extends SiriusActivity {

    private static final String TAG = VehicleDetailsActivity.class.getSimpleName();

    public static final int CAR = 3;
    public static final int BIKE = 1;
    public static final int CNG = 2;

    private ActivityVehicleDetailsBinding binding;

    private InstakeCircularPagerAdapter adapter;

    @Inject
    ApiService apiService;

    @InjectExtra("EXTRA_FLAG")
    int flag;

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_vehicle_details;
    }

    @Override
    protected void initComponents() {

        app.getAppComponent().inject(this);

        binding = (ActivityVehicleDetailsBinding) getDataBinding();

        int pageMargin = ((Resources.getSystem().getDisplayMetrics().widthPixels / 10) * 2);
        binding.vehiclePager.setPageMargin(-pageMargin);

        binding.vehiclePager.setOffscreenPageLimit(3);

        showLoader();
        apiService.getVehicleDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .onErrorResumeNext(Observable::error)
                .subscribe(new Subscriber<List<CarType>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.e(TAG, e.toString());
                    }

                    @Override
                    public void onNext(List<CarType> responseBody) {
                        hideLoader();
                        loadDataToAdapter(responseBody);
                    }
                });

        binding.buttonCloseSlider.setOnClickListener(v -> {
            finish();
        });
    }

    private void loadDataToAdapter(List<CarType> responseBody) {
        adapter = new InstakeCircularPagerAdapter(this, getSupportFragmentManager(), responseBody);
        binding.vehiclePager.setAdapter(adapter);
        binding.vehiclePager.addOnPageChangeListener(adapter);
//        if (flag == 1) {
//            binding.vehiclePager.setCurrentItem(1);
//        } else if (flag == 2) {
//            binding.vehiclePager.setCurrentItem(2);
//        } else if (flag == 3) {
//            binding.vehiclePager.setCurrentItem(0);
//        }
        binding.vehiclePager.setCurrentItem(flag - 1);
    }
}
