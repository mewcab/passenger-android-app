package com.instake.passenger.feature.splash;

import com.instake.passenger.common.base.BaseView;

/**
 * Created by masum on 2/22/17.
 */

public interface SplashView extends BaseView {
    void startAnimation();

    void stopAnimation();

    void initAppData();

    void navigateToHome();
}
