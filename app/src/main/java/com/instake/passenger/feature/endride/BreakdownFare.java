package com.instake.passenger.feature.endride;


import org.parceler.Parcel;

@Parcel
public class BreakdownFare {

    public String title;

    public float amount;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

}