
package com.instake.passenger.model.historydata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HistoryResp implements Parcelable {

    @SerializedName("trips")
    @Expose
    private List<Trip> trips = new ArrayList<>();
    @SerializedName("count")
    @Expose
    private Integer count;

    public final static Parcelable.Creator<HistoryResp> CREATOR = new Creator<HistoryResp>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HistoryResp createFromParcel(Parcel in) {
            HistoryResp instance = new HistoryResp();
            in.readList(instance.trips, (com.instake.passenger.model.historydata.Trip.class.getClassLoader()));
            instance.count = ((Integer) in.readValue((Integer.class.getClassLoader())));
            return instance;
        }

        public HistoryResp[] newArray(int size) {
            return (new HistoryResp[size]);
        }

    };

    public List<Trip> getTrips() {
        return trips;
    }

    public void setTrips(List<Trip> trips) {
        this.trips = trips;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(trips);
        dest.writeValue(count);
    }

    public int describeContents() {
        return 0;
    }

}
