
package com.instake.passenger.model.state;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("seats")
    @Expose
    private Integer seats;
    @SerializedName("fare_cost")
    @Expose
    private Integer fareCost;
    @SerializedName("per_minit")
    @Expose
    private Integer perMinit;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("killometre")
    @Expose
    private Integer killometre;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("bese_fare")
    @Expose
    private Integer beseFare;
    @SerializedName("base_fare")
    @Expose
    private Integer baseFare;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Integer getFareCost() {
        return fareCost;
    }

    public void setFareCost(Integer fareCost) {
        this.fareCost = fareCost;
    }

    public Integer getPerMinit() {
        return perMinit;
    }

    public void setPerMinit(Integer perMinit) {
        this.perMinit = perMinit;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getKillometre() {
        return killometre;
    }

    public void setKillometre(Integer killometre) {
        this.killometre = killometre;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Integer getBeseFare() {
        return beseFare;
    }

    public void setBeseFare(Integer beseFare) {
        this.beseFare = beseFare;
    }

    public Integer getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(Integer baseFare) {
        this.baseFare = baseFare;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

}
