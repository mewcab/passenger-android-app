package com.instake.passenger.model.payment;

import android.util.Log;

import com.instake.passenger.model.mapdistance.DistanceResponse;

import java.text.NumberFormat;

/**
 * Created by masum on 5/27/17.
 */

public class Fare {
    private static String TAG = Fare.class.getName();
    public float fareAmount;
    public float discount;
    public float constantOff;
    private float mBaseFare;

    private int baseFare;
    private int farePerKilo;
    private int waitingCharge;
    private float distance;
    private float estimatedTime;

    private float totalFare;

    private void calculateFareWithoutDiscount() {
        fareAmount = fareAmount - constantOff;
    }

    private String calculateFare(String fare, String time) {
        mBaseFare = 20;
        NumberFormat nf = NumberFormat.getInstance();
        float f1 = (Float.parseFloat(fare) * 12) + mBaseFare + (Float.parseFloat(time) * 0.5f);
        float f2 = f1 + 30.0f;

        nf.setMaximumFractionDigits(2);
        nf.setMinimumFractionDigits(2);

        String a = nf.format(f1);
        String b = nf.format(f2);

        String estimatedFare = a + "~" + b + "TK";

        return estimatedFare;
    }

    private void calculateFareWithDiscount() {

    }


    public String getEstimatedFare(DistanceResponse distanceInfoList) {

        Log.e(TAG, "ETA= " + distanceInfoList.getData().get(0)
                .getLegs().get(0)
                .getDuration().getText());

        Log.e(TAG, "Distance= " + distanceInfoList.getData().get(0)
                .getLegs().get(0)
                .getDistance().getText());

        String distance = distanceInfoList.getData().get(0).getLegs()
                .get(0).getDistance().getText()
                .toLowerCase().toString()
                .toLowerCase().replace("km", "").trim();

        String estimatedTimeToArrival = distanceInfoList.getData().get(0)
                .getLegs().get(0).getDuration().getText()
                .toLowerCase().toString().toLowerCase()
                .replace("mins", "").trim();

        Log.e(TAG, "Final ETA = " + estimatedTimeToArrival);


        return calculateFare(distance, estimatedTimeToArrival);
    }


    public float getFareAmount() {
        return fareAmount;
    }

    public void setFareAmount(float fareAmount) {
        this.fareAmount = fareAmount;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getConstantOff() {
        return constantOff;
    }

    public void setConstantOff(float constantOff) {
        this.constantOff = constantOff;
    }

    public float getmBaseFare() {
        return mBaseFare;
    }

    public void setmBaseFare(float mBaseFare) {
        this.mBaseFare = mBaseFare;
    }

    public int getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(int baseFare) {
        this.baseFare = baseFare;
    }

    public int getFarePerKilo() {
        return farePerKilo;
    }

    public void setFarePerKilo(int farePerKilo) {
        this.farePerKilo = farePerKilo;
    }

    public int getWaitingCharge() {
        return waitingCharge;
    }

    public void setWaitingCharge(int waitingCharge) {
        this.waitingCharge = waitingCharge;
    }

    public Fare calculateFare() {

        totalFare = (getDistance() * getFarePerKilo()) + getBaseFare() + (getEstimatedTime() * getWaitingCharge());

        Log.d(TAG, "base: " + getBaseFare());
        Log.d(TAG, "Estimate Time: " + getEstimatedTime());
        Log.d(TAG, "waiting charge: " + getWaitingCharge());
        Log.d(TAG, "calculateFare: " + totalFare);
        Log.d(TAG, "Distance: " + getDistance());
        //totalFare = totalFare - ((totalFare) * getDiscount()/100);
        //totalFare = totalFare - getConstantOff();
        return this;
    }

    public float getDistance() {
        return distance;
    }

    public void setDistance(float distance) {
        this.distance = distance;
    }

    public float getTotalFare() {
        return totalFare;
    }

    public float getEstimatedTime() {
        return estimatedTime;
    }

    public void setEstimatedTime(float estimatedTime) {
        this.estimatedTime = estimatedTime;
    }

    public String getEstimatedFare() {
        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(0);
        nf.setMinimumFractionDigits(0);
        return nf.format(getTotalFare()) + "~" + nf.format(getTotalFare() + 50);
    }

}
