
package com.instake.passenger.model.suppotnhelp;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class SupportAndHelpResponse implements Parcelable {

    @SerializedName("help_n_support")
    @Expose
    private List<HelpNSupport> helpNSupport = new ArrayList<>();
    @SerializedName("videos")
    @Expose
    private List<Video> videos = new ArrayList<>();
    public final static Creator<SupportAndHelpResponse> CREATOR = new Creator<SupportAndHelpResponse>() {


        @SuppressWarnings({
                "unchecked"
        })
        public SupportAndHelpResponse createFromParcel(Parcel in) {
            SupportAndHelpResponse instance = new SupportAndHelpResponse();
            in.readList(instance.helpNSupport, (HelpNSupport.class.getClassLoader()));
            in.readList(instance.videos, (Video.class.getClassLoader()));
            return instance;
        }

        public SupportAndHelpResponse[] newArray(int size) {
            return (new SupportAndHelpResponse[size]);
        }

    };

    public List<HelpNSupport> getHelpNSupport() {
        return helpNSupport;
    }

    public void setHelpNSupport(List<HelpNSupport> helpNSupport) {
        this.helpNSupport = helpNSupport;
    }

    public List<Video> getVideos() {
        return videos;
    }

    public void setVideos(List<Video> videos) {
        this.videos = videos;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeList(helpNSupport);
        dest.writeList(videos);
    }

    public int describeContents() {
        return 0;
    }

}
