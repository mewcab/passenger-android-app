package com.instake.passenger.model.payment;

import org.parceler.Parcel;

/**
 * Created by masum on 8/12/17.
 */

@Parcel
public class Tips {
    public String trip_id;
    public float tips_amount;
    public float tips_total;
    public float rating;

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public float getTips_amount() {
        return tips_amount;
    }

    public void setTips_amount(float tips_amount) {
        this.tips_amount = tips_amount;
    }

    public float getTips_total() {
        return tips_total;
    }

    public void setTips_total(float tips_total) {
        this.tips_total = tips_total;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }
}

