package com.instake.passenger.model;

import java.util.ArrayList;

/**
 * Created by Mostafa Monowar on 11-Sep-17.
 */

public class EducationDetails {
    private ArrayList<String> classNames = null;
    private ArrayList<String> department = null;

    public EducationDetails() {
    }

    public ArrayList<String> getClassList() {
        return classNames;
    }

    public void setClassList(ArrayList<String> classNames) {
        this.classNames = classNames;
    }

    public ArrayList<String> getDepartment() {
        return department;
    }

    public void setDepartment(ArrayList<String> department) {
        this.department = department;
    }
}
