package com.instake.passenger.model.nearestdriver;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.instake.passenger.model.state.Response;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by masum on 3/8/17.
 */

public class AcceptedTripInfoJsonParser {
    private String TAG = "AcceptedTripInfo";
    private Gson gson;

    public AcceptedTripInfoJsonParser(Gson gson) {
        this.gson = gson;
    }

    public AcceptedTripResponse getAcceptedTripInfo(String responseString) {

        AcceptedTripResponse response = null;
        try {
            JSONObject jsonObject = new JSONObject(responseString);
            JSONObject jsonObjectDriver = jsonObject.getJSONObject("driver");

            Object objectAvatar = jsonObjectDriver.get("avatar");
            if (objectAvatar instanceof String) {
                jsonObjectDriver.put("avatar", avatar(jsonObjectDriver));

            }

            response = gson.fromJson(jsonObject.toString(), AcceptedTripResponse.class);

        } catch (Exception e) {
            Log.e(TAG, "Error parsing : " + e.getMessage());
        }

        return response;

    }

    @NonNull
    private JSONObject avatar(JSONObject jsonObjectDriver) throws JSONException {
        JSONObject av = new JSONObject();
        av.put("300X300", jsonObjectDriver.getString("avatar"));
        av.put("250X360", jsonObjectDriver.getString("avatar"));
        av.put("150X150", jsonObjectDriver.getString("avatar"));
        av.put("50X50", jsonObjectDriver.getString("avatar"));
        av.put("42X60", jsonObjectDriver.getString("avatar"));
        return av;
    }


}
