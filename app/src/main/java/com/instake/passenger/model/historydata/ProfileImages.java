
package com.instake.passenger.model.historydata;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileImages implements Parcelable
{

    @SerializedName("42X60")
    @Expose
    private String _42X60;
    @SerializedName("50X50")
    @Expose
    private String _50X50;
    @SerializedName("120X175")
    @Expose
    private String _120X175;
    @SerializedName("150X150")
    @Expose
    private String _150X150;
    @SerializedName("250X360")
    @Expose
    private String _250X360;
    @SerializedName("300X300")
    @Expose
    private String _300X300;
    public final static Parcelable.Creator<ProfileImages> CREATOR = new Creator<ProfileImages>() {


        @SuppressWarnings({
            "unchecked"
        })
        public ProfileImages createFromParcel(Parcel in) {
            ProfileImages instance = new ProfileImages();
            instance._42X60 = ((String) in.readValue((String.class.getClassLoader())));
            instance._50X50 = ((String) in.readValue((String.class.getClassLoader())));
            instance._120X175 = ((String) in.readValue((String.class.getClassLoader())));
            instance._150X150 = ((String) in.readValue((String.class.getClassLoader())));
            instance._250X360 = ((String) in.readValue((String.class.getClassLoader())));
            instance._300X300 = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public ProfileImages[] newArray(int size) {
            return (new ProfileImages[size]);
        }

    }
    ;

    public String get42X60() {
        return _42X60;
    }

    public void set42X60(String _42X60) {
        this._42X60 = _42X60;
    }

    public String get50X50() {
        return _50X50;
    }

    public void set50X50(String _50X50) {
        this._50X50 = _50X50;
    }

    public String get120X175() {
        return _120X175;
    }

    public void set120X175(String _120X175) {
        this._120X175 = _120X175;
    }

    public String get150X150() {
        return _150X150;
    }

    public void set150X150(String _150X150) {
        this._150X150 = _150X150;
    }

    public String get250X360() {
        return _250X360;
    }

    public void set250X360(String _250X360) {
        this._250X360 = _250X360;
    }

    public String get300X300() {
        return _300X300;
    }

    public void set300X300(String _300X300) {
        this._300X300 = _300X300;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(_42X60);
        dest.writeValue(_50X50);
        dest.writeValue(_120X175);
        dest.writeValue(_150X150);
        dest.writeValue(_250X360);
        dest.writeValue(_300X300);
    }

    public int describeContents() {
        return  0;
    }

}
