package com.instake.passenger.model;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.instake.passenger.feature.home.GeoCoderListener;
import com.instake.passenger.utility.Utility;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

/**
 * Created by masum on 2/28/17.
 */

public class GeoCoder {
    private static String TAG = "GeoCoder";
    private Geocoder geocoder;
    private Context mConetext;
    private static GeoCoderListener geoCoderAddressListener;

    public GeoCoder(Context context, GeoCoderListener geoCoderAddressListener) {
        this.geoCoderAddressListener = geoCoderAddressListener;
        this.mConetext = context;
        geocoder = new Geocoder(mConetext, Locale.getDefault());
    }

    public void getAddressFromLocation(final double latitude, final double longitude) {
        new GeoCoderThread(geocoder, latitude, longitude).start();

    }

    private static class GeoCoderThread extends Thread {
        GeoCoderHandler geocoderHandler;
        Geocoder geocoder;
        double latitude;
        double longitude;

        GeoCoderThread(Geocoder geocoder, double latitude, final double longitude) {
            geocoderHandler = new GeoCoderHandler();
            this.latitude = latitude;
            this.longitude = longitude;
            this.geocoder = geocoder;
        }

        @Override
        public void run() {
            String result = null;
            try {
                List<Address> addressList = geocoder.getFromLocation(latitude, longitude, 1);
                if (addressList != null && addressList.size() > 0) {
                    Address address = addressList.get(0);
                    StringBuilder sb = new StringBuilder();

                    if (address.getMaxAddressLineIndex() > 0) {
                        if (address.getAddressLine(0) != null) {
                            sb.append(address.getAddressLine(0));
                        }
                    }

                    sb.append(address.getPostalCode()).append(", ");
                    sb.append(address.getCountryName());

                    result = sb.toString();
                    Log.i(TAG, "GEOCODER: " + result);

                }

            } catch (IOException e) {
                Log.e("GEOCODE", "Unable connect to Geocoder", e);

            } finally {
                Message message = Message.obtain();
                message.setTarget(geocoderHandler);
                if (result != null) {
                    message.what = 1;
                    Bundle bundle = new Bundle();
                    //result = "Latitude: " + latitude + " Longitude: " + longitude + "\n\nAddress:\n" + result;
                    bundle.putString("address", result);
                    message.setData(bundle);
                } else {
                    message.what = 1;
                    Bundle bundle = new Bundle();
                    //result = "Latitude: " + latitude + " Longitude: " + longitude + "\n Unable to get address for this lat-long.";
                    result = "Undefined address";
                    bundle.putString("address", result);
                    message.setData(bundle);
                }

                message.sendToTarget();
            }
        }

    }

    private static class GeoCoderHandler extends Handler {
        @Override
        public void handleMessage(Message message) {
            String locationAddress;
            switch (message.what) {
                case 1:
                    Bundle bundle = message.getData();
                    locationAddress = bundle.getString("address");
                    break;
                default:
                    locationAddress = null;
            }

            locationAddress = Utility.specialCharacterRemover(locationAddress, ",");
            LocationInfo locationInfo = new LocationInfo();
            locationInfo.setAddress(locationAddress);
            geoCoderAddressListener.onGotAddress(locationInfo);

        }

    }


}
