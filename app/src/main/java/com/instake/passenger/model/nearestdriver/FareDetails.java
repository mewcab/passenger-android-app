package com.instake.passenger.model.nearestdriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.util.List;

public class FareDetails {

    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("breakdown")
    @Expose
    private List<Breakdown> breakdown = null;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<Breakdown> getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(List<Breakdown> breakdown) {
        this.breakdown = breakdown;
    }

}