
package com.instake.passenger.model.historydata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class Trip implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("passenger_id")
    @Expose
    private String passengerId;
    @SerializedName("rating_user")
    @Expose
    private Integer ratingUser;
    @SerializedName("tips_amount")
    @Expose
    private Integer tipsAmount;
    @SerializedName("instake_charge")
    @Expose
    private Integer instakeCharge;
    @SerializedName("sub_total")
    @Expose
    private Double subTotal;
    @SerializedName("discount")
    @Expose
    private Double discount;
    @SerializedName("total_fear")
    @Expose
    private Integer totalFear;
    @SerializedName("total_time")
    @Expose
    private Double totalTime;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("base_fare_rate")
    @Expose
    private Integer baseFareRate;
    @SerializedName("driver_id")
    @Expose
    private DriverId driverId;
    @SerializedName("payment_method")
    @Expose
    private String paymentMethod;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("destination_time")
    @Expose
    private String destinationTime;
    @SerializedName("end_time")
    @Expose
    private String endTime;
    @SerializedName("rating_driver")
    @Expose
    private Integer ratingDriver;
    @SerializedName("pickup_name")
    @Expose
    private String pickupName;
    @SerializedName("pickup_location")
    @Expose
    private List<Double> pickupLocation = new ArrayList<>();
    @SerializedName("dropoff_name")
    @Expose
    private String dropoffName;
    @SerializedName("dropoff_location")
    @Expose
    private List<Double> dropoffLocation = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(String passengerId) {
        this.passengerId = passengerId;
    }

    public Integer getRatingUser() {
        return ratingUser;
    }

    public void setRatingUser(Integer ratingUser) {
        this.ratingUser = ratingUser;
    }

    public Integer getTipsAmount() {
        return tipsAmount;
    }

    public void setTipsAmount(Integer tipsAmount) {
        this.tipsAmount = tipsAmount;
    }

    public Integer getInstakeCharge() {
        return instakeCharge;
    }

    public void setInstakeCharge(Integer instakeCharge) {
        this.instakeCharge = instakeCharge;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Integer getTotalFear() {
        return totalFear;
    }

    public void setTotalFear(Integer totalFear) {
        this.totalFear = totalFear;
    }

    public Double getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(Double totalTime) {
        this.totalTime = totalTime;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getBaseFareRate() {
        return baseFareRate;
    }

    public void setBaseFareRate(Integer baseFareRate) {
        this.baseFareRate = baseFareRate;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public void setDriverId(DriverId driverId) {
        this.driverId = driverId;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getDestinationTime() {
        return destinationTime;
    }

    public void setDestinationTime(String destinationTime) {
        this.destinationTime = destinationTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public Integer getRatingDriver() {
        return ratingDriver;
    }

    public void setRatingDriver(Integer ratingDriver) {
        this.ratingDriver = ratingDriver;
    }

    public String getPickupName() {
        return pickupName;
    }

    public void setPickupName(String pickupName) {
        this.pickupName = pickupName;
    }

    public List<Double> getPickupLocation() {
        return pickupLocation;
    }

    public void setPickupLocation(List<Double> pickupLocation) {
        this.pickupLocation = pickupLocation;
    }

    public String getDropoffName() {
        return dropoffName;
    }

    public void setDropoffName(String dropoffName) {
        this.dropoffName = dropoffName;
    }

    public List<Double> getDropoffLocation() {
        return dropoffLocation;
    }

    public void setDropoffLocation(List<Double> dropoffLocation) {
        this.dropoffLocation = dropoffLocation;
    }

    public final static Parcelable.Creator<Trip> CREATOR = new Creator<Trip>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Trip createFromParcel(Parcel in) {
            Trip instance = new Trip();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.passengerId = ((String) in.readValue((String.class.getClassLoader())));
            instance.ratingUser = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.tipsAmount = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.instakeCharge = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.subTotal = ((Double) in.readValue((Double.class.getClassLoader())));
            instance.discount = ((Double) in.readValue((Double.class.getClassLoader())));
            instance.totalFear = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.totalTime = ((Double) in.readValue((Double.class.getClassLoader())));
            instance.distance = ((Double) in.readValue((Double.class.getClassLoader())));
            instance.baseFareRate = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.driverId = ((DriverId) in.readValue((DriverId.class.getClassLoader())));
            instance.paymentMethod = ((String) in.readValue((String.class.getClassLoader())));
            instance.status = ((String) in.readValue((String.class.getClassLoader())));
            instance.departureTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.startTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.destinationTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.endTime = ((String) in.readValue((String.class.getClassLoader())));
            instance.ratingDriver = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.pickupName = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.pickupLocation, (java.lang.Double.class.getClassLoader()));
            instance.dropoffName = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.dropoffLocation, (java.lang.Double.class.getClassLoader()));
            return instance;
        }

        public Trip[] newArray(int size) {
            return (new Trip[size]);
        }

    };

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(passengerId);
        dest.writeValue(ratingUser);
        dest.writeValue(tipsAmount);
        dest.writeValue(instakeCharge);
        dest.writeValue(subTotal);
        dest.writeValue(discount);
        dest.writeValue(totalFear);
        dest.writeValue(totalTime);
        dest.writeValue(distance);
        dest.writeValue(baseFareRate);
        dest.writeValue(driverId);
        dest.writeValue(paymentMethod);
        dest.writeValue(status);
        dest.writeValue(departureTime);
        dest.writeValue(startTime);
        dest.writeValue(destinationTime);
        dest.writeValue(endTime);
        dest.writeValue(ratingDriver);
        dest.writeValue(pickupName);
        dest.writeList(pickupLocation);
        dest.writeValue(dropoffName);
        dest.writeList(dropoffLocation);
    }

    public int describeContents() {
        return 0;
    }

}
