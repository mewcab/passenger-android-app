package com.instake.passenger.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.android.gms.maps.model.LatLng;
import com.instake.passenger.common.App;
import com.instake.passenger.feature.endride.TripFareDetails;
import com.instake.passenger.model.payment.Tips;

import org.parceler.Parcel;

/**
 * Created by masum on 6/14/17.
 */

@Parcel
public class Trip {
    public Trip trip;
    public String tripId;
    public String driverPhoneNo;
    public String driverName;
    public String driverPhotoUrl;
    public String passengerPhoneNo;
    public String passengerName;
    public LatLng pickUpLocation;
    public LatLng dropOffLocation;
    public String pickUpAddress;
    public String dropOffAddress;
    public String currentRideStatus;
    public String tripEstimatedTime;
    public String tripBigMessage;
    public String tripSmallMessage;
    public String tripFare;
    public String driverPhoto;
    public String vehicleId;
    public double distance;
    public String vehicleName;
    public double driverRating;
    public String paymentType;
    public TripFareDetails fareDetails;

    public Tips tips;

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public TripFareDetails getFareDetails() {
        return fareDetails;
    }

    public void setFareDetails(TripFareDetails fareDetails) {
        this.fareDetails = fareDetails;
    }


    public String driverEstimatedArrivalTime;

    public String getPassengerPhoneNo() {
        return passengerPhoneNo;
    }

    public void setPassengerPhoneNo(String passengerPhoneNo) {
        this.passengerPhoneNo = passengerPhoneNo;
    }

    public Trip() {

    }

    public Trip(Trip trip) {
        this.trip = trip;
    }

    public String getTripEstimatedTime() {
        return tripEstimatedTime;
    }

    public void setTripEstimatedTime(String tripEstimatedTime) {
        this.tripEstimatedTime = tripEstimatedTime;
    }

    public String getDriverEstimatedArrivalTime() {
        return driverEstimatedArrivalTime;
    }

    public void setDriverEstimatedArrivalTime(String driverEstimatedArrivalTime) {
        this.driverEstimatedArrivalTime = driverEstimatedArrivalTime;
    }

    public String getCurrentRideStatus() {
        return currentRideStatus;
    }

    public void setCurrentRideStatus(String currentRideStatus) {
        this.currentRideStatus = currentRideStatus;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public LatLng getPickUpLocation() {
        return pickUpLocation;
    }

    public void setPickUpLocation(LatLng pickUpLocation) {
        this.pickUpLocation = pickUpLocation;
    }

    public String getDriverPhoneNo() {
        return driverPhoneNo;
    }

    public void setDriverPhoneNo(String driverPhoneNo) {
        this.driverPhoneNo = driverPhoneNo;
    }

    public String getTripBigMessage() {
        return tripBigMessage;
    }

    public void setTripBigMessage(String tripBigMessage) {
        this.tripBigMessage = tripBigMessage;
    }

    public String getTripSmallMessage() {
        return tripSmallMessage;
    }

    public void setTripSmallMessage(String tripSmallMessage) {
        this.tripSmallMessage = tripSmallMessage;
    }

    public String getTripTotalFare() {
        return tripFare;
    }

    public void setTripTotalFare(String tripFare) {
        this.tripFare = tripFare;
    }

    public LatLng getDropOffLocation() {
        return dropOffLocation;
    }

    public void setDropOffLocation(LatLng dropOffLocation) {
        this.dropOffLocation = dropOffLocation;
    }

    public String getPickUpAddress() {
        return pickUpAddress;
    }

    public void setPickUpAddress(String pickUpAddress) {
        this.pickUpAddress = pickUpAddress;
    }

    public String getDropOffAddress() {
        return dropOffAddress;
    }

    public void setDropOffAddress(String dropOffAddress) {
        this.dropOffAddress = dropOffAddress;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getPassengerName() {
        return passengerName;
    }

    public void setPassengerName(String passengerName) {
        this.passengerName = passengerName;
    }

    public String getDriverPhoto() {
        return driverPhoto;
    }

    public void setDriverPhoto(String driverPhoto) {
        this.driverPhoto = driverPhoto;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public String getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(String vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getVehicleName() {
        return vehicleName;
    }

    public void setVehicleName(String vehicleName) {
        this.vehicleName = vehicleName;
    }

    public double getDriverRating() {
        return driverRating;
    }

    public void setDriverRating(double driverRating) {
        this.driverRating = driverRating;
    }

    public String getDriverPhotoUrl() {
        return App.BASE_URL + driverPhotoUrl;
    }

    public void setDriverPhotoUrl(@Nullable String driverPhotoUrl) {
        this.driverPhotoUrl = driverPhotoUrl;
    }

    public Tips getTips() {
        return tips;
    }

    public void setTips(Tips tips) {
        this.tips = tips;
    }
}
