package com.instake.passenger.model;

import com.instake.passenger.model.historydata.DriverId;

import org.parceler.Parcel;

/**
 * Created by mahmudul.hasan on 5/30/2017.
 */

@Parcel
public class RiderHistoryModel {
    public String _id, passenger_id, price, driverName, driver_id, status, destination_time, destination_location,
            departure_location, paymentType;
    public DriverId driverId;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getPassenger_id() {
        return passenger_id;
    }

    public void setPassenger_id(String passenger_id) {
        this.passenger_id = passenger_id;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDestination_time() {
        return destination_time;
    }

    public void setDestination_time(String destination_time) {
        this.destination_time = destination_time;
    }

    public String getDestination_location() {
        return destination_location;
    }

    public void setDestination_location(String destination_location) {
        this.destination_location = destination_location;
    }

    public String getDeparture_location() {
        return departure_location;
    }

    public void setDeparture_location(String departure_location) {
        this.departure_location = departure_location;
    }

    public String getDriverName() {
        return driverName;
    }

    public void setDriverName(String driverName) {
        this.driverName = driverName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public void setDriverId(DriverId driverId) {
        this.driverId = driverId;
    }

    @Override
    public String toString() {
        return "RiderHistoryModel{" +
                "_id='" + _id + '\'' +
                ", passenger_id='" + passenger_id + '\'' +
                ", price='" + price + '\'' +
                ", driver_id='" + driver_id + '\'' +
                ", status='" + status + '\'' +
                ", destination_time='" + destination_time + '\'' +
                ", destination_location='" + destination_location + '\'' +
                ", departure_location='" + departure_location + '\'' +
                '}';
    }
}
