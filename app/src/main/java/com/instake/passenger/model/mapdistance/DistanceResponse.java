package com.instake.passenger.model.mapdistance;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

import org.parceler.Generated;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * Created by masum on 5/25/17.
 */

@Generated("org.jsonschema2pojo")
public class DistanceResponse {
    @SerializedName("routes")
    private List<Route> routes;

    public List<Route> getData() {
        return routes;
    }

    /**
     * @param data The data
     */
    public void setData(List<Route> data) {
        this.routes = data;
    }

    public float getDistance() throws Exception {

        String distance = "";

        if (getData().get(0).getLegs().get(0).getDistance().getText().toLowerCase().contains("km")) {

            distance = getData().get(0).getLegs()
                    .get(0).getDistance().getText()
                    .toLowerCase().toString()
                    .toLowerCase().replace("km", "").trim();
        } else {
            distance = getData().get(0).getLegs()
                    .get(0).getDistance().getText()
                    .toLowerCase().toString()
                    .toLowerCase().replace("m", "").trim();
        }

        float distanceInMeter = (float) getData().get(0).getLegs().get(0).getDistance().getValue() / 1000;

        Log.e("API distance", "Distance==>>" + getData().get(0).getLegs().get(0).getDistance().getValue());
        Log.e("API distance", "Distance==>>" + (float) (getData().get(0).getLegs().get(0).getDistance().getValue() / 1000));

        return distanceInMeter;
    }

    public long getEstimatedTime() {

        String estimatedTimeToArrival = "";
        long numberOfMinutes = 0;

        if (getData().get(0)
                .getLegs().get(0).getDuration().getText()
                .toLowerCase().toString().toLowerCase().contains("hour")) {

            estimatedTimeToArrival = getData().get(0)
                    .getLegs().get(0).getDuration().getText()
                    .toLowerCase().toString().toLowerCase()
                    .replace("hour", "").trim();
        } else {

            estimatedTimeToArrival = getData().get(0)
                    .getLegs().get(0).getDuration().getText()
                    .toLowerCase().toString().toLowerCase()
                    .replace("mins", "").trim();

        }

        Log.e("API TIME", "before min:===>>>" + getData().get(0).getLegs().get(0).getDuration().getValue());
        numberOfMinutes = (long) ((getData().get(0).getLegs().get(0).getDuration().getValue() % 86400) % 3600) / 60;
        Log.e("API TIME", "min:===>>>" + numberOfMinutes);

        return numberOfMinutes;
    }


}
