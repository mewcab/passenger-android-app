package com.instake.passenger.model.nearestdriver;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.instake.passenger.model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by masum on 3/8/17.
 */

public class NearestJsonParser extends User {
    private String TAG = "NearestJsonParser";
    private Gson gson;

    public NearestJsonParser(Gson gson) {
        this.gson = gson;
    }

    public NearestDriverResponse getResponseToLocationList(String responseString) {
        NearestDriverResponse response = null;
        try {
            JSONObject jsonObject = new JSONObject(responseString);
            JSONArray jsonArray = jsonObject.getJSONArray("driver_list");
            for (int i = 0; i < jsonArray.length(); i++) {
                JSONObject jsonObjectDriver = jsonArray.getJSONObject(i).getJSONObject("driver");
                Object objectAvatar = jsonObjectDriver.get("avatar");
                if (objectAvatar instanceof String) {
                    jsonObjectDriver.put("avatar", avatar(jsonObjectDriver));

                }

            }

            Log.w(TAG, "build new json: " + jsonObject.toString());
            response = gson.fromJson(jsonObject.toString(), NearestDriverResponse.class);

        } catch (Exception e) {
            Log.e(TAG, "Error parsing : " + e.getMessage());
        }

        
        return response;

    }

    @NonNull
    private JSONObject avatar(JSONObject jsonObjectDriver) throws JSONException {
        JSONObject av = new JSONObject();
        av.put("300X300", jsonObjectDriver.getString("avatar"));
        av.put("250X360", jsonObjectDriver.getString("avatar"));
        av.put("150X150", jsonObjectDriver.getString("avatar"));
        av.put("50X50", jsonObjectDriver.getString("avatar"));
        av.put("42X60", jsonObjectDriver.getString("avatar"));
        return av;
    }

}
