package com.instake.passenger.model.state;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FareDetails {

    @SerializedName("breakdown")
    @Expose
    private List<Breakdown> breakdown = null;
    @SerializedName("total")
    @Expose
    private float total;

    public List<Breakdown> getBreakdown() {
        return breakdown;
    }

    public void setBreakdown(List<Breakdown> breakdown) {
        this.breakdown = breakdown;
    }

    public float getTotal() {
        return total;
    }

    public void setTotal(float total) {
        this.total = total;
    }

}