
package com.instake.passenger.model.driver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Location {
    @SerializedName("loation")
    @Expose
    private Loation location = null;

    @SerializedName("distance")
    @Expose
    private Distance distance = null;

    public Loation getLocation() {
        return location;
    }

    public void setLocation(Loation location) {
        this.location = location;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

}
