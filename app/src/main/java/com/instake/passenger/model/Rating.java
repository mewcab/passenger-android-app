package com.instake.passenger.model;

/**
 * Created by masum on 8/12/17.
 */

public class Rating {
    private float rating;
    private String text;

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
