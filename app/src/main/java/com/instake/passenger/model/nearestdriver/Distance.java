
package com.instake.passenger.model.nearestdriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Distance {

    @SerializedName("index")
    @Expose
    private Integer index;
    @SerializedName("distance")
    @Expose
    private Double distance;
    @SerializedName("distanceValue")
    @Expose
    private Integer distanceValue;
    @SerializedName("duration")
    @Expose
    private String duration;
    @SerializedName("durationValue")
    @Expose
    private Integer durationValue;
    @SerializedName("origin")
    @Expose
    private String origin;
    @SerializedName("destination")
    @Expose
    private String destination;
    @SerializedName("mode")
    @Expose
    private String mode;
    @SerializedName("units")
    @Expose
    private String units;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("avoid")
    @Expose
    private Object avoid;
    @SerializedName("sensor")
    @Expose
    private Boolean sensor;
    @SerializedName("duration_unit")
    @Expose
    private String durationUnit;

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public Integer getDistanceValue() {
        return distanceValue;
    }

    public void setDistanceValue(Integer distanceValue) {
        this.distanceValue = distanceValue;
    }

    public String getDuration() {
        return duration;
    }

    public void setDuration(String duration) {
        this.duration = duration;
    }

    public Integer getDurationValue() {
        return durationValue;
    }

    public void setDurationValue(Integer durationValue) {
        this.durationValue = durationValue;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getUnits() {
        return units;
    }

    public void setUnits(String units) {
        this.units = units;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Object getAvoid() {
        return avoid;
    }

    public void setAvoid(Object avoid) {
        this.avoid = avoid;
    }

    public Boolean getSensor() {
        return sensor;
    }

    public void setSensor(Boolean sensor) {
        this.sensor = sensor;
    }

    public String getDurationUnit() {
        return durationUnit;
    }

    public void setDurationUnit(String durationUnit) {
        this.durationUnit = durationUnit;
    }

}
