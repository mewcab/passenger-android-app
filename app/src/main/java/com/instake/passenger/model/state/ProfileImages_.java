
package com.instake.passenger.model.state;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileImages_ {

    @SerializedName("42X60")
    @Expose
    private String _42X60;
    @SerializedName("50X50")
    @Expose
    private String _50X50;
    @SerializedName("120X175")
    @Expose
    private String _120X175;
    @SerializedName("150X150")
    @Expose
    private String _150X150;
    @SerializedName("250X360")
    @Expose
    private String _250X360;
    @SerializedName("300X300")
    @Expose
    private String _300X300;

    public String get42X60() {
        return _42X60;
    }

    public void set42X60(String _42X60) {
        this._42X60 = _42X60;
    }

    public String get50X50() {
        return _50X50;
    }

    public void set50X50(String _50X50) {
        this._50X50 = _50X50;
    }

    public String get120X175() {
        return _120X175;
    }

    public void set120X175(String _120X175) {
        this._120X175 = _120X175;
    }

    public String get150X150() {
        return _150X150;
    }

    public void set150X150(String _150X150) {
        this._150X150 = _150X150;
    }

    public String get250X360() {
        return _250X360;
    }

    public void set250X360(String _250X360) {
        this._250X360 = _250X360;
    }

    public String get300X300() {
        return _300X300;
    }

    public void set300X300(String _300X300) {
        this._300X300 = _300X300;
    }

}
