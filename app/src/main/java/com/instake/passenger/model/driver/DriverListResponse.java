
package com.instake.passenger.model.driver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DriverListResponse {

    @SerializedName("type")
    @Expose
    private String type;

    @SerializedName("phone_no")
    @Expose
    private String phoneNo;

    @SerializedName("location")
    @Expose
    private List<Location> location = null;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public List<Location> getLocation() {
        return location;
    }

    public void setLocation(List<Location> location) {
        this.location = location;
    }



}
