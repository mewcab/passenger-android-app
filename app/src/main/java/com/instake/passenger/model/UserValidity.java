package com.instake.passenger.model;

import org.parceler.Parcel;

/**
 * Created by Tushar on 5/21/17.
 */

@Parcel
public class UserValidity {
    public boolean isExist;
    public User user;

    @Override
    public String toString() {
        return "UserValidity{" +
                "isExist=" + isExist +
                ", user=" + user +
                '}';
    }
}
