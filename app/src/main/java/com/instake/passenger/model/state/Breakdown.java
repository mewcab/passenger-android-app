package com.instake.passenger.model.state;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Breakdown {

    @SerializedName("amount")
    @Expose
    private float amount;
    @SerializedName("title")
    @Expose
    private String title;

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

}