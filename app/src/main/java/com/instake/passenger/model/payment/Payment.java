package com.instake.passenger.model.payment;

/**
 * Created by masum on 6/1/17.
 */

public class Payment {

    public static String paymentTypeToString(int type) {
        if (type == 1) {
            return "Cash";

        } else if (type == 2) {
            return "Instake Pay";

        } else if (type == 3) {
            return "Card";

        }

        return "N/A";
    }
}
