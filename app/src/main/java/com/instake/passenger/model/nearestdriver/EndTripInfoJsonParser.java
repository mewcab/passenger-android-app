package com.instake.passenger.model.nearestdriver;

import com.google.gson.Gson;

/**
 * Created by masum on 3/8/17.
 */

public class EndTripInfoJsonParser {
    private Gson gson;

    public EndTripInfoJsonParser(Gson gson) {
        this.gson = gson;
    }

    public EndTripResponse getAcceptedTripInfo(String responseString) {
        EndTripResponse response = gson.fromJson(responseString, EndTripResponse.class);
        return response;

    }


}
