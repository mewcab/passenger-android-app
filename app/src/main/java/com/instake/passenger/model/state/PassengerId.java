
package com.instake.passenger.model.state;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PassengerId {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("referral")
    @Expose
    private String referral;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("profile_images")
    @Expose
    private ProfileImages_ profileImages;
    @SerializedName("rating_count")
    @Expose
    private float ratingCount;
    @SerializedName("total_rating")
    @Expose
    private float totalRating;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public ProfileImages_ getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(ProfileImages_ profileImages) {
        this.profileImages = profileImages;
    }

    public float getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    public float getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(float totalRating) {
        this.totalRating = totalRating;
    }

}
