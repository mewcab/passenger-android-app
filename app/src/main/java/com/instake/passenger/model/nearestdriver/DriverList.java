
package com.instake.passenger.model.nearestdriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverList{

    @SerializedName("vehicle_info")
    @Expose
    public VehicleInfo vehicleInfo;
    @SerializedName("driver")
    @Expose
    public Driver driver;
    @SerializedName("passenger")
    @Expose
    public Passenger passenger;
    @SerializedName("driver_location")
    @Expose
    public DriverLocation driverLocation;
    @SerializedName("distance")
    @Expose
    public Distance distance;

    @SerializedName("trip")
    @Expose
    public Trip trip;


    @SerializedName("fare_details")
    @Expose
    private FareDetails fareDetails;

    public DriverList () {

    }

    public FareDetails getFareDetails() {
        return fareDetails;
    }

    public void setFareDetails(FareDetails fareDetails) {
        this.fareDetails = fareDetails;
    }

    public VehicleInfo getVehicleInfo() {
        return vehicleInfo;
    }

    public void setVehicleInfo(VehicleInfo vehicleInfo) {
        this.vehicleInfo = vehicleInfo;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public DriverLocation getDriverLocation() {
        return driverLocation;
    }

    public void setDriverLocation(DriverLocation driverLocation) {
        this.driverLocation = driverLocation;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public Passenger getPassenger() {
        return passenger;
    }

    public void setPassenger(Passenger passenger) {
        this.passenger = passenger;
    }

    public void setTrip(Trip trip) {
        this.trip = trip;
    }

    public Trip getTrip() {
        return this.trip;
    }
}
