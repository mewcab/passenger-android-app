
package com.instake.passenger.model.historydata;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DriverId implements Parcelable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("referral")
    @Expose
    private String referral;
    @SerializedName("phone_no")
    @Expose
    private String phoneNo;
    @SerializedName("profile_images")
    @Expose
    private ProfileImages profileImages;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("vehicle_id")
    @Expose
    private VehicleId vehicleId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("full_name")
    @Expose
    private String fullName;
    @SerializedName("rating_count")
    @Expose
    private Integer ratingCount;
    @SerializedName("total_rating")
    @Expose
    private Double totalRating;
    public final static Parcelable.Creator<DriverId> CREATOR = new Creator<DriverId>() {


        @SuppressWarnings({
            "unchecked"
        })
        public DriverId createFromParcel(Parcel in) {
            DriverId instance = new DriverId();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.referral = ((String) in.readValue((String.class.getClassLoader())));
            instance.phoneNo = ((String) in.readValue((String.class.getClassLoader())));
            instance.profileImages = ((ProfileImages) in.readValue((ProfileImages.class.getClassLoader())));
            instance.email = ((String) in.readValue((String.class.getClassLoader())));
            instance.vehicleId = ((VehicleId) in.readValue((VehicleId.class.getClassLoader())));
            instance.firstName = ((String) in.readValue((String.class.getClassLoader())));
            instance.fullName = ((String) in.readValue((String.class.getClassLoader())));
            instance.ratingCount = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.totalRating = ((Double) in.readValue((Double.class.getClassLoader())));
            return instance;
        }

        public DriverId[] newArray(int size) {
            return (new DriverId[size]);
        }

    }
    ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getReferral() {
        return referral;
    }

    public void setReferral(String referral) {
        this.referral = referral;
    }

    public String getPhoneNo() {
        return phoneNo;
    }

    public void setPhoneNo(String phoneNo) {
        this.phoneNo = phoneNo;
    }

    public ProfileImages getProfileImages() {
        return profileImages;
    }

    public void setProfileImages(ProfileImages profileImages) {
        this.profileImages = profileImages;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public VehicleId getVehicleId() {
        return vehicleId;
    }

    public void setVehicleId(VehicleId vehicleId) {
        this.vehicleId = vehicleId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public Integer getRatingCount() {
        return ratingCount;
    }

    public void setRatingCount(Integer ratingCount) {
        this.ratingCount = ratingCount;
    }

    public Double getTotalRating() {
        return totalRating;
    }

    public void setTotalRating(Double totalRating) {
        this.totalRating = totalRating;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(referral);
        dest.writeValue(phoneNo);
        dest.writeValue(profileImages);
        dest.writeValue(email);
        dest.writeValue(vehicleId);
        dest.writeValue(firstName);
        dest.writeValue(fullName);
        dest.writeValue(ratingCount);
        dest.writeValue(totalRating);
    }

    public int describeContents() {
        return  0;
    }

}
