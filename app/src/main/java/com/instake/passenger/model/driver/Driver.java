package com.instake.passenger.model.driver;

import com.google.gson.Gson;
import com.instake.passenger.model.User;

import java.util.List;

/**
 * Created by masum on 3/8/17.
 */

public class Driver extends User {
    private Gson gson;
    public Driver(Gson gson) {
        this.gson = gson;
    }

    public DriverListResponse getResponseToLocationList(String responseString) {
        DriverListResponse response = gson.fromJson(responseString, DriverListResponse.class);
        response.getPhoneNo();//My phone no
        List<Location> results = response.getLocation();
        return response;

    }


}
