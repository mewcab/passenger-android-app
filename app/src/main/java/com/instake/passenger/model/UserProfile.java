package com.instake.passenger.model;

import org.parceler.Parcel;

/**
 * Created by Mostafa Monowar on 11-Sep-17.
 */

@Parcel
public class UserProfile {
    public String profileType, webUrl, companyAddress, companyName, institutionName, className, email, idCardImageURL;
    public boolean isVerify;

    public UserProfile() {
    }

    public String getProfileType() {
        return profileType;
    }

    public void setProfileType(String profileType) {
        this.profileType = profileType;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getInstitutionName() {
        return institutionName;
    }

    public void setInstitutionName(String institutionName) {
        this.institutionName = institutionName;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isVerify() {
        return isVerify;
    }

    public void setVerify(boolean verify) {
        isVerify = verify;
    }

    public String getIdCardImageURL() {
        return idCardImageURL;
    }

    public void setIdCardImageURL(String idCardImageURL) {
        this.idCardImageURL = idCardImageURL;
    }
}
