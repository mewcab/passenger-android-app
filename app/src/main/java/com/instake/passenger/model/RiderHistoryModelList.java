package com.instake.passenger.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by mahmudul.hasan on 5/31/2017.
 */

public class RiderHistoryModelList {
    @SerializedName("trips")
    private ArrayList<RiderHistoryModel> trips;

    public ArrayList<RiderHistoryModel> getTrips() {
        return trips;
    }

    public void setTrips(ArrayList<RiderHistoryModel> trips) {
        this.trips = trips;
    }
}
