
package com.instake.passenger.model.historydata;

import android.os.Parcel;
import android.os.Parcelable;
import android.os.Parcelable.Creator;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleId implements Parcelable
{

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_type_name")
    @Expose
    private String vehicleTypeName;
    public final static Parcelable.Creator<VehicleId> CREATOR = new Creator<VehicleId>() {


        @SuppressWarnings({
            "unchecked"
        })
        public VehicleId createFromParcel(Parcel in) {
            VehicleId instance = new VehicleId();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.vehicleType = ((String) in.readValue((String.class.getClassLoader())));
            instance.vehicleTypeName = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public VehicleId[] newArray(int size) {
            return (new VehicleId[size]);
        }

    }
    ;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(vehicleType);
        dest.writeValue(vehicleTypeName);
    }

    public int describeContents() {
        return  0;
    }

}
