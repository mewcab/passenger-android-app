package com.instake.passenger.model;

/**
 * Created by mahmudul.hasan on 5/7/2017.
 */

public class HelpAndSupportModel {

    String id;
    String title;
    String detail;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }
}
