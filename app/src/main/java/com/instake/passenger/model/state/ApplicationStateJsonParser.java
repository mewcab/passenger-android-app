package com.instake.passenger.model.state;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.instake.passenger.model.nearestdriver.NearestDriverResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by masum on 3/8/17.
 */

public class ApplicationStateJsonParser {
    private static final String TAG = "ApplicationState";
    private Gson gson;

    public ApplicationStateJsonParser(Gson gson) {
        this.gson = gson;
    }

    public Response getStateInfo(String responseString) {

        Response response = null;
        try {
            JSONObject jsonObject = new JSONObject(responseString);
            JSONObject jsonObjectDriver = jsonObject.getJSONObject("driver_id");

            Object objectAvatar = jsonObjectDriver.get("profile_images");
            if (objectAvatar instanceof String) {
                jsonObjectDriver.put("profile_images", avatar(jsonObjectDriver));

            }

            JSONObject jsonObjectPassenger = jsonObject.getJSONObject("passenger_id");

            if (jsonObjectPassenger.has("profile_images")) {
                Object objectProfileImage = jsonObjectPassenger.get("profile_images");
                if (objectProfileImage instanceof String) {
                    jsonObjectPassenger.put("profile_images", avatar(jsonObjectPassenger));
                }
            } else {
                jsonObjectPassenger.put("profile_images", avatarEmpty(jsonObjectPassenger));
            }

            Log.w(TAG, "new json state: " + jsonObject.toString());
            response = gson.fromJson(jsonObject.toString(), Response.class);

        } catch (Exception e) {
            Log.e(TAG, "Error parsing : " + e.getMessage());
        }

        return response;

    }

    @NonNull
    private JSONObject avatar(JSONObject jsonObjectDriver) throws JSONException {
        JSONObject av = new JSONObject();
        av.put("300X300", jsonObjectDriver.getString("profile_images"));
        av.put("250X360", jsonObjectDriver.getString("profile_images"));
        av.put("150X150", jsonObjectDriver.getString("profile_images"));
        av.put("50X50", jsonObjectDriver.getString("profile_images"));
        av.put("42X60", jsonObjectDriver.getString("profile_images"));
        return av;
    }

    @NonNull
    private JSONObject avatarEmpty(JSONObject jsonObjectDriver) throws JSONException {
        JSONObject av = new JSONObject();
        av.put("300X300", "");
        av.put("250X360", "");
        av.put("150X150", "");
        av.put("50X50", "");
        av.put("42X60", "");
        return av;
    }


}
