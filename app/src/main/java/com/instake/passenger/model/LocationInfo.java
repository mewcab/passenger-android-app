package com.instake.passenger.model;

/**
 * Created by masum on 3/1/17.
 */

public class LocationInfo {
    private String mCity;
    private String mAddress;
    private String mCountry;

    public String getmCity() {
        return mCity;
    }

    public void setmCity(String mCity) {
        this.mCity = mCity;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

}
