
package com.instake.passenger.model.nearestdriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleInfo {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("seats")
    @Expose
    private Integer seats;
    @SerializedName("per_minit")
    @Expose
    private Integer perMinit;
    @SerializedName("price")
    @Expose
    private Integer price;
    @SerializedName("killometre")
    @Expose
    private Integer killometre;
    @SerializedName("base_fare")
    @Expose
    private Integer baseFare;
    @SerializedName("image")
    @Expose
    private Image image;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Integer getPerMinit() {
        return perMinit;
    }

    public void setPerMinit(Integer perMinit) {
        this.perMinit = perMinit;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public Integer getKillometre() {
        return killometre;
    }

    public void setKillometre(Integer killometre) {
        this.killometre = killometre;
    }

    public Integer getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(Integer baseFare) {
        this.baseFare = baseFare;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

}
