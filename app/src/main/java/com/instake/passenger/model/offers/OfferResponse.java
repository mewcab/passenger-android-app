
package com.instake.passenger.model.offers;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfferResponse {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("image_position")
    @Expose
    private String imagePosition;
    @SerializedName("offer_type")
    @Expose
    private String offerType;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("coupon_code")
    @Expose
    private List<String> couponCode = null;
    @SerializedName("num_of_ride")
    @Expose
    private Integer numOfRide;
    @SerializedName("valid_till")
    @Expose
    private ValidTill validTill;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("discount_amount")
    @Expose
    private Integer discountAmount;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImagePosition() {
        return imagePosition;
    }

    public void setImagePosition(String imagePosition) {
        this.imagePosition = imagePosition;
    }

    public String getOfferType() {
        return offerType;
    }

    public void setOfferType(String offerType) {
        this.offerType = offerType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<String> getCouponCode() {
        return couponCode;
    }

    public void setCouponCode(List<String> couponCode) {
        this.couponCode = couponCode;
    }

    public Integer getNumOfRide() {
        return numOfRide;
    }

    public void setNumOfRide(Integer numOfRide) {
        this.numOfRide = numOfRide;
    }

    public ValidTill getValidTill() {
        return validTill;
    }

    public void setValidTill(ValidTill validTill) {
        this.validTill = validTill;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getDiscountAmount() {
        return discountAmount;
    }

    public void setDiscountAmount(Integer discountAmount) {
        this.discountAmount = discountAmount;
    }

}
