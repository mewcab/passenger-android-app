
package com.instake.passenger.model.suppotnhelp;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Help implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("is_complain")
    @Expose
    private Boolean isComplain;
    public final static Creator<Help> CREATOR = new Creator<Help>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Help createFromParcel(Parcel in) {
            Help instance = new Help();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            instance.content = ((String) in.readValue((String.class.getClassLoader())));
            instance.isComplain = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            return instance;
        }

        public Help[] newArray(int size) {
            return (new Help[size]);
        }

    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Boolean getIsComplain() {
        return isComplain;
    }

    public void setIsComplain(Boolean isComplain) {
        this.isComplain = isComplain;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(title);
        dest.writeValue(content);
        dest.writeValue(isComplain);
    }

    public int describeContents() {
        return 0;
    }

}
