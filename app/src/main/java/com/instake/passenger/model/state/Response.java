
package com.instake.passenger.model.state;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("coordinates")
    @Expose
    private List<List<Double>> coordinates = null;
    @SerializedName("coordinates_date")
    @Expose
    private List<String> coordinatesDate = null;
    @SerializedName("departure_location")
    @Expose
    private List<Double> departureLocation = null;
    @SerializedName("destination_location")
    @Expose
    private List<Double> destinationLocation = null;
    @SerializedName("departure_time")
    @Expose
    private String departureTime;
    @SerializedName("driver_id")
    @Expose
    private DriverId driverId;
    @SerializedName("passenger_id")
    @Expose
    private PassengerId passengerId;
    @SerializedName("departure_location_name")
    @Expose
    private String departureLocationName;
    @SerializedName("destination_location_name")
    @Expose
    private String destinationLocationName;
    @SerializedName("start_time")
    @Expose
    private String startTime;
    @SerializedName("rating_user")
    @Expose
    private float ratingUser;
    @SerializedName("rating_driver")
    @Expose
    private float ratingDriver;
    @SerializedName("base_fare_rate")
    @Expose
    private Integer baseFareRate;
    @SerializedName("distance")
    @Expose
    private Distance distance;
    @SerializedName("total_time")
    @Expose
    private float totalTime;
    @SerializedName("total_fear")
    @Expose
    private float totalFear;
    @SerializedName("discount")
    @Expose
    private float discount;
    @SerializedName("sub_total")
    @Expose
    private float subTotal;
    @SerializedName("instake_charge")
    @Expose
    private Integer instakeCharge;
    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("fare_details")
    @Expose
    private FareDetails fareDetails;

    public Response(String id, List<List<Double>> coordinates, List<String> coordinatesDate,
                    List<Double> departureLocation, List<Double> destinationLocation,
                    String departureTime, DriverId driverId, PassengerId passengerId,
                    String departureLocationName, String destinationLocationName,
                    String startTime, float ratingUser, float ratingDriver,
                    Integer baseFareRate, Distance distance, float totalTime, float totalFear,
                    float discount, float subTotal, Integer instakeCharge, String status) {

        this.id = id;
        this.coordinates = coordinates;
        this.coordinatesDate = coordinatesDate;
        this.departureLocation = departureLocation;
        this.destinationLocation = destinationLocation;
        this.departureTime = departureTime;
        this.driverId = driverId;
        this.passengerId = passengerId;
        this.departureLocationName = departureLocationName;
        this.destinationLocationName = destinationLocationName;
        this.startTime = startTime;
        this.ratingUser = ratingUser;
        this.ratingDriver = ratingDriver;
        this.baseFareRate = baseFareRate;
        this.distance = distance;
        this.totalTime = totalTime;
        this.totalFear = totalFear;
        this.discount = discount;
        this.subTotal = subTotal;
        this.instakeCharge = instakeCharge;
        this.status = status;
    }

    public FareDetails getFareDetails() {
        return fareDetails;
    }

    public void setFareDetails(FareDetails fareDetails) {
        this.fareDetails = fareDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<List<Double>> getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(List<List<Double>> coordinates) {
        this.coordinates = coordinates;
    }

    public List<String> getCoordinatesDate() {
        return coordinatesDate;
    }

    public void setCoordinatesDate(List<String> coordinatesDate) {
        this.coordinatesDate = coordinatesDate;
    }

    public List<Double> getDepartureLocation() {
        return departureLocation;
    }

    public void setDepartureLocation(List<Double> departureLocation) {
        this.departureLocation = departureLocation;
    }

    public List<Double> getDestinationLocation() {
        return destinationLocation;
    }

    public void setDestinationLocation(List<Double> destinationLocation) {
        this.destinationLocation = destinationLocation;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }

    public DriverId getDriverId() {
        return driverId;
    }

    public void setDriverId(DriverId driverId) {
        this.driverId = driverId;
    }

    public PassengerId getPassengerId() {
        return passengerId;
    }

    public void setPassengerId(PassengerId passengerId) {
        this.passengerId = passengerId;
    }

    public String getDepartureLocationName() {
        return departureLocationName;
    }

    public void setDepartureLocationName(String departureLocationName) {
        this.departureLocationName = departureLocationName;
    }

    public String getDestinationLocationName() {
        return destinationLocationName;
    }

    public void setDestinationLocationName(String destinationLocationName) {
        this.destinationLocationName = destinationLocationName;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public float getRatingUser() {
        return ratingUser;
    }

    public void setRatingUser(float ratingUser) {
        this.ratingUser = ratingUser;
    }

    public float getRatingDriver() {
        return ratingDriver;
    }

    public void setRatingDriver(float ratingDriver) {
        this.ratingDriver = ratingDriver;
    }

    public Integer getBaseFareRate() {
        return baseFareRate;
    }

    public void setBaseFareRate(Integer baseFareRate) {
        this.baseFareRate = baseFareRate;
    }

    public Distance getDistance() {
        return distance;
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public float getTotalTime() {
        return totalTime;
    }

    public void setTotalTime(float totalTime) {
        this.totalTime = totalTime;
    }

    public float getTotalFare() {
        return totalFear;
    }

    public void setTotalFear(float totalFear) {
        this.totalFear = totalFear;
    }

    public float getDiscount() {
        return discount;
    }

    public void setDiscount(float discount) {
        this.discount = discount;
    }

    public float getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(float subTotal) {
        this.subTotal = subTotal;
    }

    public Integer getInstakeCharge() {
        return instakeCharge;
    }

    public void setInstakeCharge(Integer instakeCharge) {
        this.instakeCharge = instakeCharge;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

}
