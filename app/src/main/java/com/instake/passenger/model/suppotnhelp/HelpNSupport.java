
package com.instake.passenger.model.suppotnhelp;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class HelpNSupport implements Parcelable {

    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("help")
    @Expose
    private List<Help> help = new ArrayList<>();
    public final static Creator<HelpNSupport> CREATOR = new Creator<HelpNSupport>() {


        @SuppressWarnings({
                "unchecked"
        })
        public HelpNSupport createFromParcel(Parcel in) {
            HelpNSupport instance = new HelpNSupport();
            instance.title = ((String) in.readValue((String.class.getClassLoader())));
            in.readList(instance.help, (Help.class.getClassLoader()));
            return instance;
        }

        public HelpNSupport[] newArray(int size) {
            return (new HelpNSupport[size]);
        }

    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public List<Help> getHelp() {
        return help;
    }

    public void setHelp(List<Help> help) {
        this.help = help;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(title);
        dest.writeList(help);
    }

    public int describeContents() {
        return 0;
    }

}
