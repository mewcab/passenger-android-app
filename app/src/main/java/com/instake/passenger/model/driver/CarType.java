
package com.instake.passenger.model.driver;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CarType implements Parcelable {

    @SerializedName("_id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("seats")
    @Expose
    private Integer seats;
    @SerializedName("fare_cost")
    @Expose
    private Double fareCost;
    @SerializedName("per_minit")
    @Expose
    private Double perMinit;
    @SerializedName("price")
    @Expose
    private Double price;
    @SerializedName("killometre")
    @Expose
    private Double killometre;
    @SerializedName("is_active")
    @Expose
    private Boolean isActive;
    @SerializedName("__v")
    @Expose
    private Integer v;
    @SerializedName("image")
    @Expose
    private Image image;
    @SerializedName("bese_fare")
    @Expose
    private Double beseFare;
    @SerializedName("base_fare")
    @Expose
    private Double baseFare;
    @SerializedName("vehicle_type")
    @Expose
    private String vehicleType;
    @SerializedName("vehicle_type_name")
    @Expose
    private String vehicleTypeName;
    @SerializedName("updated_at")
    @Expose
    private String updatedAt;
    @SerializedName("created_at")
    @Expose
    private String createdAt;

    public final static Parcelable.Creator<CarType> CREATOR = new Creator<CarType>() {


        @SuppressWarnings({
                "unchecked"
        })
        public CarType createFromParcel(Parcel in) {
            CarType instance = new CarType();
            instance.id = ((String) in.readValue((String.class.getClassLoader())));
            instance.name = ((String) in.readValue((String.class.getClassLoader())));
            instance.seats = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.fareCost = ((Double) in.readValue((Integer.class.getClassLoader())));
            instance.perMinit = ((Double) in.readValue((Integer.class.getClassLoader())));
            instance.price = ((Double) in.readValue((Integer.class.getClassLoader())));
            instance.killometre = ((Double) in.readValue((Integer.class.getClassLoader())));
            instance.isActive = ((Boolean) in.readValue((Boolean.class.getClassLoader())));
            instance.v = ((Integer) in.readValue((Integer.class.getClassLoader())));
            instance.image = ((Image) in.readValue((Image.class.getClassLoader())));
            instance.beseFare = ((Double) in.readValue((Integer.class.getClassLoader())));
            instance.baseFare = ((Double) in.readValue((Integer.class.getClassLoader())));
            instance.vehicleType = ((String) in.readValue((String.class.getClassLoader())));
            instance.vehicleTypeName = ((String) in.readValue((String.class.getClassLoader())));
            instance.updatedAt = ((String) in.readValue((String.class.getClassLoader())));
            instance.createdAt = ((String) in.readValue((String.class.getClassLoader())));
            return instance;
        }

        public CarType[] newArray(int size) {
            return (new CarType[size]);
        }

    };

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getSeats() {
        return seats;
    }

    public void setSeats(Integer seats) {
        this.seats = seats;
    }

    public Double getFareCost() {
        return fareCost;
    }

    public void setFareCost(Double fareCost) {
        this.fareCost = fareCost;
    }

    public Double getPerMinit() {
        return perMinit;
    }

    public void setPerMinit(Double perMinit) {
        this.perMinit = perMinit;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getKillometre() {
        return killometre;
    }

    public void setKillometre(Double killometre) {
        this.killometre = killometre;
    }

    public Boolean getIsActive() {
        return isActive;
    }

    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    public Integer getV() {
        return v;
    }

    public void setV(Integer v) {
        this.v = v;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public Double getBeseFare() {
        return beseFare;
    }

    public void setBeseFare(Double beseFare) {
        this.beseFare = beseFare;
    }

    public Double getBaseFare() {
        return baseFare;
    }

    public void setBaseFare(Double baseFare) {
        this.baseFare = baseFare;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }

    public String getVehicleTypeName() {
        return vehicleTypeName;
    }

    public void setVehicleTypeName(String vehicleTypeName) {
        this.vehicleTypeName = vehicleTypeName;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(id);
        dest.writeValue(name);
        dest.writeValue(seats);
        dest.writeValue(fareCost);
        dest.writeValue(perMinit);
        dest.writeValue(price);
        dest.writeValue(killometre);
        dest.writeValue(isActive);
        dest.writeValue(v);
        dest.writeValue(image);
        dest.writeValue(beseFare);
        dest.writeValue(baseFare);
        dest.writeValue(vehicleType);
        dest.writeValue(vehicleTypeName);
        dest.writeValue(updatedAt);
        dest.writeValue(createdAt);
    }

    public int describeContents() {
        return 0;
    }

}