
package com.instake.passenger.model.nearestdriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AcceptedTripResponse extends DriverList {

    @SerializedName("address_pickup")
    @Expose
    public AddressPickup addressPickup;

    @SerializedName("address_dropoff")
    @Expose
    public AddressDropoff addressDropoff;

    public AcceptedTripResponse () {

    }

    public AddressPickup getAddressPickup() {
        return addressPickup;
    }

    public void setAddressPickup(AddressPickup addressPickup) {
        this.addressPickup = addressPickup;
    }

    public AddressDropoff getAddressDropoff() {
        return addressDropoff;
    }

    public void setAddressDropoff(AddressDropoff addressDropoff) {
        this.addressDropoff = addressDropoff;
    }

}
