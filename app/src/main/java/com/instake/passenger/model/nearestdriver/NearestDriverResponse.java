
package com.instake.passenger.model.nearestdriver;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NearestDriverResponse {

    @SerializedName("passenger")
    @Expose
    private Passenger passenger;

    @SerializedName("driver_list")
    @Expose
    private List<DriverList> drivers = null;

    public Passenger getPhoneNo() {
        return passenger;
    }

    public void setPhoneNo(Passenger phoneNo) {
        this.passenger = phoneNo;
    }

    public List<DriverList> getDriverList() {
        return drivers;
    }

    public void setDrivers(List<DriverList> drivers) {
        this.drivers = drivers;
    }

    public boolean isEmptyDriverList() {
        return getDriverList().size() == 0;
    }


}
