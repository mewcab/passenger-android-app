package com.instake.passenger.dipendency.onboard;

import com.instake.passenger.feature.onboard.OnBoardingActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Tushar on 5/12/17.
 */

@Singleton
@Subcomponent(modules = OnBoardingModule.class)
public interface OnBoardingComponent {
    void inject(OnBoardingActivity activity);
}
