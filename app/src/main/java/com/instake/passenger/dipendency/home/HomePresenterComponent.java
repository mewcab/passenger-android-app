package com.instake.passenger.dipendency.home;

import com.instake.passenger.feature.home.HomePresenter;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by masum on 5/24/17.
 */
@Singleton
@Component(modules = {HomePresenterModule.class})
public interface HomePresenterComponent {
    void inject(HomePresenter homePresenter);
}
