package com.instake.passenger.dipendency.signup.confirmaccount;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.feature.signup.confirmaccount.ConfirmAccountPresenter;
import com.instake.passenger.feature.signup.confirmaccount.ConfirmAccountView;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ConfirmAccountModule {

    ConfirmAccountView view;

    public ConfirmAccountModule(ConfirmAccountView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public ConfirmAccountPresenter provideRequestDriverPresenter(Validator validator, ApiService apiService) {
        return new ConfirmAccountPresenter(view, validator, apiService);
    }

}