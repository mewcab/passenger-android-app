package com.instake.passenger.dipendency.requestdriver;

import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.requestdriver.RequestDriverPresenter;
import com.instake.passenger.feature.requestdriver.RequestDriverView;
import com.instake.passenger.feature.requestdriver.TimeCounterImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class RequestDriverModule {

    RequestDriverView view;

    public RequestDriverModule(RequestDriverView view) {
        this.view = view;
    }

    @Provides
    public TimeCounterImpl provideTimer() {
        return new TimeCounterImpl();
    }

    @Provides
    @Singleton
    public RequestDriverPresenter provideRequestDriverPresenter(TimeCounterImpl timeCounterImpl, Session session) {
        return new RequestDriverPresenter(view, timeCounterImpl, session);
    }

}