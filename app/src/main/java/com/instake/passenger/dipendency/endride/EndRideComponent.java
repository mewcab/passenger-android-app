package com.instake.passenger.dipendency.endride;

import com.instake.passenger.feature.endreview.EndRideReviewActivity;
import com.instake.passenger.feature.endride.EndRideActivity;
import com.instake.passenger.feature.home.HomeFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by masum on 5/24/17.
 */
@Singleton
@Subcomponent(modules = EndRideModule.class)
public interface EndRideComponent {
    void inject(EndRideActivity activity);
    void inject(EndRideReviewActivity activity);
}
