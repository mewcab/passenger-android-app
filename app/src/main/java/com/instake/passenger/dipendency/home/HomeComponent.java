package com.instake.passenger.dipendency.home;

import com.instake.passenger.feature.home.HomeFragment;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by masum on 5/24/17.
 */
@Singleton
@Subcomponent(modules = HomeModule.class)
public interface HomeComponent {
    void inject(HomeFragment fragment);
}
