package com.instake.passenger.dipendency.splash;

import com.instake.passenger.feature.onboard.OnBoardingActivity;
import com.instake.passenger.feature.splash.SplashActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Tushar on 5/12/17.
 */

@Singleton
@Subcomponent(modules = SplashModule.class)
public interface SplashComponent {
    void inject(SplashActivity activity);
}
