package com.instake.passenger.dipendency.endride;

import com.instake.passenger.Rx.RxService;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.endride.EndRideContract;
import com.instake.passenger.feature.endride.EndRidePresenterImpl;
import com.instake.passenger.feature.home.HomePresenter;
import com.instake.passenger.feature.home.HomeView;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by masum on 5/24/17.
 */
@Module
public class EndRideModule {
    EndRideContract.EndRideView view;
    File cacheFile;

    public EndRideModule(EndRideContract.EndRideView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public ApiService providesNetworkService() {
        return App.getInstance().getApiServiceWithCaching(cacheFile);
    }

    @Provides
    @Singleton
    public RxService provideUser(ApiService apiService, Session sessoin) {
        return new RxService(apiService, sessoin);
    }

    @Provides
    @Singleton
    public EndRidePresenterImpl provideHomePresenter(Session session) {
        return new EndRidePresenterImpl(view, session);
    }
}
