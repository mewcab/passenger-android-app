package com.instake.passenger.dipendency.onboard;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.onboard.OnBoardingPresenter;
import com.instake.passenger.feature.onboard.OnBoardingView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tushar on 5/12/17.
 */

@Module
public class OnBoardingModule {

    OnBoardingView view;

    public OnBoardingModule(OnBoardingView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public OnBoardingPresenter provideOnBoardingPresenter(ApiService apiService, Session session) {
        return new OnBoardingPresenter(view, apiService, session);
    }

}
