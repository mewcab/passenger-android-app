package com.instake.passenger.dipendency.Login;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.login.LoginPresenter;
import com.instake.passenger.feature.login.LoginView;
import com.instake.passenger.model.User;
import com.instake.passenger.utility.validation.Validator;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tushar on 5/21/17.
 */

@Module
public class LoginModule {

    LoginView view;

    public LoginModule(LoginView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public LoginPresenter provideLoginPresenter(ApiService apiService, Session session, Validator validator) {
        return new LoginPresenter(view, apiService, session, validator);
    }
}
