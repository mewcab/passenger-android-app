package com.instake.passenger.dipendency.home;

import com.google.gson.Gson;
import com.instake.passenger.model.driver.Driver;
import com.instake.passenger.model.payment.Fare;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by masum on 5/24/17.
 */
@Module
public class HomePresenterModule {

    @Provides
    @SuppressWarnings("unused")
    public Fare provideFare() {
        return new Fare();
    }

    @Provides
    @SuppressWarnings("unused")
    public CompositeSubscription provideCompositeSubscription() {
        return new CompositeSubscription();
    }

    @Provides
    @Singleton
    public Driver provideDriver() {
        return new Driver(new Gson());
    }


}
