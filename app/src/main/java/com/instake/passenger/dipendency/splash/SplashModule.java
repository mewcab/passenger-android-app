package com.instake.passenger.dipendency.splash;

import com.instake.passenger.feature.splash.SplashActivityPresenter;
import com.instake.passenger.feature.splash.SplashView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tushar on 5/12/17.
 */

@Module
public class SplashModule {

    SplashView view;

    public SplashModule(SplashView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public SplashActivityPresenter provideOnBoardingPresenter() {
        return new SplashActivityPresenter(view);
    }

}
