package com.instake.passenger.dipendency.signup.confirmaccount;

import com.instake.passenger.feature.signup.confirmaccount.ConfirmAccountActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Tushar on 5/11/17.
 */
@Singleton
@Subcomponent(modules = ConfirmAccountModule.class)
public interface ConfirmAccountComponent {
    void inject(ConfirmAccountActivity activity);
}
