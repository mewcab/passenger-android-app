package com.instake.passenger.dipendency.offer;

import com.instake.passenger.feature.offer.OfferRideActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by GoInstake on 10/21/2017.
 */

@Singleton
@Subcomponent(modules = OfferModule.class)
public interface OfferComponent {
    void inject(OfferRideActivity offerRideActivity);
}
