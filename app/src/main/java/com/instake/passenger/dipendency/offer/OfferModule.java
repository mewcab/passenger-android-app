package com.instake.passenger.dipendency.offer;

import com.instake.passenger.Rx.RxService;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.endride.EndRideContract;
import com.instake.passenger.feature.endride.EndRidePresenterImpl;
import com.instake.passenger.feature.offer.OfferRidePresenter;
import com.instake.passenger.feature.offer.OfferRideView;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by masum on 5/24/17.
 */
@Module
public class OfferModule {
    OfferRideView view;
    File cacheFile;

    public OfferModule(OfferRideView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public ApiService providesNetworkService() {
        return App.getInstance().getApiServiceWithCaching(cacheFile);
    }

    @Provides
    @Singleton
    public RxService provideUser(Session sessoin) {
        return new RxService(sessoin);
    }

    @Provides
    @Singleton
    public OfferRidePresenter provideOfferRidePresenter(Session session, RxService rxService) {
        return new OfferRidePresenter(view, session, rxService);
    }
}
