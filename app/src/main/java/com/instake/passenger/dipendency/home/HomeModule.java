package com.instake.passenger.dipendency.home;

import com.instake.passenger.Rx.RxService;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.home.HomePresenter;
import com.instake.passenger.feature.home.HomeView;

import java.io.File;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by masum on 5/24/17.
 */
@Module
public class HomeModule {
    HomeView view;
    File cacheFile;

    public HomeModule(HomeView view, File cacheFile) {
        this.view = view;
        this.cacheFile = cacheFile;
    }

    @Provides
    @Singleton
    public ApiService providesNetworkService() {
        return App.getInstance().getApiServiceWithCaching(cacheFile);
    }

    @Provides
    @Singleton
    public RxService provideUser(ApiService apiService, Session sessoin) {
        return new RxService(apiService, sessoin);
    }

    @Provides
    @Singleton
    public HomePresenter provideHomePresenter(Session session) {
        return new HomePresenter(view, session);
    }
}
