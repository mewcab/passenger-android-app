package com.instake.passenger.dipendency.Login;

import com.instake.passenger.feature.login.LoginActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Tushar on 5/21/17.
 */

@Singleton
@Subcomponent(modules = LoginModule.class)
public interface LoginComponent {
    void inject(LoginActivity activity);
}
