package com.instake.passenger.dipendency;

import android.app.Application;

import com.google.gson.Gson;
import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.App;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.common.session.SharedPreferenceSessionImpl;
import com.instake.passenger.utility.validation.Validator;
import com.instake.passenger.utility.validation.ValidatorImpl;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Tushar on 5/11/17.
 */

@Module
public class AppModule {

    Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    App provideApp() {
        return (App) application;
    }

    @Provides
    @Singleton
    ApiService provideApiService(App app) {
        return app.getApiService();
    }

    @Provides
    @Singleton
    Validator provideValidator() {
        return new ValidatorImpl();
    }

    @Provides
    @Singleton
    Session provideSession(App app, Validator validator) {
        return new SharedPreferenceSessionImpl(app.getSharedPreferenceManager(),
                new Gson(), validator);
    }
}
