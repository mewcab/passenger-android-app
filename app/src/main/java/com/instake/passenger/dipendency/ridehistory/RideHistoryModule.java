package com.instake.passenger.dipendency.ridehistory;

import com.instake.passenger.api.ApiService;
import com.instake.passenger.common.session.Session;
import com.instake.passenger.feature.login.LoginPresenter;
import com.instake.passenger.feature.ridehistory.RideHistoryPresenter;
import com.instake.passenger.feature.ridehistory.RideHistoryView;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Created by mahmudul.hasan on 5/29/2017.
 */

@Module
public class RideHistoryModule {
    RideHistoryView view;


    public RideHistoryModule(RideHistoryView view) {
        this.view = view;
    }

    @Provides
    @Singleton
    public RideHistoryPresenter provideLoginPresenter(ApiService apiService,Session session) {
        return new RideHistoryPresenter(view, session);
    }
}
