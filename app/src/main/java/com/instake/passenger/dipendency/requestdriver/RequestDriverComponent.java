package com.instake.passenger.dipendency.requestdriver;

import com.instake.passenger.feature.requestdriver.RequestDriverActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by Tushar on 5/11/17.
 */
@Singleton
@Subcomponent(modules = RequestDriverModule.class)
public interface RequestDriverComponent {
    void inject(RequestDriverActivity activity);
}
