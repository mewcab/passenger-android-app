package com.instake.passenger.dipendency;

import com.instake.passenger.dipendency.Login.LoginComponent;
import com.instake.passenger.dipendency.Login.LoginModule;
import com.instake.passenger.dipendency.endride.EndRideComponent;
import com.instake.passenger.dipendency.endride.EndRideModule;
import com.instake.passenger.dipendency.home.HomeComponent;
import com.instake.passenger.dipendency.home.HomeModule;
import com.instake.passenger.dipendency.offer.OfferComponent;
import com.instake.passenger.dipendency.offer.OfferModule;
import com.instake.passenger.dipendency.onboard.OnBoardingComponent;
import com.instake.passenger.dipendency.onboard.OnBoardingModule;
import com.instake.passenger.dipendency.requestdriver.RequestDriverComponent;
import com.instake.passenger.dipendency.requestdriver.RequestDriverModule;
import com.instake.passenger.dipendency.ridehistory.RideHistoryComponent;
import com.instake.passenger.dipendency.ridehistory.RideHistoryModule;
import com.instake.passenger.dipendency.signup.confirmaccount.ConfirmAccountComponent;
import com.instake.passenger.dipendency.signup.confirmaccount.ConfirmAccountModule;
import com.instake.passenger.dipendency.splash.SplashComponent;
import com.instake.passenger.dipendency.splash.SplashModule;
import com.instake.passenger.feature.editprofile.EditProfileComponent;
import com.instake.passenger.feature.editprofile.EditProfileModule;
import com.instake.passenger.feature.home.HomeActivity;
import com.instake.passenger.feature.vehicledetails.VehicleDetailsActivity;
import com.instake.passenger.monowar.ambassadorprofile.a.ProvideAmbassadorEmailActivity;
import com.instake.passenger.monowar.ambassadorprofile.b.AmbassadorProfileInfoUploadComponent;
import com.instake.passenger.monowar.ambassadorprofile.b.AmbassadorProfileInfoUploadModule;
import com.instake.passenger.monowar.businessprofile.a.ProvideBusinessEmailActivity;
import com.instake.passenger.monowar.businessprofile.b.ProvideCompanyInfoActivity;
import com.instake.passenger.monowar.businessprofile.c.BusinessProfileInfoUploadComponent;
import com.instake.passenger.monowar.businessprofile.c.BusinessProfileInfoUploadModule;
import com.instake.passenger.monowar.invitefriends.InviteFriendsComponent;
import com.instake.passenger.monowar.invitefriends.InviteFriendsModule;
import com.instake.passenger.monowar.mobilenumbervalidation.a.SignupGetStartedComponent;
import com.instake.passenger.monowar.mobilenumbervalidation.a.SignupGetStartedModule;
import com.instake.passenger.monowar.mobilenumbervalidation.b.SignupOtpComponent;
import com.instake.passenger.monowar.mobilenumbervalidation.b.SignupOtpModule;
import com.instake.passenger.monowar.profile.ProfileComponent;
import com.instake.passenger.monowar.profile.ProfileModule;
import com.instake.passenger.monowar.profile.switchprofile.SwitchProfileFragment;
import com.instake.passenger.monowar.profileonboard.UserProfileOnBoardingComponent;
import com.instake.passenger.monowar.profileonboard.UserProfileOnBoardingModule;
import com.instake.passenger.monowar.signup.a.SignupCompleteProfileComponent;
import com.instake.passenger.monowar.signup.a.SignupCompleteProfileModule;
import com.instake.passenger.monowar.signup.b.SignupProvideInformationComponent;
import com.instake.passenger.monowar.signup.b.SignupProvideInformationModule;
import com.instake.passenger.monowar.signup.c.SignupEmailComponent;
import com.instake.passenger.monowar.signup.c.SignupEmailModule;
import com.instake.passenger.monowar.signup.d.SignupBirthdayComponent;
import com.instake.passenger.monowar.signup.d.SignupBirthdayModule;
import com.instake.passenger.monowar.signup.e.SignupPasswordComponent;
import com.instake.passenger.monowar.signup.e.SignupPasswordModule;
import com.instake.passenger.monowar.studentprofile.a.StudentProfileInfoComponent;
import com.instake.passenger.monowar.studentprofile.a.StudentProfileInfoModule;
import com.instake.passenger.monowar.studentprofile.b.StudentProfileInfoUploadComponent;
import com.instake.passenger.monowar.studentprofile.b.StudentProfileInfoUploadModule;
import com.instake.passenger.monowar.supportandhelp.a.SupportAndHelpComponent;
import com.instake.passenger.monowar.supportandhelp.a.SupportAndHelpModule;
import com.instake.passenger.monowar.supportandhelp.a.fragment.faqs.FaqsComponent;
import com.instake.passenger.monowar.supportandhelp.a.fragment.faqs.FaqsModule;
import com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp.TripsHelpComponent;
import com.instake.passenger.monowar.supportandhelp.a.fragment.tripshelp.TripsHelpModule;
import com.instake.passenger.monowar.supportandhelp.b.FaqsDetailsComponent;
import com.instake.passenger.monowar.supportandhelp.b.FaqsDetailsModule;
import com.instake.passenger.monowar.supportandhelp.b.RequestHelpComponent;
import com.instake.passenger.monowar.supportandhelp.b.RequestHelpModule;
import com.instake.passenger.monowar.supportandhelp.c.TripsHelpDetailsComponent;
import com.instake.passenger.monowar.supportandhelp.c.TripsHelpDetailsModule;
import com.instake.passenger.monowar.supportandhelp.c.help.HelpComponent;
import com.instake.passenger.monowar.supportandhelp.c.help.HelpModule;
import com.instake.passenger.monowar.supportandhelp.c.receipt.ReceiptComponent;
import com.instake.passenger.monowar.supportandhelp.c.receipt.ReceiptModule;
import com.instake.passenger.monowar.teacherprofile.a.ProvideTeacherEmailActivity;
import com.instake.passenger.monowar.teacherprofile.b.TeacherProfileInfoComponent;
import com.instake.passenger.monowar.teacherprofile.b.TeacherProfileInfoModule;
import com.instake.passenger.monowar.teacherprofile.c.TeacherProfileInfoUploadComponent;
import com.instake.passenger.monowar.teacherprofile.c.TeacherProfileInfoUploadModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = AppModule.class)
public interface AppComponent {
    RequestDriverComponent plus(RequestDriverModule requestDriverModule);

    OnBoardingComponent plus(OnBoardingModule onBoardingModule);

    SplashComponent plus(SplashModule splashModule);

    ConfirmAccountComponent plus(ConfirmAccountModule confirmAccountModule);

    LoginComponent plus(LoginModule loginModule);

    RideHistoryComponent plus(RideHistoryModule rideHistoryModule);

    HomeComponent plus(HomeModule homeModule);


    SignupGetStartedComponent plus(SignupGetStartedModule signupGetStartedModule);

    SignupOtpComponent plus(SignupOtpModule signupOtpModule);

    SignupCompleteProfileComponent plus(SignupCompleteProfileModule signupCompleteProfileModule);

    SignupProvideInformationComponent plus(SignupProvideInformationModule signupProvideInformationModule);

    SignupEmailComponent plus(SignupEmailModule signupEmailModule);

    SignupBirthdayComponent plus(SignupBirthdayModule signupBirthdayModule);

    SignupPasswordComponent plus(SignupPasswordModule signupPasswordModule);

    SupportAndHelpComponent plus(SupportAndHelpModule supportAndHelpModule);

    FaqsComponent plus(FaqsModule faqsModule);

    TripsHelpComponent plus(TripsHelpModule tripsHelpModule);

    FaqsDetailsComponent plus(FaqsDetailsModule faqsDetailsModule);

    RequestHelpComponent plus(RequestHelpModule requestHelpModule);

    TripsHelpDetailsComponent plus(TripsHelpDetailsModule requestHelpModule);

    HelpComponent plus(HelpModule helpModule);

    ReceiptComponent plus(ReceiptModule receiptModule);

    InviteFriendsComponent plus(InviteFriendsModule inviteFriendsModule);

    EndRideComponent plus(EndRideModule endRideModule);

    void inject(HomeActivity homeActivity);

    ProfileComponent plus(ProfileModule profileModule);

    void inject(SwitchProfileFragment switchProfileFragment);

    StudentProfileInfoComponent plus(StudentProfileInfoModule studentProfileInfoModule);

    UserProfileOnBoardingComponent plus(UserProfileOnBoardingModule userProfileOnBoardingModule);

    EditProfileComponent plus(EditProfileModule editProfileModule);

    StudentProfileInfoUploadComponent plus(StudentProfileInfoUploadModule studentProfileInfoUploadModule);

    void inject(ProvideBusinessEmailActivity provideBusinessEmailActivity);

    void inject(ProvideCompanyInfoActivity provideCompanyInfoActivity);

    BusinessProfileInfoUploadComponent plus(BusinessProfileInfoUploadModule businessProfileInfoUploadModule);

    void inject(ProvideTeacherEmailActivity provideTeacherEmailActivity);

    TeacherProfileInfoComponent plus(TeacherProfileInfoModule teacherProfileInfoModule);

    TeacherProfileInfoUploadComponent plus(TeacherProfileInfoUploadModule teacherProfileInfoUploadModule);

    void inject(ProvideAmbassadorEmailActivity ambassadorEmailActivity);

    AmbassadorProfileInfoUploadComponent plus(AmbassadorProfileInfoUploadModule ambassadorProfileInfoUploadModule);

    void inject(VehicleDetailsActivity vehicleDetailsActivity);

    OfferComponent plus(OfferModule offerModule);
}