package com.instake.passenger.dipendency.ridehistory;

import com.instake.passenger.feature.ridehistory.RideHistoryActivity;

import javax.inject.Singleton;

import dagger.Subcomponent;

/**
 * Created by mahmudul.hasan on 5/29/2017.
 */
@Singleton
@Subcomponent(modules = RideHistoryModule.class)
public interface RideHistoryComponent {
    void inject(RideHistoryActivity rideActivity);
}
