package com.instake.passenger.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;

import java.lang.reflect.Type;

/**
 * Created by Tushar on 5/21/17.
 */

public class LoginDeserializer implements JsonDeserializer<User> {
    @Override
    public User deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();
        User user = new User();

        if (!jsonObject.has("message")) {
            user.token = jsonObject.get("token").getAsString();

            JsonObject subscriberObject = jsonObject.get("subscriber").getAsJsonObject();
            user.isActive = subscriberObject.get("is_active").getAsBoolean();
            user.id = subscriberObject.get("_id").getAsString();
            user.referralCode = subscriberObject.get("referral").getAsString();
            user.phoneNumber = subscriberObject.get("phone_no").getAsString();
            if (subscriberObject.has("facebookId")) {
                user.facebookId = subscriberObject.get("facebookId").getAsString();
            }
            if (subscriberObject.has("email")) {
                user.email = subscriberObject.get("email").getAsString();
            }
            if (subscriberObject.has("full_name")) {
                user.fullName = subscriberObject.get("full_name").getAsString();
            }
            if (subscriberObject.has("date_of_birth")) {
                user.dateOfBirth = subscriberObject.get("date_of_birth").getAsString();
            }
            if (subscriberObject.has("gender")) {
                user.gender = subscriberObject.get("gender").getAsString();
            }
            if (subscriberObject.has("profile_images")) {
                if (subscriberObject.get("profile_images").isJsonObject()) {
                    user.profileImageUrl = subscriberObject.get("profile_images").getAsJsonObject().get("150X150").getAsString();
                } else {
                    user.profileImageUrl = subscriberObject.get("profile_images").getAsString();
                }
            }

            if (subscriberObject.has("user_profile")) {
                JsonObject userProfileObject = subscriberObject.get("user_profile").getAsJsonObject();
                UserProfile userProfile = new UserProfile();
                userProfile.isVerify = userProfileObject.get("is_verify").getAsBoolean();
                if (userProfileObject.has("profile_type")) {
                    userProfile.profileType = userProfileObject.get("profile_type").getAsString();
                    if (userProfileObject.has("web_url")) {
                        userProfile.webUrl = userProfileObject.get("web_url").getAsString();
                    }
                    if (userProfileObject.has("company_name")) {
                        userProfile.companyName = userProfileObject.get("company_name").getAsString();
                    }
                    if (userProfileObject.has("company_name")) {
                        userProfile.companyName = userProfileObject.get("company_name").getAsString();
                    }
                    if (userProfileObject.has("company_address")) {
                        userProfile.companyName = userProfileObject.get("company_address").getAsString();
                    }
                    if (userProfileObject.has("class")) {
                        userProfile.className = userProfileObject.get("class").getAsString();
                    }
                    if (userProfileObject.has("instituation_name")) {
                        userProfile.className = userProfileObject.get("instituation_name").getAsString();
                    }
                    if (userProfileObject.has("email")) {
                        userProfile.email = userProfileObject.get("email").getAsString();
                    }
                }
                user.userProfile = userProfile;
            }
        } else {
            user.message = jsonObject.get("message").getAsString();
        }

        return user;
    }
}
