package com.instake.passenger.api;

import com.google.gson.JsonObject;
import com.instake.passenger.model.Rating;
import com.instake.passenger.model.User;
import com.instake.passenger.model.payment.Tips;
import com.instake.passenger.utility.validation.Validator;

/**
 * Created by Tushar on 5/20/17.
 */

public class JsonBuilder {

    public static String fb_auth_token = "qwer444";

    public static String getSendSmsRequestBody(String phoneNumber) {
        JsonObject sendSmsObject = new JsonObject();
        sendSmsObject.addProperty("phone_no", phoneNumber);
        return sendSmsObject.toString();
    }

    public static String getVerificationCodeRequestBody(String phoneNumber, String verificationCode) {
        JsonObject verificationCodeObject = new JsonObject();
        verificationCodeObject.addProperty("phone_no", phoneNumber);
        verificationCodeObject.addProperty("verify_code", verificationCode);
        return verificationCodeObject.toString();
    }

    public static String createFbSignUpBody(Validator validator, String id, String phoneNumber, String firstName, String lastName, String email,
                                            String gender) {
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("id", id);
        jsonBody.addProperty("phone_no", phoneNumber);
        if (validator.isNotEmpty(firstName)) {
            jsonBody.addProperty("first_name", firstName);
        }
        if (validator.isNotEmpty(lastName)) {
            jsonBody.addProperty("last_name", lastName);
        }
        if (validator.isNotEmpty(email)) {
            jsonBody.addProperty("email", email);
        }
        if (validator.isNotEmpty(gender)) {
            jsonBody.addProperty("gender", gender);
        }
        return jsonBody.toString();
    }

    public static String createSignUpFakeBody(String phoneNumber, String verificationCode) {
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("phone_no", phoneNumber);
        jsonBody.addProperty("full_name", "");
        jsonBody.addProperty("last_name", "");
        jsonBody.addProperty("email", "");
        jsonBody.addProperty("referral_code", "");
        jsonBody.addProperty("verify_code", verificationCode);
        jsonBody.addProperty("gender", "");
        jsonBody.addProperty("date_of_birth", "");

        return jsonBody.toString();
    }

    public static String createSignUpBody(Validator validator, String phoneNumber, String fullName, String email, String referralCode) {
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("phone_no", phoneNumber);
        jsonBody.addProperty("full_name", fullName);
        jsonBody.addProperty("email", email);
        jsonBody.addProperty("phone_no", phoneNumber);
        if (validator.isNotEmpty(referralCode)) {
            jsonBody.addProperty("referral_code", referralCode);
        }
        return jsonBody.toString();
    }

    public static String createSignUpUpdateBody(String phoneNumber, String fbProfileImageUrl, String fbId, String fullName, String email,
                                                String referralCode, String gender, String birthDate, String password) {
        JsonObject jsonBody = new JsonObject();
        setProfileImgeWhenFbProfileImageNotEmpty(fbProfileImageUrl, jsonBody);
        setFbIdWhenFbIdNotEmpty(fbId, jsonBody);
        jsonBody.addProperty("phone_no", phoneNumber);
        jsonBody.addProperty("full_name", fullName);
        jsonBody.addProperty("email", email);
        if (referralCode != null) {
            jsonBody.addProperty("referral_code", referralCode);
        }
        if (gender != null) {
            jsonBody.addProperty("gender", gender);
        }
        if (birthDate != null) {
            jsonBody.addProperty("date_of_birth", birthDate);
        }
        jsonBody.addProperty("password", password);
        jsonBody.addProperty("is_signup_completed", true);
        return jsonBody.toString();
    }

    public static String createEditProfileBody(String phoneNumber, String fbProfileImageUrl, String fullName, String email) {
        JsonObject jsonBody = new JsonObject();
        setProfileImgeWhenFbProfileImageNotEmpty(fbProfileImageUrl, jsonBody);
        jsonBody.addProperty("phone_no", phoneNumber);
        jsonBody.addProperty("full_name", fullName);
        jsonBody.addProperty("email", email);
        jsonBody.addProperty("is_signup_completed", true);
        return jsonBody.toString();
    }

    private static void setProfileImgeWhenFbProfileImageNotEmpty(String fbProfileImageUrl, JsonObject jsonBody) {
        if (fbProfileImageUrl != null && fbProfileImageUrl.length() != 0) {
            jsonBody.addProperty("profile_images", fbProfileImageUrl);
            jsonBody.addProperty("fb_auth_token", fb_auth_token);
        }
    }

    private static void setFbIdWhenFbIdNotEmpty(String fbId, JsonObject jsonBody) {
        if (fbId != null && fbId.length() != 0) {
            jsonBody.addProperty("facebookId", fbId);
        }
    }

    public static String createPostRatingBody(Rating rating) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("rating", rating.getRating());
        jsonObject.addProperty("text", rating.getText());
        return jsonObject.toString();
    }

    public static String createTipsBody(Tips tips) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("text", "EducationDetails rating text");
        jsonObject.addProperty("tips_amount", tips.getTips_amount());
        return jsonObject.toString();
    }

    public static String createLoginBodyWithPassword(String phoneNumber, String password) {
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("phone_no", phoneNumber);
        jsonBody.addProperty("password", password);
        return jsonBody.toString();
    }

    public static String createLoginBodyWithVerificationCode(String phoneNumber, String verificationCode) {
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("phone_no", phoneNumber);
        jsonBody.addProperty("verify_code", verificationCode);
        return jsonBody.toString();
    }

    public static String createLoginBodyWithFbId(String phoneNumber, String facebookId) {
        JsonObject jsonBody = new JsonObject();
        jsonBody.addProperty("phone_no", phoneNumber);
        jsonBody.addProperty("facebookId", facebookId);
        jsonBody.addProperty("fb_auth_token", fb_auth_token);
        return jsonBody.toString();
    }

    public static String createBusinessProfileBody(String email, String companyName, String companyAddress) {
        JsonObject verificationCodeObject = new JsonObject();
        verificationCodeObject.addProperty("email", email);
        verificationCodeObject.addProperty("company_name", companyName);
        verificationCodeObject.addProperty("company_address", companyAddress);
        return verificationCodeObject.toString();
    }

    public static String createStudentProfileBody(String institutionName, String className) {
        JsonObject verificationCodeObject = new JsonObject();
        verificationCodeObject.addProperty("instituation_name", institutionName);
        verificationCodeObject.addProperty("class", className);
        return verificationCodeObject.toString();
    }

    public static String createTeacherProfileBody(String email, String institutionName, String departmentName) {
        JsonObject verificationCodeObject = new JsonObject();
        verificationCodeObject.addProperty("email", email);
        verificationCodeObject.addProperty("instituation_name", institutionName);
        verificationCodeObject.addProperty("department", departmentName);
        return verificationCodeObject.toString();
    }

    public static String createAmbassadorProfileBody(String email) {
        JsonObject verificationCodeObject = new JsonObject();
        verificationCodeObject.addProperty("email", email);
        return verificationCodeObject.toString();
    }

    public static String createComplainBody(String phoneNumber, String complain) {
        JsonObject verificationCodeObject = new JsonObject();
        verificationCodeObject.addProperty("phone_no", phoneNumber);
        verificationCodeObject.addProperty("text", complain);
        return verificationCodeObject.toString();
    }
}
