package com.instake.passenger.api.deserializer;

import android.support.annotation.NonNull;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.instake.passenger.model.RiderHistoryModel;
import com.instake.passenger.model.historydata.HistoryResp;
import com.instake.passenger.model.historydata.ProfileImages;
import com.instake.passenger.model.historydata.Trip;

import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Tushar on 5/21/17.
 */

public class RideHistoryDeserializer implements JsonDeserializer<HistoryResp> {
    private static String TAG = "RideHistoryDeserializer";

    @Override
    public HistoryResp deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        JsonObject jsonObject = json.getAsJsonObject();
        HistoryResp historyResp = new HistoryResp();
        List<Trip> trips = new ArrayList<>();
        Gson gson = new Gson();

        historyResp.setCount(jsonObject.get("count").getAsInt());
        int tripCount = jsonObject.get("trips").getAsJsonArray().size();

        for (int i = 0; tripCount > i; i++) {

            if (isNotProfileImageKeyJsonObject(jsonObject, i)) {

                String avatar = getAvatarUrl(jsonObject, i);

                addProfileImageObjectJsonString(jsonObject, i, avatar);

                Trip trip = gson.fromJson(jsonObject.get("trips").getAsJsonArray().get(i).getAsJsonObject().toString(), Trip.class);
                trips.add(trip);

            } else {
                Trip trip = gson.fromJson(jsonObject.get("trips").getAsJsonArray().get(i).toString(), Trip.class);
                trips.add(trip);
            }

        }

        historyResp.setTrips(trips);

        return historyResp;
    }

    private void addProfileImageObjectJsonString(JsonObject jsonObject, int i, String avatar) {
        jsonObject.get("trips").getAsJsonArray()
                .get(i).getAsJsonObject()
                .get("driver_id").getAsJsonObject()
                .add("profile_images", new Gson()
                        .toJsonTree(addProfileImageObject(avatar)));
    }

    @NonNull
    private ProfileImages addProfileImageObject(String avatar) {
        ProfileImages profileImages = new ProfileImages();
        profileImages.set42X60(avatar);
        profileImages.set50X50(avatar);
        profileImages.set120X175(avatar);
        profileImages.set150X150(avatar);
        profileImages.set250X360(avatar);
        profileImages.set300X300(avatar);
        return profileImages;
    }

    private boolean isNotProfileImageKeyJsonObject(JsonObject jsonObject, int i) {
        return !jsonObject.get("trips").getAsJsonArray().get(i).
                getAsJsonObject().get("driver_id").getAsJsonObject().get("profile_images").isJsonObject();
    }

    private String getAvatarUrl(JsonObject jsonObject, int i) {
        return jsonObject.get("trips").getAsJsonArray().get(i).
                getAsJsonObject().get("driver_id").getAsJsonObject().get("profile_images").getAsString();
    }

}
