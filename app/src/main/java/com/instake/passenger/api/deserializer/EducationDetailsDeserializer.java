package com.instake.passenger.api.deserializer;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.instake.passenger.model.EducationDetails;

import java.lang.reflect.Type;
import java.util.ArrayList;

/**
 * Created by Mostafa Monowar on 11-Sep-17.
 */

public class EducationDetailsDeserializer implements JsonDeserializer<EducationDetails> {
    @Override
    public EducationDetails deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        EducationDetails educationDetails = new EducationDetails();
        JsonObject jsonObject = json.getAsJsonObject();
        JsonArray classArray = jsonObject.getAsJsonArray("class");
        ArrayList<String> arrayList = new ArrayList<>();
        for (int i = 0; i < classArray.size(); i++) {
            JsonObject classObject = classArray.get(i).getAsJsonObject();
            arrayList.add(classObject.get("name").getAsString());
        }
        educationDetails.setClassList(arrayList);

        JsonArray departmentArray = jsonObject.getAsJsonArray("department");
        arrayList = new ArrayList<>();
        for (int i = 0; i < departmentArray.size(); i++) {
            JsonObject classObject = departmentArray.get(i).getAsJsonObject();
            arrayList.add(classObject.get("name").getAsString());
        }
        educationDetails.setDepartment(arrayList);

        return educationDetails;
    }
}
