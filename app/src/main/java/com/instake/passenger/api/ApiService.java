package com.instake.passenger.api;


import com.instake.passenger.model.EditProfile;
import com.instake.passenger.model.EducationDetails;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserProfile;
import com.instake.passenger.model.UserValidity;
import com.instake.passenger.model.driver.CarType;
import com.instake.passenger.model.geocoder.GeoCoderPlaceResponse;
import com.instake.passenger.model.historydata.HistoryResp;
import com.instake.passenger.model.mapdistance.DistanceResponse;
import com.instake.passenger.model.offers.OfferResponse;
import com.instake.passenger.model.suppotnhelp.SupportAndHelpResponse;
import com.instake.passenger.monowar.SignupResponseData;
import com.instake.passenger.monowar.VerificationStatus;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    String URL = "http://167.114.95.0/api/v1/";
    // TODO: API URL should be based on build variant
    String URL_GOOGLE_DISTANCE_MATRIX = "http://maps.googleapis.com/maps/api/";

    @GET("auth-is-signup")
    Call<UserValidity> isExistingUser(@Query("phone_no") String phoneNumber);

    @Headers("Content-Type: application/json")
    @POST("auth/signup")
    Call<User> signUp(@Body RequestBody requestBody);

    @Headers("Content-Type: application/json")
    @POST("auth/login")
    Call<User> login(@Body RequestBody requestBody);

    @GET("books/v1/volumes")
    Call<String> search(@Query("q") String search);

    @GET("directions/json")
    Observable<DistanceResponse> getDistanceInfo(
            @Query("origin") String origin,
            @Query("destination") String destination,
            @Query("sensor") String sensor,
            @Query("units") String units,
            @Query("mode") String mode

    );

    @GET("geocode/json")
    Observable<GeoCoderPlaceResponse> getGeoCoderPlaceName(
            @Query("latlng") String origin,
            @Query("sensor") boolean destination
    );

    @Headers("Content-Type: application/json")
    @POST("payment/{trip_id}")
    Observable<ResponseBody> postRating(
            @Header("token") String token,
            @Path("trip_id") String trip_id,
            @Body RequestBody requestBody
    );

    @Headers("Content-Type: application/json")
    @POST("payment/{trip_id}")
    Observable<ResponseBody> postTips(
            @Header("token") String token,
            @Path("trip_id") String trip_id,
            @Body RequestBody requestBody
    );

    /*@Headers("Content-Type: application/json")
    @GET("trip")
    Call<HistoryResponse> rideHistory(@Header("token") String token);*/

    @GET("trip")
    Observable<HistoryResp> getRideHistory(@Header("token") String token, @Query("page") String page);

    @POST("send-sms")
    Observable<ResponseBody> sendVerificationCode(@Body RequestBody body);

    @POST("phone-verify")
    Observable<VerificationStatus> verifyVerificationCode(@Body RequestBody body);

    @POST("auth/signup")
    Observable<SignupResponseData> signupNew(@Body RequestBody body);

    @PUT("auth/update")
    Observable<SignupResponseData> signupUpdate(@Header("token") String token, @Body RequestBody body);

    @PUT("auth/update")
    Observable<EditProfile> editProfile(@Header("token") String token, @Body RequestBody body);

    @POST("auth/login")
    Observable<User> loginNew(@Body RequestBody body);

    @POST("auth/facebook-login")
    Observable<User> loginFacebook(@Body RequestBody body);

    @Multipart
    @POST("upload-images")
    Observable<ResponseBody> uploadImage(@Header("token") String token, @Part MultipartBody.Part image, @Part("name") RequestBody name);

    @GET("profile-is-verify")
    Observable<UserProfile> getUserProfileVerification(@Header("token") String token);

    @GET("class-name")
    Call<EducationDetails> getEducationDetails();

    @POST("{api_action}")
    Observable<UserProfile> postUserProfiles(@Header("token") String token, @Path("api_action") String api_action, @Body RequestBody requestBody);

    @GET("cartype")
    Observable<List<CarType>> getVehicleDetails();

    @GET("support-help")
    Observable<SupportAndHelpResponse> getSupportAndHelp();

    @POST("trip-complaint/{help_id}")
    Observable<ResponseBody> postComplain(@Header("token") String token, @Path("help_id") String help_id, @Body RequestBody body);

    @GET("offer-free-ride")
    Observable<OfferResponse> getOffer(@Header ("token") String token);
}
