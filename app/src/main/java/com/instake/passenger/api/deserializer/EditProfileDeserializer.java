package com.instake.passenger.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.instake.passenger.model.EditProfile;
import com.instake.passenger.model.UserProfile;

import java.lang.reflect.Type;

/**
 * Created by Mostafa Monowar on 30-Aug-17.
 */

public class EditProfileDeserializer implements JsonDeserializer<EditProfile> {
    @Override
    public EditProfile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {

        EditProfile editProfile = new EditProfile();
        JsonObject jsonObject = json.getAsJsonObject();

        editProfile.id = jsonObject.get("_id").getAsString();

        editProfile.referralCode = jsonObject.get("referral").getAsString();

        editProfile.phoneNumber = jsonObject.get("phone_no").getAsString();

        editProfile.email = jsonObject.get("email").getAsString();

        editProfile.fullName = jsonObject.get("full_name").getAsString();

        editProfile.isActive = jsonObject.get("is_active").getAsBoolean();

        if (jsonObject.has("facebookId")) {
            editProfile.facebookId = jsonObject.get("facebookId").getAsString();
        }

        if (jsonObject.has("date_of_birth")) {
            editProfile.dateOfBirth = jsonObject.get("date_of_birth").getAsString();
        }

        if (jsonObject.has("gender")) {
            editProfile.gender = jsonObject.get("gender").getAsString();
        }

        if (jsonObject.has("profile_images")) {
            if (jsonObject.get("profile_images").isJsonObject()) {
                editProfile.profileImageUrl = jsonObject.get("profile_images").getAsJsonObject().get("150X150").getAsString();
            } else {
                editProfile.profileImageUrl = jsonObject.get("profile_images").getAsString();
            }

        }

        if (jsonObject.has("user_profile")) {
            JsonObject userProfileObject = jsonObject.get("user_profile").getAsJsonObject();
            UserProfile userProfile = new UserProfile();
            userProfile.isVerify = userProfileObject.get("is_verify").getAsBoolean();
            if (userProfileObject.has("profile_type")) {
                userProfile.profileType = userProfileObject.get("profile_type").getAsString();
                if (userProfileObject.has("web_url")) {
                    userProfile.webUrl = userProfileObject.get("web_url").getAsString();
                }
                if (userProfileObject.has("company_name")) {
                    userProfile.companyName = userProfileObject.get("company_name").getAsString();
                }
                if (userProfileObject.has("company_name")) {
                    userProfile.companyName = userProfileObject.get("company_name").getAsString();
                }
                if (userProfileObject.has("company_address")) {
                    userProfile.companyName = userProfileObject.get("company_address").getAsString();
                }
                if (userProfileObject.has("class")) {
                    userProfile.className = userProfileObject.get("class").getAsString();
                }
                if (userProfileObject.has("instituation_name")) {
                    userProfile.className = userProfileObject.get("instituation_name").getAsString();
                }
                if (userProfileObject.has("email")) {
                    userProfile.email = userProfileObject.get("email").getAsString();
                }
            }
            editProfile.userProfile = userProfile;
        }

        return editProfile;
    }
}
