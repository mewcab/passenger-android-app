package com.instake.passenger.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.instake.passenger.model.UserProfile;

import java.lang.reflect.Type;

/**
 * Created by Mostafa Monowar on 11-Sep-17.
 */

public class UserProfileDeserializer implements JsonDeserializer<UserProfile> {
    @Override
    public UserProfile deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        UserProfile userProfile = new UserProfile();
        JsonObject jsonObject = json.getAsJsonObject();

        if (jsonObject.has("is_verify")) {
            userProfile.isVerify = jsonObject.get("is_verify").getAsBoolean();
        }

        JsonObject userProfileObject = jsonObject.get("user_profile").getAsJsonObject();
        if (userProfileObject.has("profile_type")) {
            userProfile.profileType = userProfileObject.get("profile_type").getAsString();
        }
        if (userProfileObject.has("web_url")) {
            userProfile.webUrl = userProfileObject.get("web_url").getAsString();
        }
        if (userProfileObject.has("company_name")) {
            userProfile.companyName = userProfileObject.get("company_name").getAsString();
        }
        if (userProfileObject.has("company_name")) {
            userProfile.companyName = userProfileObject.get("company_name").getAsString();
        }
        if (userProfileObject.has("company_address")) {
            userProfile.companyName = userProfileObject.get("company_address").getAsString();
        }
        if (userProfileObject.has("class")) {
            userProfile.className = userProfileObject.get("class").getAsString();
        }
        if (userProfileObject.has("instituation_name")) {
            userProfile.className = userProfileObject.get("instituation_name").getAsString();
        }
        if (userProfileObject.has("email")) {
            userProfile.email = userProfileObject.get("email").getAsString();
        }
        if (userProfileObject.has("is_verify")) {
            userProfile.isVerify = userProfileObject.get("is_verify").getAsBoolean();
        }

        return userProfile;
    }
}
