package com.instake.passenger.api.deserializer;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.instake.passenger.model.User;
import com.instake.passenger.model.UserValidity;

import java.lang.reflect.Type;

/**
 * Created by Tushar on 5/21/17.
 */

public class UserValidityDeserializer implements JsonDeserializer<UserValidity> {

    @Override
    public UserValidity deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        UserValidity userValidity = new UserValidity();

        userValidity.isExist = jsonObject.get("success").getAsBoolean();

        if (userValidity.isExist) {
            JsonObject jsonDetails = jsonObject.get("subscriber").getAsJsonObject();
            User user = new User();
            user.isActive = jsonDetails.get("is_active").getAsBoolean();
            user.id = jsonDetails.get("_id").getAsString();
            user.phoneNumber = jsonDetails.get("phone_no").getAsString();
            if (jsonDetails.has("email")) {
                user.email = jsonDetails.get("email").getAsString();
            }
            if (jsonDetails.has("full_name")) {
                user.fullName = jsonDetails.get("full_name").getAsString();
            }

            userValidity.user = user;
        }
        return userValidity;
    }
}
